#include "Config.h"

#include "Features/Visuals/Menu.h"

void ConfigLib::Init(lua_State* L)
{
	BaseLibrary<ConfigLib>::Init(L);

	RegisterUserdata(L);
}

class ConVar
{
public:
#define CONVAR "ConVar"
	static ConVar* ToPtr(lua_State* L, int idx);

	static int l_new(lua_State* L);
	static int l_gc(lua_State* L);
	static int l_tostring(lua_State* L);
	static int l_GetInt(lua_State* L);
	static int l_GetFloat(lua_State* L);
	static int l_GetBool(lua_State* L);
	static int l_SetValue(lua_State* L);
private:
	Config::ConVar* m_pCvar;
};

void ConfigLib::RegisterUserdata(lua_State* L)
{
	lua_register(L, CONVAR, ConVar::l_new);
	luaL_newmetatable(L, CONVAR);

	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");

	lua_pushcfunction(L, ConVar::l_gc); 
	lua_setfield(L, -2, "__gc");

	lua_pushcfunction(L, ConVar::l_tostring);
	lua_setfield(L, -2, "__tostring");

	lua_pushcfunction(L, ConVar::l_GetInt);
	lua_setfield(L, -2, "GetInt");

	lua_pushcfunction(L, ConVar::l_GetFloat);
	lua_setfield(L, -2, "GetFloat");

	lua_pushcfunction(L, ConVar::l_GetBool);
	lua_setfield(L, -2, "GetBool");

	lua_pushcfunction(L, ConVar::l_SetValue);
	lua_setfield(L, -2, "SetValue");

	lua_pop(L, 1);
}

ConVar* ConVar::ToPtr(lua_State* L, int idx)
{
	return (ConVar*)luaL_checkudata(L, idx, CONVAR);
}

int ConVar::l_new(lua_State* L)
{
	ReverseStack<const char*> stack;

	for (int i = 1; i <= lua_gettop(L); i++)
	{
		const char* pszString = luaL_checkstring(L, i);

		//Debug::PrintToConsole("%s", pszString);

		stack.Push(pszString);
	}

	if (stack.IsEmpty())
	{
		return 0;
	}

	ConVar* pUsrData = (ConVar*)lua_newuserdata(L, sizeof(ConVar));
	luaL_setmetatable(L, CONVAR);
	if (pUsrData)
	{
		auto pVar = Config::FindVar(stack);
		if (pVar == nullptr)
		{
			//Debug::PrintToConsole("ra");

			ReverseStack<const char*> stack;

			for (int i = 1; i <= lua_gettop(L) - 1; i++)
			{
				const char* pszString = luaL_checkstring(L, i);
				stack.Push(pszString);
			}

			pVar = Config::RecursiveAdd(stack);
		}

		pUsrData->m_pCvar = pVar;

		//Debug::PrintToConsole("%p", pUsrData->m_pCvar);
	}

	return 1;
}

int ConVar::l_gc(lua_State* L)
{
	return 0;
}

int ConVar::l_tostring(lua_State* L)
{
	lua_pushfstring(L, "ConVar: %p", ToPtr(L, 1));

	return 1;
}

int ConVar::l_GetInt(lua_State* L)
{
	ConVar* pVar = (ConVar*)luaL_checkudata(L, 1, CONVAR);
	if (pVar && pVar->m_pCvar)
	{
		lua_pushinteger(L, pVar->m_pCvar->GetInt());
		return 1;
	}

	return 0;
}

int ConVar::l_GetFloat(lua_State* L)
{
	ConVar* pVar = (ConVar*)luaL_checkudata(L, 1, CONVAR);
	if (pVar && pVar->m_pCvar)
	{
		lua_pushnumber(L, pVar->m_pCvar->GetFloat());
		return 1;
	}

	return 0;
}

int ConVar::l_GetBool(lua_State* L)
{
	ConVar* pVar = (ConVar*)luaL_checkudata(L, 1, CONVAR);
	if (pVar && pVar->m_pCvar)
	{
		lua_pushboolean(L, !(pVar->m_pCvar->GetInt() == 0));
	}
	else
	{
		lua_pushboolean(L, false);
	}

	return 1;
}

int ConVar::l_SetValue(lua_State* L)
{
	ConVar* pVar = (ConVar*)luaL_checkudata(L, 1, CONVAR);

	if (pVar && pVar->m_pCvar)
	{
		if (lua_isinteger(L, 2))
		{
			pVar->m_pCvar->SetInt(lua_tointeger(L, 2));
		}
		else if (lua_isnumber(L, 2))
		{
			pVar->m_pCvar->SetFloat(lua_tonumber(L, 2));
		}
		else if (lua_isboolean(L, 2))
		{
			pVar->m_pCvar->SetInt(lua_toboolean(L, 2));
		}
		else
		{
			luaL_error(L, "Invalid value type");
		}
	}

	return 0;
}

int ConfigLib::l_SpewVars(lua_State* L)
{
	Config::SpewVars();

	return 0;
}