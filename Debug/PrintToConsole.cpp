#include "PrintToConsole.h"

#include "Features/Visuals/Menu.h"

void Debug::PrintToConsole(const char* pszFmt, ...)
{
	char szBuf[2048 + 1] = { 0 };

	va_list list;
	va_start(list, pszFmt);
	vsprintf_s(szBuf, pszFmt, list);
	va_end(list);

	/*using MsgFn = void(*)(const char*, ...);
	static MsgFn Msg;

	if (Msg == nullptr)
	{
		Msg = (MsgFn)GetProcAddress(GetModuleHandleA("tier0.dll"), "Msg");
	}

	if (Msg == nullptr)
	{
		return;
	}

	Msg(szBuf);*/

	Features::Visuals::AddLineToConsole(szBuf);
}