#include "Freecam.h"

#include "Features/Visuals/Menu.h"

#include "Util/Input.h"

using namespace Features::Misc;

bool Freecam::m_bToggle;
Vector Freecam::m_vecPosition;
Vector Freecam::m_vecAngle;

bool Freecam::Run(Source::CViewSetup* pView)
{
	FINDVAR(misc_freecam, "Miscellaneous", "Freecam");
	FINDVAR(misc_freecam_key, "Miscellaneous", "Freecam", "Key");

	if (!misc_freecam->GetBool())
	{
		m_bToggle = false;
		m_vecPosition = pView->origin();
		m_vecAngle = pView->angles();

		return false;
	}

	if (misc_freecam_key->GetInt() != 0)
	{
		if (Util::Input::GetButtonState(misc_freecam_key->GetInt()) & 1)
		{
			m_bToggle = !m_bToggle;
		}

		if (!m_bToggle)
		{
			m_vecPosition = pView->origin();
			m_vecAngle = pView->angles();

			return false;
		}
	}

	pView->origin() = m_vecPosition;
	pView->angles() = m_vecAngle;
	pView->fov() = 90.f;
	return true;
}

bool Freecam::GetIsActivated()
{
	FINDVAR(misc_freecam, "Miscellaneous", "Freecam");
	FINDVAR(misc_freecam_key, "Miscellaneous", "Freecam", "Key");

	if (misc_freecam->GetBool())
	{
		if (misc_freecam_key->GetInt() != 0)
		{
			return m_bToggle;
		}

		return true;
	}

	return false;
}

void Freecam::Update(Source::CUserCmd* cmd)
{
	{
		static auto m_pitch = Source::Interfaces::Cvar->FindVar("m_pitch");
		static auto m_yaw = Source::Interfaces::Cvar->FindVar("m_yaw");

		m_vecAngle += Vector(m_pitch->GetFloat() * cmd->mousedy(), -m_yaw->GetFloat() * cmd->mousedx(), 0.f);
		AngleNormalize(m_vecAngle);
		AngleClamp(m_vecAngle);
	}

	{
		Vector forward, right;
		AngleVectors(m_vecAngle, &forward, &right);

		float flMod = 500.f;
		if (cmd->buttons() & Source::IN_SPEED)
		{
			flMod = 1500.f;
		}
		else if (cmd->buttons() & Source::IN_WALK)
		{
			flMod = 200.f;
		}

		if (cmd->buttons() & Source::IN_FORWARD)
		{
			m_vecPosition += forward * flMod * Source::Interfaces::Globals->frametime();
		}

		if (cmd->buttons() & Source::IN_BACK)
		{
			m_vecPosition -= forward * flMod * Source::Interfaces::Globals->frametime();
		}

		if (cmd->buttons() & Source::IN_MOVERIGHT)
		{
			m_vecPosition += right * flMod * Source::Interfaces::Globals->frametime();
		}

		if (cmd->buttons() & Source::IN_MOVELEFT)
		{
			m_vecPosition -= right * flMod * Source::Interfaces::Globals->frametime();
		}

		if (cmd->buttons() & Source::IN_JUMP)
		{
			m_vecPosition += Vector(0, 0, flMod * 0.5f) * Source::Interfaces::Globals->frametime();
		}

		if (cmd->buttons() & Source::IN_DUCK)
		{
			m_vecPosition -= Vector(0, 0, flMod * 0.5f) * Source::Interfaces::Globals->frametime();
		}
	}
}