#include "WriteUserCmdToDeltaBuffer.h"

#include "Features/Misc/InterpLagComp.h"

#if defined(GAME_L4D2)
bool __fastcall Hooks::Client::WriteUsercmdDeltaToBuffer(Source::IBaseClientDLL* pThis, void*, void* buf, int unk1, int from, int to, bool isnewcommand)
#else
bool __fastcall Hooks::Client::WriteUsercmdDeltaToBuffer(Source::IBaseClientDLL* pThis, void*, void* buf, int from, int to, bool isnewcommand)
#endif
{
#if(defined GAME_GMOD)  || defined(GAME_L4D2)
	if (from == -1)
	{
		Features::Misc::InterpLagComp::WriteUserCmdDeltaToBufferCallback();

		/*if ((Source::Interfaces::Globals->tickcount() % 10) == 0)
		{
			char buf[32];
			sprintf_s(buf, "%f", Source::TICKS_TO_TIME(Source::RandomInt(10, 30)));

			Source::NET_SetConVar cl_interp("cl_interp", buf);
			Source::Interfaces::Engine->GetNetChannelInfo()->SendNetMsg(cl_interp, false, false);
		}*/
	}
#endif

	pVmt->Unhook();
#if defined(GAME_L4D2)
	bool bResult = pThis->WriteUsercmdDeltaToBuffer(buf, unk1, from, to, isnewcommand);
#else
	bool bResult = pThis->WriteUsercmdDeltaToBuffer(buf, from, to, isnewcommand);
#endif
	pVmt->Rehook();

	return bResult;
}