#include "Ents.h"

#include "SDK/Source.h"

void Ents::Init(lua_State* L)
{
	BaseLibrary<Ents>::Init(L);

	RegisterUserdata(L);
}

#define ENTITY "Entity"
class Entity
{
public:
	static Entity* ToPtr(lua_State* L, int idx);

	static int l_new(lua_State* L);
	static int l_gc(lua_State* L);
	static int l_tostring(lua_State* L);
	
	static int l_IsValid(lua_State* L);
	static int l_EntIndex(lua_State* L);
	static int l_IsPlayer(lua_State* L);
	static int l_IsLocalPlayer(lua_State* L);
	static int l_IsNPC(lua_State* L);
	static int l_IsNextBot(lua_State* L);
	static int l_IsWeapon(lua_State* L);
	static int l_GetClassName(lua_State* L);

	Source::CBaseEntity* m_pEntity;
};

void Ents::RegisterUserdata(lua_State* L)
{
	lua_register(L, ENTITY, Entity::l_new);
	luaL_newmetatable(L, ENTITY);

	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");

	lua_pushcfunction(L, Entity::l_gc);
	lua_setfield(L, -2, "__gc");

	lua_pushcfunction(L, Entity::l_tostring);
	lua_setfield(L, -2, "__tostring");

	lua_pushcfunction(L, Entity::l_IsValid);
	lua_setfield(L, -2, "IsValid");

	lua_pushcfunction(L, Entity::l_EntIndex);
	lua_setfield(L, -2, "EntIndex");

	lua_pushcfunction(L, Entity::l_IsPlayer);
	lua_setfield(L, -2, "IsPlayer");

	lua_pushcfunction(L, Entity::l_IsLocalPlayer);
	lua_setfield(L, -2, "IsLocalPlayer");

	lua_pushcfunction(L, Entity::l_IsNPC);
	lua_setfield(L, -2, "IsNPC");

	lua_pushcfunction(L, Entity::l_IsNextBot);
	lua_setfield(L, -2, "IsNextBot");

	lua_pushcfunction(L, Entity::l_IsWeapon);
	lua_setfield(L, -2, "IsWeapon");

	lua_pushcfunction(L, Entity::l_GetClassName);
	lua_setfield(L, -2, "GetClassName");

	lua_pop(L, 1);
}

int Ents::l_GetAll(lua_State* L)
{
	static int s_LastFramecount = -1;
	if (s_LastFramecount != Source::Interfaces::Globals->framecount()) // re-cache
	{
		s_LastFramecount = Source::Interfaces::Globals->framecount();

		lua_getglobal(L, "ents");
		if (lua_istable(L, -1))
		{
			lua_newtable(L);

			int count = 1;
			for (int i = 0; i <= Source::Interfaces::EntityList->GetHighestEntityIndex(); i++)
			{
				Source::CBaseEntity* pEntity = (Source::CBaseEntity*)Source::Interfaces::EntityList->GetClientEntity(i);
				if (pEntity)
				{
					lua_pushentity(L, pEntity);
					lua_seti(L, -2, count);
					count++;
				}
			}

			lua_setfield(L, -2, "AllEntities");
		}

		lua_pop(L, 1);
	}

	lua_getglobal(L, "ents");
	if (lua_istable(L, -1))
	{
		lua_getfield(L, -1, "AllEntities");
		lua_replace(L, -2);
	}
	else
	{
		lua_pop(L, 1);
		lua_newtable(L);
	}

	return 1;
}

int Ents::l_GetPlayers(lua_State* L)
{
	static int s_LastFramecount = -1;
	if (s_LastFramecount != Source::Interfaces::Globals->framecount()) // re-cache
	{
		s_LastFramecount = Source::Interfaces::Globals->framecount();

		lua_getglobal(L, "ents");
		if (lua_istable(L, -1))
		{
			lua_newtable(L);

			int count = 1;
			for (int i = 1; i <= Source::Interfaces::Globals->maxClients(); i++)
			{
				Source::CBaseEntity* pEntity = (Source::CBaseEntity*)Source::Interfaces::EntityList->GetClientEntity(i);
				if (pEntity)
				{
					lua_pushentity(L, pEntity);
					lua_seti(L, -2, count);
					count++;
				}
			}

			lua_setfield(L, -2, "AllPlayers");
		}

		lua_pop(L, 1);
	}

	lua_getglobal(L, "ents");
	if (lua_istable(L, -1))
	{
		lua_getfield(L, -1, "AllPlayers");
		lua_replace(L, -2);
	}
	else
	{
		lua_pop(L, 1);
		lua_newtable(L);
	}

	return 1;
}

Entity* Entity::ToPtr(lua_State* L, int idx)
{
	return (Entity*)luaL_checkudata(L, idx, ENTITY);
}

int Entity::l_new(lua_State* L)
{
	int idx = luaL_checkinteger(L, 1);
	lua_pushentity(L, (Source::CBaseEntity*)Source::Interfaces::EntityList->GetClientEntity(idx));
	return 1;
}

int Entity::l_gc(lua_State* L)
{
	return 0;
}

int Entity::l_tostring(lua_State* L)
{
	lua_pushfstring(L, "Entity: %p", ToPtr(L, 1));

	return 1;
}

int Entity::l_IsValid(lua_State* L)
{
	Entity* pEntity = (Entity*)luaL_checkudata(L, 1, ENTITY);
	if (pEntity && pEntity->m_pEntity)
	{
		lua_pushboolean(L, true);
	}
	else
	{
		lua_pushboolean(L, false);
	}

	return 1;
}

int Entity::l_EntIndex(lua_State* L)
{
	Entity* pEntity = (Entity*)luaL_checkudata(L, 1, ENTITY);
	if (pEntity && pEntity->m_pEntity)
	{
		lua_pushinteger(L, pEntity->m_pEntity->GetNetworkable()->EntIndex());
		return 1;
	}

	lua_pushinteger(L, -1);
	return 1;
}

int Entity::l_IsPlayer(lua_State* L)
{
	Entity* pEntity = (Entity*)luaL_checkudata(L, 1, ENTITY);
	if (pEntity && pEntity->m_pEntity)
	{
		lua_pushboolean(L, pEntity->m_pEntity->IsPlayer());

		return 1;
	}

	lua_pushboolean(L, false);
	return 1;
}

int Entity::l_IsLocalPlayer(lua_State* L)
{
	Entity* pEntity = (Entity*)luaL_checkudata(L, 1, ENTITY);
	if (pEntity && pEntity->m_pEntity)
	{
		if (pEntity->m_pEntity->GetNetworkable()->EntIndex() == Source::Interfaces::Engine->GetLocalPlayer())
		{
			lua_pushboolean(L, true);
		}
		else
		{
			lua_pushboolean(L, false);
		}

		return 1;
	}

	lua_pushboolean(L, false);
	return 1;
}

int Entity::l_IsNPC(lua_State* L)
{
	Entity* pEntity = (Entity*)luaL_checkudata(L, 1, ENTITY);
	if (pEntity && pEntity->m_pEntity)
	{
		lua_pushboolean(L, pEntity->m_pEntity->IsNPC());

		return 1;
	}

	lua_pushboolean(L, false);
	return 1;
}

int Entity::l_IsNextBot(lua_State* L)
{
	Entity* pEntity = (Entity*)luaL_checkudata(L, 1, ENTITY);
	if (pEntity && pEntity->m_pEntity)
	{
		lua_pushboolean(L, pEntity->m_pEntity->IsNextBot());

		return 1;
	}

	lua_pushboolean(L, false);
	return 1;
}

int Entity::l_IsWeapon(lua_State* L)
{
	Entity* pEntity = (Entity*)luaL_checkudata(L, 1, ENTITY);
	if (pEntity && pEntity->m_pEntity)
	{
		lua_pushboolean(L, pEntity->m_pEntity->IsBaseCombatWeapon());

		return 1;
	}

	lua_pushboolean(L, false);
	return 1;
}

int Entity::l_GetClassName(lua_State* L)
{
	Entity* pEntity = (Entity*)luaL_checkudata(L, 1, ENTITY);
	if (pEntity && pEntity->m_pEntity)
	{
		lua_pushstring(L, pEntity->m_pEntity->GetClassName());
		return 1;
	}

	lua_pushstring(L, "");
	return 1;
}

void lua_pushentity(lua_State* L, Source::CBaseEntity* entity)
{
	Entity* pEntity = (Entity*)lua_newuserdata(L, sizeof(Entity));
	luaL_setmetatable(L, ENTITY);

	pEntity->m_pEntity = entity;
}