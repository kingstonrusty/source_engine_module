#ifndef __HEADER_CONFIG_LUA__
#define __HEADER_CONFIG_LUA__
#pragma once

#include "BaseLibrary.h"

class ConfigLib : public BaseLibrary<ConfigLib>
{
public:
	friend class BaseLibrary<ConfigLib>;

	static void Init(lua_State* L);
	static const char* GetName() { return "Config"; }

	static void RegisterUserdata(lua_State* L);
private:
	static int l_SpewVars(lua_State* L);

	static constexpr const struct luaL_Reg m_Register[] =
	{
		{ "SpewVars", l_SpewVars },
		{ NULL, NULL }
	};
};

#endif