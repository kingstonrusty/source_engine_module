#ifndef __HEADER_LUA_SURFACE__
#define __HEADER_LUA_SURFACE__
#pragma once

#include "BaseLibrary.h"

class Surface : public BaseLibrary<Surface>
{
	friend class BaseLibrary<Surface>;

	static const char* GetName() { return "Surface"; }
private:
	static int l_SetDrawColor(lua_State* L);
	static int l_DrawFilledRect(lua_State* L);
	static int l_DrawOutlinedRect(lua_State* L);
	static int l_DrawLine(lua_State* L);

	static int l_SetFont(lua_State* L);
	static int l_CreateFont(lua_State* L);
	static int l_SetTextColor(lua_State* L);
	static int l_SetTextPos(lua_State* L);
	static int l_DrawString(lua_State* L);
	static int l_GetTextSize(lua_State* L);

	static int l_GetScreenSize(lua_State* L);

	static constexpr const struct luaL_Reg m_Register[] =
	{
		{ "SetDrawColor", l_SetDrawColor },
		{ "DrawFilledRect", l_DrawFilledRect },
		{ "DrawOutlinedRect", l_DrawOutlinedRect },
		{ "DrawLine", l_DrawLine },

		{ "CreateFont", l_CreateFont },
		{ "SetFont", l_SetFont },
		{ "SetTextColor", l_SetTextColor },
		{ "SetTextPos", l_SetTextPos },
		{ "DrawString", l_DrawString },
		{ "GetTextSize", l_GetTextSize },

		{ "GetScreenSize", l_GetScreenSize },
		{ NULL, NULL }
	};
};

#endif