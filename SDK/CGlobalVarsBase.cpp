#include "CGlobalVarsBase.h"

namespace Source
{
	namespace Interfaces
	{
		CGlobalVarsBase* Globals;
	}
}

using namespace Source;

CGlobalVarsBase* CGlobalVarsBase::GetPtr()
{
#if defined(GAME_GMOD)
	return *Memory::FindPattern<CGlobalVarsBase**>(Source::CGLOBALVARSBASE_SIGNATURE, "engine.dll");
#else
	auto pClient = IBaseClientDLL::GetPtr();

	for (DWORD_PTR dwpStartAddr = GET_VFUNC(DWORD_PTR, pClient, 0), dOffset = 0; dOffset <= 0xFF; dOffset++)
	{
		if (*(byte*)(dwpStartAddr + dOffset) == 0xA3)
		{
			return **(CGlobalVarsBase***)(dwpStartAddr + dOffset + 0x1);
		}
	}
#endif

	return nullptr;
}

int& CGlobalVarsBase::framecount()
{
	return *(int*)((DWORD_PTR)this + CGLOBALVARSBASE_FRAMECOUNT);
}

float& CGlobalVarsBase::curtime()
{
	return *(float*)((DWORD_PTR)this + CGLOBALVARSBASE_CURTIME);
}

float& CGlobalVarsBase::frametime()
{
	return *(float*)((DWORD_PTR)this + CGLOBALVARSBASE_FRAMETIME);
}

int& CGlobalVarsBase::maxClients()
{
	return *(int*)((DWORD_PTR)this + CGLOBALVARSBASE_MAXCLIENTS);
}

int& CGlobalVarsBase::tickcount()
{
	return *(int*)((DWORD_PTR)this + CGLOBALVARSBASE_TICKCOUNT);
}

float& CGlobalVarsBase::interval_per_tick()
{
	return *(float*)((DWORD_PTR)this + CGLOBALVARSBASE_INTERVALPERTICK);
}