#ifndef __HEADER_NOSPREAD__
#define __HEADER_NOSPREAD__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace Accuracy
	{
		class NoSpread
		{
		public:
			static void Run(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd);
#if defined(GAME_GMOD)
			static void RecordSpread(Source::CBasePlayer* pLocal, Source::FireBulletsInfo_t* info);
			static void FAS2NoSpread(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd);
			static void CW2NoSpread(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd);
		private:
			struct SSpread
			{
				Vector m_vecSpread;
				int m_iShots;
			};

			static KeyValues<unsigned int, SSpread> m_SpreadVectors;
#endif
		};
		
	}
}

#endif