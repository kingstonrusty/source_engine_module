#include "IStudioRender.h"

namespace Source
{
	namespace Interfaces
	{
		IStudioRender* StudioRender;
	}
}

using namespace Source;

IStudioRender* IStudioRender::GetPtr()
{
	return (IStudioRender*)::Util::BruteforceInterface("VStudioRender", "StudioRender.dll");
}

void IStudioRender::SetColorModulation(float* color)
{
	return GET_VFUNC(void(__thiscall*)(IStudioRender*, float*), this, ISTUDIORENDER_SETCOLORMODULATION)(this, color);
}

void IStudioRender::SetColorModulation(Color clr)
{
	float flColor[3] = { (float)clr.r / 255.f, (float)clr.g / 255.f, (float)clr.b / 255.f };

	return SetColorModulation(flColor);
}

void IStudioRender::SetAlphaModulation(float alpha)
{
	return GET_VFUNC(void(__thiscall*)(IStudioRender*, float), this, ISTUDIORENDER_SETALPHAMODULATION)(this, alpha);
}

void IStudioRender::DrawModel(DrawModelResults_t* results, DrawModelInfo_t* info, matrix3x4* bonetoworld, float* flexweights, float* flexdelayedweights, Vector& modelorigin, int flags)
{
	return GET_VFUNC(void(__thiscall*)(IStudioRender*, DrawModelResults_t*, DrawModelInfo_t*, matrix3x4*, float*, float*, Vector&, int), this, ISTUDIORENDER_DRAWMODEL)(this, results, info, bonetoworld, flexweights, flexdelayedweights, modelorigin, flags);
}

void IStudioRender::ForcedMaterialOverride(IMaterial* material, OverrideType_t overridetype)
{
	return GET_VFUNC(void(__thiscall*)(IStudioRender*, IMaterial*, OverrideType_t), this, ISTUDIORENDER_FORCEDMATERIALOVERRIDE)(this, material, overridetype);
}