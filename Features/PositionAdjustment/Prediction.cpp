#include "Prediction.h"

using namespace Features::PositionAdjustment;

bool Prediction::m_bInPrediction;;
int Prediction::m_nOldTickBase;
int Prediction::m_nPushTickBase;
float Prediction::m_flOldCurTime;
float Prediction::m_flOldFrameTime;
int Prediction::m_nOldRandomSeed;
float Prediction::m_flOldSvFootstepsValue;

void Prediction::Start(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd)
{
	static auto sv_footsteps = Source::Interfaces::Cvar->FindVar("sv_footsteps");

	if (Source::Interfaces::GameMovement->GetMoveData() == nullptr)
	{
		return;
	}

	SetIsInPrediction(true);

	BackupGlobals();

	AdjustTickBase(pLocal);

	Source::Interfaces::Globals->curtime() = Source::TICKS_TO_TIME(pLocal->GetTickBase());
	Source::Interfaces::Globals->frametime() = Source::Interfaces::Globals->interval_per_tick();

	m_nOldRandomSeed = cmd->random_seed();
	cmd->random_seed() = Source::MD5_PseudoRandom(cmd->command_number()) & 0x7FFFFFFF;

	SetPredictionPlayer(pLocal);
	SetPredictionSeed(cmd->random_seed());
	pLocal->SetCurrentCommand(cmd);

	GetMoveHelper()->SetHost(pLocal);

#if !defined(GAME_L4D2)
	if (sv_footsteps != nullptr)
	{
		m_flOldSvFootstepsValue = sv_footsteps->GetFloat();
		sv_footsteps->SetFloat(0.f);
	}
#endif

	pLocal->SetAbsVelocity(pLocal->GetVelocity()); // absvelocity can be outdated and prediction uses it so....

	DoWeaponSelection(pLocal, cmd);
	DoUpdateButtonState(pLocal, cmd);

	RunPreThink(pLocal);
	RunThink(pLocal);

	Source::Interfaces::Prediction->SetupMove(pLocal, cmd, GetMoveHelper(), Source::Interfaces::GameMovement->GetMoveData());
	Source::Interfaces::GameMovement->ProcessMovement(pLocal, Source::Interfaces::GameMovement->GetMoveData());
	Source::Interfaces::Prediction->FinishMove(pLocal, cmd, Source::Interfaces::GameMovement->GetMoveData());

#if defined(GAME_CSS_STEAM) || defined(GAME_L4D2)
	Source::CBaseWeapon* pWeapon = (Source::CBaseWeapon*)Source::Interfaces::EntityList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
	if (pWeapon != nullptr)
	{
		int iClassID = pWeapon->GetNetworkable()->GetClientClass()->m_ClassID;

		//131 116 181 164 165 170 169 168 2 171 148 1 152 153 154 155
		switch (iClassID)
		{
		case 131:
		case 116:
		case 181:
		case 164:
		case 165:
		case 170:
		case 169:
		case 168:
		case 2:
		case 171:
		case 148:
		case 1:
		case 152:
		case 153:
		case 154:
		case 155:
			pWeapon->UpdateInaccuracy();
			break;
		default:
			break;
		}
	}
#endif
}

void Prediction::End(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd)
{
	static auto sv_footsteps = Source::Interfaces::Cvar->FindVar("sv_footsteps");

	if (Source::Interfaces::GameMovement->GetMoveData() == nullptr)
	{
		return;
	}

	SetIsInPrediction(false);

	RestoreGlobals();

	cmd->random_seed() = m_nOldRandomSeed;

	SetPredictionPlayer(nullptr);
	SetPredictionSeed(-1);
	pLocal->SetCurrentCommand(nullptr);

	GetMoveHelper()->SetHost(nullptr);

#if !defined(GAME_L4D2)
	if (sv_footsteps != nullptr)
	{
		sv_footsteps->SetFloat(m_flOldSvFootstepsValue);
	}
#endif
}

void Prediction::SetIsInPrediction(bool bPrediction)
{
	m_bInPrediction = bPrediction;
}

bool Prediction::GetIsInPrediction()
{
	return m_bInPrediction;
}

void Prediction::AdjustTickBase(Source::CBasePlayer* pLocal)
{
	int m_nTickBase = pLocal->GetTickBase();

	if (m_nTickBase == m_nOldTickBase)
	{
		m_nPushTickBase++;
		m_nTickBase += m_nPushTickBase;
	}
	else
	{
		m_nPushTickBase = 0;
	}

	pLocal->SetTickBase(m_nTickBase);
	m_nOldTickBase = m_nTickBase;
}

void Prediction::BackupGlobals()
{
	m_flOldCurTime = Source::Interfaces::Globals->curtime();
	m_flOldFrameTime = Source::Interfaces::Globals->frametime();
}

void Prediction::RestoreGlobals()
{
	Source::Interfaces::Globals->curtime() = m_flOldCurTime;
	Source::Interfaces::Globals->frametime() = m_flOldFrameTime;
}

void Prediction::SetPredictionPlayer(Source::CBasePlayer* pPlayer)
{
	if (GetPredictionPlayer() == nullptr)
	{
		return;
	}

	*GetPredictionPlayer() = pPlayer;
}

Source::CBasePlayer** Prediction::GetPredictionPlayer()
{
	static Source::CBasePlayer** s_pPredictionPlayer = *Memory::FindPattern<Source::CBasePlayer***>(Source::PREDICTION_CURRENTPLAYER, "client.dll");

	return s_pPredictionPlayer;
}

void Prediction::SetPredictionSeed(int iSeed)
{
	if (GetPredictionSeed() == nullptr)
	{
		return;
	}

	*GetPredictionSeed() = iSeed;
}

int* Prediction::GetPredictionSeed()
{
	static int* s_pPredictionRandomSeed = *Memory::FindPattern<int**>(Source::PREDICTION_RANDOMSEED, "client.dll");

	return s_pPredictionRandomSeed;
}

Source::IMoveHelper* Prediction::GetMoveHelper()
{
#if defined(GAME_L4D2)
	static Source::IMoveHelper* s_pMoveHelper = nullptr;
	if (s_pMoveHelper == nullptr) 
	{
		DWORD_PTR dwpFunc = Memory::FindPattern<DWORD_PTR>("C7 05 ?? ?? ?? ?? ?? ?? ?? ?? C7 05 ?? ?? ?? ?? ?? ?? ?? ?? E8 ?? ?? ?? ?? C7 05", "client.dll");

		for (DWORD dwOffset = 0; dwOffset <= 0xFF; dwOffset++)
		{
			if (*(byte*)(dwpFunc + dwOffset) == 0xC7)
			{
				s_pMoveHelper = *(Source::IMoveHelper**)(dwpFunc + dwOffset + 2);
			}
		}
	}

	return s_pMoveHelper;
#else
	static Source::IMoveHelper* s_pMoveHelper = *Memory::FindPattern<Source::IMoveHelper**>(Source::PREDICTION_MOVEHELPER, "client.dll");

	return s_pMoveHelper;
#endif
}

void Prediction::DoWeaponSelection(Source::CBasePlayer* pPlayer, Source::CUserCmd* cmd)
{
	if (cmd->weaponselect())
	{
		Source::CBaseWeapon* pWeapon = (Source::CBaseWeapon*)Source::Interfaces::EntityList->GetClientEntity(cmd->weaponselect());
		if (pWeapon)
		{
			int v19 = GET_VFUNC(int(__thiscall*)(Source::CBaseWeapon*, int), pWeapon, 364)(pWeapon, cmd->weaponsubtype());

			GET_VFUNC(void(__thiscall*)(Source::CBasePlayer*, int), pPlayer, 318)(pPlayer, v19);
		}
	}
}

void Prediction::DoUpdateButtonState(Source::CBasePlayer* pPlayer, Source::CUserCmd* cmd)
{
	GET_VFUNC(void(__thiscall*)(Source::CBasePlayer*, int), pPlayer, 323)(pPlayer, cmd->buttons());
}

void Prediction::RunPreThink(Source::CBasePlayer* pPlayer) // void CPrediction::RunPreThink
{
	static bool(__thiscall* pPhysicsRunThink)(Source::CBasePlayer*, int) = Memory::FindPattern<bool(__thiscall*)(Source::CBasePlayer*, int)>(Source::PREDICTION_PHYSICSRUNTHINK, "client.dll");
	if (!pPhysicsRunThink(pPlayer, 0))
	{
		return;
	}

#if defined(GAME_GMOD)
	GET_VFUNC(void(__thiscall*)(Source::CBasePlayer*), pPlayer, 308)(pPlayer);
#elif defined(GAME_CSS_STEAM)
	GET_VFUNC(void(__thiscall*)(Source::CBasePlayer*), pPlayer, 253)(pPlayer);
#elif defined(GAME_L4D2)
	GET_VFUNC(void(__thiscall*)(Source::CBasePlayer*), pPlayer, 288)(pPlayer);
#endif
}

void Prediction::RunThink(Source::CBasePlayer* pPlayer) // void CPrediction::RunThink
{
	static int(__thiscall* pGetNextThinkTick)(Source::CBasePlayer*, const char*) = Memory::FindPattern<int(__thiscall*)(Source::CBasePlayer*, const char*)>(Source::PREDICTION_GETNEXTTHINKTICK, "client.dll");
	static void(__thiscall* pSetNextThink)(Source::CBasePlayer*, float, const char*) = Memory::FindPattern<void(__thiscall*)(Source::CBasePlayer*, float, const char*)>(Source::PREDICTION_SETNEXTTHINK, "client.dll");

	int nNextThinkTick = pGetNextThinkTick(pPlayer, nullptr);
	if (nNextThinkTick <= 0 || nNextThinkTick > pPlayer->GetTickBase())
	{
		return;
	}

	pSetNextThink(pPlayer, -1.f, nullptr);

#if defined(GAME_GMOD)
	GET_VFUNC(void(__thiscall*)(Source::CBasePlayer*), pPlayer, 120)(pPlayer);
#elif defined(GAME_CSS_STEAM)
	GET_VFUNC(void(__thiscall*)(Source::CBasePlayer*), pPlayer, 121)(pPlayer);
#elif defined(GAME_L4D2)
	GET_VFUNC(void(__thiscall*)(Source::CBasePlayer*), pPlayer, 130)(pPlayer);
#endif
}