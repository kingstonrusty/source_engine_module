#include "Fakelag.h"

#include "Features/Visuals/Menu.h"

using namespace Features::HackvsHack;

int Fakelag::m_nChoked;
Vector Fakelag::m_vecOldPos;
bool Fakelag::m_bLagging;

extern int g_nChoked;

void Fakelag::Run(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd, bool* bSendPacket)
{
	FINDVAR(hvh_fakelag, "Hack vs Hack", "Fakelag");
	FINDVAR(hvh_fakelag_method, "Hack vs Hack", "Fakelag", "Method");

	if (!hvh_fakelag->GetBool())
	{
		return;
	}

	int iFakelagMethod = hvh_fakelag_method->GetInt();

	if (iFakelagMethod == 0) // Static
	{
		FINDVAR(hvh_fakelag_value, "Hack vs Hack", "Fakelag", "Value");

		*bSendPacket = (Source::Interfaces::Globals->tickcount() % (hvh_fakelag_value->GetInt() + 1)) == 0;
		m_bLagging = !*bSendPacket;

		if (*bSendPacket)
		{
			m_nChoked = 0;
		}
		else
		{
			m_nChoked++;
		}
	}
	else if (iFakelagMethod == 1) // Adaptive
	{
		constexpr float flTeleportDistanceSqr = 64.f * 64.f;

		if (m_nChoked >= 14)
		{
			*bSendPacket = 1;
			m_nChoked = 0;
			m_vecOldPos = pLocal->GetLocalOrigin();
			m_bLagging = false;

			return;
		}

		Vector vDelta = m_vecOldPos - pLocal->GetLocalOrigin();
		#if defined(GAME_GMOD)
		float flDelta = vDelta.LengthSqr();
		#else
		float flDelta = vDelta.Length2DSqr();
		#endif

		if (flDelta > flTeleportDistanceSqr)
		{
			*bSendPacket = 1;
			m_nChoked = 0;
			m_vecOldPos = pLocal->GetLocalOrigin();
			m_bLagging = false;

			return;
		}

		*bSendPacket = 0;
		m_nChoked++;
		m_bLagging = true;
	}

	g_nChoked = m_nChoked;
}

bool Fakelag::IsFakelagging()
{
	FINDVAR(hvh_fakelag, "Hack vs Hack", "Fakelag");

	return hvh_fakelag->GetBool() && m_bLagging;
}