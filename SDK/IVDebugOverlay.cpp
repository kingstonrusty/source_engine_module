#include "IVDebugOverlay.h"

namespace Source
{
	namespace Interfaces
	{
		IVDebugOverlay* DebugOverlay;
	}
}

using namespace Source;

IVDebugOverlay* IVDebugOverlay::GetPtr()
{
	return (IVDebugOverlay*)::Util::BruteforceInterface("VDebugOverlay", "engine.dll");
}

int IVDebugOverlay::ScreenPosition(const Vector& point, Vector& screen)
{
	return GET_VFUNC(int(__thiscall*)(IVDebugOverlay*, const Vector&, Vector&), this, IVDEBUGOVERLAY_SCREENPOSITION)(this, point, screen);
}