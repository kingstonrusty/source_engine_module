#ifndef __HEADER_OVERRIDEVIEW__
#define __HEADER_OVERRIDEVIEW__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace ClientMode
	{
		extern CVMTHookManager* pVmt;

		void __fastcall OverrideView(Source::IClientMode* pThis, void*, Source::CViewSetup* setup);
	}
}

#endif