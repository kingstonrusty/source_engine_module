#ifndef __HEADER_CGLOBALVARSBASE__
#define __HEADER_CGLOBALVARSBASE__
#pragma once

#include "IBaseClientDLL.h"

namespace Source
{
	class CGlobalVarsBase;

	namespace Interfaces
	{
		extern CGlobalVarsBase* Globals;
	}
}

class Source::CGlobalVarsBase
{
public:
	static CGlobalVarsBase* GetPtr();

	int& framecount();
	float& curtime();
	float& frametime();
	int& maxClients();
	int& tickcount();
	float& interval_per_tick();
};

#endif