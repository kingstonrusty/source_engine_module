#include "CDetour.h"

using namespace Hooks;

CDetour::CDetour(void* pFunction, void* pHook)
{
	m_pFunction = pFunction;
	m_pHook = pHook;

	_Setup();
}

CDetour::CDetour(DWORD_PTR dwpFunction, void* pHook)
{
	m_pFunction = (void*)dwpFunction;
	m_pHook = pHook;

	_Setup();
}

void CDetour::Hook(bool bHook)
{
	if (bHook)
	{
		_Hook();
	}
	else
	{
		_Unhook();
	}
}

void CDetour::Unhook()
{
	Hook(false);
}

void CDetour::Rehook()
{
	Hook(true);
}

void CDetour::_Setup()
{
	memcpy(m_abOriginalBytes, m_pFunction, 5);

	m_abOurBytes[0] = 0xE9;
	*(DWORD_PTR*)(&m_abOurBytes[1]) = (DWORD_PTR)m_pHook - (DWORD_PTR)m_pFunction - 0x5;
}

void CDetour::_Hook()
{
	DWORD dwOldProtect;

	VirtualProtect(m_pFunction, 5, PAGE_EXECUTE_READWRITE, &dwOldProtect);
	memcpy(m_pFunction, m_abOurBytes, 5);
	VirtualProtect(m_pFunction, 5, dwOldProtect, nullptr);
}

void CDetour::_Unhook()
{
	DWORD dwOldProtect;

	VirtualProtect(m_pFunction, 5, PAGE_EXECUTE_READWRITE, &dwOldProtect);
	memcpy(m_pFunction, m_abOriginalBytes, 5);
	VirtualProtect(m_pFunction, 5, dwOldProtect, nullptr);
}