#ifndef __HEADER_IRENDER__
#define __HEADER_IRENDER__
#pragma once

#if defined(GAME_GMOD)
#include "IVRenderView.h"

namespace Source
{
	class IView;

	namespace Interfaces
	{
		extern IView* View;
	}
}

class Source::IView // actually IViewRender
{
public:
	static IView* GetPtr();

	void SetUpView();

	Frustum GetFrustum();
	CViewSetup* GetPlayerViewSetup();
};
#endif

#endif