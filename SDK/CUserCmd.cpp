#include "CUserCmd.h"

using namespace Source;

int& CUserCmd::command_number()
{
	return *(int*)((DWORD_PTR)this + CUSERCMD_COMMANDNUMBER);
}

int& CUserCmd::tick_count()
{
	return *(int*)((DWORD_PTR)this + CUSERCMD_TICKCOUNT);
}

Vector& CUserCmd::viewangles()
{
	return *(Vector*)((DWORD_PTR)this + CUSERCMD_VIEWANGLES);
}

float& CUserCmd::forwardmove()
{
	return *(float*)((DWORD_PTR)this + CUSERCMD_FORWARDMOVE);
}

float& CUserCmd::sidemove()
{
	return *(float*)((DWORD_PTR)this + CUSERCMD_SIDEMOVE);
}

float& CUserCmd::upmove()
{
	return *(float*)((DWORD_PTR)this + CUSERCMD_UPMOVE);
}

int& CUserCmd::buttons()
{
	return *(int*)((DWORD_PTR)this + CUSERCMD_BUTTONS);
}

byte& CUserCmd::impulse()
{
	return *(byte*)((DWORD_PTR)this + CUSERCMD_IMPULSE);
}

int& CUserCmd::weaponselect()
{
	return *(int*)((DWORD_PTR)this + 0x2C);
}

int& CUserCmd::weaponsubtype()
{
	return *(int*)((DWORD_PTR)this + 0x30);
}

int& CUserCmd::random_seed()
{
	return *(int*)((DWORD_PTR)this + CUSERCMD_RANDOMSEED);
}

short& CUserCmd::mousedx()
{
	return *(short*)((DWORD_PTR)this + CUSERCMD_MOUSEDX);
}

short& CUserCmd::mousedy()
{
	return *(short*)((DWORD_PTR)this + CUSERCMD_MOUSEDY);
}

#if defined(GAME_GMOD)
unsigned int CUserCmd::GetCheckSum()
{
	static unsigned int(__thiscall* pGetCheckSum)(CUserCmd*) = Memory::FindPattern<unsigned int(__thiscall*)(CUserCmd*)>("55 8B EC 51 56 8D 45 FC", "client.dll");

	return pGetCheckSum(this);
}
#endif