#ifndef __HEADER_WRITEUSERCMDTODELTABUFFER__
#define __HEADER_WRITEUSERCMDTODELTABUFFER__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace Client
	{
		extern CVMTHookManager* pVmt;

#if defined(GAME_L4D2)
		bool __fastcall WriteUsercmdDeltaToBuffer(Source::IBaseClientDLL* pThis, void*, void* buf, int unk1, int from, int to, bool isnewcommand);
#else
		bool __fastcall WriteUsercmdDeltaToBuffer(Source::IBaseClientDLL* pThis, void*, void* buf, int from, int to, bool isnewcommand);
#endif
	}
}

#endif