#include "NET_SendPacket.h"

#if defined(GAME_GMOD)

int Hooks::NetChannel::NET_SendPacket(Source::CNetChan* chan, int sock, const Source::netadr_t& to, const unsigned char* data, int length, void* pVoicePayload, bool bUseCompression)
{
	pNET_SendPacket->Unhook();
	auto iRet = pNET_SendPacket->GetOriginal<int(*)(Source::CNetChan*, int, const Source::netadr_t&, const unsigned char*, int, void*, bool)>()(chan, sock, to, data, length, pVoicePayload, bUseCompression);
	pNET_SendPacket->Rehook();

	return iRet;
}

#endif