#ifndef __HEADER_CTELEMETRYPOST__
#define __HEADER_CTELEMETRYPOST__
#pragma once

#define TELEMETRY_IP "0.0.0.0"

#include <winhttp.h>
#pragma comment(lib, "winhttp.lib")

#define KEYVALUE_EXPAND(x, y) ::Telemetry::STelemetryKeyValue(x, y)
#define KEYVALUE(x, y) KEYVALUE_EXPAND(x, y)

namespace Telemetry
{
	struct STelemetryKeyValue;

	class CTelemetryPost;
}

struct Telemetry::STelemetryKeyValue
{
	STelemetryKeyValue(const char* pszKey, const char* pszValue)
	{
		if (pszKey != nullptr)
		{
			m_pszKey = new char[strlen(pszKey) + 1];
			strcpy(m_pszKey, pszKey);
		}

		if (pszValue != nullptr)
		{
			m_pszValue = new char[strlen(pszValue) + 1];
			strcpy(m_pszValue, pszValue);
		}
	}

	STelemetryKeyValue(const STelemetryKeyValue& other)
	{
		Clear();

		if (other.m_pszKey != nullptr)
		{
			m_pszKey = new char[strlen(other.m_pszKey) + 1];
			strcpy(m_pszKey, other.m_pszKey);
		}

		if (other.m_pszValue != nullptr)
		{
			m_pszValue = new char[strlen(other.m_pszValue) + 1];
			strcpy(m_pszValue, other.m_pszValue);
		}
	}

	~STelemetryKeyValue()
	{
		Clear();
	}

	void Clear()
	{
		if (m_pszKey != nullptr)
		{
			delete[] m_pszKey;
			m_pszKey = nullptr;
		}

		if (m_pszValue != nullptr)
		{
			delete[] m_pszValue;
			m_pszValue = nullptr;
		}
	}

	char* m_pszKey = nullptr;
	char* m_pszValue = nullptr;
};

class Telemetry::CTelemetryPost
{
public:
	CTelemetryPost() = default;

	template<class... Args> 
	CTelemetryPost(STelemetryKeyValue kv, Args... args) : CTelemetryPost(args...)
	{
		m_vecKeyValues.Add(kv);
	}

	CTelemetryPost(STelemetryKeyValue kv)
	{
		m_vecKeyValues.Add(kv);
	}

	~CTelemetryPost()
	{
		m_vecKeyValues.Clear();
	}

	void Spew();
	void Send();
private:
	SmallVector<STelemetryKeyValue> m_vecKeyValues;
};

#endif