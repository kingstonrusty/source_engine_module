#include "CInput.h"

#include "Features/PositionAdjustment/Prediction.h"

namespace Source
{
	namespace Interfaces
	{
		CInput* Input;
	}
}

using namespace Source;

CInput* CInput::GetPtr()
{
	auto pClient = IBaseClientDLL::GetPtr();

	for (DWORD_PTR dwpStartAddr = GET_VFUNC(DWORD_PTR, pClient, 14), dOffset = 0; dOffset <= 0xFF; dOffset++)
	{
		if (*(byte*)(dwpStartAddr + dOffset) == 
#if defined(GAME_CSGO)
			0xB9
#else
			0x0D
#endif
		)
		{
			return **(CInput***)(dwpStartAddr + dOffset + 0x1);
		}
	}

	return nullptr;
}

#if defined(GAME_GMOD)
CVerifiedUserCmd* CInput::GetVerifiedUserCmd(int sequence_number)
{
	return (CVerifiedUserCmd*)(*(DWORD*)((DWORD_PTR)this + 200) + 356 * (sequence_number % 90));
}
#endif

CUserCmd* CInput::GetUserCmd(int sequence_number)
{
	return GET_VFUNC(CUserCmd*(__thiscall*)(CInput*, int), this, CINPUT_GETUSERCMD)(this, sequence_number);
}

int CInput::CAM_IsThirdPerson()
{
	return GET_VFUNC(int(__thiscall*)(CInput*), this, CINPUT_CAM_ISTHIRDPERSON)(this);
}

int& CInput::m_fCameraInThirdPerson()
{
	return *(int*)((DWORD_PTR)this + CINPUT_CAMERAINTHIRDPERSON);
}

#if defined(GAME_GMOD)
void CVerifiedUserCmd::SetUserCmd(CUserCmd* cmd)
{
	static void(__thiscall* pCUserCmdCopy)(CUserCmd*, CUserCmd*) = Memory::FindPattern<void(__thiscall*)(CUserCmd*, CUserCmd*)>("55 8B EC 56 57 8B 7D 08 8B F1 3B F7 0F 84 ?? ?? ?? ?? 8B 47 04", "client.dll");

	pCUserCmdCopy(m_cmd(), cmd);

	m_crc = cmd->GetCheckSum();
}
#endif