#ifndef __HEADER_IBASECLIENTDLL__
#define __HEADER_IBASECLIENTDLL__
#pragma once

#include "IClientNetworkable.h"

#include "Multigame/InterfaceBruteforcer.h"

#include "IVRenderView.h"

namespace Source
{
	enum ClientFrameStage_t
	{
		FRAME_UNDEFINED = -1,
		FRAME_START,
		FRAME_NET_UPDATE_START,
		FRAME_NET_UPDATE_POSTDATAUPDATE_START,
		FRAME_NET_UPDATE_POSTDATAUPDATE_END,
		FRAME_NET_UPDATE_END,
		FRAME_RENDER_START,
		FRAME_RENDER_END
	};

	enum RenderViewInfo_t
	{
		RENDERVIEW_UNSPECIFIED = 0,
		RENDERVIEW_DRAWVIEWMODEL = (1 << 0),
		RENDERVIEW_DRAWHUD = (1 << 1),
		RENDERVIEW_SUPPRESSMONITORRENDERING = (1 << 2),
	};

	enum ClearFlags_t
	{
		VIEW_CLEAR_COLOR = 0x1,
		VIEW_CLEAR_DEPTH = 0x2,
		VIEW_CLEAR_FULL_TARGET = 0x4,
		VIEW_NO_DRAW = 0x8,
		VIEW_CLEAR_OBEY_STENCIL = 0x10,
	};

	class IBaseClientDLL;

	namespace Interfaces
	{
		extern IBaseClientDLL* Client;
	}
}

class Source::IBaseClientDLL
{
public:
	static IBaseClientDLL* GetPtr();

	ClientClass* GetAllClasses();
#if defined(GAME_L4D2)
	void CreateMove(int sequence_number, float sample_time, bool active);
#endif
#if defined(GAME_L4D2)
	bool WriteUsercmdDeltaToBuffer(void* buf, int unk1, int from, int to, bool isnewcommand);
#else
	bool WriteUsercmdDeltaToBuffer(void* buf, int from, int to, bool isnewcommand);
#endif
#if defined(GAME_GMOD)
	void RenderView(CViewSetup& view, int flags, int whattorender);
#endif
	void FrameStageNotify(ClientFrameStage_t stage);
};

#endif