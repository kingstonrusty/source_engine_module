#include "Input.h"

#include "SDK/Source.h"

#include "Util/Input.h"

int Input::l_GetCursorPos(lua_State* L)
{
	int mx, my;
	Source::Interfaces::Surface->GetCursorPos(mx, my);

	lua_pushinteger(L, mx);
	lua_pushinteger(L, my);
	return 2;
}

int Input::l_GetKeyState(lua_State* L)
{
	int key = luaL_checkinteger(L, 1);
	auto state = Util::Input::GetButtonState(*(unsigned int*)(&key));

	if (state == 0x0)
	{
		lua_pushinteger(L, 0);
	}
	else if (state & 0x1)
	{
		lua_pushinteger(L, 1);
	}
	else
	{
		lua_pushinteger(L, 2);
	}

	return 1;
}

int Input::l_GetKeyName(lua_State* L)
{
	int key = luaL_checkinteger(L, 1);
	lua_pushstring(L, Util::Input::GetButtonName(*(unsigned int*)(&key)));

	return 1;
}