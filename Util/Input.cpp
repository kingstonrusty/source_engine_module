#include "Input.h"

using namespace Util;

KeyValues<unsigned int, SHORT> Input::m_ButtonStates;

void Input::Update()
{
	for (char c = 'A'; c <= 'Z'; c++)
	{
		m_ButtonStates[c] = GetAsyncKeyState(c);
	}

	for (char c = '0'; c <= '9'; c++)
	{
		m_ButtonStates[c] = GetAsyncKeyState(c);
	}

	static unsigned int constexpr s_VirtualKeys[] = 
	{
		VK_LBUTTON, VK_RBUTTON, VK_CANCEL, VK_MBUTTON, VK_XBUTTON1, VK_XBUTTON2, VK_BACK, VK_TAB, VK_CLEAR,
		VK_RETURN, VK_PAUSE, VK_CAPITAL,
		VK_ESCAPE, VK_SPACE,
		VK_PRIOR, VK_NEXT, VK_END, VK_HOME, VK_LEFT, VK_UP, VK_RIGHT, VK_DOWN, VK_SELECT, VK_PRINT, VK_EXECUTE, VK_SNAPSHOT,
		VK_INSERT, VK_DELETE, VK_HELP, VK_LWIN, VK_RWIN, VK_APPS, VK_SLEEP, VK_NUMPAD0, VK_NUMPAD1, VK_NUMPAD2, VK_NUMPAD3,
		VK_NUMPAD4, VK_NUMPAD5, VK_NUMPAD6, VK_NUMPAD7, VK_NUMPAD8, VK_NUMPAD9,
		VK_F1, VK_F2, VK_F3, VK_F4, VK_F5, VK_F6, VK_F7, VK_F8, VK_F9, VK_F10, VK_F11, VK_F12,
		VK_F13, VK_F14, VK_F15, VK_F16, VK_F17, VK_F18, VK_F19, VK_F20, VK_F21, VK_F22, VK_F23, VK_F24, VK_NUMLOCK, VK_SCROLL,
		VK_LSHIFT, VK_RSHIFT, VK_LCONTROL, VK_RCONTROL, VK_LMENU, VK_RMENU, VK_OEM_PERIOD, VK_OEM_MINUS, VK_OEM_COMMA, VK_OEM_PLUS
	};
	
	for (auto key : s_VirtualKeys)
	{
		m_ButtonStates[key] = GetAsyncKeyState(key);
	}
}

short Input::GetButtonState(unsigned int key)
{
	return m_ButtonStates.DoesExist(key) ? m_ButtonStates[key] : 0;
}

const char* Input::GetButtonName(unsigned int key)
{
	static KeyValues<unsigned int, const char*> s_Key;
	if (!s_Key.DoesExist(VK_LBUTTON))
	{
		s_Key[VK_LBUTTON] = "Mouse1";
		s_Key[VK_RBUTTON] = "Mouse2";
		s_Key[VK_CANCEL] = "Cancel";
		s_Key[VK_MBUTTON] = "Mouse3";
		s_Key[VK_XBUTTON1] = "Mouse4";
		s_Key[VK_XBUTTON2] = "Mouse5";
		s_Key[VK_BACK] = "Backspace";
		s_Key[VK_TAB] = "Tab";
		s_Key[VK_CLEAR] = "Clear";
		s_Key[VK_RETURN] = "Return";
		s_Key[VK_PAUSE] = "Pause";
		s_Key[VK_CAPITAL] = "Capslock";
		s_Key[VK_ESCAPE] = "Escape";
		s_Key[VK_SPACE] = "Spacebar";
		s_Key[VK_PRIOR] = "Page Up";
		s_Key[VK_NEXT] = "Page Down";
		s_Key[VK_END] = "End";
		s_Key[VK_HOME] = "Home";
		s_Key[VK_LEFT] = "Left Arrow";
		s_Key[VK_UP] = "Up Arrow";
		s_Key[VK_RIGHT] = "Right Arrow";
		s_Key[VK_DOWN] = "Down Arrow";
		s_Key[VK_SELECT] = "Select";
		s_Key[VK_PRINT] = "Print";
		s_Key[VK_EXECUTE] = "Execute";
		s_Key[VK_SNAPSHOT] = "Print Screen";
		s_Key[VK_INSERT] = "Insert";
		s_Key[VK_DELETE] = "Delete";
		s_Key[VK_HELP] = "Help";
		s_Key['0'] = "0";
		s_Key['1'] = "1";
		s_Key['2'] = "2";
		s_Key['3'] = "3";
		s_Key['4'] = "4";
		s_Key['5'] = "5";
		s_Key['6'] = "6";
		s_Key['7'] = "7";
		s_Key['8'] = "8";
		s_Key['9'] = "9";
		s_Key['A'] = "A";
		s_Key['B'] = "B";
		s_Key['C'] = "C";
		s_Key['D'] = "D";
		s_Key['E'] = "E";
		s_Key['F'] = "F";
		s_Key['G'] = "G";
		s_Key['H'] = "H";
		s_Key['I'] = "I";
		s_Key['J'] = "J";
		s_Key['K'] = "K";
		s_Key['L'] = "L";
		s_Key['M'] = "M";
		s_Key['N'] = "N";
		s_Key['O'] = "O";
		s_Key['P'] = "P";
		s_Key['Q'] = "Q";
		s_Key['R'] = "R";
		s_Key['S'] = "S";
		s_Key['T'] = "T";
		s_Key['U'] = "U";
		s_Key['V'] = "V";
		s_Key['W'] = "W";
		s_Key['X'] = "X";
		s_Key['Y'] = "Y";
		s_Key['Z'] = "Z";
		s_Key[VK_LWIN] = "Left Windows";
		s_Key[VK_RWIN] = "Right Windows";
		s_Key[VK_APPS] = "Applications";
		s_Key[VK_SLEEP] = "Sleep";
		s_Key[VK_NUMPAD0] = "Numpad 0";
		s_Key[VK_NUMPAD1] = "Numpad 1";
		s_Key[VK_NUMPAD2] = "Numpad 2";
		s_Key[VK_NUMPAD3] = "Numpad 3";
		s_Key[VK_NUMPAD4] = "Numpad 4";
		s_Key[VK_NUMPAD5] = "Numpad 5";
		s_Key[VK_NUMPAD6] = "Numpad 6";
		s_Key[VK_NUMPAD7] = "Numpad 7";
		s_Key[VK_NUMPAD8] = "Numpad 8";
		s_Key[VK_NUMPAD9] = "Numpad 9";
		s_Key[VK_F1] = "F1";
		s_Key[VK_F2] = "F2";
		s_Key[VK_F3] = "F3";
		s_Key[VK_F4] = "F4";
		s_Key[VK_F5] = "F5";
		s_Key[VK_F6] = "F6";
		s_Key[VK_F7] = "F7";
		s_Key[VK_F8] = "F8";
		s_Key[VK_F9] = "F9";
		s_Key[VK_F10] = "F10";
		s_Key[VK_F11] = "F11";
		s_Key[VK_F12] = "F12";
		s_Key[VK_F13] = "F13";
		s_Key[VK_F14] = "F14";
		s_Key[VK_F15] = "F15";
		s_Key[VK_F16] = "F16";
		s_Key[VK_F17] = "F17";
		s_Key[VK_F18] = "F18";
		s_Key[VK_F19] = "F19";
		s_Key[VK_F20] = "F20";
		s_Key[VK_F21] = "F21";
		s_Key[VK_F22] = "F22";
		s_Key[VK_F23] = "F23";
		s_Key[VK_F24] = "F24";
		s_Key[VK_NUMLOCK] = "Num Lock";
		s_Key[VK_SCROLL] = "Scroll Lock";
		s_Key[VK_LSHIFT] = "Left Shift";
		s_Key[VK_RSHIFT] = "Right Shift";
		s_Key[VK_LCONTROL] = "Left Control";
		s_Key[VK_RCONTROL] = "Right Control";
		s_Key[VK_LMENU] = "Left Alt";
		s_Key[VK_RMENU] = "Right Alt";
		s_Key[VK_OEM_PERIOD] = ".";
		s_Key[VK_OEM_MINUS] = "-";
		s_Key[VK_OEM_COMMA] = ",";
		s_Key[VK_OEM_PLUS] = "+";
	}

	return s_Key.DoesExist(key) ? s_Key[key] : "";
}