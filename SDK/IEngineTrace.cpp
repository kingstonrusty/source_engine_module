#include "IEngineTrace.h"

namespace Source
{
	namespace Interfaces
	{
		IEngineTrace* TraceRay;
	}
}

using namespace Source;

void Ray_t::Init(Vector& vStart, Vector& vEnd)
{
	m_Delta = vEnd - vStart;
	m_IsSwept = (m_Delta.LengthSqr() != 0.f);
	m_Extents.x = m_Extents.y = m_Extents.z = 0.0f; 
	m_IsRay = true;
	m_StartOffset.x = m_StartOffset.y = m_StartOffset.z = 0.0f;
	m_Start = vStart;

#if defined(GAME_CSGO) || defined(GAME_L4D2)
	m_pWorldAxisTransform = nullptr;
#endif
}

void Ray_t::Init(Vector& vStart, Vector& vEnd, Vector& vMins, Vector& vMaxs)
{
	m_Delta = vEnd - vStart;
	m_IsSwept = (m_Delta.LengthSqr() != 0);
	m_Extents = vMaxs - vMins;
	m_Extents *= 0.5f;
	m_IsRay = (m_Extents.LengthSqr() < 1e-6);
	m_StartOffset = vMins + vMaxs;
	m_StartOffset *= 0.5f;
	m_Start = vStart + m_StartOffset;
	m_StartOffset *= -1.0f;

#if defined(GAME_CSGO) || defined(GAME_L4D2)
	m_pWorldAxisTransform = nullptr;
#endif
}

IEngineTrace* IEngineTrace::GetPtr()
{
	return (IEngineTrace*)::Util::BruteforceInterface("EngineTraceClient", "engine.dll");
}

void IEngineTrace::TraceRay(const Ray_t& ray, unsigned int mask, ITraceFilter* filter, trace_t* trace)
{
	return GET_VFUNC(void(__thiscall*)(IEngineTrace*, const Ray_t&, unsigned int, ITraceFilter*, trace_t*), this, IENGINETRACE_TRACERAY)(this, ray, mask, filter, trace);
}