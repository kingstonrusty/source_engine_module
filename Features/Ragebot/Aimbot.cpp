#include "Aimbot.h"

#include "Features/Visuals/Menu.h"
#include "Features/Misc/InterpLagComp.h"

using namespace Features::Ragebot;

Source::CBasePlayer* Aimbot::m_pLocal;
Source::CBaseWeapon* Aimbot::m_pWeapon;
Source::CUserCmd* Aimbot::m_pCurrentCommand;
AimbotLogic::AimbotTarget* Aimbot::m_pTarget;

bool Aimbot::Run(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, bool* bSendPacket)
{
	m_pLocal = pLocal;
	m_pWeapon = pWeapon;
	m_pCurrentCommand = cmd;
	m_pTarget = nullptr;

	if (*bSendPacket && !AimbotLogic::ShouldRunAimbot(m_pLocal, m_pWeapon, m_pCurrentCommand))
	{
		return false;
	}

	AimbotLogic::ResetData();

	m_pTarget = FindTarget();
	if (m_pTarget == nullptr)
	{
		return false;
	}

	return Finish();
}

bool Aimbot::Finish()
{
	return AimbotLogic::FinishAimbot(m_pLocal, m_pTarget, m_pWeapon, m_pCurrentCommand);
}

AimbotLogic::AimbotTarget* Aimbot::FindTarget()
{
	return AimbotLogic::FindTarget(m_pLocal, m_pWeapon, m_pCurrentCommand);
}