#include "InterfaceBruteforcer.h"

void* Util::BruteforceInterface(const char* pszInterfaceName, const char* pszModule)
{
	for (int nVersion = 100; nVersion >= 0; nVersion--)
	{
		char szBuf[128 + 1] = { 0 };
		sprintf_s(szBuf, "%s%03d", pszInterfaceName, nVersion);

		if (Source::CreateInterface(szBuf, pszModule) != nullptr)
		{
			return Source::CreateInterface(szBuf, pszModule);
		}
	}

	return nullptr;
}