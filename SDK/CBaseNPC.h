#ifndef __HEADER_CBASENPC__
#define __HEADER_CBASENPC__
#pragma once

#include "CBaseEntity.h"

namespace Source
{
	class CBaseNPC;
}

class Source::CBaseNPC : public CBaseEntity
{
public:
	bool IsAlive();

#if defined(GAME_L4D2)
	bool IsNeutral();
	bool IsCommonInfected();
	bool IsSpecialInfected();
	bool IsTank();
	bool IsCharger();
	bool IsJockey();
	bool IsHunter();
	bool IsSmoker();
	bool IsSpitter();
	bool IsBoomer();
	bool IsWitch();
#endif
};

#endif