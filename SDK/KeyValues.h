#ifndef __HEADER_KEYVALUES__
#define __HEADER_KEYVALUES__
#pragma once

#include "LinkedList.h"

template< class _K, class _V >
class KeyValues
{
public:
	~KeyValues()
	{
		m_Entries.Clear();
	}

	_V& operator[](const _K& key) const
	{
		auto pNode = m_Entries.GetHead();
		while (pNode != nullptr)
		{
			auto pNext = pNode->m_pNext;

			if (pNode->m_pThis->m_Key == key)
			{
				return pNode->m_pThis->m_Value;
			}

			pNode = pNext;
		}

		Entry* pNew = new Entry;

		pNew->m_Key = key;
		pNew->m_Value = _V();

		m_Entries.Insert(pNew);

		return pNew->m_Value;
	}

	_V& operator[](const _K key)
	{
		auto pNode = m_Entries.GetHead();
		while (pNode != nullptr)
		{
			auto pNext = pNode->m_pNext;

			if (pNode->m_pThis->m_Key == key)
			{
				return pNode->m_pThis->m_Value;
			}

			pNode = pNext;
		}

		Entry* pNew = new Entry;

		pNew->m_Key = key;
		pNew->m_Value = _V();

		m_Entries.Insert(pNew);

		return pNew->m_Value;
	}

	bool DoesExist(const _K key)
	{
		auto pNode = m_Entries.GetHead();
		while (pNode != nullptr)
		{
			auto pNext = pNode->m_pNext;

			if (pNode->m_pThis->m_Key == key)
			{
				return true;
			}

			pNode = pNext;
		}

		return false;
	}
private:
	struct Entry
	{
		_K m_Key;
		_V m_Value;
	};
	LinkedList<Entry> m_Entries;
};

#endif