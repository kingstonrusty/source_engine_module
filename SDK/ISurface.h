#ifndef __HEADER_ISURFACE__
#define __HEADER_ISURFACE__
#pragma once

#undef CreateFont

#include "Multigame/InterfaceBruteforcer.h"

namespace Source
{
	typedef unsigned long HFONT;

	enum FontDrawType_t
	{
		FONT_DRAW_DEFAULT = 0,
		FONT_DRAW_NONADDITIVE,
		FONT_DRAW_ADDITIVE,
		FONT_DRAW_TYPE_COUNT = 2,
	};

	enum EFontFlags
	{
		FONTFLAG_NONE,
		FONTFLAG_ITALIC = 0x001,
		FONTFLAG_UNDERLINE = 0x002,
		FONTFLAG_STRIKEOUT = 0x004,
		FONTFLAG_SYMBOL = 0x008,
		FONTFLAG_ANTIALIAS = 0x010,
		FONTFLAG_GAUSSIANBLUR = 0x020,
		FONTFLAG_ROTARY = 0x040,
		FONTFLAG_DROPSHADOW = 0x080,
		FONTFLAG_ADDITIVE = 0x100,
		FONTFLAG_OUTLINE = 0x200,
		FONTFLAG_CUSTOM = 0x400,		// custom generated font - never fall back to asian compatibility mode
		FONTFLAG_BITMAP = 0x800,		// compiled bitmap font - no fallbacks
	};

	class ISurface;

	namespace Interfaces
	{
		extern ISurface* Surface;
	}
}

class Source::ISurface
{
public:
	static ISurface* GetPtr();
	
	void DrawSetColor(Color col);

	void DrawFilledRect(int x1, int y1, int x2, int y2);
	void DrawOutlinedRect(int x1, int y1, int x2, int y2);
	void DrawLine(int x1, int y1, int x2, int y2);

	void DrawSetTextFont(HFONT font);
	void DrawSetTextColor(Color col);
	void DrawSetTextPos(int x, int y);
	void DrawPrintText(const wchar_t* text, int len, FontDrawType_t drawtype = FONT_DRAW_DEFAULT);

	void GetScreenSize(int& w, int& h);

	HFONT CreateFont();
	bool SetFontGlyphSet(HFONT font, const char* fontname, int tall, int weight, int blur, int scanlines, int flags, int rangemin, int rangemax);

	void GetTextSize(HFONT font, const wchar_t* text, int& w, int& h);

	void GetCursorPos(int& x, int& y);
	void SetCursorPos(int x, int y);

	void ResetFontCaches();

	void StartDrawing();
	void FinishDrawing();
};

#include "Util/DrawingHelper.h"

namespace Util
{
	namespace DrawingHelper
	{
		extern Source::HFONT TitleFont;
		extern Source::HFONT MenuFont;
		extern Source::HFONT LargeTitleFont;
		extern Source::HFONT DebugFont;
	}
}

#endif