#ifndef __HEADER_SETVIEWANGLES__
#define __HEADER_SETVIEWANGLES__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace Engine
	{
		extern int g_iLastFrameCount;

		extern CVMTHookManager* pVmt;

		void __fastcall SetViewAngles(Source::IVEngineClient* pThis, void*, Vector& angles);
	}
}

#endif