#ifndef __HEADER_HOOK_LUA__
#define __HEADER_HOOK_LUA__
#pragma once

#include "BaseLibrary.h"

class Hook: public BaseLibrary<Hook>
{
public:
	friend class BaseLibrary<Hook>;

	static const char* GetName() { return "Hook"; }
private:
	static constexpr const struct luaL_Reg m_Register[] =
	{
		{ NULL, NULL }
	};
};

#endif