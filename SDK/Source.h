#ifndef __HEADER_SOURCE__
#define __HEADER_SOURCE__
#pragma once

#include "CGlobalVarsBase.h"
#include "CInput.h"
#include "IBaseClientDLL.h"
#include "IClientEntityList.h"
#include "IClientMode.h"
#include "ICvar.h"
#include "IEngineTrace.h"
#include "IGameMovement.h"
#include "IMaterialSystem.h"
#include "INetworkStringTableContainer.h"
#include "IPrediction.h"
#include "IStudioRender.h"
#include "ISurface.h"
#include "IVDebugOverlay.h"
#include "IVEngineClient.h"
#include "IVEngineVGui.h"
#include "IView.h"
#include "IVModelRender.h"
#include "IVModelInfo.h"
#include "IVRenderView.h"

#if defined(GAME_GMOD)

#include "ILua.h"

#endif

#include "CBaseEntity.h"
#include "CBasePlayer.h"
#include "CBaseWeapon.h"
#include "CBaseNPC.h"

#include "IAnimState.h"

#include "CUserCmd.h"

#include "CNetVarManager.h"

namespace Source
{
	__forceinline int TIME_TO_TICKS(float time)
	{
		return ((int)(0.5f + time / Interfaces::Globals->interval_per_tick()));
	}

	__forceinline float TICKS_TO_TIME(int ticks)
	{
		return Interfaces::Globals->interval_per_tick() * (float)ticks;
	}

	__forceinline bool* GetSendPacket()
	{
		DWORD_PTR base;
		__asm
		{
			mov base, ebp
		}

#if defined(GAME_L4D2)
		return ****(bool*****)base - 0x21;
#else
		return ***(bool****)base - 0x1;
#endif
	}

	__forceinline int RandomInt(int min, int max)
	{
		static int(__cdecl* pRandomInt)(int, int) = (int(__cdecl*)(int, int))GetProcAddress(GetModuleHandleA("vstdlib.dll"), "RandomInt");

		return pRandomInt(min, max);
	}

	__forceinline float RandomFloat(float min, float max)
	{
		static float(__cdecl* pRandomFloat)(float, float) = (float(__cdecl*)(float, float))GetProcAddress(GetModuleHandleA("vstdlib.dll"), "RandomFloat");

		return pRandomFloat(min, max);
	}

	__forceinline void RandomSeed(unsigned int seed)
	{
		static void(__cdecl* pRandomSeed)(unsigned int) = (void(__cdecl*)(unsigned int))GetProcAddress(GetModuleHandleA("vstdlib.dll"), "RandomSeed");

		return pRandomSeed(seed);
	}

	__forceinline unsigned int MD5_PseudoRandom(unsigned int seed)
	{
		static unsigned int(__cdecl* pMD5_PseudoRandom)(unsigned int) = Memory::FindPattern<unsigned int(__cdecl*)(unsigned int)>(MISC_MD5PSEUDORANDOM, "client.dll");

		return pMD5_PseudoRandom(seed);
	}
}

#endif