#ifndef __HEADER_ICLIENTRENDERABLE__
#define __HEADER_ICLIENTRENDERABLE__
#pragma once

#include "CGlobalVarsBase.h"

namespace Source
{
	struct model_t;

	class IClientRenderable;
}

class Source::IClientRenderable
{
public:
	void InvalidateBoneCache();

	const Vector& GetRenderOrigin();
	const Vector& GetRenderAngles();
	const model_t* GetModel();
	int DrawModel();
	bool SetupBones(matrix3x4* matrices, int maxbones, int mask, float time);

	int& m_iMostRecentModelBoneCounter();
	int& m_fReadableBones();
	int& m_fWriteableBones();
	int& m_iPrevBoneMask();
	int& m_iAccumulatedBoneMask();
	float& m_flLastBoneSetupTime();
};

#endif