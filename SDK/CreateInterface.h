#ifndef __HEADER_CREATEINTERFACE__
#define __HEADER_CREATEINTERFACE__
#pragma once

namespace Source
{
	void* CreateInterface(const char* pszInterfaceName, const char* pszModule);
}

#endif