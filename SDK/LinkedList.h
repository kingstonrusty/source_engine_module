#ifndef __HEADER_LINKEDLIST__
#define __HEADER_LINKEDLIST__
#pragma once

template< class _T >
class LinkedList
{
public:
	struct Node
	{
		~Node()
		{
			delete m_pThis;
		}

		_T* m_pThis;
		Node* m_pNext;
		Node* m_pPrev;
	};

	LinkedList()
	{
		m_pHead = nullptr;
		m_pTail = nullptr;
		m_nSize = 0;
	}

	~LinkedList()
	{
		Node* pNode = GetHead();

		while (pNode != nullptr)
		{
			Node* pNext = pNode->m_pNext;

			delete pNode;
			pNode = pNext;
		}
	}

	Node* GetHead()
	{
		return m_pHead;
	}

	Node* GetTail()
	{
		return m_pTail;
	}

	void Remove(Node* pNodeToRemove)
	{
		Node* pNode = GetHead();

		while (pNode != nullptr)
		{
			if (pNode == pNodeToRemove)
			{
				if (pNodeToRemove == GetTail() && pNodeToRemove == GetHead())
				{
					m_pHead = nullptr;
					m_pTail = nullptr;

					delete pNodeToRemove;
				}
				else if (pNodeToRemove == GetTail())
				{
					RemoveTail();
				}
				else if (pNodeToRemove == GetHead())
				{
					RemoveHead();
				}
				else
				{
					pNode->m_pPrev->m_pNext = pNode->m_pNext;
					pNode->m_pNext->m_pPrev = pNode->m_pPrev;

					delete pNodeToRemove;
				}

				m_nSize--;

				break;
			}

			pNode = pNode->m_pNext;
		}
	}

	void RemoveHead()
	{
		Node* pHead = GetHead();

		pHead->m_pNext->m_pPrev = nullptr;
		m_pHead = pHead->m_pNext;

		delete pHead;
	}

	void RemoveTail()
	{
		Node* pTail = GetTail();

		pTail->m_pPrev->m_pNext = nullptr;
		m_pTail = pTail->m_pPrev;

		delete pTail;
	}

	void Clear()
	{
		while (GetHead() != nullptr)
		{
			Remove(GetHead());
		}
	}

	void Insert(_T* pPtr)
	{
		Node* pNew = new Node;

		pNew->m_pThis = pPtr;
		
		if (GetTail() == nullptr && GetHead() == nullptr)
		{
			pNew->m_pNext = nullptr;
			pNew->m_pPrev = nullptr;

			m_pHead = pNew;
			m_pTail = pNew;
		}
		else
		{
			pNew->m_pNext = nullptr;
			pNew->m_pPrev = GetTail();

			GetTail()->m_pNext = pNew;

			m_pTail = pNew;
		}

		m_nSize++;
	}

	size_t GetSize()
	{
		return m_nSize;
	}
private:
	Node* m_pHead;
	Node* m_pTail;
	size_t m_nSize;
};

template< class _T >
class LinkedListNoDelete
{
public:
	struct Node
	{
		~Node()
		{
			//delete m_pThis;
		}

		_T* m_pThis;
		Node* m_pNext;
		Node* m_pPrev;
	};

	LinkedListNoDelete()
	{
		m_pHead = nullptr;
		m_pTail = nullptr;
		m_nSize = 0;
	}

	~LinkedListNoDelete()
	{
		Node* pNode = GetHead();

		while (pNode != nullptr)
		{
			Node* pNext = pNode->m_pNext;

			delete pNode;
			pNode = pNext;
		}
	}

	Node* GetHead()
	{
		return m_pHead;
	}

	Node* GetTail()
	{
		return m_pTail;
	}

	void Remove(Node* pNodeToRemove)
	{
		Node* pNode = GetHead();

		while (pNode != nullptr)
		{
			if (pNode == pNodeToRemove)
			{
				if (pNodeToRemove == GetTail() && pNodeToRemove == GetHead())
				{
					m_pHead = nullptr;
					m_pTail = nullptr;

					delete pNodeToRemove;
				}
				else if (pNodeToRemove == GetTail())
				{
					RemoveTail();
				}
				else if (pNodeToRemove == GetHead())
				{
					RemoveHead();
				}
				else
				{
					pNode->m_pPrev->m_pNext = pNode->m_pNext;
					pNode->m_pNext->m_pPrev = pNode->m_pPrev;

					delete pNodeToRemove;
				}

				m_nSize--;

				break;
			}

			pNode = pNode->m_pNext;
		}
	}

	void RemoveHead()
	{
		Node* pHead = GetHead();

		pHead->m_pNext->m_pPrev = nullptr;
		m_pHead = pHead->m_pNext;

		delete pHead;
	}

	void RemoveTail()
	{
		Node* pTail = GetTail();

		pTail->m_pPrev->m_pNext = nullptr;
		m_pTail = pTail->m_pPrev;

		delete pTail;
	}

	void Clear()
	{
		while (GetHead() != nullptr)
		{
			Remove(GetHead());
		}
	}

	void Insert(_T* pPtr)
	{
		Node* pNew = new Node;

		pNew->m_pThis = pPtr;

		if (GetTail() == nullptr && GetHead() == nullptr)
		{
			pNew->m_pNext = nullptr;
			pNew->m_pPrev = nullptr;

			m_pHead = pNew;
			m_pTail = pNew;
		}
		else
		{
			pNew->m_pNext = nullptr;
			pNew->m_pPrev = GetTail();

			GetTail()->m_pNext = pNew;

			m_pTail = pNew;
		}

		m_nSize++;
	}

	size_t GetSize()
	{
		return m_nSize;
	}
private:
	Node* m_pHead;
	Node* m_pTail;
	size_t m_nSize;
};

#endif