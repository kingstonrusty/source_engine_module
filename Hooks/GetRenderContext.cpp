#include "GetRenderContext.h"

#if defined(GAME_GMOD)

bool Hooks::MaterialSystem::g_bRendering;

Source::IMatRenderContext* __fastcall Hooks::MaterialSystem::GetRenderContext(Source::IMaterialSystem* pThis, void*)
{
	{
		DWORD_PTR dwpRetAddr = (DWORD_PTR)_ReturnAddress();

#if defined(GAME_GMOD)
		static DWORD_PTR dwpRenderCapture = Memory::FindPattern<DWORD_PTR>(Source::MISC_RENDERCAPTURE_RETURNADDRESS, "client.dll");
		static DWORD_PTR dwpRenderCapturePixels = Memory::FindPattern<DWORD_PTR>(Source::MISC_RENDERCAPTUREPIXELS_RETURNADDRESS, "client.dll");

		if (dwpRenderCapture == dwpRetAddr || dwpRenderCapturePixels == dwpRetAddr)
#else
		if(false)
#endif
		{
			using namespace Source;

			g_bRendering = true;
			float flOldFrametime = Source::Interfaces::Globals->frametime();
			Source::Interfaces::Globals->frametime() = 0.f;
			Interfaces::View->SetUpView();
			Interfaces::Client->RenderView(*Interfaces::View->GetPlayerViewSetup(), VIEW_CLEAR_COLOR | VIEW_CLEAR_DEPTH, RENDERVIEW_DRAWVIEWMODEL | RENDERVIEW_DRAWHUD);
			Source::Interfaces::Globals->frametime() = flOldFrametime;
			g_bRendering = false;
		}
	}

	pVmt->Unhook();
	auto pRet = pThis->GetRenderContext();
	pVmt->Rehook();

	return pRet;
}

#endif