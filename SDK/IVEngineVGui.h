#ifndef __HEADER_IVENGINEVGUI__
#define __HEADER_IVENGINEVGUI__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

namespace Source
{
	class IVEngineVGui;

	namespace Interfaces
	{
		extern IVEngineVGui* EngineVGui;
	}
}

class Source::IVEngineVGui
{
public:
	static IVEngineVGui* GetPtr();

	void Paint(int mode);
};

#endif