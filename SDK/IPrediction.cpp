#include "IPrediction.h"

namespace Source
{
	namespace Interfaces
	{
		IPrediction* Prediction;
	}
}

using namespace Source;

IPrediction* IPrediction::GetPtr()
{
	return (IPrediction*)::Util::BruteforceInterface("VClientPrediction", "client.dll");
}

#if defined(GAME_GMOD)
bool IPrediction::IsFirstTimePredicted()
{
	return GET_VFUNC(bool(__thiscall*)(IPrediction*), this, IPREDICTION_ISFIRSTTIMEPREDICTED)(this);
}
#endif

void IPrediction::RunCommand(Source::CBasePlayer* player, Source::CUserCmd* cmd, IMoveHelper* movehelper)
{
	return GET_VFUNC(void(__thiscall*)(IPrediction*, CBasePlayer*, CUserCmd*, IMoveHelper*), this, IPREDICTION_RUNCOMMAND)(this, player, cmd, movehelper);
}

void IPrediction::SetupMove(Source::CBasePlayer* player, Source::CUserCmd* cmd, IMoveHelper* movehelper, CMoveData* movedata)
{
	return GET_VFUNC(void(__thiscall*)(IPrediction*, CBasePlayer*, CUserCmd*, IMoveHelper*, CMoveData*), this, IPREDICTION_SETUPMOVE)(this, player, cmd, movehelper, movedata);
}

void IPrediction::FinishMove(Source::CBasePlayer* player, Source::CUserCmd* cmd, CMoveData* movedata)
{
	return GET_VFUNC(void(__thiscall*)(IPrediction*, CBasePlayer*, CUserCmd*, CMoveData*), this, IPREDICTION_FINISHMOVE)(this, player, cmd, movedata);
}