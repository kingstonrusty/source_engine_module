#include "IMoveHelper.h"

using namespace Source;

void IMoveHelper::SetHost(CBasePlayer* player)
{
#if defined(GAME_CSGO) || defined(GAME_L4D2)
	GET_VFUNC(void(__thiscall*)(IMoveHelper*, CBasePlayer*), this, IMOVEHELPER_SETHOST)(this, player);
#endif
}