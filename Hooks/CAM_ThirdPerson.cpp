#include "CAM_ThirdPerson.h"

#include "Features/Visuals/Menu.h"

#include "Features/Visuals/AntiScreengrab.h"

#include "Features/Misc/Freecam.h"

int __fastcall Hooks::Input::CAM_IsThirdPerson(Source::CInput* pThis, void*)
{
	FINDVAR(misc, "Miscellaneous");
	FINDVAR(misc_thirdperson, "Miscellaneous", "Thirdperson");

#if 0
	if (!Features::Visuals::ShouldClearScreen() && ((misc->GetBool() && misc_thirdperson->GetBool()) || Features::Misc::Freecam::GetIsActivated()))
	{
		static DWORD_PTR s_dwpRetAddr = Memory::FindPattern<DWORD_PTR>(Source::MISC_THIRDPERSON, "client.dll");

		DWORD_PTR dwpReturnAddr = (DWORD_PTR)_ReturnAddress();
		if (dwpReturnAddr == s_dwpRetAddr)
		{
			return 1;
		}
	}
#endif

	pVmt->Unhook();
	auto iRet = pThis->CAM_IsThirdPerson();
	pVmt->Rehook();

	return iRet;
}