#ifndef __HEADER_STACK__
#define __HEADER_STACK__
#pragma once

template< class _T >
class Stack
{
	struct StackElement
	{
		_T m_This;
		//StackElement
	};
};

template< class _T >
class ReverseStack
{
public:
	struct StackElement
	{
		_T m_This;
		StackElement* m_pNext; // basically a linked list really....
	};

	ReverseStack()
	{
		m_pStackStart = nullptr;
	}

	~ReverseStack()
	{
		while (m_pStackStart)
		{
			auto pNext = m_pStackStart->m_pNext;

			delete m_pStackStart;

			m_pStackStart = pNext;
		}
	}

	void Push(_T v)
	{
		StackElement* pNew = new StackElement;
		pNew->m_This = v;
		pNew->m_pNext = nullptr;

		if (m_pStackStart)
		{
			auto pNode = m_pStackStart;
			while (pNode->m_pNext)
			{
				pNode = pNode->m_pNext;
			}

			pNode->m_pNext = pNew;
		}
		else
		{
			m_pStackStart = pNew;
		}
	}

	void Pop()
	{
		if (m_pStackStart)
		{
			auto tmp = m_pStackStart;

			m_pStackStart = tmp->m_pNext;

			delete tmp;
		}
	}

	bool IsEmpty()
	{
		return (m_pStackStart == nullptr);
	}

	int GetCount()
	{
		if (IsEmpty())
		{
			return 0;
		}

		int nCount = 0;
		auto pStackElement = m_pStackStart;
		while (pStackElement)
		{
			nCount++;

			pStackElement = pStackElement->m_pNext;
		}

		return nCount;
	}

	_T GetTop(_T fallback)
	{
		if (IsEmpty())
		{
			return fallback;
		}

		return m_pStackStart->m_This;
	}
private:
	StackElement* m_pStackStart;
};

#endif