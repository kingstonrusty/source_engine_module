#ifndef __HEADER_INTERPOLATION__
#define __HEADER_INTERPOLATION__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace PositionAdjustment
	{
		class Interpolation;
	}
}

class Features::PositionAdjustment::Interpolation
{
public:
	static void Disable();
	static void Reset();
private:
	static int m_iOldValue;
};

#endif