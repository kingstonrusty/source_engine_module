#ifndef __HEADER_MATRIX3X4__
#define __HEADER_MATRIX3X4__
#pragma once

class matrix3x4
{
public:
	float f[3][4];
	float* operator[](int i) {
		return f[i];
	}

	const float* operator[](int i) const {
		return f[i];
	}
};

#endif