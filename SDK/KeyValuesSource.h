#ifndef __HEADER_KEYVALUES_SOURCE__
#define __HEADER_KEYVALUES_SOURCE__
#pragma once

namespace Source
{
	class KeyValues;
}

class Source::KeyValues
{
public:
	KeyValues(const char* name);

	static KeyValues* AllocKeyValuesMemory();
	void Init(const char* name);
	void SetString(const char* key, const char* value);
	bool LoadFromBuffer(const char* resourcename, const char* buffer);

	static void* operator new(size_t sz);
};

#endif