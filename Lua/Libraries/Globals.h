#ifndef __HEADER_GLOBALS__
#define __HEADER_GLOBALS__
#pragma once

#include "BaseLibrary.h"

class Globals : public BaseLibrary<Globals>
{
public:
	friend class BaseLibrary<Globals>;

	static const char* GetName() { return "Globals"; }
private:
	static int l_FrameCount(lua_State* L);
	static int l_CurTime(lua_State* L);
	static int l_FrameTime(lua_State* L);
	static int l_MaxClients(lua_State* L);
	static int l_TickCount(lua_State* L);
	static int l_IntervalPerTick(lua_State* L);

	static constexpr const struct luaL_Reg m_Register[] =
	{
		{ "FrameCount", l_FrameCount },
		{ "CurTime", l_CurTime },
		{ "FrameTime", l_FrameTime },
		{ "MaxClients", l_MaxClients },
		{ "TickCount", l_TickCount },
		{ "IntervalPerTick", l_IntervalPerTick },
		{ NULL, NULL }
	};
};

#endif