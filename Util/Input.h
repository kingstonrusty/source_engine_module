#ifndef __HEADER_INPUT__
#define __HEADER_INPUT__
#pragma once

namespace Util
{
	class Input;
}

class Util::Input
{
public:
	static void Update();

	static short GetButtonState(unsigned int key);
	static const char* GetButtonName(unsigned int key);
private:
	static KeyValues<unsigned int, SHORT> m_ButtonStates;
};

#endif