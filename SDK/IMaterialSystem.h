#ifndef __HEADER_IMATERIALSYSTEM__
#define __HEADER_IMATERIALSYSTEM__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

#include "KeyValuesSource.h"

namespace Source
{
	class IMaterial;

	class IMatRenderContext;

	class IMaterialSystem;

	namespace Interfaces
	{
		extern IMaterialSystem* MaterialSystem;
	}
}

class Source::IMaterial
{
public:

};

class Source::IMaterialSystem
{
public:
	static IMaterialSystem* GetPtr();

	IMaterial* CreateMaterial(const char* materialname, Source::KeyValues* keyvalues);
	#if defined(GAME_GMOD)
	IMatRenderContext* GetRenderContext();
	#endif
};

#endif