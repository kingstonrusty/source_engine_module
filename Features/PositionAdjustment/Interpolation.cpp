#include "Interpolation.h"

using namespace Features::PositionAdjustment;

int Interpolation::m_iOldValue;

void Interpolation::Disable()
{
	static auto cl_interpolate = Source::Interfaces::Cvar->FindVar("cl_interpolate");

	m_iOldValue = cl_interpolate->GetInt();
	cl_interpolate->SetInt(0);
}

void Interpolation::Reset()
{
	static auto cl_interpolate = Source::Interfaces::Cvar->FindVar("cl_interpolate");

	cl_interpolate->SetInt(m_iOldValue);
}