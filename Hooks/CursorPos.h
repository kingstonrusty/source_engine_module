#ifndef __HEADER_CURSORPOS__
#define __HEADER_CURSORPOS__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace Surface
	{
		extern CVMTHookManager* pVmt;

		void __fastcall GetCursorPos(Source::ISurface* pThis, void*, int& x, int& y);
		void __fastcall SetCursorPos(Source::ISurface* pThis, void*, int x, int y);
	}
}

#endif