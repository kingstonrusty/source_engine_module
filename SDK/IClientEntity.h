#ifndef __HEADER_ICLIENTENTITY__
#define __HEADER_ICLIENTENTITY__
#pragma once

namespace Source
{
	class IClientEntity;
}

class Source::IClientEntity
{
public:
	const Vector& GetAbsOrigin();
	const Vector& GetAbsAngles();
};

#endif