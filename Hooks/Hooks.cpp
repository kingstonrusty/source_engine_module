#include "Hooks.h"

namespace Hooks
{
	namespace BaseEntity
	{
		CDetour* pGetInterpolationAmount;
	}

	namespace Client
	{
#if defined(GAME_GMOD)
		CDetour* pFireBullets;
#endif

		CVMTHookManager* pVmt;
	}

	namespace ClientMode
	{
		CVMTHookManager* pVmt;
	}

	namespace ClientState
	{
		CDetour* pGetClientInterpAmount;
	}

#if defined(GAME_GMOD)
	namespace CScriptedEntity
	{
		CDetour* pCallFunction;
	}
#endif

#if defined(GAME_GMOD)
	namespace Engine
	{
		CVMTHookManager* pVmt;
	}
#endif

	namespace EngineVGui
	{
		CVMTHookManager* pVmt;
	}

	namespace Input
	{
		CVMTHookManager* pVmt;
	}

	namespace MaterialSystem
	{
		CVMTHookManager* pVmt;
	}

#if defined(GAME_GMOD)
	namespace Misc
	{
		CDetour* pViewModelFix;
	}
#endif

#if defined(GAME_GMOD) || defined(GAME_CSS_STEAM)
	namespace NetChannel
	{
#if defined(GAME_GMOD)
		CDetour* pNET_SendPacket;
#endif
#if defined(GAME_CSS_STEAM)
		CVMTHookManager* pVmt;
#endif
	}
#endif

	namespace Prediction
	{
		CVMTHookManager* pVmt;
	}

	namespace RenderView
	{
		CVMTHookManager* pVmt;
	}

	namespace StudioRender
	{
		CVMTHookManager* pVmt;
	}

	namespace Surface
	{
		CVMTHookManager* pVmt;
	}
}

static Hooks::CDetour* pTest;
static bool __fastcall Test(void* pThis, void*, int a2)
{
	return false;
}

/*static Hooks::CVMTHookManager* pTest2Vmt;
bool __fastcall pTest2(Source::IVEngineClient* pThis, void*, Vector& vMin, Vector& vMax)
{
	//collateblabla 55 8B EC 83 EC 30 A1 ? ? ? ? 56

	static DWORD s_dwReturnAddr = Memory::FindPattern<DWORD>("84 C0 0F 85 ?? ?? ?? ?? 8A 44 F7 1D", "client.dll");;

	if ((DWORD)_ReturnAddress() == s_dwReturnAddr)
	{
		return false;
	}

	pTest2Vmt->Unhook();
	auto bRet = GET_VFUNC(bool(__thiscall*)(Source::IVEngineClient*, Vector&, Vector&), pThis, 69)(pThis, vMin, vMax);
	pTest2Vmt->Rehook();

	return bRet;
}*/

bool Hooks::Init()
{
	Client::pVmt = new CVMTHookManager(Source::Interfaces::Client);
#if defined(GAME_L4D2)
	Client::pVmt->HookFunction(Source::IBASECLIENTDLL_CREATEMOVE, Client::CreateMove);
#endif
	Client::pVmt->HookFunction(Source::IBASECLIENTDLL_WRITEUSERCMDDELTATOBUFFER, Client::WriteUsercmdDeltaToBuffer);
	Client::pVmt->HookFunction(Source::IBASECLIENTDLL_FRAMESTAGENOTIFY, Client::FrameStageNotify);
	Client::pVmt->Hook(true);

	ClientMode::pVmt = new CVMTHookManager(Source::Interfaces::ClientMode);
	//ClientMode::pVmt->HookFunction(Source::ICLIENTMODE_OVERRIDEVIEW, ClientMode::OverrideView);
	ClientMode::pVmt->HookFunction(Source::ICLIENTMODE_CREATEMOVE, ClientMode::CreateMove);
	ClientMode::pVmt->Hook(true);

#if defined(GAME_GMOD)
	//CScriptedEntity::pCallFunction = new CDetour(Memory::FindPattern<DWORD_PTR>(Source::CSCRIPTEDENTITY_CALLFUNCTION, "client.dll"), CScriptedEntity::CallFunction);
	//CScriptedEntity::pCallFunction->Hook(true);
#endif

#if defined(GAME_GMOD)
	Engine::pVmt = new CVMTHookManager(Source::Interfaces::Engine);
	Engine::pVmt->HookFunction(Source::IVENGINECLIENT_SETVIEWANGLES, Engine::SetViewAngles);
	Engine::pVmt->Hook(true);
#endif

	/*pTest2Vmt = new CVMTHookManager(Source::Interfaces::Engine);
	pTest2Vmt->HookFunction(69, pTest2);
	pTest2Vmt->Hook(true);*/

	EngineVGui::pVmt = new CVMTHookManager(Source::Interfaces::EngineVGui);
	EngineVGui::pVmt->HookFunction(Source::IVENGINEVGUI_PAINT, EngineVGui::Paint);
	EngineVGui::pVmt->Hook(true);

	Input::pVmt = new CVMTHookManager(Source::Interfaces::Input);
	Input::pVmt->HookFunction(Source::CINPUT_GETUSERCMD, Input::GetUserCmd);
	Input::pVmt->HookFunction(Source::CINPUT_CAM_ISTHIRDPERSON, Input::CAM_IsThirdPerson);
	//Input::pVmt->Hook(true);

#if defined(GAME_GMOD)
	MaterialSystem::pVmt = new CVMTHookManager(Source::Interfaces::MaterialSystem);
	MaterialSystem::pVmt->HookFunction(Source::IMATERIALSYSTEM_GETRENDERCONTEXT, MaterialSystem::GetRenderContext);
	MaterialSystem::pVmt->Hook(true);
#endif

#if defined(GAME_GMOD)
	NetChannel::pNET_SendPacket = new CDetour(Memory::FindPattern<DWORD_PTR>(Source::NET_SENDPACKET_SIGNATURE, "engine.dll"), NetChannel::NET_SendPacket);
	NetChannel::pNET_SendPacket->Hook(true);
#endif

	Prediction::pVmt = new CVMTHookManager(Source::Interfaces::Prediction);
	Prediction::pVmt->HookFunction(Source::IPREDICTION_RUNCOMMAND, Prediction::RunCommand);
	Prediction::pVmt->Hook(true);

	RenderView::pVmt = new CVMTHookManager(Source::Interfaces::RenderView);
	RenderView::pVmt->HookFunction(Source::IVRENDERVIEW_SCENEEND, RenderView::SceneEnd);
	//RenderView::pVmt->Hook(true);

	StudioRender::pVmt = new CVMTHookManager(Source::Interfaces::StudioRender);
	//StudioRender::pVmt->HookFunction(Source::ISTUDIORENDER_DRAWMODEL, StudioRender::DrawModel);
	StudioRender::pVmt->Hook(true);

	Surface::pVmt = new CVMTHookManager(Source::Interfaces::Surface);
	Surface::pVmt->HookFunction(Source::ISURFACE_GETCURSORPOS, Surface::GetCursorPos);
	Surface::pVmt->HookFunction(Source::ISURFACE_SETCURSORPOS, Surface::SetCursorPos);
	Surface::pVmt->Hook(true);

#if defined(GAME_GMOD)
	Client::pFireBullets = new CDetour(Memory::FindPattern<DWORD_PTR>(Source::MISC_FIREBULLETS, "client.dll"), Client::FireBullets);
	Client::pFireBullets->Hook(true);

	Misc::pViewModelFix = new CDetour(Memory::FindPattern<DWORD_PTR>(Source::MISC_VIEWMODELFIX, "client.dll"), Misc::ViewModelFix);
	Misc::pViewModelFix->Hook(true);
#endif

	//pTest = new CDetour(Memory::FindPattern<DWORD_PTR>("55 8B EC 53 56 8B 75 08 8B D9 57 8B 46 08 8B 7E 0C 2B C7 85 C0 7F 0B", "engine.dll"), Test);
	//pTest->Hook(true);
	
	return true;
}

#if defined(GAME_CSS_STEAM)
void Hooks::NetChannel::SetupHook()
{
	if (pVmt != nullptr)
	{
		return;
	}

	if (!Source::Interfaces::Engine->IsInGame())
	{
		return;
	}

	auto pNetChan = Source::Interfaces::Engine->GetNetChannelInfo();
	if (pNetChan == nullptr)
	{
		return;
	}

	pVmt = new CVMTHookManager(pNetChan);
	pVmt->HookFunction(36, Shutdown);
	pVmt->HookFunction(46, SendDatagram);
	pVmt->Hook(true);
}
#endif