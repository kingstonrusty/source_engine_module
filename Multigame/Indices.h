#ifndef __HEADER_INDICES__
#define __HEADER_INDICES__
#pragma once

#define DEFINEINDEX(X, IDX) \
static constexpr int X = IDX

namespace Source
{
#if defined(GAME_GMOD)
	DEFINEINDEX(CINPUT_GETUSERCMD, 8);
	DEFINEINDEX(CINPUT_CAM_ISTHIRDPERSON, 31);

	DEFINEINDEX(IBASECLIENTDLL_GETALLCLASSES, 8);
	DEFINEINDEX(IBASECLIENTDLL_HUDPROCESSINPUT, 10);
	DEFINEINDEX(IBASECLIENTDLL_WRITEUSERCMDDELTATOBUFFER, 23);
	DEFINEINDEX(IBASECLIENTDLL_RENDERVIEW, 27);
	DEFINEINDEX(IBASECLIENTDLL_FRAMESTAGENOTIFY, 35);

	DEFINEINDEX(ICLIENTENTITYLIST_GETCLIENTENTITY, 3);
	DEFINEINDEX(ICLIENTENTITYLIST_GETCLIENTENTITYFROMHANDLE, 4);
	DEFINEINDEX(ICLIENTENTITYLIST_GETHIGHESTENTITYINDEX, 6);

	DEFINEINDEX(ICLIENTMODE_OVERRIDEVIEW, 16);
	DEFINEINDEX(ICLIENTMODE_CREATEMOVE, 21);

	DEFINEINDEX(ICVAR_FINDVAR, 13);

	DEFINEINDEX(IENGINETRACE_TRACERAY, 4);

	DEFINEINDEX(IGAMEMOVEMENT_PROCESSMOVEMENT, 1);

	DEFINEINDEX(IMATERIALSYSTEM_CREATEMATERIAL, 70);
	DEFINEINDEX(IMATERIALSYSTEM_GETRENDERCONTEXT, 98);

	DEFINEINDEX(INETCHANNELINFO_GETLATENCY, 9);
	DEFINEINDEX(INETCHANNELINFO_SENDNETMSG, 40);

	DEFINEINDEX(INETWORKSTRINGTABLE_GETNAME, 1);
	DEFINEINDEX(INETWORKSTRINGTABLE_GETNUMSTRINGS, 3);
	DEFINEINDEX(INETWORKSTRINGTABLE_ADDSTRING, 8);
	DEFINEINDEX(INETWORKSTRINGTABLE_GETSTRING, 9);

	DEFINEINDEX(INETWORKSTRINGTABLECONTAINER_FINDTABLE, 3);
	DEFINEINDEX(INETWORKSTRINGTABLECONTAINER_GETTABLE, 4);
	DEFINEINDEX(INETWORKSTRINGTABLECONTAINER_GETNUMTABLES, 5);

	DEFINEINDEX(IPREDICTION_ISFIRSTTIMEPREDICTED, 15);
	DEFINEINDEX(IPREDICTION_RUNCOMMAND, 17);
	DEFINEINDEX(IPREDICTION_SETUPMOVE, 18);
	DEFINEINDEX(IPREDICTION_FINISHMOVE, 19);

	DEFINEINDEX(ISTUDIORENDER_SETCOLORMODULATION, 27);
	DEFINEINDEX(ISTUDIORENDER_SETALPHAMODULATION, 28);
	DEFINEINDEX(ISTUDIORENDER_DRAWMODEL, 29);
	DEFINEINDEX(ISTUDIORENDER_FORCEDMATERIALOVERRIDE, 33);

	DEFINEINDEX(ISURFACE_DRAWSETCOLOR, 10);
	DEFINEINDEX(ISURFACE_DRAWFILLEDRECT, 12);
	DEFINEINDEX(ISURFACE_DRAWOUTLINEDRECT, 14);
	DEFINEINDEX(ISURFACE_DRAWLINE, 15);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTFONT, 17);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTCOLOR, 18);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTPOS, 20);
	DEFINEINDEX(ISURFACE_DRAWPRINTTEXT, 22);
	DEFINEINDEX(ISURFACE_GETSCREENSIZE, 38);
	DEFINEINDEX(ISURFACE_CREATEFONT, 66);
	DEFINEINDEX(ISURFACE_SETFONTGLYPHSET, 67);
	DEFINEINDEX(ISURFACE_GETTEXTSIZE, 76);
	DEFINEINDEX(ISURFACE_GETCURSORPOS, 97);
	DEFINEINDEX(ISURFACE_SETCURSORPOS, 98);
	DEFINEINDEX(ISURFACE_RESETFONTCACHES, 137);

	DEFINEINDEX(IVDEBUGOVERLAY_SCREENPOSITION, 10);

	DEFINEINDEX(IVENGINECLIENT_GETLOCALPLAYER, 12); 
	DEFINEINDEX(IVENGINECLIENT_GETVIEWANGLES, 19);
	DEFINEINDEX(IVENGINECLIENT_SETVIEWANGLES, 20);
	DEFINEINDEX(IVENGINECLIENT_ISINGAME, 26);
	DEFINEINDEX(IVENGINECLIENT_GETNETCHANNELINFO, 72);
	DEFINEINDEX(IVENGINECLIENT_ISTAKINGSCREENSHOT, 85);
	DEFINEINDEX(IVENGINECLIENT_EXECUTECLIENTCMD, 102);

	DEFINEINDEX(IVENGINEVGUI_PAINT, 13);

	DEFINEINDEX(IVRENDERVIEW_SETCOLORMODULATION, 6);
	DEFINEINDEX(IVRENDERVIEW_SCENEEND, 9);
	DEFINEINDEX(IVRENDERVIEW_PUSH3DVIEW, 38);
	DEFINEINDEX(IVRENDERVIEW_POPVIEW, 40);

	DEFINEINDEX(IVIEW_GETFRUSTUM, 10);
	DEFINEINDEX(IVIEW_GETPLAYERVIEWSETUP, 12);

	DEFINEINDEX(IVMODELINFO_GETMODELINDEX, 3);
	DEFINEINDEX(IVMODELINFO_GETMODELNAME, 3);
	DEFINEINDEX(IVMODELINFO_GETSTUDIOMODEL, 28);
	DEFINEINDEX(IVMODELINFO_FINDORLOADMODEL, 39);

	DEFINEINDEX(IVMODELRENDER_FORCEDMATERIALOVERRIDE, 1);

	DEFINEINDEX(ICLIENTENTITY_GETABSORIGIN, 9);
	DEFINEINDEX(ICLIENTENTITY_GETABSANGLES, 10);

	DEFINEINDEX(ICLIENTNETWORKABLE_ISDORMANT, 8);
	DEFINEINDEX(ICLIENTNETWORKABLE_ENTINDEX, 9);

	DEFINEINDEX(ICLIENTRENDERABLE_GETMODEL, 9);
	DEFINEINDEX(ICLIENTRENDERABLE_DRAWMODEL, 10);
	DEFINEINDEX(ICLIENTRENDERABLE_SETUPBONES, 16);

	DEFINEINDEX(CBASEENTITY_ISPLAYER, 130);
	DEFINEINDEX(CBASEENTITY_ISBASECOMBATCHARACTER, 131);
	DEFINEINDEX(CBASEENTITY_ISNPC, 133);
	DEFINEINDEX(CBASEENTITY_ISNEXTBOT, 134);
	DEFINEINDEX(CBASEENTITY_ISBASECOMBATWEAPON, 136);
	
	DEFINEINDEX(IANIMSTATE_CLEARANIMATIONSTATE, 1);
	DEFINEINDEX(IANIMSTATE_CALCMAINACTIVITY, 3);
	DEFINEINDEX(IANIMSTATE_UPDATE, 4);
	DEFINEINDEX(IANIMSTATE_DEBUGSHOWANIMSTATE, 11);
	DEFINEINDEX(IANIMSTATE_COMPUTESEQUENCES, 31);
	DEFINEINDEX(IANIMSTATE_SHOULDUPDATEANIMSTATE, 32);

	DEFINEINDEX(ILUASHARED_GETLUAINTERFACE, 6);
#elif defined(GAME_CSS_STEAM)
	DEFINEINDEX(CINPUT_GETUSERCMD, 8);
	DEFINEINDEX(CINPUT_CAM_ISTHIRDPERSON, 31);

	DEFINEINDEX(IBASECLIENTDLL_GETALLCLASSES, 8);
	DEFINEINDEX(IBASECLIENTDLL_HUDPROCESSINPUT, 10);
	DEFINEINDEX(IBASECLIENTDLL_WRITEUSERCMDDELTATOBUFFER, 23);
	DEFINEINDEX(IBASECLIENTDLL_FRAMESTAGENOTIFY, 35);

	DEFINEINDEX(ICLIENTENTITYLIST_GETCLIENTENTITY, 3);
	DEFINEINDEX(ICLIENTENTITYLIST_GETCLIENTENTITYFROMHANDLE, 4);
	DEFINEINDEX(ICLIENTENTITYLIST_GETHIGHESTENTITYINDEX, 8);

	DEFINEINDEX(ICLIENTMODE_OVERRIDEVIEW, 16);
	DEFINEINDEX(ICLIENTMODE_CREATEMOVE, 21);

	DEFINEINDEX(ICVAR_FINDVAR, 13);

	DEFINEINDEX(IENGINETRACE_TRACERAY, 4);

	DEFINEINDEX(IGAMEMOVEMENT_PROCESSMOVEMENT, 1);

	DEFINEINDEX(IMATERIALSYSTEM_CREATEMATERIAL, 70);

	DEFINEINDEX(INETCHANNELINFO_GETLATENCY, 9);
	DEFINEINDEX(INETCHANNELINFO_SENDNETMSG, 40);

	DEFINEINDEX(IPREDICTION_RUNCOMMAND, 17);
	DEFINEINDEX(IPREDICTION_SETUPMOVE, 18);
	DEFINEINDEX(IPREDICTION_FINISHMOVE, 19);
	
	DEFINEINDEX(ISTUDIORENDER_SETCOLORMODULATION, 27);
	DEFINEINDEX(ISTUDIORENDER_SETALPHAMODULATION, 28);
	DEFINEINDEX(ISTUDIORENDER_DRAWMODEL, 29);
	DEFINEINDEX(ISTUDIORENDER_FORCEDMATERIALOVERRIDE, 33);

	DEFINEINDEX(ISURFACE_DRAWSETCOLOR, 10);
	DEFINEINDEX(ISURFACE_DRAWFILLEDRECT, 12);
	DEFINEINDEX(ISURFACE_DRAWOUTLINEDRECT, 14);
	DEFINEINDEX(ISURFACE_DRAWLINE, 15);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTFONT, 17);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTCOLOR, 18);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTPOS, 20);
	DEFINEINDEX(ISURFACE_DRAWPRINTTEXT, 22);
	DEFINEINDEX(ISURFACE_GETSCREENSIZE, 38);
	DEFINEINDEX(ISURFACE_CREATEFONT, 66);
	DEFINEINDEX(ISURFACE_SETFONTGLYPHSET, 67);
	DEFINEINDEX(ISURFACE_GETTEXTSIZE, 75);
	DEFINEINDEX(ISURFACE_GETCURSORPOS, 96);
	DEFINEINDEX(ISURFACE_SETCURSORPOS, 97);
	DEFINEINDEX(ISURFACE_RESETFONTCACHES, 125);

	DEFINEINDEX(IVDEBUGOVERLAY_SCREENPOSITION, 10);

	DEFINEINDEX(IVENGINECLIENT_GETLOCALPLAYER, 12);
	DEFINEINDEX(IVENGINECLIENT_GETVIEWANGLES, 19);
	DEFINEINDEX(IVENGINECLIENT_SETVIEWANGLES, 20);
	DEFINEINDEX(IVENGINECLIENT_ISINGAME, 26);
	DEFINEINDEX(IVENGINECLIENT_ISCONNECTED, 27);
	DEFINEINDEX(IVENGINECLIENT_GETNETCHANNELINFO, 72);
	DEFINEINDEX(IVENGINECLIENT_EXECUTECLIENTCMD, 102);

	DEFINEINDEX(IVENGINEVGUI_PAINT, 13);

	DEFINEINDEX(IVMODELINFO_GETMODELNAME, 3);
	DEFINEINDEX(IVMODELINFO_GETSTUDIOMODEL, 28);

	DEFINEINDEX(IVRENDERVIEW_SCENEEND, 9);

	DEFINEINDEX(ICLIENTENTITY_GETABSORIGIN, 9);
	DEFINEINDEX(ICLIENTENTITY_GETABSANGLES, 10);

	DEFINEINDEX(ICLIENTNETWORKABLE_ISDORMANT, 8);
	DEFINEINDEX(ICLIENTNETWORKABLE_ENTINDEX, 9);

	DEFINEINDEX(ICLIENTRENDERABLE_GETMODEL, 9);
	DEFINEINDEX(ICLIENTRENDERABLE_DRAWMODEL, 10);
	DEFINEINDEX(ICLIENTRENDERABLE_SETUPBONES, 16);

	DEFINEINDEX(CBASEENTITY_ISPLAYER, 131);
	DEFINEINDEX(CBASEENTITY_ISBASECOMBATCHARACTER, 132);
	DEFINEINDEX(CBASEENTITY_ISNPC, 134);
	DEFINEINDEX(CBASEENTITY_ISNEXTBOT, 0);
	DEFINEINDEX(CBASEENTITY_ISBASECOMBATWEAPON, 137);

	DEFINEINDEX(CBASEWEAPON_GETSPREAD, 377);
	DEFINEINDEX(CBASEWEAPON_GETINACCURACY, 376);
	DEFINEINDEX(CBASEWEAPON_UPDATEINACCURACY, 378);

	DEFINEINDEX(IANIMSTATE_CLEARANIMATIONSTATE, 0);
	DEFINEINDEX(IANIMSTATE_CALCMAINACTIVITY, 0);
	DEFINEINDEX(IANIMSTATE_UPDATE, 0);
	DEFINEINDEX(IANIMSTATE_DEBUGSHOWANIMSTATE, 0);
	DEFINEINDEX(IANIMSTATE_COMPUTESEQUENCES, 0);
	DEFINEINDEX(IANIMSTATE_SHOULDUPDATEANIMSTATE, 0);
#elif defined(GAME_L4D2)
	DEFINEINDEX(CINPUT_GETUSERCMD, 8);
	DEFINEINDEX(CINPUT_CAM_ISTHIRDPERSON, 31);

	DEFINEINDEX(IBASECLIENTDLL_GETALLCLASSES, 7);
	DEFINEINDEX(IBASECLIENTDLL_HUDPROCESSINPUT, 9);
	DEFINEINDEX(IBASECLIENTDLL_CREATEMOVE, 20);
	DEFINEINDEX(IBASECLIENTDLL_WRITEUSERCMDDELTATOBUFFER, 22);
	DEFINEINDEX(IBASECLIENTDLL_FRAMESTAGENOTIFY, 34);

	DEFINEINDEX(ICLIENTENTITYLIST_GETCLIENTENTITY, 3);
	DEFINEINDEX(ICLIENTENTITYLIST_GETCLIENTENTITYFROMHANDLE, 4);
	DEFINEINDEX(ICLIENTENTITYLIST_GETHIGHESTENTITYINDEX, 8);

	DEFINEINDEX(ICLIENTMODE_OVERRIDEVIEW, 19);
	DEFINEINDEX(ICLIENTMODE_CREATEMOVE, 27);

	DEFINEINDEX(ICVAR_FINDVAR, 12);

	DEFINEINDEX(IENGINETRACE_TRACERAY, 5);

	DEFINEINDEX(IGAMEMOVEMENT_PROCESSMOVEMENT, 1);

	DEFINEINDEX(IMATERIALSYSTEM_CREATEMATERIAL, 70);

	DEFINEINDEX(INETCHANNELINFO_GETLATENCY, 9);
	DEFINEINDEX(INETCHANNELINFO_SENDNETMSG, 41);

	DEFINEINDEX(INETWORKSTRINGTABLE_ADDSTRING, 8);

	DEFINEINDEX(INETWORKSTRINGTABLECONTAINER_FINDTABLE, 3);

	DEFINEINDEX(IPREDICTION_RUNCOMMAND, 18);
	DEFINEINDEX(IPREDICTION_SETUPMOVE, 19);
	DEFINEINDEX(IPREDICTION_FINISHMOVE, 20);

	DEFINEINDEX(ISTUDIORENDER_SETCOLORMODULATION, 0);
	DEFINEINDEX(ISTUDIORENDER_SETALPHAMODULATION, 0);
	DEFINEINDEX(ISTUDIORENDER_DRAWMODEL, 0);
	DEFINEINDEX(ISTUDIORENDER_FORCEDMATERIALOVERRIDE, 0);

	DEFINEINDEX(ISURFACE_DRAWSETCOLOR, 10);
	DEFINEINDEX(ISURFACE_DRAWFILLEDRECT, 12);
	DEFINEINDEX(ISURFACE_DRAWOUTLINEDRECT, 14);
	DEFINEINDEX(ISURFACE_DRAWLINE, 15);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTFONT, 17);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTCOLOR, 18);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTPOS, 20);
	DEFINEINDEX(ISURFACE_DRAWPRINTTEXT, 22);
	DEFINEINDEX(ISURFACE_GETSCREENSIZE, 35);
	DEFINEINDEX(ISURFACE_CREATEFONT, 63);
	DEFINEINDEX(ISURFACE_SETFONTGLYPHSET, 64);
	DEFINEINDEX(ISURFACE_GETTEXTSIZE, 72);
	DEFINEINDEX(ISURFACE_GETCURSORPOS, 93);
	DEFINEINDEX(ISURFACE_SETCURSORPOS, 94);
	DEFINEINDEX(ISURFACE_RESETFONTCACHES, 0);

	DEFINEINDEX(IVDEBUGOVERLAY_SCREENPOSITION, 12);

	DEFINEINDEX(IVENGINECLIENT_GETLOCALPLAYER, 12);
	DEFINEINDEX(IVENGINECLIENT_GETVIEWANGLES, 19);
	DEFINEINDEX(IVENGINECLIENT_SETVIEWANGLES, 20);
	DEFINEINDEX(IVENGINECLIENT_ISINGAME, 26);
	DEFINEINDEX(IVENGINECLIENT_ISCONNECTED, 27);
	DEFINEINDEX(IVENGINECLIENT_GETNETCHANNELINFO, 74);
	DEFINEINDEX(IVENGINECLIENT_EXECUTECLIENTCMD, 106);

	DEFINEINDEX(IVENGINEVGUI_PAINT, 14);

	DEFINEINDEX(IVMODELINFO_GETMODELINDEX, 2);
	DEFINEINDEX(IVMODELINFO_GETMODELNAME, 3);
	DEFINEINDEX(IVMODELINFO_GETSTUDIOMODEL, 30);
	DEFINEINDEX(IVMODELINFO_FINDORLOADMODEL, 42);

	DEFINEINDEX(IVMODELRENDER_FORCEDMATERIALOVERRIDE, 1);

	DEFINEINDEX(IVRENDERVIEW_SETCOLORMODULATION, 6);
	DEFINEINDEX(IVRENDERVIEW_SCENEEND, 9);

	DEFINEINDEX(IMOVEHELPER_SETHOST, 1);

	DEFINEINDEX(ICLIENTENTITY_GETABSORIGIN, 11);
	DEFINEINDEX(ICLIENTENTITY_GETABSANGLES, 12);

	DEFINEINDEX(ICLIENTNETWORKABLE_ISDORMANT, 7);
	DEFINEINDEX(ICLIENTNETWORKABLE_ENTINDEX, 8);

	DEFINEINDEX(ICLIENTRENDERABLE_GETMODEL, 8);
	DEFINEINDEX(ICLIENTRENDERABLE_DRAWMODEL, 9);
	DEFINEINDEX(ICLIENTRENDERABLE_SETUPBONES, 13);

	DEFINEINDEX(CBASEENTITY_ISPLAYER, 143);
	DEFINEINDEX(CBASEENTITY_ISBASECOMBATCHARACTER, 144);
	DEFINEINDEX(CBASEENTITY_ISNPC, 146);
	DEFINEINDEX(CBASEENTITY_ISNEXTBOT, 147);
	DEFINEINDEX(CBASEENTITY_ISBASECOMBATWEAPON, 149);

	DEFINEINDEX(IANIMSTATE_CLEARANIMATIONSTATE, 0);
	DEFINEINDEX(IANIMSTATE_CALCMAINACTIVITY, 0);
	DEFINEINDEX(IANIMSTATE_UPDATE, 0);
	DEFINEINDEX(IANIMSTATE_DEBUGSHOWANIMSTATE, 0);
	DEFINEINDEX(IANIMSTATE_COMPUTESEQUENCES, 0);
	DEFINEINDEX(IANIMSTATE_SHOULDUPDATEANIMSTATE, 0);
#elif defined(GAME_BB2)
	DEFINEINDEX(CINPUT_GETUSERCMD, 8);
	DEFINEINDEX(CINPUT_CAM_ISTHIRDPERSON, 31);

	DEFINEINDEX(IBASECLIENTDLL_GETALLCLASSES, 8);
	DEFINEINDEX(IBASECLIENTDLL_HUDPROCESSINPUT, 10);
	DEFINEINDEX(IBASECLIENTDLL_WRITEUSERCMDDELTATOBUFFER, 23);
	DEFINEINDEX(IBASECLIENTDLL_FRAMESTAGENOTIFY, 35);

	DEFINEINDEX(ICLIENTENTITYLIST_GETCLIENTENTITY, 3);
	DEFINEINDEX(ICLIENTENTITYLIST_GETCLIENTENTITYFROMHANDLE, 4);
	DEFINEINDEX(ICLIENTENTITYLIST_GETHIGHESTENTITYINDEX, 8);

	DEFINEINDEX(ICLIENTMODE_OVERRIDEVIEW, 16);
	DEFINEINDEX(ICLIENTMODE_CREATEMOVE, 21);

	DEFINEINDEX(ICVAR_FINDVAR, 13);

	DEFINEINDEX(IENGINETRACE_TRACERAY, 4);

	DEFINEINDEX(IGAMEMOVEMENT_PROCESSMOVEMENT, 1);

	DEFINEINDEX(IMATERIALSYSTEM_CREATEMATERIAL, 0);

	DEFINEINDEX(INETCHANNELINFO_GETLATENCY, 9);
	DEFINEINDEX(INETCHANNELINFO_SENDNETMSG, 40);

	DEFINEINDEX(INETWORKSTRINGTABLE_ADDSTRING, 8);

	DEFINEINDEX(INETWORKSTRINGTABLECONTAINER_FINDTABLE, 3);

	DEFINEINDEX(IPREDICTION_RUNCOMMAND, 17);
	DEFINEINDEX(IPREDICTION_SETUPMOVE, 18);
	DEFINEINDEX(IPREDICTION_FINISHMOVE, 19);

	DEFINEINDEX(ISTUDIORENDER_SETCOLORMODULATION, 0);
	DEFINEINDEX(ISTUDIORENDER_SETALPHAMODULATION, 0);
	DEFINEINDEX(ISTUDIORENDER_DRAWMODEL, 0);
	DEFINEINDEX(ISTUDIORENDER_FORCEDMATERIALOVERRIDE, 0);

	DEFINEINDEX(ISURFACE_DRAWSETCOLOR, 10);
	DEFINEINDEX(ISURFACE_DRAWFILLEDRECT, 12);
	DEFINEINDEX(ISURFACE_DRAWOUTLINEDRECT, 14);
	DEFINEINDEX(ISURFACE_DRAWLINE, 15);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTFONT, 17);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTCOLOR, 18);
	DEFINEINDEX(ISURFACE_DRAWSETTEXTPOS, 20);
	DEFINEINDEX(ISURFACE_DRAWPRINTTEXT, 22);
	DEFINEINDEX(ISURFACE_GETSCREENSIZE, 38);
	DEFINEINDEX(ISURFACE_CREATEFONT, 66);
	DEFINEINDEX(ISURFACE_SETFONTGLYPHSET, 67);
	DEFINEINDEX(ISURFACE_GETTEXTSIZE, 75);
	DEFINEINDEX(ISURFACE_GETCURSORPOS, 96);
	DEFINEINDEX(ISURFACE_SETCURSORPOS, 97);
	DEFINEINDEX(ISURFACE_RESETFONTCACHES, 125);

	DEFINEINDEX(IVDEBUGOVERLAY_SCREENPOSITION, 10);

	DEFINEINDEX(IVENGINECLIENT_GETLOCALPLAYER, 12);
	DEFINEINDEX(IVENGINECLIENT_GETVIEWANGLES, 19);
	DEFINEINDEX(IVENGINECLIENT_SETVIEWANGLES, 20);
	DEFINEINDEX(IVENGINECLIENT_ISINGAME, 26);
	DEFINEINDEX(IVENGINECLIENT_ISCONNECTED, 27);
	DEFINEINDEX(IVENGINECLIENT_GETNETCHANNELINFO, 72);
	DEFINEINDEX(IVENGINECLIENT_EXECUTECLIENTCMD, 102);

	DEFINEINDEX(IVENGINEVGUI_PAINT, 13);

	DEFINEINDEX(IVMODELINFO_GETMODELINDEX, 2);
	DEFINEINDEX(IVMODELINFO_GETMODELNAME, 3);
	DEFINEINDEX(IVMODELINFO_GETSTUDIOMODEL, 30);
	DEFINEINDEX(IVMODELINFO_FINDORLOADMODEL, 42);

	DEFINEINDEX(IVMODELRENDER_FORCEDMATERIALOVERRIDE, 1);

	DEFINEINDEX(IVRENDERVIEW_SETCOLORMODULATION, 6);
	DEFINEINDEX(IVRENDERVIEW_SCENEEND, 9);

	DEFINEINDEX(ICLIENTENTITY_GETABSORIGIN, 9);
	DEFINEINDEX(ICLIENTENTITY_GETABSANGLES, 10);

	DEFINEINDEX(ICLIENTNETWORKABLE_ISDORMANT, 8);
	DEFINEINDEX(ICLIENTNETWORKABLE_ENTINDEX, 9);

	DEFINEINDEX(ICLIENTRENDERABLE_GETMODEL, 9);
	DEFINEINDEX(ICLIENTRENDERABLE_DRAWMODEL, 10);
	DEFINEINDEX(ICLIENTRENDERABLE_SETUPBONES, 16);

	DEFINEINDEX(CBASEENTITY_ISPLAYER, 133);
	DEFINEINDEX(CBASEENTITY_ISBASECOMBATCHARACTER, 134);
	DEFINEINDEX(CBASEENTITY_ISNPC, 136);
	DEFINEINDEX(CBASEENTITY_ISBASECOMBATWEAPON, 139);

	DEFINEINDEX(IANIMSTATE_CLEARANIMATIONSTATE, 0);
	DEFINEINDEX(IANIMSTATE_CALCMAINACTIVITY, 0);
	DEFINEINDEX(IANIMSTATE_UPDATE, 0);
	DEFINEINDEX(IANIMSTATE_DEBUGSHOWANIMSTATE, 0);
	DEFINEINDEX(IANIMSTATE_COMPUTESEQUENCES, 0);
	DEFINEINDEX(IANIMSTATE_SHOULDUPDATEANIMSTATE, 0);
#endif
}

#endif