#include "NoSpread.h"

#include "Util/CRC32.h"
#include "Util/MD5.h"

using namespace Features::Accuracy;

#if defined(GAME_GMOD)
KeyValues<unsigned int, NoSpread::SSpread> NoSpread::m_SpreadVectors;
#endif

void NoSpread::Run(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd)
{
	if (pWeapon == nullptr)
	{
		return;
	}

#if defined(GAME_L4D2)
	static float(__cdecl* pSharedRandomFloat)(const char*, float, float, int) = nullptr;
	//static int* s_pSeed = nullptr;

	if (pSharedRandomFloat == nullptr)
	{
		DWORD_PTR dwpCall = Memory::FindPattern<DWORD_PTR>(Source::MISC_SHAREDRANDOMFLOAT, "client.dll");

		pSharedRandomFloat = (float(__cdecl*)(const char*, float, float, int))(dwpCall + *(DWORD_PTR*)dwpCall + 0x4);
	}

	float flSpread = pWeapon->GetSpread();

	//*s_pSeed = Source::MD5_PseudoRandom(cmd->command_number()) & 0x7FFFFFFF;
	cmd->viewangles().x -= pSharedRandomFloat("CTerrorGun::FireBullet HorizSpread", -flSpread, flSpread, 0);
	cmd->viewangles().y -= pSharedRandomFloat("CTerrorGun::FireBullet VertSpread", -flSpread, flSpread, 0);
	cmd->viewangles() -= pLocal->GetPunchAngle();
	AngleNormalize(cmd->viewangles());
	return;
#endif

#if defined(GAME_GMOD)
	if (strncmp(pWeapon->GetClassName(), "fas2", 4) == 0)
	{
		FAS2NoSpread(pLocal, pWeapon, cmd);
		return;
	}
	
	if (strncmp(pWeapon->GetClassName(), "cw_", 3) == 0)
	{
		CW2NoSpread(pLocal, pWeapon, cmd);
		return;
	}
#endif

#if defined(GAME_GMOD)
	Source::RandomSeed((Source::MD5_PseudoRandom(cmd->command_number()) & 0x7FFFFFFF) & 0xFF);
#else
	Source::RandomSeed(((Source::MD5_PseudoRandom(cmd->command_number()) & 0x7FFFFFFF) & 0xFF) + 1);
#endif

	Vector vSpread;

#if defined(GAME_GMOD)
	SSpread& Spread = m_SpreadVectors[Util::CRC32::HashString(pWeapon->GetClassName())];
	if (Spread.m_vecSpread.x == 0.f && Spread.m_vecSpread.y == 0.f)
	{
		return;
	}

	vSpread = Spread.m_vecSpread;
	Source::RandomSeed(cmd->random_seed() & 0xFF);

	float x, y;
	do
	{
		x = (Source::RandomFloat(-1.0, 1.0f) * 0.5f) + (Source::RandomFloat(-1.0, 1.0f) * 0.5f);
		y = (Source::RandomFloat(-1.0, 1.0f) * 0.5f) + (Source::RandomFloat(-1.0, 1.0f) * 0.5f);
	} while ((x * x + y * y) > 1.0f);

	vSpread.x *= x;
	vSpread.y *= y;

	int iShots = Spread.m_iShots;
#elif defined(GAME_CSS_STEAM)
	float rand0 = Source::RandomFloat(0.f, M_PI_F * 2.f);
	float rand1 = Source::RandomFloat(0.f, pWeapon->GetInaccuracy());
	float rand2 = Source::RandomFloat(0.f, M_PI_F * 2.f);
	float rand3 = Source::RandomFloat(0.f, pWeapon->GetSpread());

	vSpread = Vector(
		cosf(rand0) * rand1 + cosf(rand2) * rand3,
		sinf(rand0) * rand1 + sinf(rand2) * rand3,
		0.f
	);
#endif

	vSpread = vSpread * -1;

	Vector forward, right, up, dir, out, modifier, serverspread;
	AngleVectors(cmd->viewangles(), &forward, &right, &up);

	dir = forward + (right * vSpread.x) + (up * vSpread.y);
	VectorNormalize(dir);
	VectorAngles(dir, out);
	AngleNormalize(out);
	
	for (int i = 0; i < 32; i++)
	{
		AngleVectors(out, &forward, &right, &up);
		serverspread = forward + (right * (-vSpread.x)) + (up * (-vSpread.y));
		VectorNormalize(serverspread);
		VectorAngles(serverspread, modifier);
		AngleNormalize(modifier);
		modifier = cmd->viewangles() - modifier;

		if (sqrt((modifier.x * modifier.x) + (modifier.y * modifier.y)) == 0.f)
		{
			break;
		}

		out += modifier;
	}

#if defined(GAME_CSS_STEAM)
	//out -= pLocal->GetPunchAngle() * 2.f;
#endif

	AngleNormalize(out);
	cmd->viewangles() = out;
}

#if defined(GAME_GMOD)

void NoSpread::RecordSpread(Source::CBasePlayer* pLocal, Source::FireBulletsInfo_t* info)
{
	auto pWeapon = (Source::CBaseWeapon*)Source::Interfaces::EntityList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
	if (pWeapon == nullptr)
	{
		return;
	}

	const auto crc = Util::CRC32::HashString(pWeapon->GetClassName());
	m_SpreadVectors[crc].m_vecSpread = info->m_vecSpread();
	m_SpreadVectors[crc].m_iShots = info->m_iShots();
}

static Vector angFixAngles;

static void FixMovement(Vector& rangNewAngles, float& rflForwardMove, float& rflSideMove) {
	if (rangNewAngles == angFixAngles)
	{
		return;
	}

	Vector backupNew = rangNewAngles;
	AngleNormalize(angFixAngles);
	AngleNormalize(rangNewAngles);

	Vector vecMove = Vector(rflForwardMove, rflSideMove, 0.f);
	float flSpeed = vecMove.Length2D();
	Vector angMove;
	VectorAngles(vecMove, angMove);
	float flYaw = Deg2Rad((rangNewAngles.y - angFixAngles.y) + angMove.y);
	rflSideMove = sinf(flYaw);
	rflForwardMove = cosf(flYaw);
	rflSideMove *= flSpeed;
	rflForwardMove *= flSpeed;

	if (rangNewAngles.x >= 90.f || rangNewAngles.x <= -90.f) rflForwardMove *= -1.f;

	rangNewAngles = backupNew;
}

void NoSpread::FAS2NoSpread(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd)
{
	auto pMenuState = Source::Interfaces::Lua->GetLuaInterface(Source::GarrysMod::MENU_STATE);
	auto pClientState = Source::Interfaces::Lua->GetLuaInterface(Source::GarrysMod::CLIENT_STATE);

	if (pMenuState == nullptr || pClientState == nullptr)
	{
		return;
	}

	double CurCone = 0.09 + pClientState->GetNumberFromEntity(pWeapon, "MaxSpreadInc");

	pMenuState->PushSpecial(Source::GarrysMod::SPECIAL_GLOB);
	pMenuState->GetField(-1, "math");
	pMenuState->GetField(-1, "randomseed");
	
	{
		pClientState->PushSpecial(Source::GarrysMod::SPECIAL_GLOB);
		pClientState->GetField(-1, "CurTime");

		pClientState->Call(0, 1);
		pMenuState->PushNumber(pClientState->GetNumber(-1));

		pClientState->Pop(2);
	}

	pMenuState->Call(1, 0);
	pMenuState->Pop(2);

	Vector vSpread;

	for (int i = 0; i < 2; i++)
	{
		pMenuState->PushSpecial(Source::GarrysMod::SPECIAL_GLOB);
		pMenuState->GetField(-1, "math");
		pMenuState->GetField(-1, "Rand");
		pMenuState->PushNumber(-CurCone);
		pMenuState->PushNumber(CurCone);
		pMenuState->Call(2, 1);
		vSpread[i] = (float)(pMenuState->GetNumber(-1) * -25.0);
		pMenuState->Pop(3);
	}

	cmd->viewangles() += vSpread;
	cmd->viewangles() -= pLocal->GetPunchAngle();

	{
		Source::CUserCmd* prevcmd = Source::Interfaces::Input->GetUserCmd(cmd->command_number() - 1);
		angFixAngles = prevcmd->viewangles();
		prevcmd->viewangles().y = cmd->viewangles().y - 180.f;
		prevcmd->viewangles().x = -cmd->viewangles().x;
		AngleNormalize(prevcmd->viewangles());
		FixMovement(prevcmd->viewangles(), prevcmd->forwardmove(), prevcmd->sidemove());

		Source::Interfaces::Input->GetVerifiedUserCmd(prevcmd->command_number())->SetUserCmd(prevcmd);
	}
}

void NoSpread::CW2NoSpread(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd)
{
	auto pMenuState = Source::Interfaces::Lua->GetLuaInterface(Source::GarrysMod::MENU_STATE);
	auto pClientState = Source::Interfaces::Lua->GetLuaInterface(Source::GarrysMod::CLIENT_STATE);

	if (pMenuState == nullptr || pClientState == nullptr)
	{
		return;
	}

	double CurCone = 0.09 + pClientState->GetNumberFromEntity(pWeapon, "MaxSpreadInc");
	if (cmd->buttons() & (1 << 2))
	{
		CurCone *= 0.85;
	}

	pMenuState->PushSpecial(Source::GarrysMod::SPECIAL_GLOB);
	pMenuState->GetField(-1, "math");
	pMenuState->GetField(-1, "randomseed");
	pMenuState->PushNumber((double)cmd->command_number());
	pMenuState->Call(1, 0);
	pMenuState->Pop(2);

	Vector vSpread;

	for (int i = 0; i < 2; i++)
	{
		pMenuState->PushSpecial(Source::GarrysMod::SPECIAL_GLOB);
		pMenuState->GetField(-1, "math");
		pMenuState->GetField(-1, "Rand");
		pMenuState->PushNumber(-CurCone);
		pMenuState->PushNumber(CurCone);
		pMenuState->Call(2, 1);
		vSpread[i] = (float)(pMenuState->GetNumber(-1) * -25.0);
		pMenuState->Pop(3);
	}

	cmd->viewangles() += vSpread;
	cmd->viewangles() -= pLocal->GetPunchAngle();

	Source::CUserCmd* prevcmd = cmd;
	for (int i = 0; i < 3; i++)
	{
		Source::CUserCmd* cmd = Source::Interfaces::Input->GetUserCmd(prevcmd->command_number() - 1);
		angFixAngles = cmd->viewangles();
		cmd->viewangles().y = prevcmd->viewangles().y - 180.f;
		cmd->viewangles().x = -prevcmd->viewangles().x;
		AngleNormalize(cmd->viewangles());
		FixMovement(cmd->viewangles(), cmd->forwardmove(), cmd->sidemove());

		Source::Interfaces::Input->GetVerifiedUserCmd(cmd->command_number())->SetUserCmd(cmd);

		prevcmd = cmd;
	}
}

#endif