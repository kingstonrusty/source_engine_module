#ifndef __HEADER_INTERNALS__
#define __HEADER_INTERNALS__
#pragma once

#include "BaseLibrary.h"

class Internals : public BaseLibrary<Internals>
{
public:
	friend class BaseLibrary<Internals>;

	static const char* GetName() { return "Internals"; }

	//////////////////////////////////////////////////////////////
private:
	static int l_include(lua_State* L);
	static int l_getgame(lua_State* L);

	static constexpr const struct luaL_Reg m_Register[] =
	{
		{ "include", l_include },
		{ "GetGame", l_getgame },
		{ NULL, NULL }
	};
};

#endif