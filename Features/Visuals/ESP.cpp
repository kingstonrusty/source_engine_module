#include "ESP.h"

#include "Features/Visuals/Menu.h"

using namespace Features::Visuals;

void ESP::Run()
{
	FINDVAR(visuals, "Visuals");
	FINDVAR(visuals_esp, "Visuals", "ESP");
	FINDVAR(visuals_filter_players, "Visuals", "Filter", "Players");
	FINDVAR(visuals_filter_dormant, "Visuals", "Filter", "Dormant");

	if (!visuals->GetBool())
	{
		return;
	}

	if (!visuals_esp->GetBool())
	{
		return;
	}

	for (int i = Source::Interfaces::Globals->maxClients() + 1; i <= Source::Interfaces::EntityList->GetHighestEntityIndex(); i++)
	{
		Source::CBaseEntity* pEntity = (Source::CBaseEntity*)Source::Interfaces::EntityList->GetClientEntity(i);
		if (pEntity == nullptr)
		{
			continue;
		}

		if (pEntity->GetNetworkable()->IsDormant() && !visuals_filter_dormant->GetBool())
		{
			continue;
		}

		DrawEntityAndInvokeHook(pEntity);
	}

	if (visuals_filter_players->GetBool())
	{
		for (int i = 1; i <= Source::Interfaces::Globals->maxClients(); i++)
		{
			if (i == Source::Interfaces::Engine->GetLocalPlayer())
			{
				continue;
			}

			Source::CBasePlayer* pPlayer = (Source::CBasePlayer*)Source::Interfaces::EntityList->GetClientEntity(i);
			if (pPlayer == nullptr)
			{
				continue;
			}

			if (!pPlayer->IsAlive())
			{
				continue;
			}

			if (pPlayer->GetNetworkable()->IsDormant() && !visuals_filter_dormant->GetBool())
			{
				continue;
			}

			DrawEntity(pPlayer);
		}
	}
}

void ESP::DrawEntityAndInvokeHook(Source::CBaseEntity* pEntity)
{
	bool bDraw = false;

	auto L = LuaBase::GetState();
	if (L)
	{
		lua_getglobal(L, "hook");
		if (lua_istable(L, -1))
		{
			lua_getfield(L, -1, "Call");
			if (lua_isfunction(L, -1))
			{
				lua_pushstring(L, "ESP_ShouldPickupEntity");
				lua_pushentity(L, pEntity);

				if (lua_pcall(L, 2, 1, 0) != LUA_OK)
				{
					LuaBase::PrintError();
				}
				else
				{
					if (lua_isboolean(L, -1))
					{
						bDraw = lua_toboolean(L, -1);
					}

					lua_pop(L, 1);
				}
			}
			else
			{
				lua_pop(L, 1);
			}
		}

		lua_pop(L, 1);
	}

	if (!bDraw)
	{
		return;
	}

	DrawEntity(pEntity);
}

void ESP::DrawEntity(Source::CBaseEntity* pEntity)
{
	Vector vOrigin = pEntity->GetAbsOrigin() + Vector(0.f, 0.f, pEntity->GetMins().z);
	Vector vTop = vOrigin + Vector(0.f, 0.f, pEntity->GetMaxs().z);

	Vector vScreenOrigin, vScreenTop;
	if (Source::Interfaces::DebugOverlay->ScreenPosition(vOrigin, vScreenOrigin) || Source::Interfaces::DebugOverlay->ScreenPosition(vTop, vScreenTop))
	{
		return;
	}

	int w, h;
	h = vScreenOrigin.y - vScreenTop.y;
	w = h / 1.75f;

	Color clr = Color(255, 255, 255);
	#if defined(GAME_GMOD)
	if (pEntity->GetNetworkable()->EntIndex() <= Source::Interfaces::Globals->maxClients())
	{
		auto pState = Source::Interfaces::Lua->GetLuaInterface(Source::GarrysMod::CLIENT_STATE);
		if (pState)
		{
			pState->PushSpecial(Source::GarrysMod::SPECIAL_GLOB);
			pState->GetField(-1, "team");
			if (pState->IsType(-1, Source::GarrysMod::Type::TABLE))
			{
				pState->GetField(-1, "GetColor");
				if (pState->IsType(-1, Source::GarrysMod::Type::FUNCTION))
				{
					pState->PushNumber(((Source::CBasePlayer*)pEntity)->GetTeamNum());
					pState->Call(1, 1);

					pState->GetField(-1, "r");
					clr.r = pState->GetNumber(-1);
					pState->Pop();

					pState->GetField(-1, "g");
					clr.g = pState->GetNumber(-1);
					pState->Pop();

					pState->GetField(-1, "b");
					clr.b = pState->GetNumber(-1);
					pState->Pop();
				}

				pState->Pop(); // if not a function we pop nil, else we pop the return value
			}

			pState->Pop(2); // pop global, team
		}
	}
	#endif

	Util::DrawingHelper::DrawOutlinedRect(vScreenOrigin.x - w / 2, vScreenOrigin.y - h, w, h, clr);
	Util::DrawingHelper::DrawOutlinedRect(vScreenOrigin.x - w / 2 + 1, vScreenOrigin.y - h + 1, w - 2, h - 2, Color(0, 0, 0));
	Util::DrawingHelper::DrawOutlinedRect(vScreenOrigin.x - w / 2 - 1, vScreenOrigin.y - h - 1, w + 2, h + 2, Color(0, 0, 0));

	Util::DrawingHelper::DrawString(Util::DrawingHelper::MenuFont, vScreenOrigin.x - w / 2, vScreenOrigin.y, Color(255, 255, 255), pEntity->GetClassName());
}