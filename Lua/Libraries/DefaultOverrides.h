#ifndef __HEADER_DEFAULTOVERRIDES__
#define __HEADER_DEFAULTOVERRIDES__
#pragma once

#include "BaseLibrary.h"

class DefaultOverrides : public BaseLibrary<DefaultOverrides>
{
public:
	friend class BaseLibrary<DefaultOverrides>;

	static const char* GetName() { return "Default Overrides"; }
	static CStringPtr GetTable() { return _strdup("_G"); }

	//////////////////////////////////////////////////////////////
private:
	static int l_print(lua_State* l);

	static constexpr const struct luaL_Reg m_Register[] =
	{
		{ "print", l_print },
		{ NULL, NULL }
	};
};

#endif