#include "BoneCacheHelper.h"

using namespace Features::Misc;

BoneCacheHelper::BoneCacheBackup BoneCacheHelper::m_CacheBackup;

void BoneCacheHelper::BackupCache(Source::CBaseEntity* pEntity)
{
#if defined(GAME_L4D2)
	m_CacheBackup.m_nCachedBones = pEntity->m_nCachedBones();
	m_CacheBackup.m_iMostRecentModelBoneCounter = pEntity->GetRenderable()->m_iMostRecentModelBoneCounter();
	m_CacheBackup.m_fReadableBones = pEntity->GetRenderable()->m_fReadableBones();
	m_CacheBackup.m_fWriteableBones = pEntity->GetRenderable()->m_fWriteableBones();
	m_CacheBackup.m_iAccumulatedBoneMask = pEntity->GetRenderable()->m_iAccumulatedBoneMask();
	m_CacheBackup.m_flLastBoneSetupTime = pEntity->GetRenderable()->m_flLastBoneSetupTime();

	for (int i = 0; i < pEntity->m_nCachedBones(); i++)
	{
		memcpy(&m_CacheBackup.m_Bones[i], pEntity->GetCachedBone(i), sizeof(matrix3x4));
	}
#else
	m_CacheBackup.m_hitboxCacheHandle = pEntity->m_hitboxBoneCacheHandle();
#endif

	m_CacheBackup.m_vecOrigin = pEntity->GetAbsOrigin();
	m_CacheBackup.m_vecAngles = pEntity->GetAbsAngles();
	m_CacheBackup.m_vecMins = pEntity->GetMins();
	m_CacheBackup.m_vecMaxs = pEntity->GetMaxs();
}

void BoneCacheHelper::RestoreCache(Source::CBaseEntity* pEntity)
{
#if defined(GAME_L4D2)
	pEntity->m_nCachedBones() = m_CacheBackup.m_nCachedBones;
	pEntity->GetRenderable()->m_iMostRecentModelBoneCounter() = m_CacheBackup.m_iMostRecentModelBoneCounter;
	pEntity->GetRenderable()->m_fReadableBones() = m_CacheBackup.m_fReadableBones;
	pEntity->GetRenderable()->m_fWriteableBones() = m_CacheBackup.m_fWriteableBones;
	pEntity->GetRenderable()->m_iAccumulatedBoneMask() = m_CacheBackup.m_iAccumulatedBoneMask;
	pEntity->GetRenderable()->m_flLastBoneSetupTime() = m_CacheBackup.m_flLastBoneSetupTime;

	for (int i = 0; i < pEntity->m_nCachedBones(); i++)
	{
		memcpy(pEntity->GetCachedBone(i), &m_CacheBackup.m_Bones[i], sizeof(matrix3x4));
	}
#else
	if (pEntity->m_hitboxBoneCacheHandle() != m_CacheBackup.m_hitboxCacheHandle)
	{
		Source::CBoneCache::DestroyBoneCache(pEntity->m_hitboxBoneCacheHandle());
	}

	pEntity->m_hitboxBoneCacheHandle() = m_CacheBackup.m_hitboxCacheHandle;
#endif

	pEntity->SetAbsOrigin(m_CacheBackup.m_vecOrigin);
	pEntity->SetAbsAngles(m_CacheBackup.m_vecAngles);
	pEntity->SetMins(m_CacheBackup.m_vecMins);
	pEntity->SetMaxs(m_CacheBackup.m_vecMaxs);
}

void BoneCacheHelper::OverrideCache(Source::CBaseEntity* pEntity, Features::PositionAdjustment::History::Snapshot* pRecord)
{
#if defined(GAME_L4D2)
	auto pModel = pEntity->GetRenderable()->GetModel();
	if (pModel)
	{
		auto pStudioHdr = Source::Interfaces::ModelInfo->GetStudioModel(pModel);
		if (pStudioHdr)
		{
			static int* s_pModelBoneCounter = *Memory::FindPattern<int**>(Source::MISC_MODELBONECOUNTER, "client.dll");

			pEntity->GetRenderable()->m_iMostRecentModelBoneCounter() = *s_pModelBoneCounter;
			pEntity->GetRenderable()->m_iAccumulatedBoneMask() = pEntity->GetRenderable()->m_fWriteableBones() = pEntity->GetRenderable()->m_fReadableBones() = 0x100;
			pEntity->GetRenderable()->m_flLastBoneSetupTime() = Source::Interfaces::Globals->curtime();

			for (int i = 0; i < pEntity->m_nCachedBones(); i++)
			{
				memcpy(pEntity->GetCachedBone(i), &pRecord->m_BoneCache[i], sizeof(matrix3x4));
			}

			pEntity->SetAbsOrigin(pRecord->m_vecOrigin);
			pEntity->SetAbsAngles(pRecord->m_vecAngles);
			pEntity->SetMins(pRecord->m_vecMins);
			pEntity->SetMaxs(pRecord->m_vecMaxs);
		}
	}
#else
	if (pEntity->m_hitboxBoneCacheHandle() != m_CacheBackup.m_hitboxCacheHandle)
	{
		Source::CBoneCache::DestroyBoneCache(pEntity->m_hitboxBoneCacheHandle());
	}

	unsigned int handle;
	if (Source::CBoneCache::CreateBoneCache(pEntity, pRecord->m_BoneCache, handle))
	{
		pEntity->m_hitboxBoneCacheHandle() = handle;

		pEntity->SetAbsOrigin(pRecord->m_vecOrigin);
		pEntity->SetAbsAngles(pRecord->m_vecAngles);
		pEntity->SetMins(pRecord->m_vecMins);
		pEntity->SetMaxs(pRecord->m_vecMaxs);
	}
#endif
}