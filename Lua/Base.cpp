#include "Base.h"

#include "Libraries/BaseLibrary.h"
#include "Libraries/DefaultOverrides.h"
#include "Libraries/Internals.h"
#include "Libraries/Plugins.h"
#include "Libraries/Surface.h"
#include "Libraries/Config.h"
#include "Libraries/Globals.h"
#include "Libraries/Input.h"
#include "Libraries/Hook.h"
#include "Libraries/Ents.h"

#define MAINFILE "C:/m3nly/lua/init.lua"

lua_State* LuaBase::m_pState;

void LuaBase::Init(bool bForceNew)
{
	if (bForceNew || m_pState == nullptr)
	{
		if (m_pState)
		{
			lua_close(m_pState);
		}

		m_pState = luaL_newstate();
		if (!m_pState)
		{
			Debug::PrintToConsole("Failed creating Lua state :(");
			return;
		}

		Debug::PrintToConsole("Successfully created Lua state");

		luaL_openlibs(m_pState);
		Debug::PrintToConsole("Default libraries loaded");

		DefaultOverrides::Init(m_pState);
		Internals::Init(m_pState);
		Plugins::Init(m_pState);
		Surface::Init(m_pState);
		ConfigLib::Init(m_pState);
		Globals::Init(m_pState);
		Input::Init(m_pState);
		Hook::Init(m_pState);
		Ents::Init(m_pState);
	}

	Refresh();
}

void LuaBase::Refresh()
{
	if (m_pState != nullptr)
	{
		Plugins::Refresh();

		RunFile(MAINFILE);
	}
}

void LuaBase::RunFile(const char* pszFileName)
{
	if (!m_pState)
	{
		return;
	}

	if (luaL_loadfile(m_pState, pszFileName) != LUA_OK || lua_pcall(m_pState, 0, 0, 0) != LUA_OK)
	{
		PrintError();
	}
}

void LuaBase::PrintError()
{
	if (m_pState == nullptr)
	{
		return;
	}

	Debug::PrintToConsole(lua_tostring(m_pState, -1));
	lua_pop(m_pState, 1);
}

lua_State* LuaBase::GetState()
{
	return m_pState;
}