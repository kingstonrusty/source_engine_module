#include "History.h"

#include "Hooks/CVMTHookManager.h"

using namespace Features::PositionAdjustment;

LinkedList<History::Snapshot> History::m_Snapshots[4096]; // mby we can backtrack npcs

void History::Record()
{
	Source::CBaseEntity::PushAllowBoneAccess(true, true, (const char*)1);

	float flCurTimeBackup = Source::Interfaces::Globals->curtime();
	Source::Interfaces::Globals->curtime() = Source::TICKS_TO_TIME(Source::Interfaces::Globals->tickcount());

	for (int idx = 1; idx < 4096; idx++)
	{
		if (idx == Source::Interfaces::Engine->GetLocalPlayer())
		{
			ClearRecordsForIndex(idx - 1);
			continue;
		}

#if !defined(GAME_L4D2)
		if (idx > Source::Interfaces::Globals->maxClients())
		{
			ClearRecordsForIndex(idx - 1);
			continue;
		}
#endif

		Source::CBaseEntity* pEntity = (Source::CBaseEntity*)Source::Interfaces::EntityList->GetClientEntity(idx);
		if (pEntity == nullptr)
		{
			ClearRecordsForIndex(idx - 1);
			continue;
		}

		if (pEntity->IsPlayer())
		{
			auto pPlayer = (Source::CBasePlayer*)pEntity;

			if (pPlayer->IsTV())
			{
				ClearRecordsForIndex(idx - 1);
				continue;
			}

			if(!pPlayer->IsAlive())
			{
				ClearRecordsForIndex(idx - 1);
				continue;
			}
		}
		else if (pEntity->IsNPC())
		{
			auto pNPC = (Source::CBaseNPC*)pEntity;

			if (!pNPC->IsAlive())
			{
				ClearRecordsForIndex(idx - 1);
				continue;
			}
		}
		else
		{
			ClearRecordsForIndex(idx - 1);
			continue;
		}

		auto& list = m_Snapshots[idx - 1];
		
		{
			auto pNode = list.GetHead();

			while (pNode != nullptr) // Remove old records
			{
				auto pNext = pNode->m_pNext;

				if ((Source::Interfaces::Globals->curtime() - pNode->m_pThis->m_flSimulationTime) > 1.5f)
				{
					list.Remove(pNode);
				}

				pNode = pNext;
			}

			pNode = list.GetTail();

			if (pNode != nullptr) // Check for simtime
			{
				if (pNode->m_pThis->m_flSimulationTime >= pEntity->GetSimulationTime())
				{
					continue;
				}
			}
		}

		if (pEntity->GetNetworkable()->IsDormant())
		{
			continue;
		}

		struct
		{
			Vector m_vecOrigin;
			Vector m_vecAngles;
			Source::IAnimState* m_PlayerAnimState;
		} Backup;

		{
			Backup.m_vecOrigin = pEntity->GetAbsOrigin();
			Backup.m_vecAngles = pEntity->GetAbsAngles();

			pEntity->SetAbsOrigin(pEntity->GetLocalOrigin());
			pEntity->SetAbsAngles(pEntity->GetLocalAngles());

			pEntity->GetRenderable()->InvalidateBoneCache(); // MEGA SMART !!!

			/*if (idx <= Source::Interfaces::Globals->maxClients())
			{
				Source::CBasePlayer* pPlayer = (Source::CBasePlayer*)pEntity;
				Backup.m_PlayerAnimState = pPlayer->GetAnimState();

				//Source::IAnimState* pNew = new Source::IAnimState(pPlayer);
				//pPlayer->SetAnimState(pNew);
			}
			else*/
			{
				Backup.m_PlayerAnimState = nullptr;
			}
		}

		{
			Snapshot* pNew = new Snapshot;

			pNew->m_flSimulationTime = pEntity->GetSimulationTime();

			pEntity->GetRenderable()->SetupBones(pNew->m_BoneCache, 128, 0x100, pNew->m_flSimulationTime);

			pNew->m_vecOrigin = pEntity->GetLocalOrigin();
			pNew->m_vecAngles = pEntity->GetLocalAngles();
			pNew->m_vecMins = pEntity->GetMins();
			pNew->m_vecMaxs = pEntity->GetMaxs();

			pNew->m_flArriveTime = Source::Interfaces::Globals->curtime();

			/*Debug::PrintToConsole("%d %f %f (%.02f %.02f %.02f) (%.02f %.02f %.02f) (%.02f %.02f %.02f) (%.02f %.02f %.02f)",
				idx, pNew->m_flSimulationTime, pNew->m_flArriveTime,
				pNew->m_vecOrigin.x, pNew->m_vecOrigin.y, pNew->m_vecOrigin.z,
				pNew->m_vecAngles.x, pNew->m_vecAngles.y, pNew->m_vecAngles.z,
				pNew->m_vecMins.x, pNew->m_vecMins.y, pNew->m_vecMins.z,
				pNew->m_vecMaxs.x, pNew->m_vecMaxs.y, pNew->m_vecMaxs.z
				);*/
		
			/*auto pNetChannel = Source::Interfaces::Engine->GetNetChannelInfo();
			if (pNetChannel)
			{
				pNew->m_nInSequenceNr = pNetChannel->m_nInSequenceNr();
			}
			else
			{
				pNew->m_nInSequenceNr = -1;
			}*/

			list.Insert(pNew);
		}

		{
			pEntity->SetAbsOrigin(Backup.m_vecOrigin);
			pEntity->SetAbsAngles(Backup.m_vecAngles);
			pEntity->GetRenderable()->InvalidateBoneCache();
		
			if (Backup.m_PlayerAnimState)
			{
				//delete ((Source::CBasePlayer*)pEntity)->GetAnimState();
				//((Source::CBasePlayer*)pEntity)->SetAnimState(Backup.m_PlayerAnimState);
			}
		}
	}

	Source::Interfaces::Globals->curtime() = flCurTimeBackup;

	Source::CBaseEntity::PopAllowBoneAccess();
}

void History::ClearRecordsForIndex(int idx)
{
	m_Snapshots[idx].Clear();
}

#define IFTHENELSE_EXPAND(IF, THEN, ELSE) (((IF)) ? (THEN) : (ELSE))
#define IFTHENELSE(IF, THEN, ELSE) IFTHENELSE_EXPAND(IF, THEN, ELSE)
#define clamp(VAL, MIN, MAX) IFTHENELSE((VAL) > (MAX), (MAX), IFTHENELSE((VAL) < (MIN), (MIN), (VAL)))

float History::GetLerpTime()
{
	static auto sv_minupdaterate = Source::Interfaces::Cvar->FindVar("sv_minupdaterate");
	static auto sv_maxupdaterate = Source::Interfaces::Cvar->FindVar("sv_maxupdaterate");
	static auto cl_updaterate = Source::Interfaces::Cvar->FindVar("cl_updaterate");
	static auto cl_interpolate = Source::Interfaces::Cvar->FindVar("cl_interpolate");
	static auto cl_interp_ratio = Source::Interfaces::Cvar->FindVar("cl_interp_ratio");
	static auto cl_interp = Source::Interfaces::Cvar->FindVar("cl_interp");
	static auto sv_client_min_interp_ratio = Source::Interfaces::Cvar->FindVar("sv_client_min_interp_ratio");
	static auto sv_client_max_interp_ratio = Source::Interfaces::Cvar->FindVar("sv_client_max_interp_ratio");

	int nUpdateRate = clamp(cl_updaterate->GetInt(), (int)sv_minupdaterate->GetFloat(), (int)sv_maxupdaterate->GetFloat());
	float flLerpTime = 0.f;
#if !defined(GAME_CSGO)
	bool bUseInterpolation = cl_interpolate->GetInt() != 0;
#else
	bool bUseInterpolation = TRUE;
#endif
	if (bUseInterpolation)
	{
		float flLerpRatio = cl_interp_ratio->GetFloat();
		if (flLerpRatio == 0)
		{
			flLerpRatio = 1.f;
		}

		float flLerpAmount = cl_interp->GetFloat();

		if (sv_client_min_interp_ratio && sv_client_max_interp_ratio && sv_client_min_interp_ratio->GetFloat() != -1.f)
		{
			flLerpRatio = clamp(flLerpRatio, sv_client_min_interp_ratio->GetFloat(), sv_client_max_interp_ratio->GetFloat());
		}
		else
		{
			if (flLerpRatio == 0.f)
			{
				flLerpRatio = 1.f;
			}
		}

		flLerpTime = max(flLerpAmount, flLerpRatio / nUpdateRate);
	}
	else
	{
		flLerpTime = 0.f;
	}

	return flLerpTime;
}

float History::GetLowestPossibleLerpTime(int* nUpdateRate)
{
	static auto sv_maxupdaterate = Source::Interfaces::Cvar->FindVar("sv_maxupdaterate");

	if (nUpdateRate)
	{
		*nUpdateRate = sv_maxupdaterate->GetInt();
	}

	return 1.f/*cl_interp_ratio*/ / sv_maxupdaterate->GetInt();
}

#undef IFTHENELSE_EXPAND
#undef IFTHENELSE
#undef clamp

int History::EstimateServerArriveTick()
{
	int nTick = Source::Interfaces::Globals->tickcount() + 1;

	auto pNetChan = Source::Interfaces::Engine->GetNetChannelInfo();
	if (pNetChan)
	{
		nTick += Source::TIME_TO_TICKS(pNetChan->GetLatency(Source::CNetChan::FLOW_INCOMING) + pNetChan->GetLatency(Source::CNetChan::FLOW_OUTGOING));
	}

	return nTick;
}

bool History::CanRestoreToSimulationTime(float flSimulationTime, bool* bNeedToAdjustInterp)
{
	float correct = 0.f;

	auto pNetChan = Source::Interfaces::Engine->GetNetChannelInfo();
	if (pNetChan)
	{
		correct += pNetChan->GetLatency(Source::CNetChan::FLOW_OUTGOING);
	}

	int lerpTicks;
#if defined(GAME_CSGO)
	lerpTicks = GetLerpTime();
#else
	lerpTicks = 0;
#endif
	// not needed, we either adjust cl_interp or disable cl_interpolate[smart], except on newer src games where we cant but i add that later

	correct += Source::TICKS_TO_TIME(Source::TIME_TO_TICKS(lerpTicks));

	if (correct >= 1.f)
	{
		return false;
	}
	else if (correct < 0.f)
	{
		correct = 0.f;
	}

	float flDiff = Source::TICKS_TO_TIME(EstimateServerArriveTick() - Source::TIME_TO_TICKS(flSimulationTime));
	if (flDiff >= 1.f)
	{
		return false;
	}

	int targettick = Source::TIME_TO_TICKS(flSimulationTime);
	float deltaTime = correct - Source::TICKS_TO_TIME(EstimateServerArriveTick() - targettick);

	if (fabsf(deltaTime) >= Source::TICKS_TO_TIME(15))
	{
		if (flDiff >= 0.f)
		{
			if (bNeedToAdjustInterp != nullptr)
			{
				*bNeedToAdjustInterp = true;
			}

#if defined(GAME_GMOD) || defined(GAME_L4D2)
			return true;
#else
			return false;
#endif
		}

		return false;
	}

	if (bNeedToAdjustInterp != nullptr)
	{
		*bNeedToAdjustInterp = false;
	}

	return true;
}

void History::GetUsableRecordsForEntity(Source::CBaseEntity* pEntity, SmallVector<Snapshot*>* vecRecords, float flMinTime, float flMaxTime, bool* bShouldLagFix)
{
	return GetUsableRecordsForIndex(pEntity->GetNetworkable()->EntIndex() - 1, vecRecords, flMinTime, flMaxTime, bShouldLagFix);
}

bool History::IsDeltaTooBig(Vector& vPos1, Vector& vPos2)
{
#if defined(GAME_GMOD)
	return (vPos1 - vPos2).LengthSqr() > 1600000.f; // temp until lagfix
#else
	return (vPos1 - vPos2).Length2DSqr() > 4096.f;
#endif
}

void History::GetUsableRecordsForIndex(int idx, SmallVector<Snapshot*>* vecRecords, float flMinTime, float flMaxTime, bool* bShouldLagFix)
{
	auto& list = m_Snapshots[idx];
	if (list.GetSize() == 0)
	{
		return;
	}

	auto pNode = list.GetTail();
	Vector* vPrevOrigin = &pNode->m_pThis->m_vecOrigin;

	bool bFirstRecordValid = false;
	if (list.GetSize() >= 2)
	{
		if (CanRestoreToSimulationTime(pNode->m_pThis->m_flSimulationTime))
		{
			bFirstRecordValid = !IsDeltaTooBig(*vPrevOrigin, pNode->m_pPrev->m_pThis->m_vecOrigin);
		}
	}

	while (pNode != nullptr)
	{
		auto pPrev = pNode->m_pPrev;

		if(flMaxTime != -1.f)
		{
			if (flMinTime == -1.f)
			{
				flMinTime = 0.f;
			}

			static auto cl_updaterate = Source::Interfaces::Cvar->FindVar("cl_updaterate");
			float flSnapshotInterval = 1.f / cl_updaterate->GetFloat();
			float flDelta = Source::TICKS_TO_TIME(Source::Interfaces::Globals->tickcount()) - pNode->m_pThis->m_flArriveTime;

			if (flDelta < flMinTime)
			{
				if (IsDeltaTooBig(*vPrevOrigin, pNode->m_pThis->m_vecOrigin))
				{
					if (bFirstRecordValid)
					{
						vecRecords->Add(list.GetTail()->m_pThis);
					}

					if (bShouldLagFix != nullptr) // need to do more elaborate ping checks here
					{
						*bShouldLagFix = !bFirstRecordValid;
					}

					break;
				}

				vPrevOrigin = &pNode->m_pThis->m_vecOrigin;
				pNode = pPrev;
				continue;
			}

			if (flDelta > (flMaxTime + flSnapshotInterval))
			{
				break;
			}

			if (!vPrevOrigin)
			{
				vPrevOrigin = &pNode->m_pThis->m_vecOrigin;
			}
		}

		if (!CanRestoreToSimulationTime(pNode->m_pThis->m_flSimulationTime))
		{
			if (bShouldLagFix != nullptr)
			{
				*bShouldLagFix = vecRecords->GetSize() == 0;
			}

			break;
		}
		#if 0
		if (IsDeltaTooBig(*vPrevOrigin, pNode->m_pThis->m_vecOrigin))
		{
			if (vecRecords->GetSize() == 0 && bFirstRecordValid)
			{
				vecRecords->Add(list.GetTail()->m_pThis);
			}

			if (bShouldLagFix != nullptr) // need to do more elaborate ping checks here
			{
				*bShouldLagFix = !bFirstRecordValid;
			}

			break;
		}
		#endif

		vecRecords->Add(pNode->m_pThis);

		vPrevOrigin = &pNode->m_pThis->m_vecOrigin;
		pNode = pPrev;
	}
}