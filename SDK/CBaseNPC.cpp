#include "CBaseNPC.h"

using namespace Source;

bool CBaseNPC::IsAlive()
{
	if (GetSolidType() == 0)
	{
		return false;
	}

	if ((GetSolidFlags() & 4) != 0)
	{
		return false;
	}

#if defined(GAME_L4D2)
	if (strstr(GetSequenceName(GetSequence()), "Death"))
	{
		return false;
	}
#endif
	
	return true;
}

#if defined(GAME_L4D2)
bool CBaseNPC::IsNeutral()
{
	if (IsCommonInfected())
	{
		return strstr(GetSequenceName(GetSequence()), "Neutral") || strstr(GetSequenceName(GetSequence()), "Leaning");
	}
	else if (IsWitch())
	{
		return !strstr(GetSequenceName(GetSequence()), "Run") && !strstr(GetSequenceName(GetSequence()), "killing");
	}

	return false;
}

bool CBaseNPC::IsCommonInfected()
{
	return GetNetworkable()->GetClientClass()->m_ClassID == 263 || GetNetworkable()->GetClientClass()->m_ClassID == 264;
}

bool CBaseNPC::IsSpecialInfected()
{
	return !IsPlayer() && !IsCommonInfected();
}

bool CBaseNPC::IsTank()
{
	return GetNetworkable()->GetClientClass()->m_ClassID == 275;
}

bool CBaseNPC::IsCharger()
{
	return GetNetworkable()->GetClientClass()->m_ClassID == 99;
}

bool CBaseNPC::IsJockey()
{
	return GetNetworkable()->GetClientClass()->m_ClassID == 264;
}

bool CBaseNPC::IsHunter()
{
	return GetNetworkable()->GetClientClass()->m_ClassID == 262;
}

bool CBaseNPC::IsSmoker()
{
	return GetNetworkable()->GetClientClass()->m_ClassID == 269;
}

bool CBaseNPC::IsSpitter()
{
	return GetNetworkable()->GetClientClass()->m_ClassID == 271;
}

bool CBaseNPC::IsBoomer()
{
	return GetNetworkable()->GetClientClass()->m_ClassID == 0;
}

bool CBaseNPC::IsWitch()
{
	return GetNetworkable()->GetClientClass()->m_ClassID == 276;
}
#endif