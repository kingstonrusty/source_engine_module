#include "SceneEnd.h"

#include "Features/Visuals/Chams.h"

void __fastcall Hooks::RenderView::SceneEnd(Source::IVRenderView* pThis, void*)
{
	pVmt->Unhook();
	pThis->SceneEnd();
	pVmt->Rehook();

	Features::Visuals::Chams::DrawAllEntities();
}