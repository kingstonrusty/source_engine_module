#include "IMaterialSystem.h"

namespace Source
{
	namespace Interfaces
	{
		IMaterialSystem* MaterialSystem;
	}
}

using namespace Source;

IMaterialSystem* IMaterialSystem::GetPtr()
{
	return (IMaterialSystem*)::Util::BruteforceInterface("VMaterialSystem", "materialsystem.dll");
}

IMaterial* IMaterialSystem::CreateMaterial(const char* materialname, Source::KeyValues* keyvalues)
{
	return GET_VFUNC(IMaterial*(__thiscall*)(IMaterialSystem*, const char*, Source::KeyValues*), this, IMATERIALSYSTEM_CREATEMATERIAL)(this, materialname, keyvalues);
}

#if defined(GAME_GMOD)
IMatRenderContext* IMaterialSystem::GetRenderContext()
{
	return GET_VFUNC(IMatRenderContext*(__thiscall*)(IMaterialSystem*), this, IMATERIALSYSTEM_GETRENDERCONTEXT)(this);
}
#endif