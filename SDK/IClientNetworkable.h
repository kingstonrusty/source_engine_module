#ifndef __HEADER_ICLIENTNETWORKABLE__
#define __HEADER_ICLIENTNETWORKABLE__
#pragma once

namespace Source
{
	class DVariant
	{
	public:
		union
		{
			float	m_Float;
			int		m_Int;
			const char	*m_pString;
			void	*m_pData;
			float	m_Vector[3];
#if defined(GAME_CSGO)
			long long m_Int64;
#endif
		};

#if !defined(GAME_CSGO)
		int m_Type;
#endif
	};

	class CRecvProp;

	class CRecvProxyData
	{
	public:
		const CRecvProp* m_pRecvProp;
		DVariant m_Value;
		int m_iElement;
		int m_ObjectID;
	};

	typedef void(__cdecl *RecvVarProxyFn)(const CRecvProxyData *pData, void *pStruct, void *pOut);

	class CRecvTable;

	class CRecvProp
	{
	public:
		const char* m_pVarName;
		int m_RecvType;
		int m_Flags;
		int m_StringBufferSize;
		bool m_bInsideArray;
		const void *m_pExtraData;
		CRecvProp* m_pArrayProp;
		void* m_ArrayLengthProxy;
		RecvVarProxyFn m_ProxyFn;
		void* m_DataTableProxyFn;
		CRecvTable* m_pDataTable;
		int m_Offset;
		int m_ElementStride;
		int m_nElements;
		const char* m_pParentArrayPropName;
	};

	class CRecvTable
	{
	public:
		CRecvProp* m_pProps;
		int m_nProps;
		void* m_pDecoder;
		const char* m_pNetTableName;
		bool			m_bInitialized;
		bool			m_bInMainList;
	};

	class ClientClass
	{
	public:
		void* m_pCreateFn;
		void* m_pCreateEventFn;
		char* m_pNetworkName;
		CRecvTable* m_pRecvTable;
		ClientClass* m_pNext;
		int m_ClassID;
	};

	class IClientNetworkable;
}

class Source::IClientNetworkable
{
public:
	ClientClass* GetClientClass();
	bool IsDormant();
	int EntIndex();
};

#endif