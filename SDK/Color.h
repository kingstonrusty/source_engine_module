#ifndef __HEADER_COLOR__
#define __HEADER_COLOR__
#pragma once

class Color
{
public:
	Color()
	{
		r = g = b = a = 0;
	}

	Color(const Color& other)
	{
		*(int*)(this) = *(int*)(&other);
	}

	Color(byte r, byte g, byte b, byte a = 255)
	{
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

	Color(const Color& other, byte a)
	{
		r = other.r;
		g = other.g;
		b = other.b;
		this->a = a;
	}

	byte r, g, b, a;
};

#endif