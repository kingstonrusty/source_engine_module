#ifndef __HEADER_IENGINETRACE__
#define __HEADER_IENGINETRACE__
#pragma once

#include "IClientEntity.h"

#include "Multigame/InterfaceBruteforcer.h"

namespace Source
{
	enum TraceType_t 
	{
		TRACE_EVERYTHING = 0,
		TRACE_WORLD_ONLY,
		TRACE_ENTITIES_ONLY,
		TRACE_EVERYTHING_FILTER_PROPS,
	};

	enum
	{
		CONTENTS_EMPTY = 0,
		CONTENTS_SOLID = (1 << 0),
		CONTENTS_WINDOW = (1 << 1),
		CONTENTS_AUX = (1 << 2),
		CONTENTS_GRATE = (1 << 3),
		CONTENTS_SLIME = (1 << 4),
		CONTENTS_WATER = (1 << 5),
		CONTENTS_BLOCKLOS = (1 << 6),
		CONTENTS_OPAQUE = (1 << 7),
		CONTENTS_TESTFOGVOLUME = (1 << 8),
		CONTENTS_UNUSED = (1 << 9),
		CONTENTS_UNUSED6 = (1 << 10),
		CONTENTS_TEAM1 = (1 << 11),
		CONTENTS_TEAM2 = (1 << 12),
		CONTENTS_IGNORE_NODRAW_OPAQUE = (1 << 13),
		CONTENTS_MOVEABLE = (1 << 14),
		CONTENTS_AREAPORTAL = (1 << 15),
		CONTENTS_PLAYERCLIP = (1 << 16),
		CONTENTS_MONSTERCLIP = (1 << 17),
		CONTENTS_CURRENT_0 = (1 << 18),
		CONTENTS_CURRENT_90 = (1 << 19),
		CONTENTS_CURRENT_180 = (1 << 20),
		CONTENTS_CURRENT_270 = (1 << 21),
		CONTENTS_CURRENT_UP = (1 << 22),
		CONTENTS_CURRENT_DOWN = (1 << 23),
		CONTENTS_ORIGIN = (1 << 24),
		CONTENTS_MONSTER = (1 << 25),
		CONTENTS_DEBRIS = (1 << 26),
		CONTENTS_DETAIL = (1 << 27),
		CONTENTS_TRANSLUCENT = (1 << 28),
		CONTENTS_LADDER = (1 << 29),
		CONTENTS_HITBOX = (1 << 30),

		LAST_VISIBLE_CONTENTS = CONTENTS_OPAQUE,
		ALL_VISIBLE_CONTENTS = (LAST_VISIBLE_CONTENTS | (LAST_VISIBLE_CONTENTS - 1))
	};

	enum
	{
		MASK_ALL = 0xFFFFFFFF,
		MASK_SOLID = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_MONSTER | CONTENTS_GRATE),
		MASK_PLAYERSOLID = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_PLAYERCLIP | CONTENTS_WINDOW | CONTENTS_MONSTER | CONTENTS_GRATE),
		MASK_NPCSOLID = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_MONSTERCLIP | CONTENTS_WINDOW | CONTENTS_MONSTER | CONTENTS_GRATE),
		MASK_WATER = (CONTENTS_WATER | CONTENTS_MOVEABLE | CONTENTS_SLIME),
		MASK_OPAQUE = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_OPAQUE),
		MASK_OPAQUE_AND_NPCS = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_OPAQUE | CONTENTS_MONSTER),
		MASK_BLOCKLOS = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_BLOCKLOS),
		MASK_BLOCKLOS_AND_NPCS = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_BLOCKLOS | CONTENTS_MONSTER),
		MASK_VISIBLE = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_OPAQUE | CONTENTS_IGNORE_NODRAW_OPAQUE),
		MASK_VISIBLE_AND_NPCS = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_OPAQUE | CONTENTS_MONSTER | CONTENTS_IGNORE_NODRAW_OPAQUE),
		MASK_SHOT = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_MONSTER | CONTENTS_WINDOW | CONTENTS_DEBRIS | CONTENTS_HITBOX),
		MASK_SHOT_HULL = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_MONSTER | CONTENTS_WINDOW | CONTENTS_DEBRIS | CONTENTS_GRATE),
		MASK_SHOT_PORTAL = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_MONSTER),
		MASK_SOLID_BRUSHONLY = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_GRATE),
		MASK_PLAYERSOLID_BRUSHONLY = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_PLAYERCLIP | CONTENTS_GRATE),
		MASK_NPCSOLID_BRUSHONLY = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_MONSTERCLIP | CONTENTS_GRATE),
		MASK_NPCWORLDSTATIC = (CONTENTS_SOLID | CONTENTS_WINDOW | CONTENTS_MONSTERCLIP | CONTENTS_GRATE),
		MASK_SPLITAREAPORTAL = (CONTENTS_WATER | CONTENTS_SLIME),
		MASK_CURRENT = (CONTENTS_CURRENT_0 | CONTENTS_CURRENT_90 | CONTENTS_CURRENT_180 | CONTENTS_CURRENT_270 | CONTENTS_CURRENT_UP | CONTENTS_CURRENT_DOWN),
		MASK_DEADSOLID = (CONTENTS_SOLID | CONTENTS_PLAYERCLIP | CONTENTS_WINDOW | CONTENTS_GRATE)
	};

	class VectorAligned : public Vector
	{
	public:
		VectorAligned() { x = y = z = 0.0f; }
		VectorAligned(const Vector& v) { x = v.x; y = v.y; z = v.z; }
		float w;
	};

	class Ray_t
	{
	public:
#if defined(CSS_34)
		Vector  m_Start;
		Vector  m_Delta;
		Vector	m_StartOffset;
		Vector  m_Extents;
#else
		VectorAligned   m_Start;
		VectorAligned   m_Delta;
		VectorAligned	m_StartOffset;
		VectorAligned   m_Extents;
#endif

#if defined(GAME_CSGO) || defined(GAME_L4D2)
		const matrix3x4 *m_pWorldAxisTransform;
#endif

		bool    m_IsRay;
		bool    m_IsSwept;

		void Init(Vector& vStart, Vector& vEnd);
		void Init(Vector& vStart, Vector& vEnd, Vector& vMins, Vector& vMaxs);
	};

	struct cplane_t 
	{
		Vector normal;
		float dist;
		byte type;
		byte signBits;
		byte pad[2];
	};

	struct csurface_t 
	{
		const char* name;
		short surfaceProps;
		unsigned short flags;
	};

	class CBaseTrace
	{
	public:
		Vector			startpos;
		Vector			endpos;	
		cplane_t		plane;
		float			fraction;
		int				contents;
		unsigned short	dispFlags;
		bool			allsolid;
		bool			startsolid;
	};

	class trace_t: public CBaseTrace {
	public:
		float			fractionleftsolid;	
		csurface_t		surface;
		int				hitgroup;
		short			physicsbone;
#if defined(GAME_CSGO) || defined(GAME_L4D2)
		unsigned short	worldSurfaceIndex;
#endif
		IClientEntity *m_pEnt;
		int			hitbox;	
	};

	class ITraceFilter {
	public:
		virtual bool ShouldHitEntity(void *pEntity, int contentsMask) = 0;
		virtual TraceType_t	GetTraceType() const = 0;
	};

	class CTraceFilterSimple : public ITraceFilter
	{
	public:
		CTraceFilterSimple(void* pSkip)
		{
			this->pSkip = pSkip;
		}

		bool ShouldHitEntity(void* pEntityHandle, int contentsMask)
		{
			return !(pEntityHandle == pSkip);
		}

		virtual TraceType_t GetTraceType() const
		{
			return TRACE_EVERYTHING;
		}

		void* pSkip;
	};

	class CTraceFilterSkipTwoEntities : public ITraceFilter
	{
	public:
		CTraceFilterSkipTwoEntities(void *pPassEnt1, void *pPassEnt2)
		{
			passentity1 = pPassEnt1;
			passentity2 = pPassEnt2;
		}

		virtual bool ShouldHitEntity(void *pEntityHandle, int contentsMask)
		{
			return !(pEntityHandle == passentity1 || pEntityHandle == passentity2);
		}

		virtual TraceType_t    GetTraceType() const
		{
			return TRACE_EVERYTHING;
		}

		void *passentity1;
		void *passentity2;
	};

	class IEngineTrace;

	namespace Interfaces
	{
		extern IEngineTrace* TraceRay;
	}
}

class Source::IEngineTrace
{
public:
	static IEngineTrace* GetPtr();

	void TraceRay(const Ray_t& ray, unsigned int mask, ITraceFilter* filter, trace_t* trace);
};

#endif