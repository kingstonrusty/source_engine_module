#include "CVMTHookManager.h"

using namespace Hooks;

CVMTHookManager::CVMTHookManager(void* pVmt)
{
	size_t nSize = 0;

	m_pppBase = (void***)pVmt;
	m_ppOriginal = *m_pppBase;

	while (!IsBadCodePtr((FARPROC)m_ppOriginal[nSize]))
	{
		nSize++;
	}

	m_ppNew = new void*[nSize];
	for (; nSize > 0; nSize--)
	{
		m_ppNew[nSize - 1] = m_ppOriginal[nSize - 1];
	}
}

CVMTHookManager::~CVMTHookManager()
{
	delete[] m_ppNew;
}

void CVMTHookManager::Hook(bool bHook)
{
	*m_pppBase = (bHook) ? m_ppNew : m_ppOriginal;
}

void CVMTHookManager::Unhook()
{
	Hook(false);
}

void CVMTHookManager::Rehook()
{
	Hook(true);
}

void CVMTHookManager::HookFunction(int idx, void* pFunction)
{
	m_ppNew[idx] = pFunction;
}

void CVMTHookManager::UnhookFunction(int idx)
{
	m_ppNew[idx] = m_ppOriginal[idx];
}

CVMTHookUnsafeManager::CVMTHookUnsafeManager(void* pVmt)
{
	m_ppVmt = *(void***)pVmt;
}

CVMTHookUnsafeManager::~CVMTHookUnsafeManager()
{
	auto pNode = m_Hooks.GetHead();
	while (pNode)
	{
		UnhookFunction(pNode->m_pThis->m_nIdx);

		pNode = pNode->m_pNext;
	}
	
	m_Hooks.Clear();
}

void CVMTHookUnsafeManager::HookFunction(int idx, void* pFunction)
{
	DWORD dwOldProtect;
	void* pOrig = m_ppVmt[idx];

	VirtualProtect(m_ppVmt, 0x1000, PAGE_EXECUTE_READWRITE, &dwOldProtect);
	m_ppVmt[idx] = pFunction;
	VirtualProtect(m_ppVmt, 0x1000, dwOldProtect, nullptr);

	auto pNode = m_Hooks.GetHead();
	while(pNode)
	{
		if (pNode->m_pThis->m_nIdx == idx)
		{
			return;
		}

		pNode = pNode->m_pNext;
	}

	Hook* newHook = new Hook;
	newHook->m_nIdx = idx;
	newHook->m_pOrig = pOrig;

	m_Hooks.Insert(newHook);
}

void CVMTHookUnsafeManager::UnhookFunction(int idx)
{
	void* pOrig = nullptr;

	auto pNode = m_Hooks.GetHead();
	while (pNode)
	{
		if (pNode->m_pThis->m_nIdx == idx)
		{
			pOrig = pNode->m_pThis->m_pOrig;

			break;
		}

		pNode = pNode->m_pNext;
	}

	if (pOrig == nullptr)
	{
		return;
	}

	DWORD dwOldProtect;
	VirtualProtect(m_ppVmt, 0x1000, PAGE_EXECUTE_READWRITE, &dwOldProtect);
	m_ppVmt[idx] = pOrig;
	VirtualProtect(m_ppVmt, 0x1000, dwOldProtect, nullptr);
}