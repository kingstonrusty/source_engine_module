#ifndef __HEADER_GETUSERCMD__
#define __HEADER_GETUSERCMD__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace Input
	{
		extern CVMTHookManager* pVmt;

		Source::CUserCmd* __fastcall GetUserCmd(Source::CInput* pThis, void*, int sequence_number);
	}
}

#endif