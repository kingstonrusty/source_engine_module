#ifndef __HEADER_CREATEMOVE__
#define __HEADER_CREATEMOVE__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace ClientMode
	{
		extern CVMTHookManager* pVmt;

		bool __fastcall CreateMove(Source::IClientMode* pThis, void*, float sampletime, Source::CUserCmd* cmd);
	}

#if defined(GAME_L4D2)  || defined(GAME_CSS_V34)
	namespace Client
	{
		extern CVMTHookManager* pVmt;

#if defined(GAME_L4D2)
		void __fastcall CreateMove(Source::IBaseClientDLL* pThis, void*, int sequence_number, float sample_time, bool active);
#elif defined(GAME_CSS_V34)
		__declspec(naked) void CreateMove();
#endif
	}
#endif
}

#endif