#ifndef __HEADER_CBASEWEAPON__
#define __HEADER_CBASEWEAPON__
#pragma once

#include "CBaseEntity.h"

namespace Source
{
	struct FireBulletsInfo_t;

	class CBaseWeapon;
}

struct Source::FireBulletsInfo_t
{
#if defined(GAME_GMOD)
	Vector& m_vecSpread()
	{
		return *(Vector*)((DWORD_PTR)this + 0x1C);
	}

	int& m_iShots()
	{
		return *(int*)((DWORD_PTR)this);
	}
#endif
};

class Source::CBaseWeapon : public CBaseEntity
{
public:
	NETVAR(float, "DT_BaseCombatWeapon", "m_flNextPrimaryAttack", NextPrimaryAttack);

#if defined(GAME_CSS_STEAM) || defined(GAME_L4D2)
	float GetSpread();
#if defined(GAME_CSS_STEAM)
	float GetInaccuracy();
#endif
	void UpdateInaccuracy();
#endif
};

#endif