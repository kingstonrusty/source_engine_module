#include "IVModelInfo.h"

namespace Source
{
	namespace Interfaces
	{
		IVModelInfo* ModelInfo;
	}
}

using namespace Source;

IVModelInfo* IVModelInfo::GetPtr()
{
	return (IVModelInfo*)::Util::BruteforceInterface("VModelInfoClient", "engine.dll");
}

int IVModelInfo::GetModelIndex(const char* name)
{
	return GET_VFUNC(int(__thiscall*)(IVModelInfo*, const char*), this, IVMODELINFO_GETMODELINDEX)(this, name);
}

const char* IVModelInfo::GetModelName(const model_t* model)
{
	return GET_VFUNC(const char*(__thiscall*)(IVModelInfo*, const model_t*), this, IVMODELINFO_GETMODELNAME)(this, model);
}

studiohdr_t* IVModelInfo::GetStudioModel(const model_t* model)
{
	return GET_VFUNC(studiohdr_t*(__thiscall*)(IVModelInfo*, const model_t*), this, IVMODELINFO_GETSTUDIOMODEL)(this, model);
}

const model_t* IVModelInfo::FindOrLoadModel(const char* name)
{
	return GET_VFUNC(const model_t*(__thiscall*)(IVModelInfo*, const char*), this, IVMODELINFO_FINDORLOADMODEL)(this, name);
}