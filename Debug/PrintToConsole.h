#ifndef __HEADER_PRINTTOCONSOLE__
#define __HEADER_PRINTTOCONSOLE__
#pragma once

namespace Debug
{
	void PrintToConsole(const char* pszFmt, ...);
}

#endif