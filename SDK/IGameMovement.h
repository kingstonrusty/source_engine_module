#ifndef __HEADER_IGAMEMOVEMENT__
#define __HEADER_IGAMEMOVEMENT__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

#include "CMoveData.h"
#include "CBasePlayer.h"

namespace Source
{
	class IGameMovement;

	namespace Interfaces
	{
		extern IGameMovement* GameMovement;
	}
}

class Source::IGameMovement
{
public:
	static IGameMovement* GetPtr();

	CMoveData* GetMoveData();
	void ProcessMovement(CBasePlayer* player, CMoveData* movedata);
};

#endif