#ifndef __HEADER_DRAWMODEL_SOURCE__
#define __HEADER_DRAWMODEL_SOURCE__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace StudioRender
	{
		extern CVMTHookManager* pVmt;

		void __fastcall DrawModel(Source::IStudioRender* pThis, void*, Source::DrawModelResults_t* results, Source::DrawModelInfo_t* info, matrix3x4* bonetoworld, float* flexweights, float* flexdelayedweights, Vector& modelorigin, int flags);
	}
}

#endif