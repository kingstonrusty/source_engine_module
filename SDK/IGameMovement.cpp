#include "IGameMovement.h"

namespace Source
{
	namespace Interfaces
	{
		IGameMovement* GameMovement;
	}
}

using namespace Source;

IGameMovement* IGameMovement::GetPtr()
{
	return (IGameMovement*)::Util::BruteforceInterface("GameMovement", "client.dll");
}

CMoveData* IGameMovement::GetMoveData()
{
	return *(CMoveData**)((DWORD_PTR)this + 0x8);
}

void IGameMovement::ProcessMovement(CBasePlayer* player, CMoveData* movedata)
{
	return GET_VFUNC(void(__thiscall*)(IGameMovement*, CBasePlayer*, CMoveData*), this, IGAMEMOVEMENT_PROCESSMOVEMENT)(this, player, movedata);
}