#include "Menu.h"

#include "Util/MD5.h"
#include "Util/CRC32.h"
#include "Util/Input.h"

namespace DrawingHelper = Util::DrawingHelper;

static bool s_bDrawConsole;
static int tw, th;

static Config::ConVar s_Base;

Config::ConVar* Config::FindVar(ReverseStack<const char*>& stack, Config::ConVar* start)
{
	ConVar* pStart = (start) ? start : &s_Base;

	if (pStart == nullptr)
	{
		return nullptr;
	}

	if (stack.IsEmpty())
	{
		return start;
	}

	auto pNode = pStart->m_SubVars.GetHead();
	while (pNode)
	{
		//Debug::PrintToConsole("%s", pNode->m_pThis->m_pszName);

		if (pNode->m_pThis->m_pszName)
		{
			if (!strcmp(pNode->m_pThis->m_pszName, stack.GetTop(nullptr)))
			{
				stack.Pop();

				return FindVar(stack, pNode->m_pThis);
			}
		}

		pNode = pNode->m_pNext;
	}

	return nullptr;
}

Config::ConVar* Config::RecursiveAdd(ReverseStack<const char*>& stack, Config::ConVar* start)
{
	ConVar* pStart = (start) ? start : &s_Base;
	if (pStart == nullptr)
	{
		return nullptr;
	}

	if (stack.GetCount() == 1) // this is our call to create the convar & return it
	{
		ConVar* pNew = new ConVar(stack.GetTop(nullptr));
		stack.Pop();
		
		if (pStart)
		{
			pStart->m_SubVars.Insert(pNew);
		}

		return pNew;
	}

	auto pNode = pStart->m_SubVars.GetHead();
	while (pNode)
	{
		if (!strcmp(pNode->m_pThis->m_pszName, stack.GetTop(nullptr)))
		{
			stack.Pop();

			return RecursiveAdd(stack, pNode->m_pThis);
		}

		pNode = pNode->m_pNext;
	}

	// we didnt find out node, so create it now...

	ConVar* pNew = new ConVar(stack.GetTop(nullptr));
	pStart->m_SubVars.Insert(pNew);

	stack.Pop();

	return RecursiveAdd(stack, pNew);
}

void Config::SpewVars(int iTabs, Config::ConVar* start)
{
	auto pStart = (start) ? start : &s_Base;

	if (pStart == nullptr)
	{
		return;
	}

	Debug::PrintToConsole("%s: %d %f", pStart->m_pszName ? pStart->m_pszName : "base", pStart->GetInt(), pStart->GetFloat());

	auto pNode = pStart->m_SubVars.GetHead();
	while (pNode)
	{
		SpewVars(iTabs + 1, pNode->m_pThis);

		pNode = pNode->m_pNext;
	}
}

#define ADDCVAR(...) \
{ \
constexpr static const char* s_Vars[] = {__VA_ARGS__}; \
static ReverseStack<const char*> s_Stack; \
for(int i = 0; i < (sizeof(s_Vars) / 4); i++) \
{ \
s_Stack.Push(s_Vars[i]); \
} \
RecursiveAdd(s_Stack, nullptr); \
}

#define ADDCATEGORY(...) \
ADDCVAR(__VA_ARGS__)


void Config::RegisterVars()
{
	ADDCATEGORY("Ragebot");
	{
		ADDCATEGORY("Ragebot", "Aimbot");
		{
			ADDCVAR("Ragebot", "Aimbot", "Autofire");
			ADDCVAR("Ragebot", "Aimbot", "Aimkey");
			ADDCVAR("Ragebot", "Aimbot", "Silent");
			ADDCVAR("Ragebot", "Aimbot", "FOV");

			ADDCATEGORY("Ragebot", "Aimbot", "Filter")
			{
				ADDCVAR("Ragebot", "Aimbot", "Filter", "Players");
				ADDCVAR("Ragebot", "Aimbot", "Filter", "Friendly Fire");
			}

			ADDCATEGORY("Ragebot", "Aimbot", "Target")
			{
				ADDCVAR("Ragebot", "Aimbot", "Target", "Hitboxes");
			}

			ADDCATEGORY("Ragebot", "Aimbot", "Position Adjustment")
			{
				ADDCVAR("Ragebot", "Aimbot", "Position Adjustment", "Force Highest Update Rate");
				ADDCVAR("Ragebot", "Aimbot", "Position Adjustment", "Minimum Time");
				ADDCVAR("Ragebot", "Aimbot", "Position Adjustment", "Maximum Time");
			}
		}
	}

	ADDCATEGORY("Visuals")
	{
		ADDCATEGORY("Visuals", "ESP")
		{

		}

		ADDCATEGORY("Visuals", "Chams")
		{

		}

		ADDCATEGORY("Visuals", "Filter")
		{
			ADDCVAR("Visuals", "Filter", "Players");
			ADDCVAR("Visuals", "Filter", "Enemy Only");
			ADDCVAR("Visuals", "Filter", "Dormant");
		}
	}

	ADDCATEGORY("Miscellaneous")
	{
		ADDCVAR("Miscellaneous", "Field of View");

		ADDCATEGORY("Miscellaneous", "Freecam")
		{
			ADDCVAR("Miscellaneous", "Freecam", "Key");
		}

		ADDCATEGORY("Miscellaneous", "Thirdperson")
		{
			ADDCVAR("Miscellaneous", "Thirdperson", "Collisions");
			ADDCVAR("Miscellaneous", "Thirdperson", "Distance");
		}
	}

	ADDCATEGORY("Hack vs Hack")
	{
		ADDCATEGORY("Hack vs Hack", "Fakelag")
		{
			ADDCVAR("Hack vs Hack", "Fakelag", "Method");
			ADDCVAR("Hack vs Hack", "Fakelag", "Value");
		}
	}

	SpewVars();
}

/*static void _DrawListView(int x, int y, int w, int lines, const char** pItems, int nItems, int& value, int& curline)
{
	int h = lines * 15;

	DrawingHelper::DrawFilledRect(x, y, w, h, Color(80, 80, 80));

	if (MouseInArea(x, y, w, lines * 15) && nItems > lines)
	{
		if (s_iMouseWheelScroll == 1)
		{
			curline--;
		}
		else if (s_iMouseWheelScroll == -1)
		{
			curline++;
		}
	}

	if(curline > nItems)
	{
		curline = 1;
		value = 0;
	}

	if (((nItems - lines + 1) > 0) && curline > (nItems - lines + 1))
	{
		curline = (nItems - lines + 1);
	}
	else if (curline == 0)
	{
		curline = 1;
	}

	if (value >= nItems)
	{
		value = nItems - 1;
	}
	else if (value < 0)
	{
		value = 0;
	}

	for (int offset = 0, idx = (curline - 1); offset < lines; idx++, offset++)
	{
		if (idx >= nItems)
		{
			break;
		}

		if (MouseInArea(x, y + 15 * offset, w, 15) && s_MouseLeftClick)
		{
			value = idx;
			break;
		}
	}

	for (int offset = 0, idx = (curline - 1); offset < lines; idx++, offset++)
	{
		if (idx >= nItems)
		{
			break;
		}

		//DrawingHelper::DrawFilledRect(x, y + 15 * offset, w, 15, Color(80, 80, 80));

		if (value == idx)
		{
			DrawingHelper::DrawFilledRect(x, y + 15 * offset, w, 15, s_MenuColor);
		}
		else if (MouseInArea(x, y + 15 * offset, w, 15))
		{
			DrawingHelper::DrawFilledRect(x, y + 15 * offset, w, 15, Color(s_MenuColor, 150));
		}

		DrawingHelper::GetTextSize(DrawingHelper::MenuFont, tw, th, pItems[idx]);

		DrawingHelper::DrawString(DrawingHelper::MenuFont, x + 8, y + 15 * offset + 15 / 2 - th / 2, Color(220, 220, 220), pItems[idx]);
	}

	if(nItems > lines)
	{
		DrawingHelper::DrawFilledRect(x + w - 5, y, 5, h, Color(30, 30, 30));
		
		int scrollh = h / 3;
		int hleft = h - scrollh;
		int scrollper = hleft / (nItems - lines);
		int scrolloffset = scrollper * (curline - 1);
		
		DrawingHelper::DrawFilledRect(x + w - 5, y + scrolloffset, 5, scrollh, Color(60, 60, 60));

		DrawingHelper::DrawOutlinedRect(x + w - 5, y, 5, h, Color(0, 0, 0));
	}

	DrawingHelper::DrawOutlinedRect(x, y, w, h, Color(0, 0, 0));
}*/

bool MouseInArea(int x, int y, int w, int h)
{
	int mx, my;
	Source::Interfaces::Surface->GetCursorPos(mx, my);

	return (
		mx >= x &&
		mx <= (x + w) &&
		my >= y &&
		my <= (y + h)
	);
}

static short s_iMouseWheelScroll;
static LONG s_lOldWndProc;
static LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

void Features::Visuals::DrawMenu()
{
	if (s_lOldWndProc == 0x0)
	{
		s_lOldWndProc = SetWindowLongPtr(GetForegroundWindow(), GWL_WNDPROC, (LONG)WndProc);
	}

	lua_State* L = LuaBase::GetState();

	if (L)
	{
		lua_getglobal(L, "menu");
		if (lua_istable(L, -1))
		{
			lua_getfield(L, -1, "Paint");
			if (lua_isfunction(L, -1))
			{
				if (lua_pcall(L, 0, 0, 0) != LUA_OK)
				{
					LuaBase::PrintError();
				}
			}
			else
			{
				lua_pop(L, 1);
			}
		}
		lua_pop(L, 1);
	}
}

bool Features::Visuals::ShouldDrawMenu()
{
	lua_State* L = LuaBase::GetState();

	bool bRet = false;

	if (L)
	{
		lua_getglobal(L, "menu");
		if (lua_istable(L, -1))
		{
			lua_getfield(L, -1, "ShouldDraw");
			if (lua_isfunction(L, -1))
			{
				if (lua_pcall(L, 0, LUA_MULTRET, 0) != LUA_OK)
				{
					LuaBase::PrintError();
				}
				else
				{
					bRet = lua_isboolean(L, -1) ? lua_toboolean(L, -1) : false;
					lua_pop(L, 1);
				}
			}
			else
			{
				lua_pop(L, 1);
			}
		}
		lua_pop(L, 1);
	}

	return bRet;
}

static LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_MOUSEWHEEL)
	{
		s_iMouseWheelScroll = GET_WHEEL_DELTA_WPARAM(wParam);

		if (s_iMouseWheelScroll > 1)
		{
			s_iMouseWheelScroll = 1;
		}
		else if (s_iMouseWheelScroll < -1)
		{
			s_iMouseWheelScroll = -1;
		}
	}

	return CallWindowProc((WNDPROC)s_lOldWndProc, hwnd, uMsg, wParam, lParam);
}

#define MAX_LINES 1024

struct SConsoleLine
{
	SConsoleLine(const char* pszLine)
	{
		m_pText = new char[strlen(pszLine) + 1];
		memcpy(m_pText, pszLine, strlen(pszLine) + 1);
	}

	~SConsoleLine()
	{
		if (m_pText)
		{
			delete[] m_pText;
		}
	}

	char* m_pText;
};

static LinkedList<SConsoleLine> s_Lines;
static int s_ConX, s_ConY;
static bool s_bConsoleDragging;

bool Features::Visuals::ShouldDrawConsole()
{
	return s_bDrawConsole;
}

static void DragConsole()
{
	static bool s_bSavePos = false;
	static int s_iSaveX, s_iSaveY;

	if (!Util::Input::GetButtonState(VK_LBUTTON))
	{
		s_bConsoleDragging = false;
		s_bSavePos = false;

		return;
	}

	if (!MouseInArea(s_ConX, s_ConY, 400, 20) && !s_bConsoleDragging)
	{
		s_bSavePos = false;

		return;
	}

	int mousex, mousey;
	Source::Interfaces::Surface->GetCursorPos(mousex, mousey);

	if (!s_bSavePos)
	{
		s_bSavePos = true;
		s_bConsoleDragging = true;

		s_iSaveX = mousex - s_ConX;
		s_iSaveY = mousey - s_ConY;
	}

	s_ConX = mousex - s_iSaveX;
	s_ConY = mousey - s_iSaveY;
}

static int s_Curline = s_Lines.GetSize() - 36;

static void DrawLines()
{
	int nCount = 0;
	auto pNode = s_Lines.GetHead();

	if (s_Lines.GetSize() > 36)
	{
		pNode = s_Lines.GetHead();

		for (int i = 0; i < (s_Curline - 1); i++)
		{
			pNode = pNode->m_pNext;
		}

		if (s_iMouseWheelScroll != 0)
		{
			s_Curline -= 1 * s_iMouseWheelScroll;
		}

		if (s_Curline > s_Lines.GetSize())
		{
			s_Curline = 1;
		}

		float scrollh = ((float)4096 / (float)(s_Lines.GetSize() - 36)) * (7.5f);
		if (scrollh < (float)15)
		{
			scrollh = (float)15;
		}
		else if (scrollh > (float)308)
		{
			scrollh = (float)308;
		}

		if (MouseInArea(s_ConX + 400 - 10, s_ConY + 25, 5, 400 - 30) && Util::Input::GetButtonState(VK_LBUTTON))
		{
			int x, y;
			Source::Interfaces::Surface->GetCursorPos(x, y);

			s_Curline = ((float)(y - (s_ConY + 25)) / (float)370) * (s_Lines.GetSize() - 36);

			//Debug::PrintToConsole("%d", s_Curline);
		}

		if (((s_Lines.GetSize() - 36 + 1) > 0) && s_Curline > (s_Lines.GetSize() - 36 + 1))
		{
			s_Curline = (s_Lines.GetSize() - 36 + 1);
		}
		else if (s_Curline <= 0)
		{
			s_Curline = 1;
		}

		float hleft = 370 - scrollh;
		float scrollper = hleft / (s_Lines.GetSize() - 36);
		float scrolloffset = scrollper * (s_Curline - 1);

		DrawingHelper::DrawFilledRect(s_ConX + 400 - 10, s_ConY + 25, 5, 400 - 30, Color(0, 0, 0));
		DrawingHelper::DrawFilledRect(s_ConX + 400 - 10, s_ConY + 25 + scrolloffset, 5, scrollh, Color(120, 120, 120));
		DrawingHelper::DrawOutlinedRect(s_ConX + 400 - 10, s_ConY + 25, 5, 400 - 30, Color(0, 0, 0));
	}

	while (pNode && nCount < 36)
	{
		DrawingHelper::DrawString(DrawingHelper::DebugFont, s_ConX + 10, s_ConY + 30 + nCount * 10, Color(220, 220, 220), pNode->m_pThis->m_pText);

		pNode = pNode->m_pNext;
		nCount++;
	}
}

static void AddCharactersRecursive(const char* pszLine)
{
	char* pBuf = nullptr;
	int nCount = 0;

	while (*pszLine != '\0')
	{
		if (pBuf == nullptr)
		{
			pBuf = (char*)malloc(2);
		}
		else
		{
			pBuf = (char*)realloc(pBuf, nCount + 2);
		}

		pBuf[nCount] = *pszLine;
		pBuf[nCount + 1] = '\0';
		DrawingHelper::GetTextSize(DrawingHelper::DebugFont, tw, th, pBuf);

		if (tw >= 380)
		{
			pBuf[nCount] = '\0';

			Debug::PrintToConsole("%s", pBuf);

			free(pBuf);
			pBuf = nullptr;
			nCount = 0;
		}
		else
		{
			pszLine++;
			nCount++;
		}
	}

	if(pBuf)
	{
		Debug::PrintToConsole("%s", pBuf);

		free(pBuf);
	}
}

static void AddLineRecursive(const char* pszLine)// idek what im doing at this point
{
	ReverseStack<const char*> stack;

	for (int i = 0, start = 0; i <= strlen(pszLine); i++)
	{
		if (!(*(pszLine + i) == ' ') && *(pszLine + i) != '\0')
		{
			continue;
		}

		const char* ptr = (pszLine + i - 1);
		while (*ptr != ' ' && ptr > pszLine)
		{
			ptr--;
		}

		int add = 0;
		if (pszLine == ptr)
		{
			add = 1;
		}

		char* buf = new char[(pszLine + i) - ptr + add];
		memcpy(buf, (ptr == pszLine) ? pszLine : (ptr + 1), ((pszLine + i - 1) - ptr) + add);
		buf[(pszLine + i) - ptr - 1 + add] = '\0';

		start = i + 1;

		stack.Push(buf);
	}

	if (stack.IsEmpty())
	{
		AddCharactersRecursive(pszLine);
		return;
	}

	DrawingHelper::GetTextSize(DrawingHelper::DebugFont, tw, th, stack.GetTop(nullptr));
	if (tw >= 380)
	{
		AddCharactersRecursive(pszLine);

		while (stack.GetTop(nullptr))
		{
			delete[] stack.GetTop(nullptr);
			stack.Pop();
		}

		return;
	}

	char* pBuf = nullptr;
	while (stack.GetTop(nullptr))
	{
		const char* pWord = stack.GetTop(nullptr);

		if (!pBuf)
		{
			pBuf = (char*)malloc(strlen(pWord) + 1);
			memcpy(pBuf, pWord, strlen(pWord));
			pBuf[strlen(pWord)] = '\0';

			DrawingHelper::GetTextSize(DrawingHelper::DebugFont, tw, th, pBuf);
			if (tw >= 380)
			{
				break; // failed completly
			}

			delete[] pWord;
			stack.Pop();
		}
		else
		{
			char* pTmp = (char*)malloc(strlen(pWord) + strlen(pBuf) + 2);
			memcpy(pTmp, pBuf, strlen(pBuf));
			pTmp[strlen(pBuf)] = ' ';
			memcpy(pTmp + strlen(pBuf) + 1, pWord, strlen(pWord));
			pTmp[strlen(pWord) + strlen(pBuf) + 1] = '\0';

			DrawingHelper::GetTextSize(DrawingHelper::DebugFont, tw, th, pTmp);
			if (tw >= 380)
			{
				Debug::PrintToConsole("%s", pBuf);

				free(pBuf);
				free(pTmp);
				
				pBuf = nullptr;
			}
			else
			{
				free(pBuf);
				pBuf = pTmp;

				delete[] pWord;
				stack.Pop();
			}
		}

		/*delete[] pWord;
		stack.Pop();*/
	}

	if (pBuf)
	{
		DrawingHelper::GetTextSize(DrawingHelper::DebugFont, tw, th, pBuf);

		if (tw < 380)
		{
			Debug::PrintToConsole("%s", pBuf);
		}

		free(pBuf);
	}

	while (stack.GetTop(nullptr))
	{
		delete[] stack.GetTop(nullptr);
		stack.Pop();
	}
}

void Features::Visuals::AddLineToConsole(const char* pszFmt, ...)
{
	if (DrawingHelper::DebugFont == 0)
	{
		DrawingHelper::DebugFont = Source::Interfaces::Surface->CreateFont();
		Source::Interfaces::Surface->SetFontGlyphSet(DrawingHelper::DebugFont, "Consolas", 12, 500, 0, 0, 0, 0, 0);
	}

	bool bSet = ((s_Lines.GetSize() - 35) == s_Curline);

	while (s_Lines.GetSize() > MAX_LINES)
	{
		s_Lines.Remove(s_Lines.GetHead());
	}

	char szBuf[2048 + 1] = { 0 };

	va_list list;
	va_start(list, pszFmt);
	vsprintf_s(szBuf, pszFmt, list);
	va_end(list);

	DrawingHelper::GetTextSize(DrawingHelper::DebugFont, tw, th, szBuf);
	if (tw >= 380)
	{	
		AddLineRecursive(szBuf);
	}
	else
	{
		SConsoleLine* pNewLine = new SConsoleLine(szBuf);
		s_Lines.Insert(pNewLine);
	}

	if (bSet)
	{
		s_Curline = s_Lines.GetSize() - 35;
	}

	/*SConsoleLine* pNew = new SConsoleLine(szBuf);
	s_Lines.Insert(pNew);*/
}

void Features::Visuals::DrawConsole()
{
	if (Util::Input::GetButtonState(VK_DELETE) & 1)
	{
		s_bDrawConsole = !s_bDrawConsole;
	}
	
	if (!s_bDrawConsole)
	{
		s_iMouseWheelScroll = 0;
		return;
	}

	DragConsole();

	DrawingHelper::DrawFilledRect(s_ConX, s_ConY, 400, 400, Color(50, 50, 50));
	DrawingHelper::DrawFilledRect(s_ConX, s_ConY, 400, 20, Color(255, 93, 0));

	DrawingHelper::DrawOutlinedRect(s_ConX, s_ConY, 400, 400, Color(0, 0, 0));
	DrawingHelper::DrawOutlinedRect(s_ConX, s_ConY, 400, 20, Color(0, 0, 0));

	DrawingHelper::GetTextSize(DrawingHelper::TitleFont, tw, th, "Console");
	DrawingHelper::DrawString(DrawingHelper::TitleFont, s_ConX + (400) / 2 - tw / 2, s_ConY + (10) - th / 2 - 1, Color(220, 220, 220), "Console");

	DrawingHelper::DrawFilledRect(s_ConX + 5, s_ConY + 25, 390, 400 - 30, Color(30, 30, 30));

	DrawLines();

	//DrawMouse();

	s_iMouseWheelScroll = 0;
}