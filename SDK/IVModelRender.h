#ifndef __HEADER_IVMODELRENDER__
#define __HEADER_IVMODELRENDER__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

#include "IMaterialSystem.h"
#include "IStudioRender.h"

namespace Source
{
	class IVModelRender;

	namespace Interfaces
	{
		extern IVModelRender* ModelRender;
	}
}

class Source::IVModelRender
{
public:
	static IVModelRender* GetPtr();

	void ForcedMaterialOverride(IMaterial* material, OverrideType_t overridetype = OVERRIDE_NORMAL);
};

#endif