#ifndef __HEADER_PREDICTION_SOURCE__
#define __HEADER_PREDICTION_SOURCE__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace PositionAdjustment
	{
		class Prediction;
	}
}

class Features::PositionAdjustment::Prediction
{
public:
	static void Start(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd);
	static void End(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd);

	static void SetIsInPrediction(bool bPrediction);
	static bool GetIsInPrediction();

	static void AdjustTickBase(Source::CBasePlayer* pLocal);
	static void BackupGlobals();
	static void RestoreGlobals();

	static void SetPredictionPlayer(Source::CBasePlayer* pPlayer);
	static Source::CBasePlayer** GetPredictionPlayer();

	static void SetPredictionSeed(int iSeed);
	static int* GetPredictionSeed();

	static Source::IMoveHelper* GetMoveHelper();

	static void DoWeaponSelection(Source::CBasePlayer* pPlayer, Source::CUserCmd* cmd);
	static void DoUpdateButtonState(Source::CBasePlayer* pPlayer, Source::CUserCmd* cmd);
	static void RunPreThink(Source::CBasePlayer* pPlayer);
	static void RunThink(Source::CBasePlayer* pPlayer);

	static void CheckForTeleports();

	struct PredictionData
	{
		Vector m_vecOrigin;
		Vector m_vecVelocity;
	};
private:
	static bool m_bInPrediction;
	static int m_nOldTickBase;
	static int m_nPushTickBase;
	static float m_flOldCurTime;
	static float m_flOldFrameTime;
	static int m_nOldRandomSeed;
	static float m_flOldSvFootstepsValue;
};

#endif