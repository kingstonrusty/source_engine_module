#include "IVModelRender.h"

namespace Source
{
	namespace Interfaces
	{
		IVModelRender* ModelRender;
	}
}

using namespace Source;

IVModelRender* IVModelRender::GetPtr()
{
	return (IVModelRender*)::Util::BruteforceInterface("VEngineModel", "engine.dll");
}

void IVModelRender::ForcedMaterialOverride(IMaterial* material, OverrideType_t overridetype)
{
	return GET_VFUNC(void(__thiscall*)(IVModelRender*, IMaterial*, OverrideType_t), this, IVMODELRENDER_FORCEDMATERIALOVERRIDE)(this, material, overridetype);
}