#include "Surface.h"

#include "SDK/Source.h"
#include "Util/DrawingHelper.h"

int Surface::l_SetDrawColor(lua_State* L)
{
	int type = lua_type(L, 1);
	if (type == LUA_TTABLE) // called with Color
	{
		Color clr;

		{
			lua_getfield(L, 1, "r");
			clr.r = luaL_checknumber(L, -1);
			lua_pop(L, 1);
		}

		{
			lua_getfield(L, 1, "g");
			clr.g = luaL_checknumber(L, -1);
			lua_pop(L, 1);
		}

		{
			lua_getfield(L, 1, "b");
			clr.b = luaL_checknumber(L, -1);
			lua_pop(L, 1);
		}

		{
			lua_getfield(L, 1, "a");
			clr.a = luaL_checknumber(L, -1);
			lua_pop(L, 1);
		}

		Source::Interfaces::Surface->DrawSetColor(clr);
	}
	else if (type == LUA_TNUMBER) // called with r,g,b [a]
	{
		int r = luaL_checknumber(L, 1);
		int g = luaL_checknumber(L, 2);
		int b = luaL_checknumber(L, 3);
		int a = 255;

		if (lua_isnumber(L, 4))
		{
			a = lua_tonumber(L, 4);
		}

		Source::Interfaces::Surface->DrawSetColor(Color(r, g, b, a));
	}
	else
	{
		luaL_error(L, "surface.SetDraWColor: invalid arguments");
	}

	return 0;
}

int Surface::l_DrawFilledRect(lua_State* L)
{
	int x = luaL_checknumber(L, 1);
	int y = luaL_checknumber(L, 2);
	int w = luaL_checknumber(L, 3);
	int h = luaL_checknumber(L, 4);

	Source::Interfaces::Surface->DrawFilledRect(x, y, x + w, y + h);

	return 0;
}

int Surface::l_DrawOutlinedRect(lua_State* L)
{
	int x = luaL_checknumber(L, 1);
	int y = luaL_checknumber(L, 2);
	int w = luaL_checknumber(L, 3);
	int h = luaL_checknumber(L, 4);

	Source::Interfaces::Surface->DrawOutlinedRect(x, y, x + w, y + h);

	return 0;
}

int Surface::l_DrawLine(lua_State* L)
{
	int x1 = luaL_checknumber(L, 1);
	int y1 = luaL_checknumber(L, 2);
	int x2 = luaL_checknumber(L, 3);
	int y2 = luaL_checknumber(L, 4);

	Source::Interfaces::Surface->DrawLine(x1, y1, x2, y2);

	return 0;
}

class CFont
{
public:
	CFont(const char* pszName, const char* pszWindowsName, int t, int w, bool shadow, bool antialias)
	{
		m_pszName = _strdup(pszName);
		m_pszWindowsName = _strdup(pszWindowsName);

		int flags = 0;

		if (shadow)
		{
			flags |= Source::FONTFLAG_DROPSHADOW;
		}

		if (antialias)
		{
			flags |= Source::FONTFLAG_ANTIALIAS;
		}

		m_hFont = Source::Interfaces::Surface->CreateFont();
		Source::Interfaces::Surface->SetFontGlyphSet(m_hFont, pszWindowsName, t, w, 0, 0, flags, 0, 0);

		m_Tall = t;
		m_Weight = w;
		m_Flags = flags;
	}

	~CFont()
	{
		free((void*)m_pszName);
		free((void*)m_pszWindowsName);
	}

	const char* GetName()
	{
		return m_pszName;
	}

	Source::HFONT GetFont()
	{
		return m_hFont;
	}

	void SetWindowsFontName(const char* pszWindowsName)
	{
		if (m_pszWindowsName)
		{
			free((void*)m_pszWindowsName);
		}

		m_pszWindowsName = _strdup(pszWindowsName);
	}

	const char* GetWindowsFontName()
	{
		return m_pszWindowsName;
	}

	void SetTall(int t)
	{
		m_Tall = t;
	}

	int GetTall()
	{
		return m_Tall;
	}

	void SetWeight(int w)
	{
		m_Weight = w;
	}

	int GetWeight()
	{
		return m_Weight;
	}

	void SetFlags(int f)
	{
		m_Flags = f;
	}

	int GetFlags()
	{
		return m_Flags;
	}
private:
	const char* m_pszName;
	const char* m_pszWindowsName;
	Source::HFONT m_hFont;
	int m_Tall;
	int m_Weight;
	int m_Flags;
};

static bool s_bReset = false;

static void InitDefaultFonts()
{
	using namespace Util;

	if (DrawingHelper::MenuFont == 0)
	{
		DrawingHelper::MenuFont = Source::Interfaces::Surface->CreateFont();
		Source::Interfaces::Surface->SetFontGlyphSet(DrawingHelper::MenuFont, "", 14, 500, 0, 0, Source::FONTFLAG_DROPSHADOW, 0, 0);
	}

	if (s_bReset && DrawingHelper::MenuFont)
	{
		Source::Interfaces::Surface->SetFontGlyphSet(DrawingHelper::MenuFont, "", 14, 500, 0, 0, Source::FONTFLAG_DROPSHADOW, 0, 0);
	}

	if (DrawingHelper::TitleFont == 0)
	{
		DrawingHelper::TitleFont = Source::Interfaces::Surface->CreateFont();
		Source::Interfaces::Surface->SetFontGlyphSet(DrawingHelper::TitleFont, "", 16, 500, 0, 0, Source::FONTFLAG_DROPSHADOW, 0, 0);
	}

	if (s_bReset && DrawingHelper::TitleFont)
	{
		Source::Interfaces::Surface->SetFontGlyphSet(DrawingHelper::TitleFont, "", 16, 500, 0, 0, Source::FONTFLAG_DROPSHADOW, 0, 0);
	}

	if (DrawingHelper::LargeTitleFont == 0)
	{
		DrawingHelper::LargeTitleFont = Source::Interfaces::Surface->CreateFont();
		Source::Interfaces::Surface->SetFontGlyphSet(DrawingHelper::LargeTitleFont, "", 32, 600, 0, 0, Source::FONTFLAG_ANTIALIAS, 0, 0);
	}

	if (s_bReset && DrawingHelper::LargeTitleFont)
	{
		Source::Interfaces::Surface->SetFontGlyphSet(DrawingHelper::LargeTitleFont, "", 32, 600, 0, 0, Source::FONTFLAG_ANTIALIAS, 0, 0);
	}

	if (DrawingHelper::DebugFont == 0)
	{
		DrawingHelper::DebugFont = Source::Interfaces::Surface->CreateFont();
		Source::Interfaces::Surface->SetFontGlyphSet(DrawingHelper::DebugFont, "Consolas", 12, 500, 0, 0, 0, 0, 0);
	}

	if (s_bReset && DrawingHelper::DebugFont)
	{
		Source::Interfaces::Surface->SetFontGlyphSet(DrawingHelper::DebugFont, "Consolas", 12, 500, 0, 0, 0, 0, 0);
	}
}

static LinkedList<CFont> s_Fonts;
static Source::HFONT s_TextFont;

int Surface::l_CreateFont(lua_State* L)
{
	const char* pszFont = luaL_checkstring(L, 1);

	if (!strcmp(pszFont, "Menu") || !strcmp(pszFont, "Title") || !strcmp(pszFont, "LargeTitle") || !strcmp(pszFont, "Debug"))
	{
		luaL_error(L, "Can't use '%s' as font name", pszFont);
		return 0;
	}

	CFont* pFound = nullptr;

	{
		auto pNode = s_Fonts.GetHead();
		while (pNode)
		{
			if (!strcmp(pszFont, pNode->m_pThis->GetName()))
			{
				pFound = pNode->m_pThis;
				break;
			}

			pNode = pNode->m_pNext;
		}
	}

	const char* pszWindowsName = luaL_checkstring(L, 2);
	int tall = luaL_checkinteger(L, 3);
	int weight = luaL_checkinteger(L, 4);
	
	if (!lua_isboolean(L, 5) && !lua_isnil(L, 5))
	{
		luaL_error(L, "Invalid argument #5: boolean or nil expected");
		return 0;
	}

	if (!lua_isboolean(L, 6) && !lua_isnil(L, 6))
	{
		luaL_error(L, "Invalid argument #6: boolean or nil expected");
		return 0;
	}

	bool dropshadow = (lua_isnil(L, 5)) ? false : lua_toboolean(L, 5);
	bool antialias = (lua_isnil(L, 6)) ? false : lua_toboolean(L, 6);

	if (pFound)
	{
		int flags = 0;

		if (dropshadow)
		{
			flags |= Source::FONTFLAG_DROPSHADOW;
		}

		if (antialias)
		{
			flags |= Source::FONTFLAG_ANTIALIAS;
		}

		s_bReset = true;

		//Source::Interfaces::Surface->ResetFontCaches();
		Source::Interfaces::Surface->SetFontGlyphSet(pFound->GetFont(), pszWindowsName, tall, weight, 0, 0, flags, 0, 0);

		pFound->SetWindowsFontName(pszWindowsName);
		pFound->SetTall(tall);
		pFound->SetWeight(weight);
		pFound->SetFlags(flags);

		InitDefaultFonts();
		
		{
			auto pNode = s_Fonts.GetHead();
			while (pNode)
			{
				if (pNode->m_pThis != pFound)
				{
					Source::Interfaces::Surface->SetFontGlyphSet(pNode->m_pThis->GetFont(), pNode->m_pThis->GetWindowsFontName(), pNode->m_pThis->GetTall(), pNode->m_pThis->GetWeight(), 0, 0, pNode->m_pThis->GetFlags(), 0, 0);
				}

				pNode = pNode->m_pNext;
			}
		}

		s_bReset = false;
	}
	else
	{
		s_Fonts.Insert(new CFont(pszFont, pszWindowsName, tall, weight, dropshadow, antialias));
	}

	return 0;
}

int Surface::l_SetFont(lua_State* L)
{
	InitDefaultFonts();

	const char* pszFont = luaL_checkstring(L, 1);

	if (!strcmp(pszFont, "Menu"))
	{
		s_TextFont = Util::DrawingHelper::MenuFont;
		return 0;
	}
	else if (!strcmp(pszFont, "Title"))
	{
		s_TextFont = Util::DrawingHelper::TitleFont;
		return 0;
	}
	else if (!strcmp(pszFont, "LargeTitle"))
	{
		s_TextFont = Util::DrawingHelper::LargeTitleFont;
		return 0;
	}
	else if (!strcmp(pszFont, "Debug"))
	{
		s_TextFont = Util::DrawingHelper::DebugFont;
		return 0;
	}
	else
	{
		auto pNode = s_Fonts.GetHead();
		while (pNode)
		{
			if (!strcmp(pszFont, pNode->m_pThis->GetName()))
			{
				s_TextFont = pNode->m_pThis->GetFont();
				return 0;
			}

			pNode = pNode->m_pNext;
		}
	}

	luaL_error(L, "Font '%s' does not exist", pszFont);

	return 0;
}

static Color s_TextCol;

int Surface::l_SetTextColor(lua_State* L)
{
	int type = lua_type(L, 1);
	if (type == LUA_TTABLE) // called with Color
	{
		Color clr;

		{
			lua_getfield(L, 1, "r");
			clr.r = luaL_checknumber(L, -1);
			lua_pop(L, 1);
		}

		{
			lua_getfield(L, 1, "g");
			clr.g = luaL_checknumber(L, -1);
			lua_pop(L, 1);
		}

		{
			lua_getfield(L, 1, "b");
			clr.b = luaL_checknumber(L, -1);
			lua_pop(L, 1);
		}

		{
			lua_getfield(L, 1, "a");
			clr.a = luaL_checknumber(L, -1);
			lua_pop(L, 1);
		}

		s_TextCol = clr;
	}
	else if (type == LUA_TNUMBER) // called with r,g,b [a]
	{
		int r = luaL_checknumber(L, 1);
		int g = luaL_checknumber(L, 2);
		int b = luaL_checknumber(L, 3);
		int a = 255;

		if (lua_isnumber(L, 4))
		{
			a = lua_tonumber(L, 4);
		}

		s_TextCol = Color(r, g, b, a);
	}
	else
	{
		luaL_error(L, "surface.SetDraWColor: invalid arguments");
	}

	return 0;
}

static int s_TextX, s_TextY;

int Surface::l_SetTextPos(lua_State* L)
{
	int x = luaL_checknumber(L, 1);
	int y = luaL_checknumber(L, 2);

	s_TextX = x;
	s_TextY = y;

	return 0;
}

int Surface::l_DrawString(lua_State* L)
{
	InitDefaultFonts();

	const char* pszString = luaL_checkstring(L, 1);
	Util::DrawingHelper::DrawString(s_TextFont ? s_TextFont : Util::DrawingHelper::MenuFont, s_TextX, s_TextY, s_TextCol, pszString);

	return 0;
}

int Surface::l_GetTextSize(lua_State* L)
{
	InitDefaultFonts();

	int w, h;

	const char* pszString = luaL_checkstring(L, 1);
	Util::DrawingHelper::GetTextSize(s_TextFont ? s_TextFont : Util::DrawingHelper::MenuFont, w, h, pszString);

	lua_pushinteger(L, w);
	lua_pushinteger(L, h);

	return 2;
}

int Surface::l_GetScreenSize(lua_State* L)
{
	int w, h;
	Source::Interfaces::Surface->GetScreenSize(w, h);

	lua_pushinteger(L, w);
	lua_pushinteger(L, h);

	return 2;
}