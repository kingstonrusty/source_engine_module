#ifndef __HEADER_STDAFX__
#define __HEADER_STDAFX__
#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES

#include <Windows.h>
#include <stdio.h>
#include <Psapi.h>
#include <cmath>
#include <float.h>
#include <intrin.h> 

#define M_PI_F (float)M_PI

#include "Multigame/Indices.h"
#include "Multigame/Offsets.h"
#include "Multigame/Signatures.h"

#include "Debug/PrintToConsole.h"

#include "Memory/SigScan.h"

#include "SDK/matrix3x4.h"
#include "SDK/Vector.h"
#include "SDK/Color.h"

#include "SDK/LinkedList.h"
#include "SDK/SmallVector.h"
#include "SDK/KeyValues.h"
#include "SDK/Stack.h"

#include "Lua/Base.h"

#define GET_VFUNC(FN, X, IDX) \
((FN)(((*(DWORD_PTR**)((DWORD_PTR)(X)))[(IDX)])))

#endif