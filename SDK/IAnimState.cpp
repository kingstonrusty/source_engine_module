#include "IAnimState.h"

using namespace Source;

struct MultiPlayerMovementData_t
{
	float		m_flWalkSpeed;
	float		m_flRunSpeed;
	float		m_flSprintSpeed;
	float		m_flBodyYawRate;
};

IAnimState::IAnimState(CBasePlayer* pPlayer)
{
#if defined(GAME_GMOD) || defined(GAME_BB2)
	{ // constructor
		MultiPlayerMovementData_t data;
#if defined(GAME_GMOD)
		data.m_flBodyYawRate = 720.0f;
		data.m_flRunSpeed = 320.0f;
		data.m_flWalkSpeed = 75.0f;
		data.m_flSprintSpeed = -1.f;
#elif defined(GAME_BB2)
		data.m_flBodyYawRate = 720.0f;
		data.m_flRunSpeed = 160.0f;
		data.m_flWalkSpeed = 70.0f;
		data.m_flSprintSpeed = -1.f;
#endif

		static IAnimState*(__thiscall* pConstructor)(IAnimState*, CBasePlayer*, MultiPlayerMovementData_t*) = Memory::FindPattern<IAnimState*(__thiscall*)(IAnimState*, CBasePlayer*, MultiPlayerMovementData_t*)>(IANIMSTATE_CONSTRUCTOR, "client.dll");

		pConstructor(this, pPlayer, &data);
	}

	{ // vmt
		static DWORD_PTR dwpVMT = *Memory::FindPattern<DWORD_PTR*>(IANIMSTATE_VTABLE, "client.dll");

		*(DWORD_PTR*)(this) = dwpVMT;
	}

	{ // rest
#if defined(GAME_GMOD)
 		*(CBasePlayer**)((DWORD_PTR)this + 244) = pPlayer;
#elif defined(GAME_BB2)
		*(CBasePlayer**)((DWORD_PTR)this + 248) = pPlayer;
#endif
	}

	//ClearAnimationState();
	//Vector eye = GET_VFUNC(Vector&(__thiscall*)(CBasePlayer*), GetOuter(), 140)(GetOuter());
	//Update(eye.y, eye.x);
	//DebugShowAnimState(false);
#endif
}

CBasePlayer* IAnimState::GetOuter()
{
#if defined(GAME_GMOD)
	return *(CBasePlayer**)((DWORD_PTR)this + 244);
#endif
}

void IAnimState::ClearAnimationState()
{
	return GET_VFUNC(void(__thiscall*)(IAnimState*), this, IANIMSTATE_CLEARANIMATIONSTATE)(this);
}

int IAnimState::CalcMainActivity()
{
	return GET_VFUNC(int(__thiscall*)(IAnimState*), this, IANIMSTATE_CALCMAINACTIVITY)(this);
}

void IAnimState::Update(float flEyeYaw, float flEyePitch)
{
	return GET_VFUNC(void(__thiscall*)(IAnimState*, float, float), this, IANIMSTATE_UPDATE)(this, flEyeYaw, flEyePitch);
}

void IAnimState::DebugShowAnimState(bool bIsServer)
{
	return GET_VFUNC(void(__thiscall*)(IAnimState*, bool), this, IANIMSTATE_DEBUGSHOWANIMSTATE)(this, bIsServer);
}

void IAnimState::ComputeSequences(studiohdr_t* pHdr)
{
	return GET_VFUNC(void(__thiscall*)(IAnimState*, studiohdr_t*), this, IANIMSTATE_COMPUTESEQUENCES)(this, pHdr);
}

bool IAnimState::ShouldUpdateAnimState()
{
	return GET_VFUNC(bool(__thiscall*)(IAnimState*), this, IANIMSTATE_SHOULDUPDATEANIMSTATE)(this);
}