#ifndef __HEADER_LUA_BASE__
#define __HEADER_LUA_BASE__
#pragma once

#include "Core/lua.hpp"

#include "SDK/CBaseEntity.h"

class LuaBase
{
public:
	static void Init(bool bForceNew = false);
	static void Refresh();

	static void RunFile(const char* pszFileName);
	static void PrintError();

	static lua_State* GetState();

	//static void RegisterPlugin(const char* pszName, const char* pszTable);
	//static void FillPlugins(SmallVector<const char*>* pItems);
private:
	static lua_State* m_pState;
};

extern void lua_pushentity(lua_State* L, Source::CBaseEntity* entity);

#endif