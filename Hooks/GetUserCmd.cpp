#include "GetUserCmd.h"

Source::CUserCmd* __fastcall Hooks::Input::GetUserCmd(Source::CInput* pThis, void*, int sequence_number)
{
	pVmt->Unhook();
	auto pRet = pThis->GetUserCmd(sequence_number);
	pVmt->Rehook();

	return pRet;
}