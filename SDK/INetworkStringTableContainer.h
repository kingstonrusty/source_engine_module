#ifndef __HEADER_INETWORKSTRINGTABLECONTAINER__
#define __HEADER_INETWORKSTRINGTABLECONTAINER__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

namespace Source
{
	const unsigned short INVALID_STRING_INDEX = (unsigned short)-1;

	class INetworkStringTable;

	class INetworkStringTableContainer;

	namespace Interfaces
	{
		extern INetworkStringTableContainer* StringTableContainer;
	}
}

class Source::INetworkStringTable
{
public:
	const char* GetName();
	int GetNumStrings();
	int AddString(bool isserver, const char* value, int length = -1, const void* userdata = nullptr);
	const char* GetString(int index);
};

class Source::INetworkStringTableContainer
{
public:
	static INetworkStringTableContainer* GetPtr();

	INetworkStringTable* FindTable(const char* name);
	INetworkStringTable* GetTable(int id);
	int GetNumTables();
};

#endif