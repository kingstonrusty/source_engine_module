#ifndef __HEADER_CBASEPLAYER__
#define __HEADER_CBASEPLAYER__
#pragma once

#include "CBaseEntity.h"
#include "CUserCmd.h"

namespace Source
{
	class IAnimState;

	class CBasePlayer;
}

class Source::CBasePlayer : public CBaseEntity
{
public:
	bool IsAlive();

	IAnimState* GetAnimState();
	void SetAnimState(IAnimState* pState);

	CUserCmd* GetCurrentCommand();
	void SetCurrentCommand(CUserCmd* pCmd);

	bool IsTV();

	NETVAR(char, "DT_BasePlayer", "m_lifeState", LifeState);

	NETVAR(int, "DT_BasePlayer", "m_nTickBase", TickBase);
	NETVAR(int, "DT_BasePlayer", "m_iTeamNum", TeamNum);

	NETVAR(float, "DT_BasePlayer", "m_flNextAttack", NextAttack);
	NETVAR(float, "DT_BasePlayer", "m_flFallVelocity", FallVelocity);

	NETVAR(EHANDLE, "DT_BasePlayer", "m_hActiveWeapon", ActiveWeaponHandle);
	NETVAR(EHANDLE*, "DT_BasePlayer", "m_hMyWeapons", MyWeapons);

#if defined(GAME_GMOD)
	NETVAR_VECTOR("DT_HL2MP_Player", "m_vecPunchAngle", PunchAngle);
#elif defined(GAME_L4D2)
	NETVAR_VECTOR("DT_TerrorPlayer", "m_vecPunchAngle", PunchAngle);
#else
	NETVAR_VECTOR("DT_BasePlayer", "m_vecPunchAngle", PunchAngle);
#endif

#if defined(GAME_GMOD)
	NETVAR_VECTOR("DT_BaseCombatCharacter", "m_vecViewOffset[0]", ViewOffset);
#else
	NETVAR_VECTOR("DT_BasePlayer", "m_vecViewOffset[0]", ViewOffset);
#endif

	NETVAR_VECTOR("DT_GMOD_Player", "m_angEyeAngles[0]", EyeAngles);
	NETVAR_VECTOR("DT_BasePlayer", "m_vecBaseVelocity", BaseVelocity);

#if defined(GAME_L4D2)
	NETVAR_VECTOR("DT_BasePlayer", "m_vecVelocity[0]", Velocity);
#endif

#if defined(GAME_CSS_STEAM)
	NETVAR_VECTOR("DT_BasePlayer", "m_vecVelocity[0]", Velocity)
#endif
};

#endif