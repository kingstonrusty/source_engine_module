#ifndef __HEADER_SCENENED__
#define __HEADER_SCENENED__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace RenderView
	{
		extern CVMTHookManager* pVmt;

		void __fastcall SceneEnd(Source::IVRenderView* pThis, void*);
	}
}

#endif