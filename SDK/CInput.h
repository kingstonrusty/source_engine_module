#ifndef __HEADER_CINPUT__
#define __HEADER_CINPUT__
#pragma once

#include "IBaseClientDLL.h"

#include "CUserCmd.h"

namespace Source
{
#if defined(GAME_GMOD)
	class CVerifiedUserCmd;
#endif

	class CInput;

	namespace Interfaces
	{
		extern CInput* Input;
	}
}

#if defined(GAME_GMOD)
class Source::CVerifiedUserCmd
{
public:
	void SetUserCmd(CUserCmd* cmd);
	CUserCmd* m_cmd() const { return (CUserCmd*)&m_cmdpad[0]; }

	byte m_cmdpad[352];
	unsigned int m_crc;
};
#endif

class Source::CInput
{
public:
	static CInput* GetPtr();

#if defined(GAME_GMOD)
	CVerifiedUserCmd* GetVerifiedUserCmd(int sequence_number);
#endif

	CUserCmd* GetUserCmd(int sequence_number);
	int CAM_IsThirdPerson();

	int& m_fCameraInThirdPerson();
};

#endif