#ifndef __HEADER_CVMTHOOKMANAGER__
#define __HEADER_CVMTHOOKMANAGER__
#pragma once

namespace Hooks
{
	class CVMTHookManager;
	class CVMTHookUnsafeManager;
}

class Hooks::CVMTHookManager
{
public:
	CVMTHookManager(void* pVmt);
	~CVMTHookManager();
	
	void Hook(bool bHook);
	void Unhook();
	void Rehook();

	void HookFunction(int idx, void* pFunction);
	void UnhookFunction(int idx);
private:
	void*** m_pppBase;
	void** m_ppOriginal;
	void** m_ppNew;
};

class Hooks::CVMTHookUnsafeManager
{
public:
	CVMTHookUnsafeManager(void* pVmt);
	~CVMTHookUnsafeManager();

	void HookFunction(int idx, void* pFunction);
	void UnhookFunction(int idx);
private:
	struct Hook
	{
		int m_nIdx;
		void* m_pOrig;
	};

	LinkedList<Hook> m_Hooks;
	void** m_ppVmt;
};

#endif