#ifndef __HEADER_BASELIBRARY__
#define __HEADER_BASELIBRARY__
#pragma once

class CStringPtr
{
public:
	CStringPtr(const char* pszString)
	{
		m_pszPtr = pszString;
	}

	~CStringPtr()
	{
		free((void*)m_pszPtr);
	}

	const char* GetString()
	{
		return m_pszPtr;
	}
private:
	const char* m_pszPtr;
};

template< class _T >
class BaseLibrary
{
public:
	static const char* GetName()
	{
		return "BaseLibrary";
	}

	static CStringPtr GetTable()
	{
		char* buf = (char*)malloc(strlen(_T::_GetTable()) + 1);
		strcpy(buf, _T::_GetTable());

		return _strlwr(buf);
	}

	static void Init(lua_State* L)
	{
		Debug::PrintToConsole("Initializing library '%s' ('%s')", _T::GetName(), _T::GetTable());

		lua_getglobal(L, _T::GetTable().GetString());

		if (lua_isnil(L, -1))
		{
			lua_pop(L, 1);
			lua_newtable(L);
		}

		luaL_setfuncs(L, _T::m_Register, 0);
		lua_setglobal(L, _T::GetTable().GetString());
	}

	static const char* _GetTable()
	{
		return _T::GetName();
	}

	static constexpr const struct luaL_Reg m_Register[] = 
	{
		{ NULL, NULL }
	};
};

#endif