#include "DefaultOverrides.h"

int DefaultOverrides::l_print(lua_State* L)
{
	int nArgs = lua_gettop(L);
	char szBuf[2048 + 1] = { 0 };

	for (int i = 1; i <= nArgs; ++i)
	{
		luaL_checkany(L, i);
		if (luaL_callmeta(L, i, "__tostring"))  /* is there a metafield? */
		{
			sprintf_s(szBuf, "%s   %s", szBuf, lua_tostring(L, -1));
			lua_pop(L, 1);

			continue;
		}

		switch (lua_type(L, i)) {
		case LUA_TNUMBER:
			sprintf_s(szBuf, "%s   %s", szBuf, lua_tostring(L, i));
			break;
		case LUA_TSTRING:
			sprintf_s(szBuf, "%s   %s", szBuf, lua_tostring(L, i));
			break;
		case LUA_TBOOLEAN:
			sprintf_s(szBuf, "%s   %s", szBuf, (lua_toboolean(L, i) ? "true" : "false"));
			break;
		case LUA_TNIL:
			sprintf_s(szBuf, "%s   %s", szBuf, "nil");
			break;
		default:
			sprintf_s(szBuf, "%s   %s: %p", szBuf, luaL_typename(L, i), lua_topointer(L, i));
			break;
		}
	}

	sprintf_s(szBuf, "%s", &szBuf[3]);

	Debug::PrintToConsole(szBuf);

	return 0;
}
