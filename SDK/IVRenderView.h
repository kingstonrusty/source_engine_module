#ifndef __HEADER_IVRENDERVIEW__
#define __HEADER_IVRENDERVIEW__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

namespace Source
{
	class IVRenderView;

	class CViewSetup;

	typedef class VPlane *Frustum;

	namespace Interfaces
	{
		extern IVRenderView* RenderView;
	}
}

class Source::CViewSetup
{
public:
	CViewSetup() {}

	CViewSetup(const CViewSetup& other)
	{
		memcpy(this, &other, sizeof(__pad));
	}

	int& x()
	{
		return *(int*)((DWORD_PTR)this);
	}

	int& y()
	{
		return *(int*)((DWORD_PTR)this + 8);
	}

	int& w()
	{
		return *(int*)((DWORD_PTR)this + 16);
	}

	int& h()
	{
		return *(int*)((DWORD_PTR)this + 24);
	}

	float& fov()
	{
		return *(float*)((DWORD_PTR)this + 56);
	}

	Vector& origin()
	{
		return *(Vector*)((DWORD_PTR)this + 64);
	}

	Vector& angles()
	{
		return *(Vector*)((DWORD_PTR)this + 76);
	}
private:
#if defined(GAME_GMOD)
	byte __pad[196];
#else
	byte __pad[196];
#endif
};

class Source::IVRenderView
{
public:
	static IVRenderView* GetPtr();

	void SetColorModulation(float* color);
	void SetColorModulation(Color clr);
	void SceneEnd();
#if defined(GAME_GMOD)
	void Push3DView(const CViewSetup& view, int flags, void* rendertarget, Frustum frustumPlanes);
	//void Push2DView(const CViewSetup& view, int flags, void* rendertarget, Frustum frustumPlanes);
	void PopView(Frustum frustumPlanes);
#endif
};

#endif