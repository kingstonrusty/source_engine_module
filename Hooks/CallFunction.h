#ifndef __HEADER_CALLFUNCTION_GMOD__
#define __HEADER_CALLFUNCTION_GMOD__
#pragma once

#include "CDetour.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace CScriptedEntity
	{
		extern CDetour* pCallFunction;

		bool __fastcall CallFunction(void* pThis, void*, int a2);
	}
}

#endif