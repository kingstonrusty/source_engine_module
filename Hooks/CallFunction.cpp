#include "CallFunction.h"

#include "Features/PositionAdjustment/Prediction.h"

#if defined(GAME_GMOD)

bool __fastcall Hooks::CScriptedEntity::CallFunction(void* pThis, void*, int a2)
{
	if (pThis && (a2 == 154 || a2 == 155)) // mac libs :-)
	{
		Source::CBaseEntity* pEntity = (Source::CBaseEntity*)((DWORD_PTR)pThis - 6296);
		if (pEntity && strncmp(pEntity->GetClassName(), "fas2", 4) == 0)
		{
			if (Features::PositionAdjustment::Prediction::GetIsInPrediction() || !Source::Interfaces::Prediction->IsFirstTimePredicted())
			{
				return true;
			}
		}
	}

	pCallFunction->Unhook();
	auto bRet = pCallFunction->GetOriginal<bool(__thiscall*)(void*, int)>()(pThis, a2);
	pCallFunction->Rehook();

	return bRet;
}

#endif