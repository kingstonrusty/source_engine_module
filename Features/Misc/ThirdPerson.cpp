#include "ThirdPerson.h"

#include "Features/Visuals/Menu.h"

using namespace Features::Misc;

bool ThirdPerson::Run(Source::CViewSetup* pView)
{
	FINDVAR(misc_thirdperson, "Miscellaneous", "Thirdperson");
	FINDVAR(misc_thirdperson_collisions, "Miscellaneous", "Thirdperson", "Collisions");
	FINDVAR(misc_thirdperson_distance, "Miscellaneous", "Thirdperson", "Distance");

	if (!misc_thirdperson->GetBool())
	{
		return false;
	}

	Source::CBasePlayer* pLocal = (Source::CBasePlayer*)Source::Interfaces::EntityList->GetClientEntity(Source::Interfaces::Engine->GetLocalPlayer());
	if (pLocal == nullptr || !pLocal->IsAlive())
	{
		return false;
	}

	pView->origin() = pLocal->GetAbsOrigin() + pLocal->GetViewOffset();

	Vector forward;
	AngleVectors(pView->angles(), &forward);
	forward *= misc_thirdperson_distance->GetFloat();

	if (misc_thirdperson_collisions->GetBool())
	{
		Source::trace_t trace;
		Source::Ray_t ray;

		ray.Init(pView->origin(), pView->origin() - forward, Vector(-2.f, -2.f, -2.f), Vector(2.f, 2.f, 2.f));
		Source::Interfaces::TraceRay->TraceRay(ray, Source::MASK_SOLID, nullptr, &trace);

		pView->origin() = trace.endpos;
	}
	else
	{
		pView->origin() -= forward;
	}

	pView->fov() = 90.f;

	return true;
}