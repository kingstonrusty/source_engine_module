#ifndef __HEADER_BONECACHEHELPER__
#define __HEADER_BONECACHEHELPER__
#pragma once

#include "SDK/Source.h"

#include "Features/PositionAdjustment/History.h"

namespace Features
{
	namespace Misc
	{
		class BoneCacheHelper;
	}
}

class Features::Misc::BoneCacheHelper
{
public:
	static void BackupCache(Source::CBaseEntity* pEntity);
	static void RestoreCache(Source::CBaseEntity* pEntity);

	static void OverrideCache(Source::CBaseEntity* pEntity, Features::PositionAdjustment::History::Snapshot* pRecord);
private:
	struct BoneCacheBackup
	{
#if defined(GAME_L4D2)
		matrix3x4 m_Bones[128];
		int m_nCachedBones;
		int m_iMostRecentModelBoneCounter;
		int m_fReadableBones; // not sure if we need to store this
		int m_fWriteableBones;
		int m_iAccumulatedBoneMask;
		float m_flLastBoneSetupTime;
#else
		unsigned int m_hitboxCacheHandle;
#endif

		Vector m_vecOrigin;
		Vector m_vecAngles;
		Vector m_vecMins;
		Vector m_vecMaxs;
	};

	static BoneCacheBackup m_CacheBackup;
};

#endif