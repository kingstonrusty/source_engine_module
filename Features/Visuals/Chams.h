#ifndef __HEADER_CHAMS__
#define __HEADER_CHAMS__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace Visuals
	{
		class Chams;
	}
}

class Features::Visuals::Chams
{
public:
	static bool ShouldDrawChamsOnEntity(Source::CBaseEntity* pEntity);

	static void DrawAllEntities();
	static void DrawEntity(Source::CBaseEntity* pEntity);
	static Source::IMaterial* GetMaterial(bool bLit, bool bIgnoreZ, bool bWireframe);
private:
	static Source::IMaterial* m_pUnlit;
	static Source::IMaterial* m_pUnlitIgnoreZ;
	static Source::IMaterial* m_pLit;
	static Source::IMaterial* m_pLitIgnoreZ;
	static Source::IMaterial* m_pUnlitWireframe;
	static Source::IMaterial* m_pUnlitWireframeIgnoreZ;
	static Source::IMaterial* m_pLitWireframe;
	static Source::IMaterial* m_pLitWireframeIgnoreZ;
};

#endif