#ifndef __HEADER_IVENGINECLIENT__
#define __HEADER_IVENGINECLIENT__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

#include "CNetChan.h"

namespace Source
{
	class IVEngineClient;

	namespace Interfaces
	{
		extern IVEngineClient* Engine;
	}
}

class Source::IVEngineClient
{
public:
	static IVEngineClient* GetPtr();

	int GetLocalPlayer();
	void GetViewAngles(Vector& angles);
	void SetViewAngles(Vector& angles);
	bool IsInGame();
	CNetChan* GetNetChannelInfo();
#if defined(GAME_GMOD)
	bool IsTakingScreenshot();
#endif
	void ExecuteClientCmd(const char* cmd);
};

#endif