#include "CursorPos.h"

#include "Features/Visuals/Menu.h"

static int s_iMouseX, s_iMouseY;

void __fastcall Hooks::Surface::GetCursorPos(Source::ISurface* pThis, void*, int& x, int& y)
{
	if (Features::Visuals::ShouldDrawMenu() || Features::Visuals::ShouldDrawConsole())
	{
		x = s_iMouseX;
		y = s_iMouseY;
	}

	pVmt->Unhook();
	pThis->GetCursorPos(x, y);
	pVmt->Rehook();
}

void __fastcall Hooks::Surface::SetCursorPos(Source::ISurface* pThis, void*, int x, int y)
{
	s_iMouseX = x;
	s_iMouseY = y;

	if (Features::Visuals::ShouldDrawMenu() || Features::Visuals::ShouldDrawConsole())
	{
		return;
	}

	pVmt->Unhook();
	pThis->SetCursorPos(x, y);
	pVmt->Rehook();
}