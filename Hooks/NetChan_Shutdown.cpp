#include "NetChan_Shutdown.h"

#if defined(GAME_CSS_STEAM)

void __fastcall Hooks::NetChannel::Shutdown(Source::CNetChan* pThis, void*, const char* reason)
{
	pVmt->Unhook();
	delete pVmt;
	pVmt = nullptr;

	pThis->Shutdown(reason);
}

#endif