#include "Chams.h"

#include "Hooks/CVMTHookManager.h"

#include "Features/Visuals/Menu.h"

using namespace Features::Visuals;

Source::IMaterial* Chams::m_pUnlit;
Source::IMaterial* Chams::m_pUnlitIgnoreZ;
Source::IMaterial* Chams::m_pLit;
Source::IMaterial* Chams::m_pLitIgnoreZ;
Source::IMaterial* Chams::m_pUnlitWireframe;
Source::IMaterial* Chams::m_pUnlitWireframeIgnoreZ;
Source::IMaterial* Chams::m_pLitWireframe;
Source::IMaterial* Chams::m_pLitWireframeIgnoreZ;

static Source::IMaterial* _CreateMaterial(bool bUnlit, bool bIgnoreZ, bool bWireframe)
{
	static int nCount;
	nCount++;

	char szBuf[512];
	sprintf_s(szBuf, "\"%s\"\n{\n\t\"$basetexture\" \"vgui/white_additive\"\n\t\"$ignorez\" \"%d\"\n\t\"$nofog\" \"1\"\n\t\"$model\" \"1\"\n\t\"$halflambert\" \"1\"\n\t\"$wireframe\" \"%d\"\n}", bUnlit ? "UnlitGeneric" : "VertexLitGeneric", bIgnoreZ, bWireframe);

	char szName[64];
	sprintf_s(szName, "mat_%d.vmt", nCount);

	Source::KeyValues* pKeyValues = Source::KeyValues::AllocKeyValuesMemory();
	pKeyValues->Init(bUnlit ? "UnlitGeneric" : "VertexLitGeneric");
	pKeyValues->LoadFromBuffer(szName, szBuf);

	Source::IMaterial* pMaterial = Source::Interfaces::MaterialSystem->CreateMaterial(szName, pKeyValues);
	return pMaterial;
}

bool Chams::ShouldDrawChamsOnEntity(Source::CBaseEntity* pEntity)
{
	FINDVAR(visuals_filter_players, "Visuals", "Filter", "Players");
	FINDVAR(visuals_filter_enemy_only, "Visuals", "Filter", "Enemy Only");

	bool bAdd = false;

	if (!pEntity->IsPlayer())
	{
		auto L = LuaBase::GetState();
		if (L)
		{
			lua_getglobal(L, "hook");
			if (lua_istable(L, -1))
			{
				lua_getfield(L, -1, "Call");
				if (lua_isfunction(L, -1))
				{
					lua_pushstring(L, "Chams_ShouldPickupEntity");
					lua_pushentity(L, pEntity);

					if (lua_pcall(L, 2, 1, 0) != LUA_OK)
					{
						LuaBase::PrintError();
					}
					else
					{
						if (lua_isboolean(L, -1))
						{
							bAdd = lua_toboolean(L, -1);
						}

						lua_pop(L, 1);
					}
				}
				else
				{
					lua_pop(L, 1);
				}
			}

			lua_pop(L, 1);
		}
	}
	else 
	{
		if (visuals_filter_players->GetBool())
		{
			if (visuals_filter_enemy_only->GetBool())
			{
				Source::CBasePlayer* pLocal = (Source::CBasePlayer*)Source::Interfaces::EntityList->GetClientEntity(Source::Interfaces::Engine->GetLocalPlayer());

				bAdd = (pLocal) ? ((Source::CBasePlayer*)pEntity)->GetTeamNum() != pLocal->GetTeamNum() : true;
			}
			else
			{
				bAdd = true;
			}
		}
	}

	return bAdd;
}

#include "Hooks/DrawModel.h"

void Chams::DrawAllEntities()
{
	/*Hooks::StudioRender::pVmt->Unhook();

	for (int i = 1; i <= Source::Interfaces::EntityList->GetHighestEntityIndex(); i++)
	{
		Source::CBaseEntity* pEntity = (Source::CBaseEntity*)Source::Interfaces::EntityList->GetClientEntity(i);
		if (pEntity == nullptr)
		{
			return;
		}

		if (ShouldDrawChamsOnEntity(pEntity))
		{
			DrawEntity(pEntity);
		}
	}

	Hooks::StudioRender::pVmt->Rehook();*/
}

void Chams::DrawEntity(Source::CBaseEntity* pEntity)
{
	Color clr1 = Color(65, 255, 65);
	Color clr2 = Color(120, 255, 120);

	if (pEntity->IsPlayer())
	{
		if (((Source::CBasePlayer*)pEntity)->GetTeamNum() == 2)
		{
			clr1 = Color(255, 65, 65);
			clr2 = Color(255, 120, 120);
		}
		else
		{
			clr1 = Color(65, 65, 255);
			clr2 = Color(120, 120, 255);
		}
	}

	/*pRender->ForcedMaterialOverride(GetMaterial(false, true, true));
	pRender->SetColorModulation(Color(255, 255, 255));
	pRender->DrawModel(results, info, bonetoworld, flexweights, flexdelayedweights, modelorigin, flags);*/

	Source::Interfaces::ModelRender->ForcedMaterialOverride(GetMaterial(false, true, false));
	Source::Interfaces::RenderView->SetColorModulation(clr2);
	pEntity->GetRenderable()->DrawModel();

	Source::Interfaces::ModelRender->ForcedMaterialOverride(GetMaterial(false, false, false));
	Source::Interfaces::RenderView->SetColorModulation(clr1);
	pEntity->GetRenderable()->DrawModel();

	Source::Interfaces::ModelRender->ForcedMaterialOverride(nullptr);
}

Source::IMaterial* Chams::GetMaterial(bool bLit, bool bIgnoreZ, bool bWireframe)
{
	if (!m_pLit)
	{
		m_pUnlit = _CreateMaterial(true, false, false);
		m_pUnlitIgnoreZ = _CreateMaterial(true, true, false);

		m_pLit = _CreateMaterial(false, false, false);
		m_pLitIgnoreZ = _CreateMaterial(false, true, false);

		m_pUnlitWireframe = _CreateMaterial(true, false, true);
		m_pUnlitWireframeIgnoreZ = _CreateMaterial(true, true, true);

		m_pLitWireframe = _CreateMaterial(false, false, true);
		m_pLitWireframeIgnoreZ = _CreateMaterial(false, true, true);
	}

	if (bLit)
	{
		if (bIgnoreZ)
		{
			return (bWireframe) ? m_pLitWireframeIgnoreZ : m_pLitIgnoreZ;
		}
		else
		{
			return (bWireframe) ? m_pLitWireframe : m_pLit;
		}
	}
	else
	{
		if (bIgnoreZ)
		{
			return (bWireframe) ? m_pUnlitWireframeIgnoreZ : m_pUnlitIgnoreZ;
		}
		else
		{
			return (bWireframe) ? m_pUnlitWireframe : m_pUnlit;
		}
	}
}