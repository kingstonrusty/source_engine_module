#include "ICvar.h"

namespace Source
{
	namespace Interfaces
	{
		ICvar* Cvar;
	}
}

using namespace Source;

ICvar* ICvar::GetPtr()
{
	return (ICvar*)::Util::BruteforceInterface("VEngineCvar", "vstdlib.dll");
}

ConVar* ICvar::FindVar(const char* name)
{
	return GET_VFUNC(ConVar*(__thiscall*)(ICvar*, const char*), this, ICVAR_FINDVAR)(this, name);
}