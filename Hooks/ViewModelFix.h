#ifndef __HEADER_VIEWMODELFIX__
#define __HEADER_VIEWMODELFIX__
#pragma once

#include "CDetour.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace Misc
	{
		extern CDetour* pViewModelFix;

		int __fastcall ViewModelFix(Source::CBasePlayer* pThis, void*, int a2, int a3);
	}
}

#endif