#include "ILua.h"

#if defined(GAME_GMOD)

namespace Source
{
	namespace Interfaces
	{
		ILuaShared* Lua;
	}
}

using namespace Source;

ILuaShared* ILuaShared::GetPtr()
{
	return (ILuaShared*)::Util::BruteforceInterface("LUASHARED", "lua_shared.dll");
}

ILuaInterface* ILuaShared::GetLuaInterface(unsigned char state)
{
	return GET_VFUNC(ILuaInterface*(__thiscall*)(ILuaShared*, unsigned char), this, ILUASHARED_GETLUAINTERFACE)(this, state);
}

#endif