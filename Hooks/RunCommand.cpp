#include "RunCommand.h"

#include "Features/PositionAdjustment/Prediction.h"

void __fastcall Hooks::Prediction::RunCommand(Source::IPrediction* pThis, void*, Source::CBasePlayer* pPlayer, Source::CUserCmd* cmd, Source::IMoveHelper* pMoveHelper) 
{
	/*static int s_LastTickCount;
	if (Source::Interfaces::Globals->tickcount() == s_LastTickCount)
	{
		Features::PositionAdjustment::Prediction::Start(pPlayer, cmd);
		Features::PositionAdjustment::Prediction::End(pPlayer, cmd);

		return;
	}
	else
	{
		s_LastTickCount = Source::Interfaces::Globals->tickcount();
	}*/

	if (cmd->tick_count() == 0xFFFFFFFF)
	{
		return;
	}

	pVmt->Unhook();
	pThis->RunCommand(pPlayer, cmd, pMoveHelper);
	pVmt->Rehook();
}