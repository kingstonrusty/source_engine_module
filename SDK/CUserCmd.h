#ifndef __HEADER_CUSERCMD__
#define __HEADER_CUSERCMD__
#pragma once

namespace Source
{
	enum
	{
		IN_ATTACK = (1 << 0),
		IN_JUMP = (1 << 1),
		IN_DUCK = (1 << 2),
		IN_FORWARD = (1 << 3),
		IN_BACK = (1 << 4),
		IN_USE = (1 << 5),
		IN_LEFT = (1 << 7),
		IN_RIGHT = (1 << 8),
		IN_MOVELEFT = (1 << 9),
		IN_MOVERIGHT = (1 << 10),
		IN_ATTACK2 = (1 << 11),
		IN_RUN = (1 << 12),
		IN_RELOAD = (1 << 13),
		IN_SPEED = (1 << 17),
		IN_WALK = (1 << 18),
	};

	class CUserCmd;
}

class Source::CUserCmd
{
public:
	int& command_number();
	int& tick_count();
	Vector& viewangles();
	float& forwardmove();
	float& sidemove();
	float& upmove();
	int& buttons();
	byte& impulse();
	int& weaponselect();
	int& weaponsubtype();
	int& random_seed();
	short& mousedx();
	short& mousedy();

#if defined(GAME_GMOD)
	bool& worldclicking()
	{
		return *(bool*)((DWORD_PTR)this + 90);
	}

	Vector& worldclickingforward()
	{
		return *(Vector*)((DWORD_PTR)this + 92);
	}

	unsigned int GetCheckSum();
#endif
};

#endif