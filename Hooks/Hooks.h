#ifndef __HEADER_HOOKS__
#define __HEADER_HOOKS__
#pragma once

#include "CVMTHookManager.h"
#include "CDetour.h"

#if defined(GAME_GMOD)

#include "CallFunction.h"

#endif

#include "CAM_ThirdPerson.h"

#include "CreateMove.h"

#include "CursorPos.h"

#include "DrawModel.h"

#if defined(GAME_GMOD)

#include "FireBullets.h"

#endif

#include "FrameStageNotify.h"

#if defined(GAME_GMOD)

#include "GetRenderContext.h"

#endif

#include "GetUserCmd.h"

#if defined(GAME_GMOD)

#include "NET_SendPacket.h"

#endif

#if defined(GAME_CSS_STEAM)

#include "NetChan_Shutdown.h"

#endif

#include "OverrideView.h"

#include "Paint.h"

#include "RunCommand.h"

#include "SceneEnd.h"

#if defined(GAME_CSS_STEAM)

#include "SendDataGram.h"

#endif

#if defined(GAME_GMOD)

#include "SetViewAngles.h"

#include "ViewModelFix.h"

#endif

#include "WriteUserCmdToDeltaBuffer.h"

namespace Hooks
{
	bool Init();

#if defined(GAME_CSS_STEAM)
	namespace NetChannel
	{
		void SetupHook();
	}
#endif
}

#endif