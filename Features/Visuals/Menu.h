#ifndef __HEADER_MENU__
#define __HEADER_MENU__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace Visuals
	{ 
		bool ShouldDrawMenu();
		bool ShouldDrawConsole();
		void AddLineToConsole(const char* pszFmt, ...);
		void DrawMenu();
		void DrawConsole();
	}
}

namespace Config
{
	class ConVar
	{
	public:
		ConVar()
		{
			m_pszName = nullptr;
		}

		ConVar(const char* pszName)
		{
			m_pszName = new char[strlen(pszName) + 1];
			memcpy(m_pszName, pszName, strlen(pszName) + 1);
		}

		~ConVar()
		{
			m_SubVars.Clear();

			if (m_pszName)
			{
				delete[] m_pszName;
			}
		}

		bool GetBool()
		{
			return m_iValue;
		}

		int GetInt()
		{
			return m_iValue;
		}

		float GetFloat()
		{
			return m_flValue;
		}

		void SetInt(int iValue)
		{
			m_iValue = iValue;
			m_flValue = (float)iValue;
		}

		void SetFloat(float flValue)
		{
			m_iValue = (int)flValue;
			m_flValue = flValue;
		}

		int m_iValue;
		float m_flValue;
		char m_szValue[128 + 1]; // should b enuf??

		char* m_pszName;
		LinkedList<ConVar> m_SubVars;
	};

	void RegisterVars();
	void LoadConfig(const char* pName = "default");
	ConVar* FindVar(ReverseStack<const char*>& stack, Config::ConVar* start = nullptr);
	ConVar* RecursiveAdd(ReverseStack<const char*>& stack, Config::ConVar* start = nullptr);
	void SpewVars(int iTabs = 0, Config::ConVar* start = nullptr);
}

#define FINDVAR(name, ...) \
static ::Config::ConVar* name = nullptr; \
if(name == nullptr) \
{ \
constexpr static const char* s_Vars[] = {__VA_ARGS__}; \
static ReverseStack<const char*> s_Stack; \
for(int i = 0; i < (sizeof(s_Vars) / 4); i++) \
{ \
s_Stack.Push(s_Vars[i]); \
} \
name = ::Config::FindVar(s_Stack, nullptr); \
}

#endif