#include "IClientEntityList.h"

namespace Source
{
	namespace Interfaces
	{
		IClientEntityList* EntityList;
	}
}

using namespace Source;

IClientEntityList* IClientEntityList::GetPtr()
{
	return (IClientEntityList*)::Util::BruteforceInterface("VClientEntityList", "client.dll");
}

IClientEntity* IClientEntityList::GetClientEntity(int idx)
{
	return GET_VFUNC(IClientEntity*(__thiscall*)(IClientEntityList*, int), this, ICLIENTENTITYLIST_GETCLIENTENTITY)(this, idx);
}

IClientEntity* IClientEntityList::GetClientEntityFromHandle(EHANDLE handle)
{
	return GET_VFUNC(IClientEntity*(__thiscall*)(IClientEntityList*, EHANDLE), this, ICLIENTENTITYLIST_GETCLIENTENTITYFROMHANDLE)(this, handle);
}

int IClientEntityList::GetHighestEntityIndex()
{
	return GET_VFUNC(int(__thiscall*)(IClientEntityList*), this, ICLIENTENTITYLIST_GETHIGHESTENTITYINDEX)(this);
}