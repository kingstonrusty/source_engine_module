#include "CBasePlayer.h"

using namespace Source;

bool CBasePlayer::IsAlive()
{
	return GetLifeState() == 0;
}

IAnimState* CBasePlayer::GetAnimState()
{
#if defined(GAME_GMOD)
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(CBASEENTITY_ANIMSTATE, "client.dll");
#else
	static DWORD s_dwOffset = 0;
#endif

	return *(IAnimState**)((DWORD_PTR)this + s_dwOffset);
}

void CBasePlayer::SetAnimState(IAnimState* pState)
{
#if defined(GAME_GMOD)
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(CBASEENTITY_ANIMSTATE, "client.dll");
#else
	static DWORD s_dwOffset = 0;
#endif

	*(IAnimState**)((DWORD_PTR)this + s_dwOffset) = pState;
}

CUserCmd* CBasePlayer::GetCurrentCommand()
{
#if defined(GAME_GMOD) || defined(GAME_CSS_STEAM) || defined(GAME_BB2)
	static DWORD s_dwOffset = Source::Util::NetVarManager->GetOffset("DT_BasePlayer", "m_hConstraintEntity") - 0x4;
#elif defined(GAME_L4D2)
	static DWORD s_dwOffset = Source::Util::NetVarManager->GetOffset("DT_BasePlayer", "m_hConstraintEntity") - 0x8;
#endif

	return *(Source::CUserCmd**)((DWORD_PTR)this + s_dwOffset);
}

void CBasePlayer::SetCurrentCommand(CUserCmd* pCmd)
{
#if defined(GAME_GMOD) || defined(GAME_CSS_STEAM) || defined(GAME_BB2)
	static DWORD s_dwOffset = Source::Util::NetVarManager->GetOffset("DT_BasePlayer", "m_hConstraintEntity") - 0x4;
#elif defined(GAME_L4D2)
	static DWORD s_dwOffset = Source::Util::NetVarManager->GetOffset("DT_BasePlayer", "m_hConstraintEntity") - 0x8;
#endif

	*(Source::CUserCmd**)((DWORD_PTR)this + s_dwOffset) = pCmd;
}

bool CBasePlayer::IsTV()
{
#if defined(GAME_CSS_STEAM)
	return (GetTeamNum() != 2 && GetTeamNum() != 3);
#else
	return false;
#endif
}