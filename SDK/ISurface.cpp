#include "ISurface.h"

namespace Source
{
	namespace Interfaces
	{
		ISurface* Surface;
	}
}

using namespace Source;

ISurface* ISurface::GetPtr()
{
	return (ISurface*)::Util::BruteforceInterface("VGUI_Surface", "vguimatsurface.dll");
}

void ISurface::DrawSetColor(Color col)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, Color), this, ISURFACE_DRAWSETCOLOR)(this, col);
}

void ISurface::DrawFilledRect(int x1, int y1, int x2, int y2)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, int, int, int, int), this, ISURFACE_DRAWFILLEDRECT)(this, x1, y1, x2, y2);
}

void ISurface::DrawOutlinedRect(int x1, int y1, int x2, int y2)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, int, int, int, int), this, ISURFACE_DRAWOUTLINEDRECT)(this, x1, y1, x2, y2);
}

void ISurface::DrawLine(int x1, int y1, int x2, int y2)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, int, int, int, int), this, ISURFACE_DRAWLINE)(this, x1, y1, x2, y2);
}

void ISurface::DrawSetTextFont(Source::HFONT font)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, Source::HFONT), this, ISURFACE_DRAWSETTEXTFONT)(this, font);
}

void ISurface::DrawSetTextColor(Color col)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, Color), this, ISURFACE_DRAWSETTEXTCOLOR)(this, col);
}

void ISurface::DrawSetTextPos(int x, int y)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, int, int), this, ISURFACE_DRAWSETTEXTPOS)(this, x, y);
}

void ISurface::DrawPrintText(const wchar_t* text, int len, FontDrawType_t drawtype)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, const wchar_t*, int, FontDrawType_t), this, ISURFACE_DRAWPRINTTEXT)(this, text, len, drawtype);
}

void ISurface::GetScreenSize(int& w, int& h)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, int&, int&), this, ISURFACE_GETSCREENSIZE)(this, w, h);
}

Source::HFONT ISurface::CreateFont()
{
	return GET_VFUNC(Source::HFONT(__thiscall*)(ISurface*), this, ISURFACE_CREATEFONT)(this);
}

bool ISurface::SetFontGlyphSet(Source::HFONT font, const char* fontname, int tall, int weight, int blur, int scanlines, int flags, int rangemin, int rangemax)
{
	return GET_VFUNC(bool(__thiscall*)(ISurface*, Source::HFONT, const char*, int, int, int, int, int, int, int), this, ISURFACE_SETFONTGLYPHSET)(this, font, fontname, tall, weight, blur, scanlines, flags, rangemin, rangemax);
}

void ISurface::GetTextSize(Source::HFONT font, const wchar_t* text, int& w, int& h)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, Source::HFONT, const wchar_t*, int&, int&), this, ISURFACE_GETTEXTSIZE)(this, font, text, w, h);
}

void ISurface::GetCursorPos(int& x, int& y)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, int&, int&), this, ISURFACE_GETCURSORPOS)(this, x, y);
}

void ISurface::SetCursorPos(int x, int y)
{
	return GET_VFUNC(void(__thiscall*)(ISurface*, int, int), this, ISURFACE_SETCURSORPOS)(this, x, y);
}

void ISurface::ResetFontCaches()
{
	return GET_VFUNC(void(__thiscall*)(ISurface*), this, ISURFACE_RESETFONTCACHES)(this);
}

void ISurface::StartDrawing()
{
	static void(__thiscall* pStartDrawing)(ISurface*) = Memory::FindPattern<void(__thiscall*)(ISurface*)>(ISURFACE_STARTDRAWING, "vguimatsurface.dll");

	return pStartDrawing(this);
}

void ISurface::FinishDrawing()
{
	static void(__thiscall* pFinishDrawing)(ISurface*) = Memory::FindPattern<void(__thiscall*)(ISurface*)>(ISURFACE_FINISHDRAWING, "vguimatsurface.dll");

	return pFinishDrawing(this);
}

namespace Util
{
	namespace DrawingHelper 
	{
		Source::HFONT TitleFont;
		Source::HFONT MenuFont;
		Source::HFONT LargeTitleFont;
		Source::HFONT DebugFont;
	}
}