#ifndef __HEADER_FIREBULLETS__
#define __HEADER_FIREBULLETS__
#pragma once

#include "CDetour.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace Client
	{
		extern CDetour* pFireBullets;

		void __fastcall FireBullets(Source::CBasePlayer* pThis, void*, Source::FireBulletsInfo_t* info);
	}
}

#endif