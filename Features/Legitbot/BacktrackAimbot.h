#ifndef __HEADER_BACKTRACKAIMBOT__
#define __HEADER_BACKTRACKAIMBOT__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace Legitbot
	{
		class BacktrackAimbot;
	}
}

class Features::Legitbot::BacktrackAimbot
{
public:
	static bool Run(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd);
};

#endif