#include "SendDataGram.h"

#include "Util/Input.h"

#if defined(GAME_CSS_STEAM)

int __fastcall Hooks::NetChannel::SendDatagram(Source::CNetChan* pThis, void*, Source::bf_write* data)
{
	if (Util::Input::GetButtonState('F'))
	{
		//pThis->m_nInSequenceNr() -= 30;
	}

	pVmt->Unhook();
	auto iRet = pThis->SendDataGram(data);
	pVmt->Rehook();

	return iRet;
}

#endif