#include "Triggerbot.h"

#include "Features/Misc/InterpLagComp.h"

#include "Features/Misc/BoneCacheHelper.h"

using namespace Features::Legitbot;

bool Triggerbot::Run(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd)
{
	if (Features::Misc::InterpLagComp::GetIsLocked())
	{
		return false;
	}

	for (int i = 1; i <= Source::Interfaces::Globals->maxClients(); i++)
	{
		if (i == Source::Interfaces::Engine->GetLocalPlayer())
		{
			continue;
		}

		Source::CBasePlayer* pTarget = (Source::CBasePlayer*)Source::Interfaces::EntityList->GetClientEntity(i);
		if (pTarget == nullptr)
		{
			continue;
		}

		if (!pTarget->IsAlive())
		{
			continue;
		}

		SmallVector<Features::PositionAdjustment::History::Snapshot*> vecRecords;
		Features::PositionAdjustment::History::GetUsableRecordsForEntity(pTarget, &vecRecords, 0.f, 0.9f);
		if (vecRecords.GetSize() == 0)
		{
			continue;
		}

		auto pRecord = vecRecords[vecRecords.GetSize() - 1];

		Features::Misc::BoneCacheHelper::BackupCache(pTarget);
		Features::Misc::BoneCacheHelper::OverrideCache(pTarget, pRecord);

		Vector eyepos = pLocal->GetAbsOrigin() + pLocal->GetViewOffset();
		Vector forward;

		AngleVectors(cmd->viewangles(), &forward);
		Source::Ray_t ray;
		ray.Init(eyepos, eyepos + forward * 8192.f);
		Source::CTraceFilterSimple filter(pLocal);
		Source::trace_t tr;

		Source::Interfaces::TraceRay->TraceRay(ray, Source::MASK_SHOT, &filter, &tr);

		Features::Misc::BoneCacheHelper::RestoreCache(pTarget);

		if (tr.m_pEnt == pTarget)
		{
			bool bAdjustInterp;

			Features::PositionAdjustment::History::CanRestoreToSimulationTime(pRecord->m_flSimulationTime, &bAdjustInterp);
			if (bAdjustInterp)
			{
				float flInterp = Source::TICKS_TO_TIME(Source::TIME_TO_TICKS(Source::TICKS_TO_TIME(cmd->tick_count() - Source::TIME_TO_TICKS(pRecord->m_flSimulationTime))));

				Features::Misc::InterpLagComp::UpdateDesiredValues(true, flInterp, 1.f);
			}
			else
			{
				cmd->tick_count() = Source::TIME_TO_TICKS(pRecord->m_flSimulationTime);

				Features::Misc::InterpLagComp::UpdateDesiredValues(false);
			}

			cmd->buttons() |= 1;
			return true;
		}
	}

	return false;
}