#ifndef __HEADER_CDETOUR__
#define __HEADER_CDETOUR__
#pragma once

namespace Hooks
{
	class CDetour;
}

class Hooks::CDetour
{
public:
	CDetour(void* pFuntion, void* pHook);
	CDetour(DWORD_PTR dwpFunction, void* pHook);

	void Hook(bool bHook);
	void Unhook();
	void Rehook();
	
	template<typename Fn>
	Fn GetOriginal()
	{
		return (Fn)m_pFunction;
	}
private:
	void _Setup();
	void _Hook();
	void _Unhook();

	void* m_pFunction;
	void* m_pHook;
	byte m_abOurBytes[5];
	byte m_abOriginalBytes[5];
};

#endif