#include "CNetChan.h"

using namespace Source;

float CNetChan::GetLatency(int flow)
{
	return GET_VFUNC(float(__thiscall*)(CNetChan*, int), this, INETCHANNELINFO_GETLATENCY)(this, flow);
}

#if defined(GAME_CSS_STEAM)
void CNetChan::Shutdown(const char* reason)
{
	return GET_VFUNC(void(__thiscall*)(CNetChan*, const char*), this, 36)(this, reason);
}
#endif

bool CNetChan::SendNetMsg(CNetMessage& msg, bool forcereliable, bool voice)
{
	return GET_VFUNC(bool(__thiscall*)(CNetChan*, CNetMessage&, bool, bool), this, INETCHANNELINFO_SENDNETMSG)(this, msg, forcereliable, voice);
}

#if defined(GAME_CSS_STEAM)
int CNetChan::SendDataGram(bf_write* data)
{
	return GET_VFUNC(int(__thiscall*)(CNetChan*, bf_write*), this, 46)(this, data);
}
#endif

int& CNetChan::m_nOutSequenceNr()
{
#if defined(GAME_CSS_STEAM) || defined(GAME_GMOD)
	return *(int*)((DWORD_PTR)this + CNETCHAN_OUTSEQUENCENR);
#endif
}

int& CNetChan::m_nInSequenceNr()
{
#if defined(GAME_CSS_STEAM) || defined(GAME_GMOD)
	return *(int*)((DWORD_PTR)this + CNETCHAN_INSEQUENCENR);
#endif
}