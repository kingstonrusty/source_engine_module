#ifndef __HEADER_NETCHAN_SHUTDOWN__
#define __HEADER_NETCHAN_SHUTDOWN__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace NetChannel
	{
		extern CVMTHookManager* pVmt;

		void __fastcall Shutdown(Source::CNetChan* pThis, void*, const char* reason);
	}
}

#endif