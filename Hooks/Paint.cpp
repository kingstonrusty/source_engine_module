#include "Paint.h"

#include "Util/Input.h"

#include "Features/PositionAdjustment/History.h"

#include "Features/Visuals/Menu.h"

#include "Features/Visuals/AntiScreengrab.h"

#include "Features/Visuals/ESP.h"

#include "Features/Misc/HitBoxCache.h"

namespace DrawingHelper = Util::DrawingHelper;

bool bDrawDanger;

static void DrawHitBox(Vector& min, Vector& max, matrix3x4& matrix, int idx = -1)
{
	Vector pointsWorld[] =
	{
		Vector(min.x, min.y, min.z),
		Vector(min.x, max.y, min.z),
		Vector(max.x, max.y, min.z),
		Vector(max.x, min.y, min.z),
		Vector(max.x, max.y, max.z),
		Vector(min.x, max.y, max.z),
		Vector(min.x, min.y, max.z),
		Vector(max.x, min.y, max.z),
	};

	Vector points[9];
	for (int i = 0; i < 8; i++)
	{
		VectorTransform(pointsWorld[i], matrix, points[i]);
	}

	points[8] = (points[4] + points[0]) * 0.5f;

	Vector startPos, endPos, centerPos;

	Color clr = Color(255, 255, 255);
	if (bDrawDanger)
	{
		clr = Color(255, 165, 0);
	}

	for (int i = 0; i < 3; i++)
	{
		if (Source::Interfaces::DebugOverlay->ScreenPosition(points[i], startPos) != 1 && Source::Interfaces::DebugOverlay->ScreenPosition(points[i + 1], endPos) != 1)
		{
			DrawingHelper::DrawLine(startPos.x, startPos.y, endPos.x, endPos.y, clr);
		}
	}

	if (Source::Interfaces::DebugOverlay->ScreenPosition(points[0], startPos) != 1 && Source::Interfaces::DebugOverlay->ScreenPosition(points[3], endPos) != 1)
	{
		DrawingHelper::DrawLine(startPos.x, startPos.y, endPos.x, endPos.y, clr);
	}

	for (int i = 4; i < 7; i++)
	{
		if (Source::Interfaces::DebugOverlay->ScreenPosition(points[i], startPos) != 1 && Source::Interfaces::DebugOverlay->ScreenPosition(points[i + 1], endPos) != 1)
		{
			DrawingHelper::DrawLine(startPos.x, startPos.y, endPos.x, endPos.y, clr);
		}
	}

	if (Source::Interfaces::DebugOverlay->ScreenPosition(points[4], startPos) != 1 && Source::Interfaces::DebugOverlay->ScreenPosition(points[7], endPos) != 1)
	{
		DrawingHelper::DrawLine(startPos.x, startPos.y, endPos.x, endPos.y, clr);
	}

	if (Source::Interfaces::DebugOverlay->ScreenPosition(points[0], startPos) != 1 && Source::Interfaces::DebugOverlay->ScreenPosition(points[6], endPos) != 1)
	{
		DrawingHelper::DrawLine(startPos.x, startPos.y, endPos.x, endPos.y, clr);
	}

	if (Source::Interfaces::DebugOverlay->ScreenPosition(points[1], startPos) != 1 && Source::Interfaces::DebugOverlay->ScreenPosition(points[5], endPos) != 1)
	{
		DrawingHelper::DrawLine(startPos.x, startPos.y, endPos.x, endPos.y, clr);
	}

	if (Source::Interfaces::DebugOverlay->ScreenPosition(points[2], startPos) != 1 && Source::Interfaces::DebugOverlay->ScreenPosition(points[4], endPos) != 1)
	{
		DrawingHelper::DrawLine(startPos.x, startPos.y, endPos.x, endPos.y, clr);
	}

	if (Source::Interfaces::DebugOverlay->ScreenPosition(points[3], startPos) != 1 && Source::Interfaces::DebugOverlay->ScreenPosition(points[7], endPos) != 1)
	{
		DrawingHelper::DrawLine(startPos.x, startPos.y, endPos.x, endPos.y, clr);

		DrawingHelper::DrawString(DrawingHelper::MenuFont, startPos.x, startPos.y, Color(255, 0, 0), "%d", idx);
	}
}

void DrawHitBoxes(Source::CBasePlayer* pPlayer, matrix3x4* matrices)
{
	auto model = pPlayer->GetRenderable()->GetModel();
	if (model == nullptr)
	{
		return;
	}

	auto studiohdr = Source::Interfaces::ModelInfo->GetStudioModel(model);
	if (studiohdr == nullptr)
	{
		return;
	}

	auto pHitboxSet = studiohdr->pHitboxSet(pPlayer->GetHitboxSet());
	for (int idx = 0; idx < pHitboxSet->numhitboxes; idx++)
	{
		auto pHitbox = pHitboxSet->pHitbox(idx);

		DrawHitBox(pHitbox->bbmin, pHitbox->bbmax, matrices[pHitbox->bone], pHitbox->group);
	}
}

void DrawHitBoxes(Source::CBasePlayer* pPlayer)
{
	SmallVector<Features::PositionAdjustment::History::Snapshot*> vecRecords;
	Features::PositionAdjustment::History::GetUsableRecordsForEntity(pPlayer, &vecRecords);
	if (vecRecords.GetSize() == 0)
	{
		return;
	}

	auto pRecord = vecRecords.GetAt(0);

	Features::Misc::HitBoxCache::HitBoxSet* pSet;
	if (!Features::Misc::HitBoxCache::GetSet(pPlayer, &pSet))
	{
		return;
	}

	for (int i = 0; i < pSet->m_nHitBoxes; i++)
	{
		DrawHitBox(pSet->m_HitBoxes[i].m_vMin, pSet->m_HitBoxes[i].m_vMax, pRecord->m_BoneCache[pSet->m_HitBoxes[i].m_iBone], pSet->m_HitBoxes[i].m_iGroup);
	}
}

bool g_bSendPacket;
int g_nChoked;

int g_nPlayer;
int g_nSnapshot;

void __fastcall Hooks::EngineVGui::Paint(Source::IVEngineVGui* pThis, void*, int mode)
{
	pVmt->Unhook();

#if defined(GAME_GMOD)
	if (Features::Visuals::ShouldClearScreen() && !Source::Interfaces::Engine->IsTakingScreenshot())
	{
		pThis->Paint(2);
		pThis->Paint(1 | 4);
	}
	else
#endif
	{
		pThis->Paint(mode);
	}

	pVmt->Rehook();

	if (Features::Visuals::ShouldClearScreen())
	{
		return;
	}

	if (!(mode & 0x1))
	{
		return;
	}

	Util::Input::Update();

#if defined(GAME_GMOD)
	if (Source::Interfaces::Engine->IsInGame())
	{
		Source::Interfaces::RenderView->Push3DView(*Source::Interfaces::View->GetPlayerViewSetup(), 0, nullptr, Source::Interfaces::View->GetFrustum());
	}
#endif

	Source::Interfaces::Surface->StartDrawing();

	/*if(g_nPlayer != -1 && g_nSnapshot != -1)
	{
		Source::CBasePlayer* pPlayer = (Source::CBasePlayer*)Source::Interfaces::EntityList->GetClientEntity(g_nPlayer);
		if (pPlayer)
		{
			SmallVector<Features::PositionAdjustment::History::Snapshot*> records;
			Features::PositionAdjustment::History::GetUsableRecordsForEntity(pPlayer, &records, 0.f, 1.f);
			if (records.GetSize() >= (g_nSnapshot + 1))
			{
				auto record = records[g_nSnapshot];
				Features::PositionAdjustment::History::CanRestoreToSimulationTime(record->m_flSimulationTime, &bDrawDanger);

				DrawHitBoxes(pPlayer, record->m_BoneCache);
			}
		}
	}

	Source::CBasePlayer* pLocal = (Source::CBasePlayer*)Source::Interfaces::EntityList->GetClientEntity(Source::Interfaces::Engine->GetLocalPlayer());
	if (pLocal != nullptr)
	{
		Vector forward, angles;
		Source::Interfaces::Engine->GetViewAngles(angles);
		AngleVectors(angles, &forward);

		Source::Ray_t ray;
		ray.Init(pLocal->GetAbsOrigin() + pLocal->GetViewOffset(), (pLocal->GetAbsOrigin() + pLocal->GetViewOffset() + forward * 8192.f));
		Source::CTraceFilterSimple filter(pLocal);
		Source::trace_t tr;

		Source::Interfaces::TraceRay->TraceRay(ray, Source::MASK_SHOT, &filter, &tr);

		if (tr.m_pEnt)
		{
			auto cc = ((Source::CBaseEntity*)tr.m_pEnt)->GetNetworkable()->GetClientClass();

			DrawingHelper::DrawString(DrawingHelper::MenuFont, 5, 20, Color(255, 255, 255), "(%d) %s", cc->m_ClassID, cc->m_pNetworkName);
		}
	}*/

	/*for (int i = 1; i <= Source::Interfaces::EntityList->GetHighestEntityIndex(); i++)
	{
		Source::CBaseNPC* pNPC = (Source::CBaseNPC*)Source::Interfaces::EntityList->GetClientEntity(i);
		if (pNPC == nullptr || !pNPC->IsCharger())
		{
			continue;
		}

		DrawHitBoxes((Source::CBasePlayer*)pNPC);
	}*/

	DrawingHelper::DrawString(DrawingHelper::MenuFont, 5, 5, g_bSendPacket ? Color(0, 255, 0) : Color(255, 0, 0), "bSendPacket (%d)", g_nChoked);

	Features::Visuals::ESP::Run();

	Features::Visuals::DrawMenu();
	Features::Visuals::DrawConsole();

	Source::Interfaces::Surface->FinishDrawing();

#if defined(GAME_GMOD)
	if (Source::Interfaces::Engine->IsInGame())
	{
		Source::Interfaces::RenderView->PopView(Source::Interfaces::View->GetFrustum());
	}
#endif

	if (Util::Input::GetButtonState(VK_F6) & 1)
	{
		LuaBase::Refresh();
	}
}