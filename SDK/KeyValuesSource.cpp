#include "KeyValuesSource.h"

Source::KeyValues::KeyValues(const char* name)
{
	Init(name);
}

Source::KeyValues* Source::KeyValues::AllocKeyValuesMemory()
{
#if defined (GAME_BB2)
	static KeyValues*(__cdecl* pAllocKeyValuesMemory)(int) = nullptr;
	if (pAllocKeyValuesMemory == nullptr)
	{
		DWORD_PTR dwpCall = Memory::FindPattern<DWORD_PTR>(KEYVALUES_ALLOCKEYVALUESMEMORY, "engine.dll");

		pAllocKeyValuesMemory = (KeyValues*(__cdecl*)(int))(dwpCall + *(DWORD_PTR*)dwpCall + 0x4);
	}

	return pAllocKeyValuesMemory(32);
#else
	static KeyValues*(__cdecl* pAllocKeyValuesMemory)(int) = Memory::FindPattern<KeyValues*(__cdecl*)(int)>(KEYVALUES_ALLOCKEYVALUESMEMORY, "engine.dll");

	return pAllocKeyValuesMemory(32);
#endif
}

void Source::KeyValues::Init(const char* name)
{
#if defined(GAME_GMOD) || defined(GAME_L4D2) || defined(GAME_BB2)
	static void(__thiscall* pInit)(KeyValues*, const char*) = nullptr;
	if (pInit == nullptr)
	{
		DWORD_PTR dwpCall = Memory::FindPattern<DWORD_PTR>(KEYVALUES_INIT, "engine.dll");

		pInit = (void(__thiscall*)(KeyValues*, const char*))(dwpCall + *(DWORD_PTR*)dwpCall + 0x4);
	}

	return pInit(this, name);
#elif(GAME_CSS_STEAM)
	static void(__thiscall* pInit)(KeyValues*, const char*) = Memory::FindPattern<void(__thiscall*)(KeyValues*, const char*)>(KEYVALUES_INIT, "client.dll");

	return pInit(this, name);
#endif
}

void Source::KeyValues::SetString(const char* key, const char* value)
{
	
}

bool Source::KeyValues::LoadFromBuffer(const char* resourcename, const char* buffer)
{
	static bool(__thiscall* pLoadFromBuffer)(KeyValues*, const char*, const char*, void*, void*) = Memory::FindPattern<bool(__thiscall*)(KeyValues*, const char*, const char*, void*, void*)>(KEYVALUES_LOADFROMBUFFER, "engine.dll");

	return pLoadFromBuffer(this, resourcename, buffer, nullptr, nullptr);
}

void* Source::KeyValues::operator new(size_t sz)
{
	return AllocKeyValuesMemory();
}