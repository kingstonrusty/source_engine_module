#ifndef __HEADER_TRIGGERBOT__
#define __HEADER_TRIGGERBOT__
#pragma once

#include "SDK/Source.h"

#include "Features/PositionAdjustment/History.h"

namespace Features
{
	namespace Legitbot
	{
		class Triggerbot;
	}
}

class Features::Legitbot::Triggerbot
{
public:
	static bool Run(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd);
};

#endif