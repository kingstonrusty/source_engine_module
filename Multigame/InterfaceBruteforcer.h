#ifndef __HEADER_INTERFACEBRUTEFORCER__
#define __HEADER_INTERFACEBRUTEFORCER__
#pragma once

#include "SDK/CreateInterface.h"

namespace Util
{
	void* BruteforceInterface(const char* pszInterfaceName, const char* pszModule);
}

#endif