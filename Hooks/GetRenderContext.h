#ifndef __HEADER_GETRENDERCONTEXT__
#define __HEADER_GETRENDERCONTEXT__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace MaterialSystem
	{
		extern bool g_bRendering;

		extern CVMTHookManager* pVmt;

		Source::IMatRenderContext* __fastcall GetRenderContext(Source::IMaterialSystem* pThis, void*);
	}
}

#endif