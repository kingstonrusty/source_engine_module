#include "CBaseWeapon.h"

using namespace Source;

#if defined(GAME_L4D2)
float CBaseWeapon::GetSpread()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(CBASEWEAPON_SPREAD, "client.dll");

	return *(float*)((DWORD_PTR)this + s_dwOffset);
}

void CBaseWeapon::UpdateInaccuracy()
{
	static void(__thiscall* pUpdateInaccuracy)(CBaseWeapon*) = Memory::FindPattern<void(__thiscall*)(CBaseWeapon*)>("53 8B DC 83 EC 08 83 E4 F0 83 C4 04 55 8B 6B 04 89 6C 24 04 8B EC 83 EC 28 56 57 8B F9 E8", "client.dll");

	return pUpdateInaccuracy(this);
}
#endif

#if defined(GAME_CSS_STEAM)
float CBaseWeapon::GetSpread()
{
	return GET_VFUNC(float(__thiscall*)(CBaseWeapon*), this, CBASEWEAPON_GETSPREAD)(this);
}

float CBaseWeapon::GetInaccuracy()
{
	return GET_VFUNC(float(__thiscall*)(CBaseWeapon*), this, CBASEWEAPON_GETINACCURACY)(this);
}

void CBaseWeapon::UpdateInaccuracy()
{
	return GET_VFUNC(void(__thiscall*)(CBaseWeapon*), this, CBASEWEAPON_UPDATEINACCURACY)(this);
}
#endif