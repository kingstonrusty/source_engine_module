#include "Globals.h"

#include "SDK/Source.h"

int Globals::l_FrameCount(lua_State* L)
{
	lua_pushinteger(L, Source::Interfaces::Globals->framecount());

	return 1;
}

int Globals::l_CurTime(lua_State* L)
{
	lua_pushnumber(L, Source::Interfaces::Globals->curtime());

	return 1;
}

int Globals::l_FrameTime(lua_State* L)
{
	lua_pushnumber(L, Source::Interfaces::Globals->frametime());

	return 1;
}

int Globals::l_MaxClients(lua_State* L)
{
	lua_pushinteger(L, Source::Interfaces::Globals->maxClients());

	return 1;
}

int Globals::l_TickCount(lua_State* L)
{
	lua_pushinteger(L, Source::Interfaces::Globals->tickcount());

	return 1;
}

int Globals::l_IntervalPerTick(lua_State* L)
{
	lua_pushnumber(L, Source::Interfaces::Globals->interval_per_tick());

	return 1;
}