#ifndef __HEADER_ENTS_LUA__
#define __HEADER_ENTS_LUA__
#pragma once

#include "BaseLibrary.h"

namespace Source
{
	class CBaseEntity;
	class CBasePlayer;
}

class Ents : public BaseLibrary<Ents>
{
public:
	friend class BaseLibrary<Ents>;

	static void Init(lua_State* L);
	static const char* GetName() { return "Ents"; }

	static void RegisterUserdata(lua_State* L);
private:
	static int l_GetAll(lua_State* L);
	static int l_GetPlayers(lua_State* L);

	static constexpr const struct luaL_Reg m_Register[] =
	{
		{ "GetAll", l_GetAll },
		{ "GetPlayers", l_GetPlayers },
		{ NULL, NULL }
	};
};

#endif