#include "IVEngineVGui.h"

namespace Source
{
	namespace Interfaces
	{
		IVEngineVGui* EngineVGui;
	}
}

using namespace Source;

IVEngineVGui* IVEngineVGui::GetPtr()
{
	return (IVEngineVGui*)::Util::BruteforceInterface("VEngineVGui", "engine.dll");
}

void IVEngineVGui::Paint(int mode)
{
	return GET_VFUNC(void(__thiscall*)(IVEngineVGui*, int), this, IVENGINEVGUI_PAINT)(this, mode);
}