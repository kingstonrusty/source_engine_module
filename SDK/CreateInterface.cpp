#include "CreateInterface.h"

void* Source::CreateInterface(const char* pszInterfaceName, const char* pszModule)
{
	using CreateInterfaceFn = void*(*)(const char*, int*);

	CreateInterfaceFn moduleFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA(pszModule), "CreateInterface");
	if (moduleFactory == nullptr)
	{
		return nullptr;
	}

	return moduleFactory(pszInterfaceName, nullptr);
}