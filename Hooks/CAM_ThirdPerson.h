#ifndef __HEADER_CAM_THIRDPERSON__
#define __HEADER_CAM_THIRDPERSON__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace Input
	{ // 204
		extern CVMTHookManager* pVmt;

		int __fastcall CAM_IsThirdPerson(Source::CInput* pThis, void*);
	}
}

#endif