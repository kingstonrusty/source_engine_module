#include "INetworkStringTableContainer.h"

namespace Source
{
	namespace Interfaces
	{
		INetworkStringTableContainer* StringTableContainer;
	}
}

using namespace Source;

const char* INetworkStringTable::GetName()
{
	//return GET_VFUNC(const char*(__thiscall*)(INetworkStringTable*), this, INETWORKSTRINGTABLE_GETNAME)(this);
}

int INetworkStringTable::GetNumStrings()
{
	//return GET_VFUNC(int(__thiscall*)(INetworkStringTable*), this, INETWORKSTRINGTABLE_GETNUMSTRINGS)(this);
}

int INetworkStringTable::AddString(bool isserver, const char* value, int length, const void* userdata)
{
	return GET_VFUNC(int(__thiscall*)(INetworkStringTable*, bool, const char*, int, const void*), this, INETWORKSTRINGTABLE_ADDSTRING)(this, isserver, value, length, userdata);
}

const char* INetworkStringTable::GetString(int index)
{
	//return GET_VFUNC(const char*(__thiscall*)(INetworkStringTable*, int), this, INETWORKSTRINGTABLE_GETSTRING)(this, index);
}

INetworkStringTableContainer* INetworkStringTableContainer::GetPtr()
{
	return (INetworkStringTableContainer*)::Util::BruteforceInterface("VEngineClientStringTable", "engine.dll");
}

INetworkStringTable* INetworkStringTableContainer::FindTable(const char* name)
{
	return GET_VFUNC(INetworkStringTable*(__thiscall*)(INetworkStringTableContainer*, const char*), this, INETWORKSTRINGTABLECONTAINER_FINDTABLE)(this, name);
}

INetworkStringTable* INetworkStringTableContainer::GetTable(int id)
{
	//return GET_VFUNC(INetworkStringTable*(__thiscall*)(INetworkStringTableContainer*, int), this, INETWORKSTRINGTABLECONTAINER_GETTABLE)(this, id);
}

int INetworkStringTableContainer::GetNumTables()
{
	//return GET_VFUNC(int(__thiscall*)(INetworkStringTableContainer*), this, INETWORKSTRINGTABLECONTAINER_GETNUMTABLES)(this);
}