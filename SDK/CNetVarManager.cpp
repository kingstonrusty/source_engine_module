#include "CNetVarManager.h"

namespace Source
{
	namespace Util
	{
		CNetVarManager* NetVarManager;
	}
}

using namespace Source;

CNetVarManager::CNetVarManager()
{
	ClientClass* pClass = Interfaces::Client->GetAllClasses();
	while (pClass)
	{
		m_vecRecvTable.Add(pClass->m_pRecvTable);
		pClass = pClass->m_pNext;
	}
}

offset_t CNetVarManager::GetOffset(const char* pszTableName, const char* pszPropName)
{
	return GetProp(pszTableName, pszPropName);
}

RecvVarProxyFn CNetVarManager::HookProp(const char* pszTableName, const char* pszPropName, void* pProxy)
{
	CRecvProp* pProp = nullptr;
	GetProp(pszTableName, pszPropName, &pProp);

	if (pProp == nullptr)
	{
		return nullptr;
	}

	RecvVarProxyFn pOld = pProp->m_ProxyFn;
	pProp->m_ProxyFn = (RecvVarProxyFn)pProxy;
	return pOld;
}

offset_t CNetVarManager::GetProp(const char* pszTableName, const char* pszPropName, CRecvProp** pProps)
{
	CRecvTable* pRecvTable = GetTable(pszTableName);
	if (pRecvTable == nullptr)
	{
		return 0;
	}

	return GetProp(pRecvTable, pszPropName, pProps);
}

offset_t CNetVarManager::GetProp(CRecvTable* pRecvTable, const char* pszPropName, CRecvProp** pProps)
{
	offset_t curOffset = 0;

	for (int i = 0; i < pRecvTable->m_nProps; i++)
	{
		auto* pRecvProp = &pRecvTable->m_pProps[i];
		auto* child = pRecvProp->m_pDataTable;

		curOffset += (child && (child->m_nProps > 0) && GetProp(child, pszPropName, pProps)) ? pRecvProp->m_Offset + GetProp(child, pszPropName, pProps) : 0;

		if (_stricmp(pRecvProp->m_pVarName, pszPropName))
		{
			continue;
		}

		if (pProps)
		{
			*pProps = pRecvProp;
		}

		return pRecvProp->m_Offset + curOffset;
	}

	return curOffset;
}

CRecvTable* CNetVarManager::GetTable(const char* pszTableName)
{
	for (int idx = 0; idx < m_vecRecvTable.GetSize(); idx++)
	{
		auto table = m_vecRecvTable[idx];

		if (!table)
		{
			continue;
		}

		if (!_stricmp(table->m_pNetTableName, pszTableName))
		{
			return table;
		}
	}

	return nullptr;
}