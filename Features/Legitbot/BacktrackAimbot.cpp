#include "BacktrackAimbot.h"

#include "Features/Misc/InterpLagComp.h"

#include "Features/Misc/BoneCacheHelper.h"

#include "Util/Input.h"

using namespace Features::Legitbot;

extern int g_nPlayer;
extern int g_nSnapshot;

bool BacktrackAimbot::Run(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd)
{
	if (Features::Misc::InterpLagComp::GetIsLocked())
	{
		return false;
	}

	g_nPlayer = g_nSnapshot = -1;

	Vector eyepos = pLocal->GetAbsOrigin() + pLocal->GetViewOffset();
	Vector forward;
	AngleVectors(cmd->viewangles()/* + pLocal->GetPunchAngle() * 2.f*/, &forward);

	Features::PositionAdjustment::History::Snapshot* pSnapshot = nullptr;

	for (int i = 1; i <= Source::Interfaces::EntityList->GetHighestEntityIndex(); i++)
	{
		if (i == Source::Interfaces::Engine->GetLocalPlayer())
		{
			continue;
		}

		Source::CBaseEntity* pTarget = (Source::CBaseEntity*)Source::Interfaces::EntityList->GetClientEntity(i);
		if (pTarget == nullptr)
		{
			continue;
		}

		if (pTarget->IsPlayer())
		{
			continue;

			auto pPlayer = (Source::CBasePlayer*)pTarget;

			if (pPlayer->IsTV())
			{
				//continue;
			}

			if (pPlayer->GetTeamNum() == pLocal->GetTeamNum())
			{
				//continue;
			}
		}
		else if (pTarget->IsNPC())
		{
			auto pNPC = (Source::CBaseNPC*)pTarget;

			if (!pNPC->IsAlive())
			{
				continue;
			}
		}
		else
		{
			continue;
		}

		SmallVector<Features::PositionAdjustment::History::Snapshot*> vecRecords;
		Features::PositionAdjustment::History::GetUsableRecordsForEntity(pTarget, &vecRecords, 0.f, 1.f);
		if (vecRecords.GetSize() == 0)
		{
			continue;
		}

		Features::Misc::BoneCacheHelper::BackupCache(pTarget);

		int iLastHitGroup = 10;

		for (int i = vecRecords.GetSize() - 1; i >= 0; i--)
		{
			auto pRecord = vecRecords[i];
			Features::Misc::BoneCacheHelper::OverrideCache(pTarget, pRecord);

			Source::Ray_t ray;
			ray.Init(eyepos, eyepos + forward * 32768.f);
			Source::CTraceFilterSimple filter(pLocal);
			Source::trace_t tr;

			Source::Interfaces::TraceRay->TraceRay(ray, Source::MASK_SHOT, &filter, &tr);

			if ((tr.hitgroup < iLastHitGroup) && tr.m_pEnt == pTarget && tr.hitgroup > 0 && tr.hitgroup < 8)
			{
				iLastHitGroup = tr.hitgroup;
				pSnapshot = pRecord;

				g_nPlayer = pTarget->GetNetworkable()->EntIndex();
				g_nSnapshot = i;

				if (tr.hitgroup == 1)
				{
					break;
				}
			}
		}

		Features::Misc::BoneCacheHelper::RestoreCache(pTarget);

		if (pSnapshot)
		{
			break;
		}
	}

	if (pSnapshot == nullptr)
	{
		return false;
	}

	if (!Util::Input::GetButtonState('F'))
	{
		return false;
	}

	bool bAdjustInterp = false;
	Features::PositionAdjustment::History::CanRestoreToSimulationTime(pSnapshot->m_flSimulationTime, &bAdjustInterp);
	if (bAdjustInterp)
	{
		float flInterp = Source::TICKS_TO_TIME(Source::TIME_TO_TICKS(Source::TICKS_TO_TIME(cmd->tick_count() - Source::TIME_TO_TICKS(pSnapshot->m_flSimulationTime))));

		Features::Misc::InterpLagComp::UpdateDesiredValues(true, flInterp, 1.f);
	}
	else
	{
		cmd->tick_count() = Source::TIME_TO_TICKS(pSnapshot->m_flSimulationTime);

		Features::Misc::InterpLagComp::UpdateDesiredValues(false);
	}

	cmd->buttons() |= Source::IN_ATTACK;

	return true;
}