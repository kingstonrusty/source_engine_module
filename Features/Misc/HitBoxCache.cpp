#include "HitBoxCache.h"

#include "Util/CRC32.h"

#include "Util/Input.h"

using namespace Features::Misc;

KeyValues<unsigned int, HitBoxCache::HitBoxSet> HitBoxCache::m_HitBoxes;

bool HitBoxCache::GetSet(Source::CBaseEntity* pEntity, HitBoxSet** pSet)
{
	/*
	order:
	head
	chest
	stomach
	leftleg
	rightleg
	leftarm
	rightarm
	*/

	auto pModel = pEntity->GetRenderable()->GetModel();
	if (pModel == nullptr)
	{
		return false;
	}

	auto pszModelName = Source::Interfaces::ModelInfo->GetModelName(pModel);
	if (pszModelName == nullptr)
	{
		return false;
	}

	unsigned int crc;

	int hitboxset = pEntity->GetHitboxSet();
	{
		Util::CRC32::Init(&crc);
		Util::CRC32::ProcessBuffer(&crc, pszModelName, strlen(pszModelName));
		Util::CRC32::ProcessBuffer(&crc, &hitboxset, sizeof(int));
		Util::CRC32::Final(&crc);

		if (m_HitBoxes.DoesExist(crc))
		{
			*pSet = &m_HitBoxes[crc];
			return true;
		}
	}

	auto pStudioHdr = Source::Interfaces::ModelInfo->GetStudioModel(pModel);
	if (pStudioHdr == nullptr)
	{
		return false;
	}

	HitBoxSet& set = m_HitBoxes[crc];
	set.m_nHitBoxes = 0;

	auto pHitboxSet = pStudioHdr->pHitboxSet(hitboxset);
	for (int _iHitgroup = 1/*HITGROUP_HEAD*/; _iHitgroup <= 7/*HITGROUP_RIGHTLEG*/; _iHitgroup++)
	{
		int iHitgroup = _iHitgroup;

		if (iHitgroup == 4 || iHitgroup == 5)
		{
			iHitgroup += 2;
		}
		else if (iHitgroup == 6 || iHitgroup == 7)
		{
			iHitgroup -= 2;
		}

		int start = set.m_nHitBoxes;

		for (int idx = 0; idx < pHitboxSet->numhitboxes; idx++)
		{
			auto pHitbox = pHitboxSet->pHitbox(idx);
			if (pHitbox->group == iHitgroup)
			{
				set.m_HitBoxes[set.m_nHitBoxes].m_vMin = pHitbox->bbmin;
				set.m_HitBoxes[set.m_nHitBoxes].m_vMax = pHitbox->bbmax;
				set.m_HitBoxes[set.m_nHitBoxes].m_iBone = pHitbox->bone;
				set.m_HitBoxes[set.m_nHitBoxes].m_iGroup = pHitbox->group;
				set.m_HitBoxes[set.m_nHitBoxes].m_flSize = GetBoundingBoxSize(pHitbox->bbmin, pHitbox->bbmax);

				set.m_nHitBoxes++;
			}
		}

		if (set.m_nHitBoxes != start)
		{
			for (int i = start; i < set.m_nHitBoxes; i++)
			{
				for (int j = start; j < set.m_nHitBoxes - 1; j++)
				{
					if (set.m_HitBoxes[j].m_flSize < set.m_HitBoxes[j + 1].m_flSize)
					{
						auto tmp = set.m_HitBoxes[j];
						set.m_HitBoxes[j] = set.m_HitBoxes[j + 1];
						set.m_HitBoxes[j + 1] = tmp;
					}
				}
			}
		}
	}

	for (int idx = 0; idx < set.m_nHitBoxes; idx++)
	{
		set.m_HitBoxes[idx].ComputePoints(0.9f);
	}

	*pSet = &set;
	return true;
}

float HitBoxCache::GetBoundingBoxSize(Vector& min, Vector& max)
{
	float length = 0.f;

	for (int i = 0; i < 3; i++)
	{
		if ((min[i] > 0.f && max[i] > 0.f) || (min[i] < 0.f && max[i] < 0.f))
		{
			if (min[i] > max[i])
			{
				length += (min[i] - max[i]) * (min[i] - max[i]);
			}
			else
			{
				length += (max[i] - min[i]) * (max[i] - min[i]);
			}
		}
		else
		{
			float abs = fabsf(min[i]) + fabsf(max[i]);
			length += abs * abs;
		}
	}

	return length;
}