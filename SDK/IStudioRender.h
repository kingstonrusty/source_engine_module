#ifndef __HEADER_ISTUDIORENDER__
#define __HEADER_ISTUDIORENDER__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

#include "IMaterialSystem.h"
#include "CBaseEntity.h"

namespace Source
{
	enum OverrideType_t
	{
		OVERRIDE_NORMAL = 0,
		OVERRIDE_BUILD_SHADOWS,
		OVERRIDE_DEPTH_WRITE,
		OVERRIDE_SSAO_DEPTH_WRITE,
	};

	typedef void DrawModelResults_t;

	class DrawModelInfo_t
	{
	public:
		__forceinline Source::CBaseEntity* m_pClientEntity()
		{
			return *(Source::CBaseEntity**)((DWORD_PTR)this + 0x18);
		}
	};

	class IStudioRender;

	namespace Interfaces
	{
		extern IStudioRender* StudioRender;
	}
}

class Source::IStudioRender
{
public:
	static IStudioRender* GetPtr();
	
	void SetColorModulation(float* color);
	void SetColorModulation(Color clr);
	void SetAlphaModulation(float alpha);
	void DrawModel(DrawModelResults_t* results, DrawModelInfo_t* info, matrix3x4* bonetoworld, float* flexweights, float* flexdelayedweights, Vector& modelorigin, int flags);
	void ForcedMaterialOverride(IMaterial* material, OverrideType_t overridetype = OVERRIDE_NORMAL);
};

#endif