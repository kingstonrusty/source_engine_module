#include "IVEngineClient.h"

namespace Source
{
	namespace Interfaces
	{
		IVEngineClient* Engine;
	}
}

using namespace Source;

IVEngineClient* IVEngineClient::GetPtr()
{
	return (IVEngineClient*)::Util::BruteforceInterface("VEngineClient", "engine.dll");
}

int IVEngineClient::GetLocalPlayer()
{
	return GET_VFUNC(int(__thiscall*)(IVEngineClient*), this, IVENGINECLIENT_GETLOCALPLAYER)(this);
}

void IVEngineClient::GetViewAngles(Vector& angles)
{
	return GET_VFUNC(void(__thiscall*)(IVEngineClient*, Vector&), this, IVENGINECLIENT_GETVIEWANGLES)(this, angles);
}

void IVEngineClient::SetViewAngles(Vector& angles)
{
	return GET_VFUNC(void(__thiscall*)(IVEngineClient*, Vector&), this, IVENGINECLIENT_SETVIEWANGLES)(this, angles);
}

bool IVEngineClient::IsInGame()
{
	return GET_VFUNC(bool(__thiscall*)(IVEngineClient*), this, IVENGINECLIENT_ISINGAME)(this);
}

CNetChan* IVEngineClient::GetNetChannelInfo()
{
	return GET_VFUNC(CNetChan*(__thiscall*)(IVEngineClient*), this, IVENGINECLIENT_GETNETCHANNELINFO)(this);
}

#if defined(GAME_GMOD)
bool IVEngineClient::IsTakingScreenshot()
{
	return GET_VFUNC(bool(__thiscall*)(IVEngineClient*), this, IVENGINECLIENT_ISTAKINGSCREENSHOT)(this);
}
#endif

void IVEngineClient::ExecuteClientCmd(const char* cmd)
{
	return GET_VFUNC(void(__thiscall*)(IVEngineClient*, const char*), this, IVENGINECLIENT_EXECUTECLIENTCMD)(this, cmd);
}