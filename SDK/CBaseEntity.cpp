#include "CBaseEntity.h"

using namespace Source;

#if !defined(GAME_L4D2)
CBoneCache* CBoneCache::CreateBoneCache(Source::CBaseEntity* pEntity, matrix3x4* pBoneToWorld, unsigned int& handle)
{
	Source::CStudioHdr* pStudioHdr = pEntity->GetModelPtr();
	if (pStudioHdr == nullptr)
	{
		return nullptr;
	}

	struct bonecacheparams_t
	{
		CStudioHdr		*pStudioHdr;
		matrix3x4		*pBoneToWorld;
		float			curtime;
		int				boneMask;
	} params;

	params.pStudioHdr = pStudioHdr;
	params.pBoneToWorld = pBoneToWorld;
	params.curtime = Source::Interfaces::Globals->curtime();
	params.boneMask = 0x100;

	static unsigned int(__cdecl* pStudio_CreateBoneCache)(bonecacheparams_t*) = Memory::FindPattern<unsigned int(__cdecl*)(bonecacheparams_t*)>(CBONECACHE_STUDIO_CREATEBONECACHE, "client.dll");

	handle = pStudio_CreateBoneCache(&params);

	static CBoneCache*(__cdecl* pStudio_GetBoneCache)(unsigned int) = nullptr;
	if (pStudio_GetBoneCache == nullptr)
	{
		DWORD_PTR dwpCall = Memory::FindPattern<DWORD_PTR>(CBONECACHE_STUDIO_GETBONECACHE, "client.dll");

		pStudio_GetBoneCache = (CBoneCache*(__cdecl*)(unsigned int))(dwpCall + *(DWORD_PTR*)dwpCall + 0x4);
	}

	return pStudio_GetBoneCache(handle);
}

void CBoneCache::DestroyBoneCache(unsigned int handle)
{
	static void(__cdecl* pStudio_DestroyBoneCache)(unsigned int) = nullptr;
	if (pStudio_DestroyBoneCache == nullptr)
	{
		DWORD_PTR dwpCall = Memory::FindPattern<DWORD_PTR>(CBONECACHE_STUDIO_DESTROYBONECACHE, "client.dll");

		pStudio_DestroyBoneCache = (void(__cdecl*)(unsigned int))(dwpCall + *(DWORD_PTR*)dwpCall + 0x4);
	}

	return pStudio_DestroyBoneCache(handle);
}
#endif

void CBaseEntity::PushAllowBoneAccess(bool normalmodels, bool viewmodels, const char* tagpush)
{
	static void(__cdecl* pPushAllowBoneAccess)(bool, bool, const char*) = Memory::FindPattern<void(__cdecl*)(bool, bool, const char*)>(CBASEENTITY_PUSHALLOWBONEACCESS, "client.dll");
	
	return pPushAllowBoneAccess(normalmodels, viewmodels, tagpush);
}

void CBaseEntity::PopAllowBoneAccess()
{
#if defined(GAME_L4D2)
	static void(__cdecl* pPopAllowBoneAccess)() = nullptr;
	if (pPopAllowBoneAccess == nullptr)
	{
		DWORD_PTR dwpCall = Memory::FindPattern<DWORD_PTR>(CBASEENTITY_POPALLOWBONEACCESS, "client.dll");

		pPopAllowBoneAccess = ((void(__cdecl*)())(dwpCall + *(DWORD_PTR*)dwpCall + 0x4));
	}

	return pPopAllowBoneAccess();
#else
	static void(__cdecl* pPopAllowBoneAccess)() = Memory::FindPattern<void(__cdecl*)()>(CBASEENTITY_POPALLOWBONEACCESS, "client.dll");

	return pPopAllowBoneAccess();
#endif
}

IClientRenderable* CBaseEntity::GetRenderable()
{
	return (IClientRenderable*)((DWORD_PTR)this + 0x4);
}

IClientNetworkable* CBaseEntity::GetNetworkable()
{
	return (IClientNetworkable*)((DWORD_PTR)this + 0x8);
}

void CBaseEntity::SetAbsOrigin(Vector& origin)
{
	static void(__thiscall* pSetAbsOrigin)(CBaseEntity*, Vector&) = Memory::FindPattern<void(__thiscall*)(CBaseEntity*, Vector&)>(CBASEENTITY_SETABSORIGIN, "client.dll");

	pSetAbsOrigin(this, origin);
}

void CBaseEntity::SetAbsAngles(Vector& angles)
{
	static void(__thiscall* pSetAbsAngles)(CBaseEntity*, Vector&) = Memory::FindPattern<void(__thiscall*)(CBaseEntity*, Vector&)>(CBASEENTITY_SETABSANGLES, "client.dll");

	pSetAbsAngles(this, angles);
}

void CBaseEntity::SetAbsVelocity(Vector& velocity)
{
	static void(__thiscall* pSetAbsVelocity)(CBaseEntity*, Vector&) = Memory::FindPattern<void(__thiscall*)(Source::CBaseEntity*, Vector&)>(CBASEENTITY_SETABSVELOCITY, "client.dll");

	return pSetAbsVelocity(this, velocity);
}

void CBaseEntity::InvalidatePhysicsRecursive(int flags)
{
	static void(__thiscall* pInvalidatePhysicsRecursive)(CBaseEntity*, int) = Memory::FindPattern<void(__thiscall*)(CBaseEntity*, int)>(CBASEENTITY_INVALIDATEPHYSICSRECURSIVE, "client.dll");

	pInvalidatePhysicsRecursive(this, flags);
}

void CBaseEntity::SetModelIndex(int modelindex)
{
	/*static void(__thiscall* pSetModelIndex)(CBaseEntity*, int) = Memory::FindPattern<void(__thiscall*)(CBaseEntity*, int)>(CBASEENTITY_SETMODELINDEX, "client.dll");

	return pSetModelIndex(this, modelindex);*/
}

const char* CBaseEntity::GetClassName()
{
	static const char*(__thiscall* pGetClassName)(CBaseEntity*) = Memory::FindPattern<const char*(__thiscall*)(Source::CBaseEntity*)>(CBASEENTITY_GETCLASSNAME, "client.dll");

	return pGetClassName(this);
}

bool CBaseEntity::IsPlayer()
{
	return GET_VFUNC(bool(__thiscall*)(CBaseEntity*), this, CBASEENTITY_ISPLAYER)(this);
}

bool CBaseEntity::IsBaseCombatCharacter()
{
	return GET_VFUNC(bool(__thiscall*)(CBaseEntity*), this, CBASEENTITY_ISBASECOMBATCHARACTER)(this);
}

bool CBaseEntity::IsNPC()
{
#if defined(GAME_L4D2)
	int iClassID = GetNetworkable()->GetClientClass()->m_ClassID;

	return (
		GET_VFUNC(bool(__thiscall*)(CBaseEntity*), this, CBASEENTITY_ISNPC)(this) ||
		iClassID == 263 ||
		iClassID == 264 ||
		iClassID == 275 ||
		iClassID == 99 ||
		iClassID == 264 ||
		iClassID == 262 ||
		iClassID == 269 ||
		iClassID == 271 ||
		iClassID == 0 ||
		iClassID == 276
	);
#else
	return GET_VFUNC(bool(__thiscall*)(CBaseEntity*), this, CBASEENTITY_ISNPC)(this);
#endif
}

bool CBaseEntity::IsNextBot()
{
#if defined(GAME_L4D2) || defined(GAME_GMOD)
	return GET_VFUNC(bool(__thiscall*)(CBaseEntity*), this, CBASEENTITY_ISNEXTBOT)(this);
#else
	return false;
#endif
}

bool CBaseEntity::IsBaseCombatWeapon()
{
	return GET_VFUNC(bool(__thiscall*)(CBaseEntity*), this, CBASEENTITY_ISBASECOMBATWEAPON)(this);
}

#if defined(GAME_L4D2)
const char* CBaseEntity::GetSequenceName(int sequence)
{
	static const char*(__thiscall* pGetSequenceName)(CBaseEntity*, int) = Memory::FindPattern<const char*(__thiscall*)(CBaseEntity*, int)>(CBASEENTITY_GETSEQUENCENAME, "client.dll");

	return pGetSequenceName(this, sequence);
}
#endif

#if !defined(GAME_L4D2)
CStudioHdr* CBaseEntity::GetModelPtr()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(CBASEENTITY_STUDIOHDR, "client.dll");

	return *(CStudioHdr**)((DWORD_PTR)this + s_dwOffset);
}

unsigned int& CBaseEntity::m_hitboxBoneCacheHandle()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(CBASEENTITY_HITBOXBONECACHEHANDLE, "client.dll");

	return *(unsigned int*)((DWORD_PTR)this + s_dwOffset);
}
#else
int& CBaseEntity::m_nCachedBones()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(CBASEENTITY_CACHEDBONENUM, "client.dll");

	return *(int*)((DWORD_PTR)this + s_dwOffset);
}

matrix3x4* CBaseEntity::GetCachedBone(int idx)
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(CBASEENTITY_CACHEDBONEMATRICES, "client.dll");

	return (matrix3x4*)(idx * sizeof(matrix3x4) + *(DWORD*)((DWORD_PTR)this + s_dwOffset));
}

void CBaseEntity::SetCachedBone(int idx, matrix3x4* matrix)
{
	memcpy(GetCachedBone(idx), matrix, sizeof(matrix3x4));
}
#endif