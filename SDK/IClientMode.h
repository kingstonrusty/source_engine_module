#ifndef __HEADER_ICLIENTMODE__
#define __HEADER_ICLIENTMODE__
#pragma once

#include "IBaseClientDLL.h"
#include "IVRenderView.h"

#include "CUserCmd.h"

namespace Source
{
	class IClientMode;

	namespace Interfaces
	{
		extern IClientMode* ClientMode;
	}
}

class Source::IClientMode
{
public:
	static IClientMode* GetPtr();

	void OverrideView(CViewSetup* setup);
	bool CreateMove(float sampletime, CUserCmd* cmd);
};

#endif