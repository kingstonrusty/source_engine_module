#include "FireBullets.h"
#include "SetViewAngles.h"

#include "Features/Accuracy/NoSpread.h"

#if defined(GAME_GMOD)
void __fastcall Hooks::Client::FireBullets(Source::CBasePlayer* pThis, void*, Source::FireBulletsInfo_t* info)
{
	pFireBullets->Unhook();
	pFireBullets->GetOriginal<void(__thiscall*)(Source::CBasePlayer*, void*)>()(pThis, info);
	pFireBullets->Rehook();

	static int s_nLastFrameCount = 0;

	if (Source::Interfaces::Prediction->IsFirstTimePredicted() && s_nLastFrameCount != Source::Interfaces::Globals->framecount() && pThis->GetNetworkable()->EntIndex() == Source::Interfaces::Engine->GetLocalPlayer()) // this will work for !!proper!! bases [not m9k gay shit]
	{
		s_nLastFrameCount = Source::Interfaces::Globals->framecount();

		auto pWeapon = (Source::CBaseWeapon*)Source::Interfaces::EntityList->GetClientEntityFromHandle(pThis->GetActiveWeaponHandle());
		if (pWeapon != nullptr)
		{
			if (strstr(pWeapon->GetClassName(), "m9k"))
			{
				if (info->m_vecSpread().x == 0.f && info->m_vecSpread().y == 0.f)
				{
					return;
				}
			}
			else if (strncmp(pWeapon->GetClassName(), "fas2", 4) == 0 || strncmp(pWeapon->GetClassName(), "cw_", 3) == 0)
			{
				return;
			}
		}

		Features::Accuracy::NoSpread::RecordSpread(pThis, info);
	}
}
#endif