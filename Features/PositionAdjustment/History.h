#ifndef __HEADER_HISTORY__
#define __HEADER_HISTORY__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace PositionAdjustment
	{
		class History;
	}
}

class Features::PositionAdjustment::History
{
public:
	struct Snapshot
	{
	public:
		//int m_nInSequenceNr;
		matrix3x4 m_BoneCache[128];

		Vector m_vecOrigin;
		Vector m_vecAngles;
		Vector m_vecMins;
		Vector m_vecMaxs;
		
		float m_flSimulationTime;
		float m_flArriveTime;
	};

	static void Record();
	static void ClearRecordsForIndex(int idx);

	static float GetLerpTime();
	static float GetLowestPossibleLerpTime(int* nUpdateRate = nullptr);
	static int EstimateServerArriveTick();

	static bool CanRestoreToSimulationTime(float flSimulationTime, bool* bNeedToAdjustInSequence = nullptr);
	static bool IsDeltaTooBig(Vector& vPos1, Vector& vPos2);
	static void GetUsableRecordsForEntity(Source::CBaseEntity* pEntity, SmallVector<Snapshot*>* vecRecords, float flMinTime = -1.f, float flMaxTime = -1.f, bool* bShouldLagFix = nullptr);
	static void GetUsableRecordsForIndex(int idx, SmallVector<Snapshot*>* vecRecords, float flMinTime = -1.f, float flMaxTime = -1.f, bool* bShouldLagFix = nullptr);
private:
	static LinkedList<Snapshot> m_Snapshots[4096];
};

#endif