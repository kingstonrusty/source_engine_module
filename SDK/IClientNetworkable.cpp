#include "IClientNetworkable.h"

using namespace Source;

ClientClass* IClientNetworkable::GetClientClass()
{
	return GET_VFUNC(ClientClass*(__thiscall*)(IClientNetworkable*), this, 1)(this);
}

bool IClientNetworkable::IsDormant()
{
	return GET_VFUNC(bool(__thiscall*)(IClientNetworkable*), this, ICLIENTNETWORKABLE_ISDORMANT)(this);
}

int IClientNetworkable::EntIndex()
{
	return GET_VFUNC(int(__thiscall*)(IClientNetworkable*), this, ICLIENTNETWORKABLE_ENTINDEX)(this);
}