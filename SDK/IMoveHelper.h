#ifndef __HEADER_IMOVEHELPER__
#define __HEADER_IMOVEHELPER__
#pragma once

#include "CBasePlayer.h"

namespace Source
{
	class IMoveHelper;
}

class Source::IMoveHelper
{
public:
	void SetHost(Source::CBasePlayer* player);
};

#endif