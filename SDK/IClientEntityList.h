#ifndef __HEADER_ICLIENTENTITYLIST__
#define __HEADER_ICLIENTENTITYLIST__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

#include "IClientEntity.h"

namespace Source
{
	typedef void* EHANDLE;

	class IClientEntityList;

	namespace Interfaces
	{
		extern IClientEntityList* EntityList;
	}
}

class Source::IClientEntityList
{
public:
	static IClientEntityList* GetPtr();

	IClientEntity* GetClientEntity(int idx);
	IClientEntity* GetClientEntityFromHandle(EHANDLE handle);
	int GetHighestEntityIndex();
};

#endif