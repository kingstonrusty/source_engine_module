#include "IClientEntity.h"

using namespace Source;

const Vector& IClientEntity::GetAbsOrigin()
{
	return GET_VFUNC(const Vector&(__thiscall*)(IClientEntity*), this, ICLIENTENTITY_GETABSORIGIN)(this);
}

const Vector& IClientEntity::GetAbsAngles()
{
	return GET_VFUNC(const Vector&(__thiscall*)(IClientEntity*), this, ICLIENTENTITY_GETABSANGLES)(this);
}