#include "DrawModel.h"

#include "Features/Visuals/Chams.h"
#include "Features/Visuals/AntiScreengrab.h"

#include "Features/Visuals/Menu.h"

void __fastcall Hooks::StudioRender::DrawModel(Source::IStudioRender* pThis, void*, Source::DrawModelResults_t* results, Source::DrawModelInfo_t* info, matrix3x4* bonetoworld, float* flexweights, float* flexdelayedweights, Vector& modelorigin, int flags)
{
	FINDVAR(visuals, "Visuals");
	FINDVAR(visuals_chams, "Visuals", "Chams");

	pVmt->Unhook();

	if (visuals->GetBool() && visuals_chams->GetBool())
	{
		if (!Features::Visuals::ShouldClearScreen())
		{
			if (info->m_pClientEntity())
			{
				Source::CBaseEntity* pClientEntity = (Source::CBaseEntity*)((DWORD_PTR)info->m_pClientEntity() - 0x4);

				if (Features::Visuals::Chams::ShouldDrawChamsOnEntity(pClientEntity))
				{
					pVmt->Rehook();
					return;
				}
			}
		}
	}

	pThis->DrawModel(results, info, bonetoworld, flexweights, flexdelayedweights, modelorigin, flags);
	pVmt->Rehook();
}