#ifndef __HEADER_FRAMESTAGENOTIFY__
#define __HEADER_FRAMESTAGENOTIFY__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace Client
	{
		extern CVMTHookManager* pVmt;

		void __fastcall FrameStageNotify(Source::IBaseClientDLL* pThis, void*, Source::ClientFrameStage_t stage);
	}
}

#endif