#ifndef __HEADER_RAGEBOT_AIMBOT__
#define __HEADER_RAGEBOT_AIMBOT__
#pragma once

#include "SDK/Source.h"

#include "AimbotLogic.h"

namespace Features
{
	namespace Ragebot
	{
		class Aimbot;
	}
}

class Features::Ragebot::Aimbot
{
public:
	static bool Run(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, bool* bSendPacket);
private:
	static bool Finish();
	static AimbotLogic::AimbotTarget* FindTarget();

	static Source::CBasePlayer* m_pLocal;
	static Source::CBaseWeapon* m_pWeapon;
	static Source::CUserCmd* m_pCurrentCommand;
	static AimbotLogic::AimbotTarget* m_pTarget;
};

#endif