#include "InterpLagComp.h"

using namespace Features::Misc;

float InterpLagComp::m_flInterp;
float InterpLagComp::m_flRatio;
bool InterpLagComp::m_bInterpolate;
bool InterpLagComp::m_bHasToChange;
bool InterpLagComp::m_bHasToReset;
int InterpLagComp::m_nResetTicks;

void InterpLagComp::WriteUserCmdDeltaToBufferCallback()
{
	if (m_bHasToReset)
	{
		ResetValues();

		return;
	}

	if (!m_bHasToChange)
	{
		return;
	}

	auto pNetChan = Source::Interfaces::Engine->GetNetChannelInfo();
	if (pNetChan == nullptr)
	{
		return;
	}

	{
		Source::NET_SetConVar net_interpolate("cl_interpolate", m_bInterpolate == 0 ? "0" : "1");
		pNetChan->SendNetMsg(net_interpolate, false, false);
	}

	{
		char buf[32];
		sprintf_s(buf, "%f", m_flInterp);

		Source::NET_SetConVar net_interp("cl_interp", buf);
		pNetChan->SendNetMsg(net_interp, false, false);
	}

	{
		char buf[32];
		sprintf_s(buf, "%f", m_flRatio);

		Source::NET_SetConVar net_interpratio("cl_interp_ratio", buf);
		pNetChan->SendNetMsg(net_interpratio, false, false);
	}

	m_bHasToChange = false;
	m_bHasToReset = true;
}

void InterpLagComp::ResetValues()
{
	auto cl_interpolate = Source::Interfaces::Cvar->FindVar("cl_interpolate");
	auto cl_interp = Source::Interfaces::Cvar->FindVar("cl_interp");
	auto cl_interp_ratio = Source::Interfaces::Cvar->FindVar("cl_interp_ratio");

	auto pNetChan = Source::Interfaces::Engine->GetNetChannelInfo();
	if (pNetChan == nullptr)
	{
		return;
	}

	{
		Source::NET_SetConVar net_interpolate("cl_interpolate", cl_interpolate->GetInt() == 0 ? "0" : "1");
		pNetChan->SendNetMsg(net_interpolate, false, false);
	}

	{
		char buf[32];
		sprintf_s(buf, "%f", cl_interp->GetFloat());

		Source::NET_SetConVar net_interp("cl_interp", buf);
		pNetChan->SendNetMsg(net_interp, false, false);
	}

	{
		char buf[32];
		sprintf_s(buf, "%f", cl_interp_ratio->GetFloat());

		Source::NET_SetConVar net_interpratio("cl_interp_ratio", buf);
		pNetChan->SendNetMsg(net_interpratio, false, false);
	}

	m_nResetTicks = 3;
	m_bHasToReset = false;
}

void InterpLagComp::UpdateDesiredValues(bool bInterpolate, float flInterp, float flRatio)
{
	static auto cl_interp = Source::Interfaces::Cvar->FindVar("cl_interp");
	static auto cl_interp_ratio = Source::Interfaces::Cvar->FindVar("cl_interp_ratio");

	m_bInterpolate = bInterpolate;

	m_flInterp = flInterp;
	if (m_flInterp == -1.f)
	{
		m_flInterp = cl_interp->GetFloat();
	}

	m_flRatio = flRatio;
	if (m_flRatio == -1.f)
	{
		m_flRatio = cl_interp_ratio->GetFloat();
	}

	m_bHasToChange = true;
}

void InterpLagComp::CancelUpdates()
{
	m_bHasToChange = false;
}

bool InterpLagComp::GetIsLocked()
{
#if defined(GAME_GMOD) || defined(GAME_L4D2)
	/*static int s_LastTickCount = 0;
	if (!m_bHasToReset && m_nResetTicks > 0 && s_LastTickCount != Source::Interfaces::Globals->tickcount())
	{
		s_LastTickCount = Source::Interfaces::Globals->tickcount();

		m_nResetTicks--;
	}*/

	return (m_bHasToReset/* || m_nResetTicks > 0*/);
#else
	return false;
#endif
}