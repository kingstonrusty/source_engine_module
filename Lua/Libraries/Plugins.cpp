#include "Plugins.h"

LinkedList<Plugins::CPlugin> Plugins::m_Plugins;

/*static int l_Register(lua_State* L);
static int l_IsActive(lua_State* L);
static int l_Exists(lua_State* L);*/

void Plugins::Fill(SmallVector<CPlugin*>* vecPlugins)
{
	auto pNode = m_Plugins.GetHead();
	while (pNode)
	{
		vecPlugins->Add(pNode->m_pThis);

		pNode = pNode->m_pNext;
	}
}

int Plugins::l_Register(lua_State* L)
{
	const char* pszName = luaL_checkstring(L, 1);
	const char* pszTable = luaL_checkstring(L, 2);
	const char* pszAuthor = luaL_checkstring(L, 3);
	const char* pszVersion = luaL_checkstring(L, 4);

	auto pNode = m_Plugins.GetHead();
	while (pNode)
	{
		if (!strcmp(pNode->m_pThis->GetName(), pszName))
		{
			luaL_error(L, "A plugin with the name '%s' already exists.", pszName);
			return 0;
		}

		if (!strcmp(pNode->m_pThis->GetTable(), pszTable))
		{
			luaL_error(L, "A plugin with the table '%s' already exists.", pszTable);
			return 0;
		}
		
		pNode = pNode->m_pNext;
	}

	CPlugin* pPlugin = new CPlugin(L, pszName, pszTable, pszAuthor, pszVersion);
	m_Plugins.Insert(pPlugin);
}

using CPlugin = Plugins::CPlugin;

CPlugin::CPlugin(lua_State* L, const char* pszName, const char* pszTable, const char* pszAuthor, const char* pszVersion)
{
	m_pszName = _strdup(pszName);
	m_pszTable = _strdup(pszTable);
	m_pszAuthor = _strdup(pszAuthor);
	m_pszVersion = _strdup(pszVersion);

	m_bHasMenuCallback = false;

	lua_getglobal(L, m_pszTable);
	if (lua_type(L, -1) == LUA_TTABLE)
	{
		lua_getfield(L, -1, "MenuCallback");

		if (lua_type(L, -1) == LUA_TFUNCTION)
		{
			m_bHasMenuCallback = true;
		}

		lua_pop(L, 1);
	}
	lua_pop(L, 1);

	m_pState = L;
}

CPlugin::~CPlugin()
{
	if (m_pszName) free(m_pszName);
	if (m_pszTable) free(m_pszTable);
	if (m_pszAuthor) free(m_pszAuthor);
	if (m_pszVersion) free(m_pszVersion);
}

const char* CPlugin::GetName()
{
	return m_pszName;
}

const char* CPlugin::GetTable()
{
	return m_pszTable;
}

const char* CPlugin::GetAuthor()
{
	return m_pszAuthor;
}

const char* CPlugin::GetVersion()
{
	return m_pszVersion;
}

bool CPlugin::HasMenuCallback()
{
	return m_bHasMenuCallback;
}

void CPlugin::MenuCallback(int x, int y, int w, int h)
{
	lua_getglobal(m_pState, GetTable());
	if (lua_type(m_pState, -1) == LUA_TTABLE)
	{
		lua_getfield(m_pState, -1, "MenuCallback");
		if (lua_type(m_pState, -1) == LUA_TFUNCTION)
		{
			lua_pushvalue(m_pState, -2); // self
			lua_pushinteger(m_pState, x);
			lua_pushinteger(m_pState, y);
			lua_pushinteger(m_pState, w);
			lua_pushinteger(m_pState, h);

			if (lua_pcall(m_pState, 5, 0, 0) != LUA_OK)
			{
				LuaBase::PrintError();
			}
		}
		else
		{
			lua_pop(m_pState, 1);
		}
	}
	lua_pop(m_pState, 1);
}