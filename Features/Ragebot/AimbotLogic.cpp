#include "AimbotLogic.h"

#include "Features/Misc/InterpLagComp.h"

#include "Features/Misc/BoneCacheHelper.h"

using namespace Features::Ragebot;

SmallVector<AimbotLogic::AimbotTarget*> AimbotLogic::m_vecTargets;
float AimbotLogic::m_flLowestDistance;

#if defined(GAME_L4D2)
bool AimbotLogic::m_bSpecialInfectedFound;
AimbotLogic::InfectedData AimbotLogic::m_InfectedData;
#endif

bool AimbotLogic::ShouldRunAimbot(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd)
{
	if (Features::Misc::InterpLagComp::GetIsLocked())
	{
		return false;
	}

	return true;
}

void AimbotLogic::ResetData()
{
	for (int i = 0; i < m_vecTargets.GetSize(); i++)
	{
		delete m_vecTargets.GetAt(i);
	}

	m_vecTargets.Clear();
	m_flLowestDistance = FLT_MAX;

#if defined(GAME_L4D2)
	m_bSpecialInfectedFound = false;

	m_InfectedData.m_bTankFound = false;
	m_InfectedData.m_flClosestTankDistance = 100000000.f;

	m_InfectedData.m_bChargerFound = false;
	m_InfectedData.m_flClosestChargerDistance = 100000000.f;

	m_InfectedData.m_bJockeyFound = false;
	m_InfectedData.m_flClosestJockeyDistance = 100000000.f;

	m_InfectedData.m_bHunterFound = false;
	m_InfectedData.m_flClosestHunterDistance = 100000000.f;

	m_InfectedData.m_bSmokerFound = false;
	m_InfectedData.m_flClosestSmokerDistance = 100000000.f;

	m_InfectedData.m_bSpitterFound = false;
	m_InfectedData.m_flClosestSpitterDistance = 100000000.f;

	m_InfectedData.m_bBoomerFound = false;
	m_InfectedData.m_flClosestBoomerDistance = 100000000.f;

	m_InfectedData.m_bWitchFound = false;
	m_InfectedData.m_flClosestWitchDistance = 100000000.f;

	m_InfectedData.m_flClosestCommonInfectedDistance = 100000000.f;
#endif
}

AimbotLogic::AimbotTarget* AimbotLogic::FindTarget(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd)
{
	for (int i = 1; i <= Source::Interfaces::EntityList->GetHighestEntityIndex(); i++)
	{
		if (i == Source::Interfaces::Engine->GetLocalPlayer())
		{
			continue;
		}

		Source::CBaseEntity* pEntity = (Source::CBaseEntity*)Source::Interfaces::EntityList->GetClientEntity(i);
		if (pEntity == nullptr)
		{
			continue;
		}

		Vector vAimPos;
		Features::PositionAdjustment::History::Snapshot* pTemp = nullptr;

		//if (pEntity->IsNPC() && ShouldAimAtNPCs() && IsValidNPC(pLocal, (Source::CBaseNPC*)pEntity, pWeapon, cmd, vAimPos, &pTemp))
		{
			//m_vecTargets.Add(new AimbotTarget{ pEntity, false, true, vAimPos, (pLocal->GetAbsOrigin() - pEntity->GetAbsOrigin()).Length2D(), pTemp });
		}
		
		if (pEntity->IsPlayer() && ShouldAimAtPlayers() && IsValidPlayer(pLocal, (Source::CBasePlayer*)pEntity, pWeapon, cmd, vAimPos, &pTemp))
		{
			m_vecTargets.Add(new AimbotTarget{ pEntity, true, false, vAimPos, (pLocal->GetAbsOrigin() - pEntity->GetAbsOrigin()).Length2D(), pTemp });
		}
	}

	if (m_vecTargets.GetSize() == 0)
	{
		return nullptr;
	}

	SortTargets(pLocal, pWeapon, cmd);

	return m_vecTargets.GetAt(0);
}

void AimbotLogic::SortTargets(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd)
{
#if defined(GAME_L4D2)
	if (m_InfectedData.m_bTankFound || m_InfectedData.m_bChargerFound || m_InfectedData.m_bWitchFound)
	{
		ClearPath(pLocal, pWeapon, cmd);
	}
	else if (m_bSpecialInfectedFound)
	{
		SortSpecialInfected();
	}
	else
	{
		SortCommonInfected();
	}
#endif
}

bool AimbotLogic::FinishAimbot(Source::CBasePlayer* pLocal, AimbotTarget* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd)
{
	CalcAngle(pLocal->GetAbsOrigin() + pLocal->GetViewOffset(), pTarget->m_vAimPosition, cmd->viewangles());
	AngleNormalize(cmd->viewangles());
	cmd->buttons() |= 1;

	if (pTarget->m_pSnapshot)
	{
		bool bAdjustInterp;
		Features::PositionAdjustment::History::CanRestoreToSimulationTime(pTarget->m_pSnapshot->m_flSimulationTime, &bAdjustInterp);

		if (bAdjustInterp)
		{

			float flInterp = Source::TICKS_TO_TIME(cmd->tick_count() - Source::TIME_TO_TICKS(pTarget->m_pSnapshot->m_flSimulationTime));

			Features::Misc::InterpLagComp::UpdateDesiredValues(true, flInterp, 1.f);

		}
		else
		{
			
			cmd->tick_count() = Source::TIME_TO_TICKS(pTarget->m_pSnapshot->m_flSimulationTime);

			Features::Misc::InterpLagComp::UpdateDesiredValues(false);
		}
	}

	return true;
}

bool AimbotLogic::ShouldAimAtPlayers()
{
	return true;
}

bool AimbotLogic::ShouldAimAtNPCs()
{
	return true;
}

bool AimbotLogic::ShouldPositionAdjustEntity(Source::CBaseEntity* pEntity)
{
	return true;
}

/*
*
* NPC logic here
*
*/

bool AimbotLogic::IsValidNPC(Source::CBasePlayer* pLocal, Source::CBaseNPC* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, Features::PositionAdjustment::History::Snapshot** ppSnapshot)
{
	static Vector s_Origin;
	static int s_LastFrameCount;
	if (s_LastFrameCount != Source::Interfaces::Globals->framecount())
	{
		s_Origin = pLocal->GetAbsOrigin();
	}

	if (!pTarget->IsAlive())
	{
		return false;
	}

#if defined(GAME_L4D2)
	if (pTarget->IsCommonInfected() && m_bSpecialInfectedFound)
	{
		return false;
	}

	float flDistance = (s_Origin - pTarget->GetAbsOrigin()).Length2D();
	if (flDistance >= GetClosestForInfectedType(pTarget))
	{
		return false;
	}

	if (pTarget->IsCommonInfected())
	{
		if (pTarget->IsNeutral())
		{
			if (flDistance > 350.f)
			{
				return false;
			}
		}
		else if(flDistance > 700.f)
		{
			return false;
		}
	}

	if (pTarget->IsWitch() && pTarget->IsNeutral())
	{
		return false;
	}
#endif

	SmallVector<Features::PositionAdjustment::History::Snapshot*> vecRecords;
	if (ShouldPositionAdjustEntity(pTarget))
	{
		Features::PositionAdjustment::History::GetUsableRecordsForEntity(pTarget, &vecRecords, 0.f, 0.2f);
	}

	bool bValid = FindVisibleSpot(pLocal, pTarget, pWeapon, cmd, vAimPos, &vecRecords, ppSnapshot);
#if defined(GAME_L4D2)
	if (bValid)
	{
		SetClosestForInfectedType(pTarget, flDistance);
	}
#endif

	return bValid;
}

/*
*
* Player Logic Here
*
*/

bool AimbotLogic::IsValidPlayer(Source::CBasePlayer* pLocal, Source::CBasePlayer* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, Features::PositionAdjustment::History::Snapshot** ppSnapshot)
{
	static Vector s_Origin;
	static int s_LastFrameCount;
	if (s_LastFrameCount != Source::Interfaces::Globals->framecount())
	{
		s_Origin = pLocal->GetAbsOrigin();
	}

	if (!pTarget->IsAlive())
	{
		return false;
	}

	if (pLocal->GetTeamNum() == pTarget->GetTeamNum())
	{
		return false;
	}

	float flDistance = (s_Origin - pTarget->GetAbsOrigin()).Length2D();
	if (flDistance >= m_flLowestDistance)
	{
		return false;
	}

	SmallVector<Features::PositionAdjustment::History::Snapshot*> vecRecords;
	if (ShouldPositionAdjustEntity(pTarget))
	{
		Features::PositionAdjustment::History::GetUsableRecordsForEntity(pTarget, &vecRecords, 0.f, 0.2f);
	}

	bool bValid = FindVisibleSpot(pLocal, pTarget, pWeapon, cmd, vAimPos, &vecRecords, ppSnapshot);

	if(bValid)	m_flLowestDistance = flDistance;

	return bValid;
}

bool AimbotLogic::FindVisibleSpot(Source::CBasePlayer* pLocal, Source::CBaseEntity* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, SmallVector<Features::PositionAdjustment::History::Snapshot*>* vecRecords, Features::PositionAdjustment::History::Snapshot** ppSnapshot)
{
	if (vecRecords->GetSize() != 0)
	{
		return FindVisibleSpot_PositionAdjusted(pLocal, pTarget, pWeapon, cmd, vAimPos, vecRecords, ppSnapshot);
	}

	if (ppSnapshot)
	{
		*ppSnapshot = nullptr;
	}

	if (pTarget->GetNetworkable()->IsDormant())
	{
		return false;
	}

	Features::Misc::HitBoxCache::HitBoxSet* pSet;
	if (!Features::Misc::HitBoxCache::GetSet(pTarget, &pSet))
	{
		return false;
	}

	matrix3x4 bones[128];
	if (!pTarget->GetRenderable()->SetupBones(bones, 128, 0x100, pTarget->GetSimulationTime()))
	{
		return false;
	}

	for (int j = 0; j < pSet->m_nHitBoxes; j++)
	{
		auto pHitBox = &pSet->m_HitBoxes[j];
		if (pHitBox->m_iGroup != 1)
		{
			continue;
		}

		if (ScanHitBox(pLocal, pTarget, pWeapon, cmd, vAimPos, pHitBox, &bones[pHitBox->m_iBone]))
		{
			return true;
		}
	}

	return false;
}

bool AimbotLogic::FindVisibleSpot_PositionAdjusted(Source::CBasePlayer* pLocal, Source::CBaseEntity* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, SmallVector<Features::PositionAdjustment::History::Snapshot*>* vecRecords, Features::PositionAdjustment::History::Snapshot** ppSnapshot)
{
	Features::Misc::HitBoxCache::HitBoxSet* pSet;
	if (!Features::Misc::HitBoxCache::GetSet(pTarget, &pSet))
	{
		*ppSnapshot = nullptr;
		return false;
	}

	Features::PositionAdjustment::History::Snapshot* pTemp = nullptr;
	Features::Misc::BoneCacheHelper::BackupCache(pTarget);

	for (int j = 0; j < pSet->m_nHitBoxes; j++)
	{
		auto pHitBox = &pSet->m_HitBoxes[j];
		if (pHitBox->m_iGroup != 1)
		{
			continue;
		}

		for (int i = 0; i < vecRecords->GetSize(); i++)
		{
			auto pRecord = vecRecords->GetAt(i);
			Features::Misc::BoneCacheHelper::OverrideCache(pTarget, pRecord);

			if (ScanHitBox(pLocal, pTarget, pWeapon, cmd, vAimPos, pHitBox, &pRecord->m_BoneCache[pHitBox->m_iBone]))
			{
				pTemp = pRecord;
				break;
			}
		}

		if (!(pTemp == nullptr))
		{
			break;
		}
	}

	Features::Misc::BoneCacheHelper::RestoreCache(pTarget);

	*ppSnapshot = pTemp;
	return !(pTemp == nullptr);
}

bool AimbotLogic::ScanHitBox(Source::CBasePlayer* pLocal, Source::CBaseEntity* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, Features::Misc::HitBoxCache::HitBox* pHitBox, matrix3x4* pBoneMatrix)
{
	for (int i = 0; i < 1; i++)
	{
		Vector vPoint;
		VectorTransform(pHitBox->m_Points[i], *pBoneMatrix, vPoint);

		if (IsPointVisible(pLocal, pTarget, pWeapon, cmd, vPoint))
		{
			vAimPos = vPoint;
			return true;
		}
	}

	return false;
}

bool AimbotLogic::IsPointVisible(Source::CBasePlayer* pLocal, Source::CBaseEntity* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vPoint)
{
	static Vector s_EyePos;
	static int s_LastFrameCount;

	if (s_LastFrameCount != Source::Interfaces::Globals->framecount())
	{
		s_LastFrameCount = Source::Interfaces::Globals->framecount();
		s_EyePos = pLocal->GetAbsOrigin() + pLocal->GetViewOffset();
	}

	Source::Ray_t ray;
	ray.Init(s_EyePos, vPoint);
	Source::CTraceFilterSkipTwoEntities filter(pLocal, pTarget);
	Source::trace_t tr;

	Source::Interfaces::TraceRay->TraceRay(ray, Source::MASK_SHOT, &filter, &tr);

	return tr.fraction == 1.f;
}

/*
*
* Game specific stuff here
*
*/

#if defined(GAME_L4D2)
float AimbotLogic::GetClosestForInfectedType(Source::CBaseNPC* pTarget)
{
	if (pTarget->IsTank())
	{
		return m_InfectedData.m_flClosestTankDistance;
	}

	if (pTarget->IsCharger())
	{
		return m_InfectedData.m_flClosestChargerDistance;
	}

	if (pTarget->IsJockey())
	{
		return m_InfectedData.m_flClosestJockeyDistance;
	}

	if (pTarget->IsHunter())
	{
		return m_InfectedData.m_flClosestHunterDistance;
	}

	if (pTarget->IsSmoker())
	{
		return m_InfectedData.m_flClosestSmokerDistance;
	}

	if (pTarget->IsSpitter())
	{
		return m_InfectedData.m_flClosestSpitterDistance;
	}

	if (pTarget->IsBoomer())
	{
		return m_InfectedData.m_flClosestBoomerDistance;
	}

	if (pTarget->IsWitch())
	{
		return m_InfectedData.m_flClosestWitchDistance;
	}

	return m_InfectedData.m_flClosestCommonInfectedDistance;
}

void AimbotLogic::SetClosestForInfectedType(Source::CBaseNPC* pTarget, float flDistance)
{
	if (pTarget->IsTank())
	{
		m_InfectedData.m_flClosestTankDistance = flDistance;
	}
	else if (pTarget->IsCharger())
	{
		m_InfectedData.m_flClosestChargerDistance = flDistance;
	}
	else if (pTarget->IsJockey())
	{
		m_InfectedData.m_flClosestJockeyDistance = flDistance;
	}
	else if (pTarget->IsHunter())
	{
		m_InfectedData.m_flClosestHunterDistance = flDistance;
	}
	else if (pTarget->IsSmoker())
	{
		m_InfectedData.m_flClosestSmokerDistance = flDistance;
	}
	else if (pTarget->IsSpitter())
	{
		m_InfectedData.m_flClosestSpitterDistance = flDistance;
	}
	else if (pTarget->IsBoomer())
	{
		m_InfectedData.m_flClosestBoomerDistance = flDistance;
	}
	else if (pTarget->IsWitch())
	{
		m_InfectedData.m_flClosestWitchDistance = flDistance;
	}
	else if (pTarget->IsCommonInfected())
	{
		m_InfectedData.m_flClosestCommonInfectedDistance = flDistance;
	}
}

void AimbotLogic::ClearPath(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd)
{
	if (m_InfectedData.m_bJockeyFound || m_InfectedData.m_bHunterFound || m_InfectedData.m_bHunterFound || m_InfectedData.m_bSpitterFound || m_InfectedData.m_bBoomerFound)
	{
		return SortSpecialInfected();
	}

	m_bSpecialInfectedFound = false; // HACK HACK

	AimbotTarget* pCommonTarget = nullptr;
	Vector vOrigin = pLocal->GetAbsOrigin();
	float flLowestDistance = 500.f;

	for (int i = Source::Interfaces::Globals->maxClients() + 1/*skip special infected*/; i <= Source::Interfaces::EntityList->GetHighestEntityIndex(); i++)
	{
		Source::CBaseNPC* pTarget = (Source::CBaseNPC*)Source::Interfaces::EntityList->GetClientEntity(i);
		if (pTarget == nullptr)
		{
			continue;
		}

		if (!pTarget->IsCommonInfected())
		{
			continue;
		}

		if (!pTarget->IsAlive())
		{
			continue;
		}

		float flDistance = (vOrigin - pTarget->GetAbsOrigin()).Length2D();
		if (flDistance >= flLowestDistance)
		{
			continue;
		}

		SmallVector<Features::PositionAdjustment::History::Snapshot*> vecRecords;
		if (ShouldPositionAdjustEntity(pTarget))
		{
			Features::PositionAdjustment::History::GetUsableRecordsForEntity(pTarget, &vecRecords, 0.f, 0.1f);
		}

		Vector vAimPos;
		Features::PositionAdjustment::History::Snapshot* pTemp = nullptr;

		bool bValid = FindVisibleSpot(pLocal, pTarget, pWeapon, cmd, vAimPos, &vecRecords, &pTemp);
		if (bValid)
		{
			m_bSpecialInfectedFound = true;
			flLowestDistance = flDistance;

			if (pCommonTarget)
			{
				delete pCommonTarget;
			}

			pCommonTarget = new AimbotTarget{ pTarget, false, true, vAimPos, flDistance, pTemp };
		}
	}

	if (pCommonTarget != nullptr)
	{
		auto pTemp = m_vecTargets.GetAt(0);

		m_vecTargets.Add(pCommonTarget);
		m_vecTargets.SetAt(0, pCommonTarget);
		m_vecTargets.SetAt(m_vecTargets.GetSize() - 1, pTemp);
		
		return;
	}

	int iScanForThis;
	if (m_InfectedData.m_bChargerFound)
	{
		iScanForThis = 99;
		flLowestDistance = m_InfectedData.m_flClosestChargerDistance;
	}
	else if (m_InfectedData.m_bWitchFound)
	{
		iScanForThis = 276;
		flLowestDistance = m_InfectedData.m_flClosestWitchDistance;
	}
	else
	{
		iScanForThis = 275;
		flLowestDistance = m_InfectedData.m_flClosestTankDistance;
	}

	for (int i = 0; i < m_vecTargets.GetSize(); i++)
	{
		auto pTarget = (Source::CBaseNPC*)m_vecTargets.GetAt(i)->m_pEntity;

		if (pTarget->GetNetworkable()->GetClientClass()->m_ClassID == iScanForThis && m_vecTargets.GetAt(i)->m_flDistance == flLowestDistance)
		{
			auto pTemp = m_vecTargets.GetAt(0);

			m_vecTargets.SetAt(0, m_vecTargets.GetAt(i));
			m_vecTargets.SetAt(i, pTemp);

			break;
		}
	}
}

void AimbotLogic::SortSpecialInfected()
{
	/*
	priority:
	jockey - hunter - smoker - spitter - boomer
	*/

	int iScanForThis;
	float flLowestDistance;
	if (m_InfectedData.m_bJockeyFound)
	{
		iScanForThis = 264;
		flLowestDistance = m_InfectedData.m_flClosestJockeyDistance;
	}
	else if (m_InfectedData.m_bHunterFound)
	{
		iScanForThis = 262;
		flLowestDistance = m_InfectedData.m_flClosestHunterDistance;
	}
	else if (m_InfectedData.m_bSmokerFound)
	{
		iScanForThis = 269;
		flLowestDistance = m_InfectedData.m_flClosestSmokerDistance;
	}
	else if (m_InfectedData.m_bSpitterFound)
	{
		iScanForThis = 271;
		flLowestDistance = m_InfectedData.m_flClosestSpitterDistance;
	}
	else if (m_InfectedData.m_bBoomerFound)
	{
		iScanForThis = 0;
		flLowestDistance = m_InfectedData.m_flClosestBoomerDistance;
	}

	for (int i = 0; i < m_vecTargets.GetSize(); i++)
	{
		auto pTarget = (Source::CBaseNPC*)m_vecTargets.GetAt(i)->m_pEntity;

		if (pTarget->GetNetworkable()->GetClientClass()->m_ClassID == iScanForThis && m_vecTargets.GetAt(i)->m_flDistance == flLowestDistance)
		{
			auto pTemp = m_vecTargets.GetAt(0);

			m_vecTargets.SetAt(0, m_vecTargets.GetAt(i));
			m_vecTargets.SetAt(i, pTemp);

			break;
		}
	}
}

void AimbotLogic::SortCommonInfected()
{
	for (int i = 0; i < m_vecTargets.GetSize(); i++)
	{
		auto pTarget = (Source::CBaseNPC*)m_vecTargets.GetAt(i)->m_pEntity;

		if (pTarget->IsCommonInfected() && m_vecTargets.GetAt(i)->m_flDistance == m_InfectedData.m_flClosestCommonInfectedDistance)
		{
			auto pTemp = m_vecTargets.GetAt(0);

			m_vecTargets.SetAt(0, m_vecTargets.GetAt(i));
			m_vecTargets.SetAt(i, pTemp);

			break;
		}
	}
}
#endif