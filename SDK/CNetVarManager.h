#ifndef __HEADER_CNETVARMANAGER__
#define __HEADER_CNETVARMANAGER__
#pragma once

#include "IBaseClientDLL.h"

#include "IClientNetworkable.h"

namespace Source
{
	class CNetVarManager;

	namespace Util
	{
		extern CNetVarManager* NetVarManager;
	}
}

#undef GetProp

class Source::CNetVarManager
{
public:
	CNetVarManager();

	offset_t GetOffset(const char* pszDataTable, const char* pszProp);
	RecvVarProxyFn HookProp(const char* pszDataTable, const char* pszProp, void* pHook);
private:
	offset_t GetProp(const char*, const char*, CRecvProp** = nullptr);
	offset_t GetProp(CRecvTable*, const char*, CRecvProp** = nullptr);
	CRecvTable* GetTable(const char*);
	SmallVector<CRecvTable*> m_vecRecvTable;
};

#endif