#ifndef __HEADER_IVMODELINFO__
#define __HEADER_IVMODELINFO__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

#include "IClientRenderable.h"

namespace Source
{
	class mstudiobone_t
	{
	public:
		int					sznameindex;
		inline char * const pszName(void) const { return ((char *)this) + sznameindex; }
		int		 			parent;		// parent bone
		int					bonecontroller[6];	// bone controller index, -1 == none

												// default values
		Vector				pos;
		byte				quat[16]; //Quaternion
		byte				rot[16]; // RadianEuler
									 // compression scale
		Vector				posscale;
		Vector				rotscale;

		matrix3x4			poseToBone;
		byte				qAlignment[16];
		int					flags;
		int					proctype;
		int					procindex;		// procedural rule
		mutable int			physicsbone;	// index into physically simulated bone
		inline void *pProcedure() const { if (procindex == 0) return NULL; else return  (void *)(((byte *)this) + procindex); };
		int					surfacepropidx;	// index into string tablefor property name
		inline char * const pszSurfaceProp(void) const { return ((char *)this) + surfacepropidx; }
		inline int			GetSurfaceProp(void) const { return surfacepropLookup; }

		int					contents;		// See BSPFlags.h for the contents flags
		int					surfacepropLookup;	// this index must be cached by the loader, not saved in the file
		int					unused[7];		// remove as appropriate
	};

	class mstudiobbox_t
	{
	public:
		int					bone;
		int					group;
		Vector				bbmin;
		Vector				bbmax;
		int					szhitboxnameindex;
		int					unused[8];
		char* pszHitboxName() {
			if (szhitboxnameindex == 0)
				return "";
			return ((char*)this) + szhitboxnameindex;
		}
	};

	class mstudiohitboxset_t
	{
	public:
		int					sznameindex;
		inline char * const	pszName(void) const { return ((char *)this) + sznameindex; }
		int					numhitboxes;
		int					hitboxindex;
		inline mstudiobbox_t *pHitbox(int i) const { return (mstudiobbox_t *)(((byte *)this) + hitboxindex) + i; };
	};

	class studiohdr_t
	{
	public:
		int& hitboxsetindex()
		{
			return *(int*)((DWORD_PTR)this + 0xB0);
		}

		int& numbones()
		{
			return *(int*)((DWORD_PTR)this + 0x9C);
		}

		int& boneindex() const
		{
			return *(int*)((DWORD_PTR)this + 0xA0);
		}

		mstudiobone_t* pBone(int index) const
		{
			return (mstudiobone_t *)(((byte *)this) + boneindex()) + index;
		}

		mstudiohitboxset_t* pHitboxSet(int index)
		{
			return (mstudiohitboxset_t *)(((byte *)this) + hitboxsetindex()) + index;
		}

		int& numtextures()
		{
			return *(int*)((DWORD_PTR)this + 0xCC);
		}
	};

	class IVModelInfo;

	namespace Interfaces
	{
		extern IVModelInfo* ModelInfo;
	}
}

class Source::IVModelInfo
{
public:
	static IVModelInfo* GetPtr();

	int GetModelIndex(const char* name);
	const char* GetModelName(const model_t* model);
	studiohdr_t* GetStudioModel(const model_t* model);
	const model_t* FindOrLoadModel(const char* name);
	//void GetModelMaterials(const model_t* model, int count, IMaterial** materials)
};

#endif