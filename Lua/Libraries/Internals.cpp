#include "Internals.h"

int Internals::l_include(lua_State* L)
{
	if (!lua_isstring(L, 1))
	{
		luaL_error(L, "file is not a string value");
		LuaBase::PrintError();

		return 0;
	}

	char* buf = nullptr;

	{
		lua_Debug dbg;
		lua_getstack(L, 1, &dbg);
		lua_getinfo(L, "S", &dbg);

		const char* str = dbg.source;
		str++;

		int len = strlen(str);

		for (int i = len - 1; i >= 0; i--, len--)
		{
			if (str[i] == '\\' || str[i] == '/')
			{
				break;
			}
		}

		if (len != 0)
		{
			buf = new char[len + 1];
			memcpy(buf, str, len);
			buf[len] = '\0';
		}
	}

	if (buf != nullptr)
	{
		const char* tmp = buf;

		buf = new char[strlen(tmp) + strlen(luaL_checkstring(L, 1)) + 1];
		memcpy(buf, tmp, strlen(tmp));
		memcpy(buf + strlen(tmp), luaL_checkstring(L, 1), strlen(luaL_checkstring(L, 1)));
		buf[strlen(tmp) + strlen(luaL_checkstring(L, 1))] = '\0';

		delete[] tmp;
	}

	LuaBase::RunFile((buf) ? buf : luaL_checkstring(L, 1));

	if (buf)
	{
		delete[] buf;
	}

	return 0;
}

int Internals::l_getgame(lua_State* L)
{
#if defined(GAME_GMOD)
	lua_pushstring(L, "Garry's Mod");
#endif

	return 1;
}