#include "FrameStageNotify.h"

#include "Features/PositionAdjustment/History.h"
#include "Features/PositionAdjustment/Interpolation.h"

#if defined(GAME_CSS_STEAM)
namespace Hooks
{
	namespace NetChannel
	{
		extern void SetupHook();
	}
}
#endif

static bool s_bDo;

void __fastcall Hooks::Client::FrameStageNotify(Source::IBaseClientDLL* pThis, void*, Source::ClientFrameStage_t stage)
{
	if (!Source::Interfaces::Engine->IsInGame())
	{
		for (int idx = 1; idx <= 4096; idx++)
		{
			Features::PositionAdjustment::History::ClearRecordsForIndex(idx - 1);
		}
	}

	/*if (Source::Interfaces::Engine->IsInGame())
	{
		if (!s_bDo)
		{
			auto pNetChan = Source::Interfaces::Engine->GetNetChannelInfo();
			if (pNetChan != nullptr)
			{
				Source::NET_SetConVar cl_interpolate("cl_interpolate", "0");
				pNetChan->SendNetMsg(cl_interpolate, 0, 0);

				s_bDo = true;
			}
		}
	}
	else
	{
		s_bDo = false;
	}*/

	pVmt->Unhook();
	pThis->FrameStageNotify(stage);
	pVmt->Rehook();

	if (stage == Source::FRAME_NET_UPDATE_POSTDATAUPDATE_START)
	{
		Features::PositionAdjustment::Interpolation::Disable();
		Features::PositionAdjustment::History::Record();
		Features::PositionAdjustment::Interpolation::Reset();
	}

	/*static bool s_bOnce = false;
	if (stage == Source::FRAME_NET_UPDATE_POSTDATAUPDATE_START && Source::Interfaces::Engine->IsInGame())
	{
		
		if (!s_bOnce)
		{
			s_bOnce = true;

			Source::Interfaces::ModelInfo->FindOrLoadModel("models/props_vehicles/racecar.mdl");
			Source::Interfaces::StringTableContainer->FindTable("modelprecache")->AddString(false, "models/props_vehicles/racecar.mdl");
		}



		for (int i = 1; i <= Source::Interfaces::Globals->maxClients(); i++)
		{
			auto pLocal = (Source::CBasePlayer*)Source::Interfaces::EntityList->GetClientEntity(i);
			if (pLocal)
			{
				if (i == Source::Interfaces::Engine->GetLocalPlayer() || pLocal->GetModelIndex() != Source::Interfaces::ModelInfo->GetModelIndex("models/props_vehicles/racecar.mdl"))
				{
					pLocal->SetModelIndex(Source::Interfaces::ModelInfo->GetModelIndex("models/props_vehicles/racecar.mdl"));
				}
			}
		}
	}
	else if(!Source::Interfaces::Engine->IsInGame())
	{
		s_bOnce = false;
	}*/

#if defined(GAME_CSS_STEAM)
	Hooks::NetChannel::SetupHook();
#endif
}