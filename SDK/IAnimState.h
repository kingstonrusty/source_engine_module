#ifndef __HEADER_IANIMSTATE__
#define __HEADER_IANIMSTATE__
#pragma once

#include "CBasePlayer.h"
#include "IVModelInfo.h"

namespace Source
{
	class IAnimState
	{
	public:
		IAnimState(CBasePlayer* pPlayer);

		Source::CBasePlayer* GetOuter();

		void ClearAnimationState();
		int CalcMainActivity();
		void Update(float flEyeYaw, float flEyePitch);
		void DebugShowAnimState(bool bIsServer);
		void ComputeSequences(studiohdr_t* pHdr);
		bool ShouldUpdateAnimState();
	private:
#if defined(GAME_GMOD)
		byte pad[256];
#elif defined(GAME_CSS_STEAM)
		byte pad[452];
#elif defined(GAME_L4D2)
		byte pad[404];
#elif defined(GAME_BB2)
		byte pad[260];
#endif
	};
}

#endif