#include "ViewModelFix.h"

#if defined(GAME_GMOD)

int __fastcall Hooks::Misc::ViewModelFix(Source::CBasePlayer* pThis, void*, int a2, int a3)
{
	int ret;

	if (pThis != nullptr && pThis->GetNetworkable()->EntIndex() == Source::Interfaces::Engine->GetLocalPlayer())
	{
		bool bCur = *(bool*)((DWORD_PTR)pThis + 11100);
		*(bool*)((DWORD_PTR)pThis + 11100) = false;

		pViewModelFix->Unhook();
		ret = pViewModelFix->GetOriginal<int(__thiscall*)(Source::CBasePlayer*, int, int)>()(pThis, a2, a3);
		pViewModelFix->Rehook();

		*(bool*)((DWORD_PTR)pThis + 11100) = bCur;
	}
	else
	{
		pViewModelFix->Unhook();
		ret = pViewModelFix->GetOriginal<int(__thiscall*)(Source::CBasePlayer*, int, int)>()(pThis, a2, a3);
		pViewModelFix->Rehook();
	}

	return ret;
}

#endif