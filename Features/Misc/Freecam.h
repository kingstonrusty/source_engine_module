#ifndef __HEADER_FREECAM__
#define __HEADER_FREECAM__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace Misc
	{
		class Freecam;
	}
}

class Features::Misc::Freecam
{
public:
	static bool Run(Source::CViewSetup* pView);
	static bool GetIsActivated();
	static void Update(Source::CUserCmd* cmd);
private:
	static bool m_bToggle;
	static Vector m_vecPosition;
	static Vector m_vecAngle;
};

#endif