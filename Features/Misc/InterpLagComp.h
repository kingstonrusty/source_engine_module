#ifndef __HEADER_INTERPLAGCOMP__
#define __HEADER_INTERPLAGCOMP__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace Misc
	{
		class InterpLagComp;
	}
}

class Features::Misc::InterpLagComp
{
public:
	static void WriteUserCmdDeltaToBufferCallback();
	static void ResetValues();
	static void UpdateDesiredValues(bool bInterpolate, float flInterp = -1.f, float flRatio = -1.f);
	static void CancelUpdates();
	static bool GetIsLocked();
private:
	static float m_flInterp;
	static float m_flRatio;
	static bool m_bInterpolate;
	static bool m_bHasToChange;
	static bool m_bHasToReset;
	static int m_nResetTicks;
};

#endif