#ifndef __HEADER_NET_SENDPACKET__
#define __HEADER_NET_SENDPACKET__
#pragma once

#include "CDetour.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace NetChannel
	{
		extern CDetour* pNET_SendPacket;

		int NET_SendPacket(Source::CNetChan* chan, int sock, const Source::netadr_t& to, const unsigned char* data, int length, void* pVoicePayload, bool bUseCompression);
	}
}

#endif