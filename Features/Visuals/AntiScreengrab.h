#ifndef __HEADER_ANTISCREENGRAB__
#define __HEADER_ANTISCREENGRAB__
#pragma once

#include "Hooks/GetRenderContext.h"

namespace Features
{
	namespace Visuals
	{
		inline bool ShouldClearScreen()
		{
			#if defined(GAME_GMOD)
			return Hooks::MaterialSystem::g_bRendering || Source::Interfaces::Engine->IsTakingScreenshot();
			#else
			return false;
			#endif
		}
	}
}

#endif