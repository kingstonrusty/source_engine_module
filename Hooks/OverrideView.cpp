#include "OverrideView.h"

#include "Features/Visuals/Menu.h"

#include "Features/Visuals/AntiScreengrab.h"

#include "Features/Misc/ThirdPerson.h"

#include "Features/Misc/Freecam.h"

#include "Util/Input.h"

void __fastcall Hooks::ClientMode::OverrideView(Source::IClientMode* pThis, void*, Source::CViewSetup* setup)
{
	FINDVAR(misc, "Miscellaneous");
	FINDVAR(misc_fov, "Miscellaneous", "Field of View");

	pVmt->Unhook();
	pThis->OverrideView(setup);
	pVmt->Rehook();

	if (!Features::Visuals::ShouldClearScreen() && misc->GetBool())
	{
		if (!Features::Misc::Freecam::Run(setup))
		{
			if (!Features::Misc::ThirdPerson::Run(setup))
			{
				if (misc_fov->GetFloat() > 0.f && setup->fov() >= 70.f)
				{
					setup->fov() = misc_fov->GetFloat();
				}
			}
		}
	}
}