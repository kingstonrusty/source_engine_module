#ifndef __HEADER_CBASEENTITY__
#define __HEADER_CBASEENTITY__
#pragma once

#include "CNetVarManager.h"

#include "IClientEntityList.h"

#define ADD_EXPAND(X, Y) X ## Y
#define ADD(X, Y) ADD_EXPAND(X, Y)

#define NETVAR(TYPE, DT, P, NAME) \
TYPE ADD(Get, NAME)() \
{ \
static offset_t dOffset = ::Source::Util::NetVarManager->GetOffset(DT, P); \
return *(TYPE*)((DWORD_PTR)this + dOffset); \
} \
void ADD(Set, NAME)(TYPE x) \
{ \
	static offset_t dOffset = ::Source::Util::NetVarManager->GetOffset(DT, P); \
	*(TYPE*)((DWORD_PTR)this + dOffset) = x; \
}

#define NETVAR_VECTOR(DT, P, NAME) \
Vector& ADD(Get, NAME)() \
{ \
static offset_t dOffset = ::Source::Util::NetVarManager->GetOffset(DT, P); \
return *(Vector*)((DWORD_PTR)this + dOffset); \
} \
void ADD(Set, NAME)(Vector& x) \
{ \
	static offset_t dOffset = ::Source::Util::NetVarManager->GetOffset(DT, P); \
	*(Vector*)((DWORD_PTR)this + dOffset) = x; \
}

#include "IClientEntity.h"
#include "IClientRenderable.h"
#include "IClientNetworkable.h"

#include "IVModelInfo.h"

namespace Source
{
#if !defined(GAME_L4D2)
	class CStudioHdr;
	class CBoneCache;
#endif

	class CBaseEntity;
}

#undef GetClassName

#if !defined(GAME_L4D2)
class Source::CBoneCache
{
public:
	static CBoneCache* CreateBoneCache(Source::CBaseEntity* pEntity, matrix3x4* pBoneToWorld, unsigned int& handle);
	static void DestroyBoneCache(unsigned int handle);
};
#endif

class Source::CBaseEntity : public IClientEntity
{
public:
	static void PushAllowBoneAccess(bool normalmodels, bool viewmodels, const char* tagpush);
	static void PopAllowBoneAccess();

	IClientRenderable* GetRenderable();
	IClientNetworkable* GetNetworkable();

	void SetAbsOrigin(Vector& origin);
	void SetAbsAngles(Vector& angles);
	void SetAbsVelocity(Vector& velocity);
	void InvalidatePhysicsRecursive(int flags);

	void SetModelIndex(int modelindex);

	const char* GetClassName();

	bool IsPlayer();
	bool IsBaseCombatCharacter();
	bool IsNPC();
	bool IsNextBot();
	bool IsBaseCombatWeapon();

#if defined(GAME_L4D2)	
	const char* GetSequenceName(int sequence);
#endif

#if !defined(GAME_L4D2)
	CStudioHdr* GetModelPtr();
	unsigned int& m_hitboxBoneCacheHandle();
#else
	int& m_nCachedBones();
	matrix3x4* GetCachedBone(int idx);
	void SetCachedBone(int idx, matrix3x4* matrix);
#endif

	NETVAR(byte, "DT_BaseEntity", "m_nSolidType", SolidType);

	NETVAR(short, "DT_BaseEntity", "m_usSolidFlags", SolidFlags);
	NETVAR(short, "DT_BaseEntity", "m_nModelIndex", ModelIndex);

	NETVAR(int, "DT_BaseEntity", "m_iHealth", Health);
	NETVAR(int, "DT_BaseAnimating", "m_nHitboxSet", HitboxSet);
	NETVAR(int, "DT_BaseAnimating", "m_fEffects", Effects);
	NETVAR(int, "DT_BasePlayer", "m_fFlags", Flags);
	NETVAR(int, "DT_BaseAnimating", "m_nSequence", Sequence);

	NETVAR(float, "DT_BaseEntity", "m_flSimulationTime", SimulationTime);
	NETVAR(float, "DT_BaseAnimating", "m_flCycle", Cycle);
	NETVAR(float, "DT_BaseAnimating", "m_flAnimTime", AnimTime);

	NETVAR_VECTOR("DT_BaseEntity", "m_vecOrigin", LocalOrigin);
	NETVAR_VECTOR("DT_BaseEntity", "m_angRotation", LocalAngles);
	NETVAR_VECTOR("DT_BaseEntity", "m_vecMins", Mins);
	NETVAR_VECTOR("DT_BaseEntity", "m_vecMaxs", Maxs);
	//NETVAR_VECTOR("DT_HL2MP_Player", "m_angEyeAngles[0]", EyeAngles);
	NETVAR_VECTOR("DT_BaseEntity", "m_vecVelocity[0]", Velocity);
};

#endif