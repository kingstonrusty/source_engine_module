#ifndef __HEADER_MD5__
#define __HEADER_MD5__
#pragma once

namespace Util
{
	namespace MD5
	{
		struct MD5Context_t
		{
			unsigned int	buf[4];
			unsigned int	bits[2];
			unsigned char	in[64];
		};

		void Init(MD5Context_t *ctx);
		void Update(MD5Context_t *ctx, unsigned char const *buf, unsigned int len);
		void Final(unsigned char digest[16], MD5Context_t *ctx);
	}
}

#endif