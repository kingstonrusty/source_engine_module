#ifndef __HEADER_PAINT__
#define __HEADER_PAINT__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace EngineVGui
	{
		extern CVMTHookManager* pVmt;

		void __fastcall Paint(Source::IVEngineVGui* pThis, void*, int mode);
	}
}

#endif