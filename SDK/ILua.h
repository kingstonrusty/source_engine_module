#ifndef __HEADER_CLUA__
#define __HEADER_CLUA__
#pragma once

#if defined(GAME_GMOD)

#include "Multigame/InterfaceBruteforcer.h"

#include "CBaseEntity.h"

namespace Source
{
	struct lua_State;
	struct UserData;

	class ILuaInterface;
	class ILuaShared;

	namespace GarrysMod
	{
		enum
		{
			SPECIAL_GLOB,		// Global table
			SPECIAL_ENV,		// Environment table
			SPECIAL_REG,		// Registry table
		};

		enum
		{
			CLIENT_STATE = 0,
			SERVER_STATE,
			MENU_STATE
		};

		struct Type
		{
			enum
			{
				INVALID = -1,
				NIL,
				BOOL,
				LIGHTUSERDATA,
				NUMBER,
				STRING,
				TABLE,
				FUNCTION,
				USERDATA,
				THREAD,

				// UserData
				ENTITY,
				VECTOR,
				ANGLE,
				PHYSOBJ,
				SAVE,
				RESTORE,
				DAMAGEINFO,
				EFFECTDATA,
				MOVEDATA,
				RECIPIENTFILTER,
				USERCMD,
				SCRIPTEDVEHICLE,

				// Client Only
				MATERIAL,
				PANEL,
				PARTICLE,
				PARTICLEEMITTER,
				TEXTURE,
				USERMSG,

				CONVAR,
				IMESH,
				MATRIX,
				SOUND,
				PIXELVISHANDLE,
				DLIGHT,
				VIDEO,
				FILE,

				COUNT
			};
		};

		typedef int(*CFunc) (lua_State *L);


	}

	namespace Interfaces
	{
		extern ILuaShared* Lua;
	}
}

struct Source::lua_State 
{
	unsigned char _ignore_this_common_lua_header_[69];
	ILuaInterface* luabase;
};

struct Source::UserData
{
	void* data;
	unsigned char type;
};

class Source::ILuaInterface
{
public:
	virtual int			Top(void) = 0;
	virtual void		Push(int iStackPos) = 0;
	virtual void		Pop(int iAmt = 1) = 0;
	virtual void		GetTable(int iStackPos) = 0;
	virtual void		GetField(int iStackPos, const char* strName) = 0;
	virtual void		SetField(int iStackPos, const char* strName) = 0;
	virtual void		CreateTable() = 0;
	virtual void		SetTable(int i) = 0;
	virtual void		SetMetaTable(int i) = 0;
	virtual bool		GetMetaTable(int i) = 0;
	virtual void		Call(int iArgs, int iResults) = 0;
	virtual int			PCall(int iArgs, int iResults, int iErrorFunc) = 0;
	virtual int			Equal(int iA, int iB) = 0;
	virtual int			RawEqual(int iA, int iB) = 0;
	virtual void		Insert(int iStackPos) = 0;
	virtual void		Remove(int iStackPos) = 0;
	virtual int			Next(int iStackPos) = 0;
	virtual void*		NewUserdata(unsigned int iSize) = 0;
	virtual void		ThrowError(const char* strError) = 0;
	virtual void		CheckType(int iStackPos, int iType) = 0;
	virtual void		ArgError(int iArgNum, const char* strMessage) = 0;
	virtual void		RawGet(int iStackPos) = 0;
	virtual void		RawSet(int iStackPos) = 0;
	virtual const char*		GetString(int iStackPos = -1, unsigned int* iOutLen = NULL) = 0;
	virtual double			GetNumber(int iStackPos = -1) = 0;
	virtual bool			GetBool(int iStackPos = -1) = 0;
	virtual GarrysMod::CFunc			GetCFunction(int iStackPos = -1) = 0;
	virtual void*			GetUserdata(int iStackPos = -1) = 0;
	virtual void		PushNil() = 0;
	virtual void		PushString(const char* val, unsigned int iLen = 0) = 0;
	virtual void		PushNumber(double val) = 0;
	virtual void		PushBool(bool val) = 0;
	virtual void		PushCFunction(GarrysMod::CFunc val) = 0;
	virtual void		PushCClosure(GarrysMod::CFunc val, int iVars) = 0;
	virtual void		PushUserdata(void*) = 0;
	virtual int			ReferenceCreate() = 0;
	virtual void		ReferenceFree(int i) = 0;
	virtual void		ReferencePush(int i) = 0;
	virtual void		PushSpecial(int iType) = 0;
	virtual bool			IsType(int iStackPos, int iType) = 0;
	virtual int				GetType(int iStackPos) = 0;
	virtual const char*		GetTypeName(int iType) = 0;
	virtual void			CreateMetaTableType(const char* strName, int iType) = 0;
	virtual const char* CheckString(int iStackPos = -1) = 0;
	virtual double	CheckNumber(int iStackPos = -1) = 0;

	inline double GetNumberFromEntity(Source::CBaseEntity* pEntity, const char* index)
	{
		PushSpecial(GarrysMod::SPECIAL_GLOB);
			GetField(-1, "Entity");
			PushNumber(pEntity->GetNetworkable()->EntIndex());
			Call(1, 1);
			GetField(-1, index);
			if (!IsType(-1, GarrysMod::Type::NUMBER))
			{
				Pop(3);
				return 0.0;
			}

		double ret = GetNumber(-1);
		Pop(3);

		return ret;
	}

	inline Vector GetVectorFromEntity(Source::CBaseEntity* pEntity, const char* index)
	{
		PushSpecial(GarrysMod::SPECIAL_GLOB);
		GetField(-1, "Entity");
		PushNumber(pEntity->GetNetworkable()->EntIndex());
		Call(1, 1);
		GetField(-1, index);
		if (!IsType(-1, GarrysMod::Type::VECTOR))
		{
			Pop(3);
			return Vector(0.f, 0.f, 0.f);
		}
		Vector ret;
		
		GetField(-1, "x");
		ret.x = GetNumber();
		Pop();

		GetField(-1, "y");
		ret.y = GetNumber();
		Pop();

		GetField(-1, "z");
		ret.z = GetNumber();
		Pop();

		Pop(3);

		return ret;
	}
};

class Source::ILuaShared
{
public:
	static ILuaShared* GetPtr();

	ILuaInterface* GetLuaInterface(unsigned char state);
};

#endif

#endif