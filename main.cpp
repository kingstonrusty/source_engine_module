#include "main.h"

#include "Features/Visuals/Menu.h"

/*
] lua_run_cl local seed local lowest = 5000 for i = 0, 255 do big.RandomSeed(i) local a = math.abs(big.RandomFloat(0, math.pi * 2)) if a < lowest then lowest = a seed = i end end print(lowest, seed)
	0.0021309626062924	26

] lua_run_cl local seed local lowest = 5000 for i = 0, 255 do big.RandomSeed(i) local a = math.abs(big.RandomFloat(-1, 1)) if a < lowest then lowest = a seed = i end end print(lowest, seed)
	0.0030960985473805	174
*/


/*
bool __cdecl IsBoxIntersectingSphere(int a1, int a2, int a3, float a4)
{
  float v4; // xmm1_4@1
  float v5; // xmm0_4@2
  float v6; // xmm1_4@5
  float v7; // xmm2_4@5
  float v8; // xmm2_4@7
  float v9; // xmm2_4@9
  float v10; // xmm3_4@9
  float v11; // xmm3_4@11

  v4 = *(float *)a3;
  if ( *(float *)a1 <= *(float *)a3 )
  {
    v5 = 0.0;
    if ( v4 > *(float *)a2 )
      v5 = (float)((float)(*(float *)a2 - v4) * (float)(*(float *)a2 - v4)) + 0.0;
  }
  else
  {
    v5 = (float)((float)(v4 - *(float *)a1) * (float)(v4 - *(float *)a1)) + 0.0;
  }
  v6 = *(float *)(a3 + 4);
  v7 = *(float *)(a1 + 4);
  if ( v7 <= v6 )
  {
    v8 = *(float *)(a2 + 4);
    if ( v6 > v8 )
      v5 = v5 + (float)((float)(v8 - v6) * (float)(v8 - v6));
  }
  else
  {
    v5 = v5 + (float)((float)(v6 - v7) * (float)(v6 - v7));
  }
  v9 = *(float *)(a3 + 8);
  v10 = *(float *)(a1 + 8);
  if ( v10 <= v9 )
  {
    v11 = *(float *)(a2 + 8);
    if ( v9 > v11 )
      v5 = v5 + (float)((float)(v11 - v9) * (float)(v11 - v9));
  }
  else
  {
    v5 = v5 + (float)((float)(v9 - v10) * (float)(v9 - v10));
  }
  return (float)(a4 * a4) > v5;
}
*/

DWORD
WINAPI
Init(LPVOID)
{
	while (GetModuleHandleA("serverbrowser.dll") == nullptr)
	{
		Sleep(100);
	}

	{ // Interfaces Init
		using namespace Source;

		Interfaces::Client = IBaseClientDLL::GetPtr();
		Interfaces::ClientMode = IClientMode::GetPtr();
		Interfaces::Cvar = ICvar::GetPtr();
		Interfaces::DebugOverlay = IVDebugOverlay::GetPtr();
		Interfaces::Engine = IVEngineClient::GetPtr();
		Interfaces::EngineVGui = IVEngineVGui::GetPtr();
		Interfaces::EntityList = IClientEntityList::GetPtr();
		Interfaces::GameMovement = IGameMovement::GetPtr();
		Interfaces::Globals = CGlobalVarsBase::GetPtr();
		Interfaces::Input = CInput::GetPtr();
		Interfaces::MaterialSystem = IMaterialSystem::GetPtr();
		Interfaces::ModelInfo = IVModelInfo::GetPtr();
		Interfaces::ModelRender = IVModelRender::GetPtr();
		Interfaces::Prediction = IPrediction::GetPtr();
		Interfaces::RenderView = IVRenderView::GetPtr();
		Interfaces::StringTableContainer = INetworkStringTableContainer::GetPtr();
		Interfaces::StudioRender = IStudioRender::GetPtr();
		Interfaces::Surface = ISurface::GetPtr();
		Interfaces::TraceRay = IEngineTrace::GetPtr();
	
#if defined(GAME_GMOD)
		Interfaces::View = IView::GetPtr();
		Interfaces::Lua = ILuaShared::GetPtr();
#endif

		Source::Util::NetVarManager = new CNetVarManager();
	}

	Debug::PrintToConsole(__TIMESTAMP__);

	Config::RegisterVars();
	//Config::LoadConfig();

	LuaBase::Init();
	Hooks::Init();

	return 0;
}

BOOL
WINAPI
DllMain(HINSTANCE, DWORD dwReason, LPVOID)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		auto hHandle = CreateThread(nullptr, 0, Init, nullptr, 0, nullptr);

		if (hHandle != nullptr)
		{
			CloseHandle(hHandle);
		}
	}

	return TRUE;
}