#ifndef __HEADER_DRAWINGHELPER__
#define __HEADER_DRAWINGHELPER__
#pragma once

#include "SDK/Source.h"

namespace Util
{
	namespace DrawingHelper
	{
		inline void DrawFilledRect(int x, int y, int w, int h, Color col)
		{
			Source::Interfaces::Surface->DrawSetColor(col);
			Source::Interfaces::Surface->DrawFilledRect(x, y, x + w, y + h);
		}

		inline void DrawOutlinedRect(int x, int y, int w, int h, Color col)
		{
			Source::Interfaces::Surface->DrawSetColor(col);
			Source::Interfaces::Surface->DrawOutlinedRect(x, y, x + w, y + h);
		}

		inline void DrawLine(int x1, int y1, int x2, int y2, Color col)
		{
			Source::Interfaces::Surface->DrawSetColor(col);
			Source::Interfaces::Surface->DrawLine(x1, y1, x2, y2);
		}

		inline void DrawString(Source::HFONT font, int x, int y, Color col, const char* pszFmt, ...)
		{
			va_list args;
			va_start(args, pszFmt);

			char szBuf[2048];
			wchar_t wszBuf[2048];
			vsprintf_s(szBuf, pszFmt, args);
			MultiByteToWideChar(CP_UTF8, 0, szBuf, 256, wszBuf, 256);

			Source::Interfaces::Surface->DrawSetTextFont(font);
			Source::Interfaces::Surface->DrawSetTextPos(x, y);
			Source::Interfaces::Surface->DrawSetTextColor(col);
			Source::Interfaces::Surface->DrawPrintText(wszBuf, wcslen(wszBuf));

			va_end(args);
		}

		inline void GetTextSize(Source::HFONT font, int& w, int& h, const char* pszFmt, ...)
		{
			va_list args;
			va_start(args, pszFmt);

			char szBuf[2048];
			wchar_t wszBuf[2048];
			vsprintf_s(szBuf, pszFmt, args);
			MultiByteToWideChar(CP_UTF8, 0, szBuf, 256, wszBuf, 256);

			Source::Interfaces::Surface->GetTextSize(font, wszBuf, w, h);

			va_end(args);
		}
	}
}

#endif