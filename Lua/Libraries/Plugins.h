#ifndef __HEADER_PLUGINS__
#define __HEADER_PLUGINS__
#pragma once

#include "BaseLibrary.h"

class Plugins : public BaseLibrary<Plugins>
{
public:
	friend class BaseLibrary<Plugins>;
	class CPlugin;

	static void Init(lua_State* L)
	{
		m_Plugins.Clear();
		BaseLibrary<Plugins>::Init(L);
	}

	static const char* GetName() { return "Plugins"; }

	static void Refresh()
	{
		m_Plugins.Clear();
	}

	static void Fill(SmallVector<CPlugin*>* vecPlugins);
private:
	static int l_Register(lua_State* L);
	static int l_IsActive(lua_State* L);
	static int l_Exists(lua_State* L);

	static constexpr const struct luaL_Reg m_Register[] =
	{
		{ "Register", l_Register },
		//{ "IsActive", l_IsActive },
		//{ "Exists", l_Exists },

		{ NULL, NULL }
	};

	static LinkedList<CPlugin> m_Plugins;
};

class Plugins::CPlugin 
{
public:
	CPlugin(lua_State* L, const char* pszName, const char* pszTable, const char* pszAuthor, const char* pszVersion);
	~CPlugin();

	const char* GetName();
	const char* GetTable();
	const char* GetAuthor();
	const char* GetVersion();

	bool HasMenuCallback();

	void MenuCallback(int x, int y, int w, int h);
private:
	char* m_pszName;
	char* m_pszTable;
	char* m_pszAuthor;
	char* m_pszVersion;

	bool m_bHasMenuCallback;

	lua_State* m_pState;
};

#endif