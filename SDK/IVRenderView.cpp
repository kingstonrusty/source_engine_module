#include "IVRenderView.h"

namespace Source
{
	namespace Interfaces
	{
		IVRenderView* RenderView;
	}
}

using namespace Source;

IVRenderView* IVRenderView::GetPtr()
{
	return (IVRenderView*)::Util::BruteforceInterface("VEngineRenderView", "engine.dll");
}

void IVRenderView::SceneEnd()
{
	return GET_VFUNC(void(__thiscall*)(IVRenderView*), this, IVRENDERVIEW_SCENEEND)(this);
}

void IVRenderView::SetColorModulation(float* clr)
{
	return GET_VFUNC(void(__thiscall*)(IVRenderView*, float*), this, IVRENDERVIEW_SETCOLORMODULATION)(this, clr);
}

void IVRenderView::SetColorModulation(Color clr)
{
	float flColor[3] = { (float)clr.r / 255.f, (float)clr.g / 255.f, (float)clr.b / 255.f };

	return SetColorModulation(flColor);
}

#if defined(GAME_GMOD)
void IVRenderView::Push3DView(const CViewSetup& view, int flags, void* rendertarget, Frustum frustumPlanes)
{
	return GET_VFUNC(void(__thiscall*)(IVRenderView*, const CViewSetup&, int, void*, Frustum), this, IVRENDERVIEW_PUSH3DVIEW)(this, view, flags, rendertarget, frustumPlanes);
}

void IVRenderView::PopView(Frustum frustumPlanes)
{
	return GET_VFUNC(void(__thiscall*)(IVRenderView*, Frustum), this, IVRENDERVIEW_POPVIEW)(this, frustumPlanes);
}
#endif