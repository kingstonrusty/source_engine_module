#ifndef __HEADER_IVDEBUGOVERLAY__
#define __HEADER_IVDEBUGOVERLAY__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

namespace Source
{
	class IVDebugOverlay;

	namespace Interfaces
	{
		extern IVDebugOverlay* DebugOverlay;
	}
}

class Source::IVDebugOverlay
{
public:
	static IVDebugOverlay* GetPtr();

	int ScreenPosition(const Vector& point, Vector& screen);
};

#endif