#ifndef __HEADER_INPUT_LUA__
#define __HEADER_INPUT_LUA__
#pragma once

#include "BaseLibrary.h"

class Input : public BaseLibrary<Input>
{
public:
	friend class BaseLibrary<Input>;

	static const char* GetName() { return "Input"; }
private:
	static int l_GetCursorPos(lua_State* L);
	static int l_GetKeyState(lua_State* L);
	static int l_GetKeyName(lua_State* L);

	static constexpr const struct luaL_Reg m_Register[] =
	{
		{ "GetCursorPos", l_GetCursorPos },
		{ "GetKeyState", l_GetKeyState },
		{ "GetKeyName", l_GetKeyName },
		{ NULL, NULL }
	};
};

#endif