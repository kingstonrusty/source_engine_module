#include "IView.h"

#if defined(GAME_GMOD)

namespace Source
{
	namespace Interfaces
	{
		IView* View;
	}
}

using namespace Source;

IView* IView::GetPtr()
{
	return **Memory::FindPattern<IView***>(IVIEW_SIGNATURE, "client.dll");
}

void IView::SetUpView()
{
	static void(__thiscall* pSetUpView)(IView*) = Memory::FindPattern<void(__thiscall*)(IView*)>(IVIEW_SETUPVIEW, "client.dll");

	return pSetUpView(this);
}

Frustum IView::GetFrustum()
{
	return GET_VFUNC(Frustum(__thiscall*)(IView*), this, IVIEW_GETFRUSTUM)(this);
}

CViewSetup* IView::GetPlayerViewSetup()
{
	return GET_VFUNC(CViewSetup*(__thiscall*)(IView*), this, IVIEW_GETPLAYERVIEWSETUP)(this);
}

#endif