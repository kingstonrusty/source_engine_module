#ifndef __HEADER_CNETCHAN__
#define __HEADER_CNETCHAN__
#pragma once

namespace Source
{
	class CNetMessage;

	typedef enum
	{
		NA_NULL = 0,
		NA_LOOPBACK,
		NA_BROADCAST,
		NA_IP,
	} netadrtype_t;

	typedef struct netadr_s
	{
		netadrtype_t	type;
		unsigned char	ip[4];
		unsigned short	port;
	} netadr_t;

	class bufhelper
	{
	public:
		bufhelper(const unsigned char* data, int length)
		{
			m_data = data;
			m_len = length;
			m_pos = 0;
		}

		template< typename _T >
		_T read()
		{
			if ((m_pos + sizeof(_T)) >= m_len)
			{
				return _T(0);
			}

			m_pos += sizeof(_T);
			return *(_T*)((unsigned long)m_data + (m_pos - sizeof(_T)));
		}

		int getlen()
		{
			return m_len;
		}

		void reset()
		{
			m_pos = 0;
		}
	private:
		const unsigned char* m_data;
		int m_len;
		int m_pos;
	};

	class bufwrite
	{
	public:
		template< typename _T >
		void write(_T val)
		{
			for (int i = 0; i < sizeof(_T); i++, m_len++)
			{
				m_data.Add(*(unsigned char*)((unsigned long)&val + i));
			}
		}

		void writestring(const char* str)
		{
			for (int i = 0; i <= strlen(str); i++)
			{
				write<unsigned char>(str[i]);
			}
		}

		const unsigned char* getdata()
		{
			return m_data.GetData();
		}

		int getlen()
		{
			return m_len;
		}
	private:
		SmallVector<unsigned char> m_data;
		int m_len;
	};

	class bf_write;

	class CNetChan;
}

class Source::CNetMessage
{
private:
	byte __CNetMessage[32];
};

class Source::CNetChan
{
public:
	enum
	{
		FLOW_OUTGOING,
		FLOW_INCOMING,
		MAX_FLOWS
	};

	float GetLatency(int flow);
#if defined(GAME_CSS_STEAM)
	void Shutdown(const char* reason);
#endif
	bool SendNetMsg(CNetMessage& msg, bool forcereliable, bool voice);
#if defined(GAME_CSS_STEAM)
	int SendDataGram(bf_write* data);
#endif

	int& m_nOutSequenceNr();
	int& m_nInSequenceNr();
};

#endif