#ifndef __HEADER_AIMBOTLOGIC__
#define __HEADER_AIMBOTLOGIC__
#pragma once

#include "SDK/Source.h"

#include "Features/PositionAdjustment/History.h"

#include "Features/Misc/HitBoxCache.h"

namespace Features
{
	namespace Ragebot
	{
		class AimbotLogic;
	}
}

class Features::Ragebot::AimbotLogic
{
public:
	struct AimbotTarget
	{
		Source::CBaseEntity* m_pEntity;
		bool m_bIsPlayer;
		bool m_bIsNPC;
		Vector m_vAimPosition;
		float m_flDistance;
		Features::PositionAdjustment::History::Snapshot* m_pSnapshot;
	};

	static bool ShouldRunAimbot(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd);
	static void ResetData();
	
	static AimbotTarget* FindTarget(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd);
	static void SortTargets(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd);
	static bool FinishAimbot(Source::CBasePlayer* pLocal, AimbotTarget* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd);

	static bool ShouldAimAtPlayers();
	static bool ShouldAimAtNPCs();

	static bool ShouldPositionAdjustEntity(Source::CBaseEntity* pTarget);

	static bool IsValidNPC(Source::CBasePlayer* pLocal, Source::CBaseNPC* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, Features::PositionAdjustment::History::Snapshot** ppSnapshot);
	static bool IsValidPlayer(Source::CBasePlayer* pLocal, Source::CBasePlayer* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, Features::PositionAdjustment::History::Snapshot** ppSnapshot);

	static bool FindVisibleSpot(Source::CBasePlayer* pLocal, Source::CBaseEntity* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, SmallVector<Features::PositionAdjustment::History::Snapshot*>* vecRecords, Features::PositionAdjustment::History::Snapshot** ppSnapshot);
	static bool FindVisibleSpot_PositionAdjusted(Source::CBasePlayer* pLocal, Source::CBaseEntity* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, SmallVector<Features::PositionAdjustment::History::Snapshot*>* vecRecords, Features::PositionAdjustment::History::Snapshot** ppSnapshot);
	static bool ScanHitBox(Source::CBasePlayer* pLocal, Source::CBaseEntity* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vAimPos, Features::Misc::HitBoxCache::HitBox* pHitBox, matrix3x4* pBoneMatrix);
	static bool IsPointVisible(Source::CBasePlayer* pLocal, Source::CBaseEntity* pTarget, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd, Vector& vPoint);

#if defined(GAME_L4D2)
	static float GetClosestForInfectedType(Source::CBaseNPC* pTarget);
	static void SetClosestForInfectedType(Source::CBaseNPC* pTarget, float flDistance);

	static void ClearPath(Source::CBasePlayer* pLocal, Source::CBaseWeapon* pWeapon, Source::CUserCmd* cmd);
	static void SortSpecialInfected();
	static void SortCommonInfected();
#endif
private:
	static SmallVector<AimbotTarget*> m_vecTargets;
	static float m_flLowestDistance;

#if defined(GAME_L4D2)
	static bool m_bSpecialInfectedFound;

	struct InfectedData
	{
		bool m_bTankFound;
		float m_flClosestTankDistance;

		bool m_bChargerFound;
		float m_flClosestChargerDistance;
		
		bool m_bJockeyFound;
		float m_flClosestJockeyDistance;

		bool m_bHunterFound;
		float m_flClosestHunterDistance;

		bool m_bSmokerFound;
		float m_flClosestSmokerDistance;

		bool m_bSpitterFound;
		float m_flClosestSpitterDistance;

		bool m_bBoomerFound;
		float m_flClosestBoomerDistance;

		bool m_bWitchFound;
		float m_flClosestWitchDistance;

		float m_flClosestCommonInfectedDistance;
	} static m_InfectedData;
#endif
};

#endif