#ifndef __HEADER_FAKELAG__
#define __HEADER_FAKELAG__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace HackvsHack
	{
		class Fakelag;
	}
}

class Features::HackvsHack::Fakelag
{
public:
	static void Run(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd, bool* bSendPacket);
	static bool IsFakelagging();
private:
	static int m_nChoked;
	static Vector m_vecOldPos;
	static bool m_bLagging;
};

#endif