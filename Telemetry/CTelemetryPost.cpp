#include "CTelemetryPost.h"

using namespace Telemetry;

void CTelemetryPost::Spew()
{
	Debug::PrintToConsole("===== TELEMETRY POST =====");

	for (int idx = 0; idx < m_vecKeyValues.GetSize(); idx++)
	{
		Debug::PrintToConsole("%s = %s", m_vecKeyValues[idx].m_pszKey, m_vecKeyValues[idx].m_pszValue);
	}
}

void CTelemetryPost::Send()
{

}