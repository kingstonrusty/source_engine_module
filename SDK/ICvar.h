#ifndef __HEADER_ICVAR__
#define __HEADER_ICVAR__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

#include "CNetChan.h"

namespace Source
{
	class ConVar;

	class NET_SetConVar;

	class ICvar;

	namespace Interfaces
	{
		extern ICvar* Cvar;
	}
}

class Source::ConVar
{
public:
	int& m_iValue()
	{
		return *(int*)((DWORD_PTR)this + 0x30);
	}

	float& m_flValue()
	{
		return *(float*)((DWORD_PTR)this + 0x2C);
	}

	int GetInt()
	{
		return m_iValue();
	}

	void SetInt(int iValue)
	{
		m_iValue() = iValue;
	}

	float GetFloat()
	{
		return m_flValue();
	}

	void SetFloat(int flValue)
	{
		m_flValue() = flValue;
	}
};

class Source::NET_SetConVar : public CNetMessage
{
public:
	NET_SetConVar(const char* name, const char* value)
	{
		static int(__thiscall* _NET_SetConVar)(NET_SetConVar*, const char*, const char*) = Memory::FindPattern<int(__thiscall*)(NET_SetConVar*, const char*, const char*)>(NET_SETCONVAR_CONSTRUCTOR, "engine.dll");
		_NET_SetConVar(this, name, value);
	}
	byte __NET_SetConVar[16];
};

class Source::ICvar
{
public:
	static ICvar* GetPtr();

	ConVar* FindVar(const char* name);
};

#endif