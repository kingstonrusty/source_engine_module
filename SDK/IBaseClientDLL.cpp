#include "IBaseClientDLL.h"

namespace Source
{
	namespace Interfaces
	{
		IBaseClientDLL* Client;
	}
}

using namespace Source;

IBaseClientDLL* IBaseClientDLL::GetPtr()
{
	return (IBaseClientDLL*)::Util::BruteforceInterface("VClient", "client.dll");
}

ClientClass* IBaseClientDLL::GetAllClasses()
{
	return GET_VFUNC(ClientClass*(__thiscall*)(IBaseClientDLL*), this, IBASECLIENTDLL_GETALLCLASSES)(this);
}

#if defined(GAME_L4D2)
void IBaseClientDLL::CreateMove(int sequence_number, float sample_time, bool active)
{
	return GET_VFUNC(void(__thiscall*)(IBaseClientDLL*, int, float, bool), this, IBASECLIENTDLL_CREATEMOVE)(this, sequence_number, sample_time, active);
}
#endif

#if defined(GAME_L4D2)
bool IBaseClientDLL::WriteUsercmdDeltaToBuffer(void* buf, int unk1, int from, int to, bool isnewcommand)
{
	return GET_VFUNC(bool(__thiscall*)(IBaseClientDLL*, void*, int, int, int, bool), this, IBASECLIENTDLL_WRITEUSERCMDDELTATOBUFFER)(this, buf, unk1, from, to, isnewcommand);
}
#else
bool IBaseClientDLL::WriteUsercmdDeltaToBuffer(void* buf, int from, int to, bool isnewcommand)
{
	return GET_VFUNC(bool(__thiscall*)(IBaseClientDLL*, void*, int, int, bool), this, IBASECLIENTDLL_WRITEUSERCMDDELTATOBUFFER)(this, buf, from, to, isnewcommand);
}
#endif

#if defined(GAME_GMOD)
void IBaseClientDLL::RenderView(CViewSetup& view, int flags, int whattorender)
{
	return GET_VFUNC(void(__thiscall*)(IBaseClientDLL*, CViewSetup&, int, int), this, IBASECLIENTDLL_RENDERVIEW)(this, view, flags, whattorender);
}
#endif

void IBaseClientDLL::FrameStageNotify(ClientFrameStage_t stage)
{
	return GET_VFUNC(void(__thiscall*)(IBaseClientDLL*, ClientFrameStage_t), this, IBASECLIENTDLL_FRAMESTAGENOTIFY)(this, stage);
}