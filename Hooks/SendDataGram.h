#ifndef __HEADER_SENDDATAGRAM__
#define __HEADER_SENDDATAGRAM__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace NetChannel
	{
		extern CVMTHookManager* pVmt;

		int __fastcall SendDatagram(Source::CNetChan* pThis, void*, Source::bf_write* data);
	}
}

#endif