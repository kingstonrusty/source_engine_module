#ifndef __HEADER_VECTOR__
#define __HEADER_VECTOR__
#pragma once

class Vector
{
public:
	Vector()
	{
		Init(0.f, 0.f, 0.f);
	}

	Vector(float x, float y, float z)
	{
		Init(x, y, z);
	}

	Vector(const Vector& other)
	{
		Init(other.x, other.y, other.z);
	}

	inline void Init(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	inline Vector operator+(const Vector& other) const
	{
		return Vector(x + other.x, y + other.y, z + other.z);
	}

	inline Vector& operator+=(const Vector& other)
	{
		x += other.x;
		y += other.y;
		z += other.z;

		return *this;
	}

	inline Vector operator-(const Vector& other) const
	{
		return Vector(x - other.x, y - other.y, z - other.z);
	}

	inline Vector& operator-=(const Vector& other)
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;

		return *this;
	}

	inline Vector operator*(float mult) const
	{
		return Vector(x * mult, y * mult, z * mult);
	}

	inline Vector& operator*=(float mult)
	{
		x *= mult;
		y *= mult;
		z *= mult;

		return *this;
	}

	inline Vector operator/(float div) const
	{
		return Vector(x / div, y / div, z / div);
	}
	
	inline Vector& operator/=(float div)
	{
		x /= div;
		y /= div;
		z /= div;

		return *this;
	}

	inline bool operator==(const Vector& other)
	{
		return x == other.x && y == other.y && z == other.z;
	}

	inline bool operator!=(const Vector& other)
	{
		return !(*this == other);
	}

	inline float& operator[](int i)
	{
		return ((float*)(this))[i];
	}

	inline float operator[](int i) const
	{
		return ((float*)(this))[i];
	}

	inline float Length()
	{
		float root = 0.0f;
		float sqsr = LengthSqr();

		__asm
		{
			sqrtss xmm0, sqsr
			movss root, xmm0
		}

		return root;
	}

	inline float Length2D()
	{
		float root = 0.f;
		float sqst = Length2DSqr();

		__asm
		{
			sqrtss xmm0, sqst
			movss root, xmm0
		}

		return root;
	}

	inline float Length2DSqr()
	{
		return (x * x + y * y);
	}

	inline float LengthSqr()
	{
		return (x * x + y * y + z * z);
	}

	inline bool IsZero() const
	{
		return x == 0.f && y == 0.f && z == 0.f;
	}

	inline Vector Abs()
	{
		Vector ret;
		ret.x = fabsf(x);
		ret.y = fabsf(y);
		ret.z = fabsf(z);

		return ret;
	}
	
	inline void NormalizeInPlace();

	float x, y, z;
};

__forceinline void CalcAngle(Vector &src, Vector &dst, Vector &angles)
{
	Vector delta = Vector((src[0] - dst[0]), (src[1] - dst[1]), (src[2] - dst[2]));
	float hyp = std::sqrt(delta[0] * delta[0] + delta[1] * delta[1]);
	angles[0] = std::atan(delta[2] / hyp)        *(180.0 / M_PI_F);
	angles[1] = std::atan(delta[1] / delta[0])    *(180.0 / M_PI_F);
	angles[2] = 0.0f;
	if (delta[0] >= 0.0) { angles[1] += 180.0f; }
}

__forceinline void VectorAngles(const Vector& forward, Vector &angles)
{
	float	tmp, yaw, pitch;
	if (forward[1] == 0 && forward[0] == 0)
	{
		yaw = 0;
		if (forward[2] > 0)
			pitch = 270;
		else
			pitch = 90;
	}
	else
	{
		yaw = (std::atan2(forward[1], forward[0]) * 180.f / M_PI);
		if (yaw < 0)
			yaw += 360;

		tmp = std::sqrt(forward[0] * forward[0] + forward[1] * forward[1]);
		pitch = (std::atan2(-forward[2], tmp) * 180.f / M_PI);
		if (pitch < 0)
			pitch += 360;
	}
	angles.x = pitch;
	angles.y = yaw;
	angles.z = 0.f;
}

__forceinline float Deg2Rad(float flAngle) {
	constexpr float _DEG2RAD = M_PI_F / 180.f;
	constexpr float _RAD2DEG = 180.f / M_PI_F;
	return flAngle * _DEG2RAD;
}

__forceinline void Deg2Rad(float flAngle, float& rflPut) {
	rflPut = Deg2Rad(flAngle);
}

__forceinline float Rad2Deg(float flAngle) {
	constexpr float _DEG2RAD = M_PI_F / 180.f;
	constexpr float _RAD2DEG = 180.f / M_PI_F;
	return flAngle * _RAD2DEG;
}

__forceinline void SinCos(const float flValue, float& rflSin, float& rflCos) {
	rflSin = std::sin(flValue);
	rflCos = std::cos(flValue);
}

__forceinline void AngleVectors(const Vector& rangAngle, Vector* pForward = nullptr, Vector* pRight = nullptr, Vector* pUp = nullptr)
{
	constexpr float _DEG2RAD = M_PI_F / 180.f;
	constexpr float cpi = _DEG2RAD;

	float flAngle, flAngle2, flAngle3;
	float SinRoll, SinPitch, SinYaw, CosRoll, CosPitch, CosYaw;

	Deg2Rad(rangAngle.y, flAngle);
	SinCos(flAngle, SinYaw, CosYaw);

	Deg2Rad(rangAngle.x, flAngle2);
	SinCos(flAngle2, SinPitch, CosPitch);

	Deg2Rad(rangAngle.z, flAngle3);
	SinCos(flAngle3, SinRoll, CosRoll);

	if (pForward != nullptr) pForward->Init(CosPitch*CosYaw, CosPitch*SinYaw, -SinPitch);
	if (pUp != nullptr) pUp->Init(CosRoll*SinPitch*CosYaw + -SinRoll*-SinYaw, CosRoll*SinPitch*SinYaw + -SinRoll*CosYaw, CosRoll*CosPitch);
	if (pRight != nullptr) pRight->Init(
		-1.f * SinRoll*SinPitch*CosYaw + -1.f * CosRoll*-SinYaw,
		-1.f * SinRoll*SinPitch*SinYaw + -1.f * CosRoll*CosYaw,
		-1.f * SinRoll*CosPitch
	);
}

inline float VectorNormalize(Vector& vec)
{
	float sqrlen = vec.LengthSqr() + 1.0e-10f, invlen;
	invlen = 1.f / sqrt(sqrlen);
	vec.x *= invlen;
	vec.y *= invlen;
	vec.z *= invlen;
	return sqrlen * invlen;
}

inline void Vector::NormalizeInPlace()
{
	VectorNormalize(*this);
}

inline float AngleNormalize(float in)
{
	// ultra fast [maybe] [probably not]

	/*if (!std::isfinite(in))
	{
		return 0.f;
	}

	float revolutions = -floorf(in / 360.f);
	float ang = in + revolutions * 360.f;

	if (ang > 180.f)
	{
		ang -= 360.f;
	}

	if ((ang == 180.f || ang == 0.f) && std::signbit(in))
	{
		ang *= -1.f;
	}

	return ang;*/

	if (!std::isfinite(in))
	{
		return 0.f;
	}

	while (in > 180.f)
	{
		in -= 360.f;
	}

	while (in < -180.f)
	{
		in += 360.f;
	}

	return in;
}

inline void AngleNormalize(Vector& in)
{
	in.x = AngleNormalize(in.x);
	in.y = AngleNormalize(in.y);
	in.z = AngleNormalize(in.z);
}

inline void AngleClamp(Vector& in)
{
	if (in.x > 89.f)
	{
		in.x = 89.f;
	}

	if (in.x < -89.f)
	{
		in.x = -89.f;
	}

	if (in.y > 180.f)
	{
		in.y = 180.f;
	}

	if (in.y < -180.f)
	{
		in.y = -180.f;
	}

	in.z = 0.f;
}

inline void VectorAngles2(const Vector& forward, Vector &angles)
{
	float tmp, yaw, pitch;
	if (forward[1] == 0 && forward[0] == 0)
	{
		yaw = 0;
		pitch = 0;
	}
	else
	{
		yaw = atan2(forward[1], forward[0]) * 180 / M_PI;
		if (yaw < 0) yaw += 360;
		tmp = sqrt(forward[0] * forward[0] + forward[1] * forward[1]);
		pitch = atan2(-forward[2], tmp) * 180 / M_PI;
	}
	angles[0] = pitch;
	angles[1] = yaw;
	angles[2] = 0;
}

inline void AngleVectors(const Vector& angles, Vector& forward)
{
	double v2; // ST24_8@1
	float v3; // ST14_4@1
	float v4; // ST18_4@1
	double v5; // ST24_8@1
	unsigned int v6; // ST1C_4@1
	float v7; // ST20_4@1

	v2 = (float)(angles.y * 0.017453292);
	v3 = sin(v2);
	v4 = cos(v2);
	v5 = (float)(0.017453292 * angles.x);
	*(float *)&v6 = sin(v5);
	v7 = cos(v5);

	forward.x = v4 * v7;
	forward.y = v7 * v3;
	forward.z = -(*(float*)&v6);
}

inline void AngleVectorsTranspose(const Vector &angles, Vector *forward, Vector *right, Vector *up)
{
	float sr, sp, sy, cr, cp, cy;

	SinCos(Deg2Rad(angles[1]), sy, cy);
	SinCos(Deg2Rad(angles[0]), sp, cp);
	SinCos(Deg2Rad(angles[2]), sr, cr);

	if (forward)
	{
		forward->x = cp*cy;
		forward->y = (sr*sp*cy + cr*-sy);
		forward->z = (cr*sp*cy + -sr*-sy);
	}

	if (right)
	{
		right->x = cp*sy;
		right->y = (sr*sp*sy + cr*cy);
		right->z = (cr*sp*sy + -sr*cy);
	}

	if (up)
	{
		up->x = -sp;
		up->y = sr*cp;
		up->z = cr*cp;
	}
}

__forceinline float DotProduct(const float *v1, const float *v2)
{
	return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

__forceinline void VectorTransform(const float *in1, const matrix3x4 & in2, float *out)
{
	out[0] = DotProduct(in1, in2[0]) + in2[0][3];
	out[1] = DotProduct(in1, in2[1]) + in2[1][3];
	out[2] = DotProduct(in1, in2[2]) + in2[2][3];
}

__forceinline void VectorTransform(const Vector& in1, const matrix3x4 &in2, Vector &out)
{
	VectorTransform(&in1.x, in2, &out.x);
}

#endif