#ifndef __HEADER_RUNCOMMAND__
#define __HEADER_RUNCOMMAND__
#pragma once

#include "CVMTHookManager.h"

#include "SDK/Source.h"

namespace Hooks
{
	namespace Prediction
	{
		extern CVMTHookManager* pVmt;

		void __fastcall RunCommand(Source::IPrediction* pThis, void*, Source::CBasePlayer* pPlayer, Source::CUserCmd* cmd, Source::IMoveHelper* pMoveHelper);
	}
}

#endif