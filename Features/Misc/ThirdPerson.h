#ifndef __HEADER_THIRDPERSON__
#define __HEADER_THIRDPERSON__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace Misc
	{
		class ThirdPerson;
	}
}

class Features::Misc::ThirdPerson
{
public:
	static bool Run(Source::CViewSetup* pView);
};

#endif