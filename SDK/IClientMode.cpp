#include "IClientMode.h"

namespace Source
{
	namespace Interfaces
	{
		IClientMode* ClientMode;
	}
}

using namespace Source;

IClientMode* IClientMode::GetPtr()
{
#if defined(GAME_L4D2)
	auto pClient = IBaseClientDLL::GetPtr();

	for (DWORD_PTR dwpStartAddr = GET_VFUNC(DWORD_PTR, pClient, IBASECLIENTDLL_HUDPROCESSINPUT), dOffset = 0; dOffset <= 0xFF; dOffset++)
	{
		if (*(byte*)(dwpStartAddr + dOffset) == 0xE8)
		{
			DWORD_PTR dwpCall = dwpStartAddr + dOffset + 1;
			
			return ((IClientMode*(__cdecl*)())(dwpCall + *(DWORD_PTR*)dwpCall + 0x4))();
		}
	}
#else
	auto pClient = IBaseClientDLL::GetPtr();

	for (DWORD_PTR dwpStartAddr = GET_VFUNC(DWORD_PTR, pClient, IBASECLIENTDLL_HUDPROCESSINPUT), dOffset = 0; dOffset <= 0xFF; dOffset++)
	{
		if (*(byte*)(dwpStartAddr + dOffset) == 0x0D)
		{
			return **(IClientMode***)(dwpStartAddr + dOffset + 0x1);
		}
	}
#endif

	return nullptr;
}

void IClientMode::OverrideView(CViewSetup* setup)
{
	return GET_VFUNC(void(__thiscall*)(IClientMode*, CViewSetup*), this, ICLIENTMODE_OVERRIDEVIEW)(this, setup);
}

bool IClientMode::CreateMove(float sampletime, CUserCmd* cmd)
{
	return GET_VFUNC(bool(__thiscall*)(IClientMode*, float, CUserCmd*), this, ICLIENTMODE_CREATEMOVE)(this, sampletime, cmd);
}