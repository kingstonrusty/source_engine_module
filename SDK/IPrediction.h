#ifndef __HEADER_IPREDICTION__
#define __HEADER_IPREDICTION__
#pragma once

#include "Multigame/InterfaceBruteforcer.h"

#include "CMoveData.h"
#include "CBasePlayer.h"
#include "CUserCmd.h"
#include "IMoveHelper.h"

namespace Source
{
	class IPrediction;

	namespace Interfaces
	{
		extern IPrediction* Prediction;
	}
}

class Source::IPrediction
{
public:
	static IPrediction* GetPtr();

#if defined(GAME_GMOD)
	bool IsFirstTimePredicted();
#endif
	void RunCommand(Source::CBasePlayer* player, Source::CUserCmd* cmd, IMoveHelper* movehelper);
	void SetupMove(Source::CBasePlayer* player, Source::CUserCmd* cmd, IMoveHelper* movehelper, CMoveData* movedata);
	void FinishMove(Source::CBasePlayer* player, Source::CUserCmd* cmd, CMoveData* movedata);
};

#endif