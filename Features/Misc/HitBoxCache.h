#ifndef __HEADER_HITBOXCACHE__
#define __HEADER_HITBOXCACHE__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace Misc
	{
		class HitBoxCache;
	}
}

class Features::Misc::HitBoxCache
{
public:
	struct HitBoxSet;

	static bool GetSet(Source::CBaseEntity* pEntity, HitBoxSet** pSet);

	struct HitBox
	{
		HitBox()
		{
			m_vMin.Init(0.f, 0.f, 0.f);
			m_vMax.Init(0.f, 0.f, 0.f);
			m_flLastScale = -1.f;
			m_iBone = 0;
			m_iGroup = 0;
			m_flSize = 0;

			for (int i = 0; i < 25; i++)
			{
				m_Points[i].Init(0.f, 0.f, 0.f);
			}
		}

		HitBox(const HitBox& other)
		{
			m_vMin = other.m_vMin;
			m_vMax = other.m_vMax;
			m_flLastScale = other.m_flLastScale;
			m_iBone = other.m_iBone;
			m_iGroup = other.m_iGroup;
			m_flSize = other.m_flSize;

			for (int i = 0; i < 25; i++)
			{
				m_Points[i] = other.m_Points[i];
			}
		}

		void ComputePoints(float flScale)
		{
			m_Points[0] = (m_vMin + m_vMax) / 2.f;

			Vector vMin = m_vMin * flScale;
			Vector vMax = m_vMax * flScale;

			m_Points[1] = vMin;
			m_Points[1].x = vMax.x;

			m_Points[2] = vMin;
			m_Points[2].x = vMax.x;
			m_Points[2].z = vMax.z;

			m_Points[3] = vMax;

			m_Points[4] = vMax;
			m_Points[4].z = vMin.z;

			m_Points[5] = vMin;

			m_Points[6] = vMin;
			m_Points[6].y = vMax.y;

			m_Points[7] = vMax;
			m_Points[7].x = vMin.x;
			m_Points[7].y = vMin.y;

			m_Points[8] = vMax;
			m_Points[8].x = vMin.x;

			float xLen = fabsf(vMin.x) + fabsf(vMax.x);
			float yLen = fabsf(vMin.y) + fabsf(vMax.y);
			float zLen = fabsf(vMin.z) + fabsf(vMax.z);

			m_Points[9] = vMin;
			m_Points[9].x = vMax.x;
			m_Points[9].z = vMax.z - zLen * 0.35f;

			m_Points[10] = vMin;
			m_Points[10].x = vMax.x;
			m_Points[10].z = vMin.z + zLen * 0.35f;

			m_Points[11] = vMin;
			m_Points[11].z = vMax.z - zLen * 0.35f;

			m_Points[12] = vMin;
			m_Points[12].z = vMin.z + zLen * 0.35f;

			m_Points[13] = vMin;
			m_Points[13].y = vMax.y;
			m_Points[13].z = vMax.z - zLen * 0.35f;

			m_Points[14] = vMin;
			m_Points[14].y = vMax.y;
			m_Points[14].z = vMin.z + zLen * 0.35f;

			m_Points[15] = vMax;
			m_Points[15].z = vMax.z - zLen * 0.35f;

			m_Points[16] = vMax;
			m_Points[16].z = vMin.z + zLen * 0.35f;

			m_Points[17] = vMax;
			m_Points[17].y = vMax.y - yLen * 0.35f;

			m_Points[18] = vMax;
			m_Points[18].y = vMin.y + yLen * 0.35f;

			m_Points[19] = vMin;
			m_Points[19].y = vMax.y - yLen * 0.35f;
			m_Points[19].z = vMax.z;

			m_Points[20] = vMin;
			m_Points[20].y = vMin.y + yLen * 0.35f;
			m_Points[20].z = vMax.z;

			m_Points[21] = vMax;
			m_Points[21].y = vMax.y - yLen * 0.35f;
			m_Points[21].z = vMin.z;

			m_Points[22] = vMax;
			m_Points[22].y = vMin.y + yLen * 0.35f;
			m_Points[22].z = vMin.z;

			m_Points[23] = vMin;
			m_Points[23].y = vMax.y - yLen * 0.35f;
			m_Points[23].z = vMin.z;

			m_Points[24] = vMin;
			m_Points[24].y = vMin.y + yLen * 0.35f;
			m_Points[24].z = vMin.z;
		}

		Vector m_vMin;
		Vector m_vMax;
		Vector m_Points[24 + 1]; // 24 + center
		float m_flLastScale;
		int m_iBone;
		int m_iGroup;
		float m_flSize;
	};
	struct HitBoxSet
	{
		HitBoxSet()
		{
			memset(this, 0, sizeof(*this));
		}

		HitBox m_HitBoxes[128]; //1 for each bone ((will never happen))
		int m_nHitBoxes;
	};
private:
	static float GetBoundingBoxSize(Vector& min, Vector& max);

	static KeyValues<unsigned int, HitBoxSet> m_HitBoxes;
};

#endif