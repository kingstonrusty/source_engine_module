#ifndef __HEADER_SMALLVECTOR__
#define __HEADER_SMALLVECTOR__
#pragma once

template< class _T >
class SmallVector
{
public:
	SmallVector()
	{
		m_pData = nullptr;
		m_nElements = 0;
	}

	SmallVector(const SmallVector& other)
	{
		Clear();
		Copy(other.m_pData, other.m_nElements);
	}

	~SmallVector()
	{
		Clear();
	}

	void Clear()
	{
		if (m_pData != nullptr)
		{
			free(m_pData);
		}

		m_pData = nullptr;
		m_nElements = 0;
	}

	void Copy(_T* pData, size_t nElements)
	{
		m_pData = (_T*)malloc(sizeof(_T) * nElements);

		memcpy(m_pData, pData, sizeof(_T) * nElements);
	}

	void Add(_T toAdd)
	{
		m_nElements++;

		if (m_pData != nullptr)
		{
			m_pData = (_T*)realloc(m_pData, sizeof(_T) * m_nElements);
			SetAt(m_nElements - 1, toAdd);
		}
		else
		{
			m_pData = (_T*)malloc(sizeof(_T));
			SetAt(0, toAdd);
		}
	}

	void SetAt(int idx, _T data)
	{
		m_pData[idx] = data;
	}

	_T GetAt(int idx)
	{
		return m_pData[idx];
	}

	size_t GetSize()
	{
		return m_nElements;
	}

	_T* GetData()
	{
		return m_pData;
	}

	_T operator[](int idx)
	{
		return GetAt(idx);
	}

	const _T operator[](int idx) const
	{
		return GetAt(idx);
	}
private:
	_T* m_pData;
	size_t m_nElements;
};

#endif