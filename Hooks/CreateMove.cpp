#include "CreateMove.h"

#include "Util/Input.h"

#include "Features/Visuals/Menu.h"

#include "Features/PositionAdjustment/History.h"
#include "Features/PositionAdjustment/Prediction.h"

#include "Features/Ragebot/Aimbot.h"

#include "Features/Legitbot/Triggerbot.h"

#include "Features/Accuracy/NoSpread.h"

#include "Features/HackvsHack/Fakelag.h"

#include "Features/Legitbot/BacktrackAimbot.h"

#include "Features/Misc/Freecam.h"

#define IFTHENELSE_EXPAND(IF, THEN, ELSE) (((IF)) ? (THEN) : (ELSE))
#define IFTHENELSE(IF, THEN, ELSE) IFTHENELSE_EXPAND(IF, THEN, ELSE)


#define clamp(VAL, MIN, MAX) IFTHENELSE((VAL) > (MAX), (MAX), IFTHENELSE((VAL) < (MIN), (MIN), (VAL)))

static Vector angFixAngles;

static void FixMovement(Vector& rangNewAngles, float& rflForwardMove, float& rflSideMove) {
	if (rangNewAngles == angFixAngles)
	{
		return;
	}

	Vector backupNew = rangNewAngles;
	AngleNormalize(angFixAngles);
	AngleNormalize(rangNewAngles);

	Vector vecMove = Vector(rflForwardMove, rflSideMove, 0.f);
	float flSpeed = vecMove.Length2D();
	Vector angMove;
	VectorAngles(vecMove, angMove);
	float flYaw = Deg2Rad((rangNewAngles.y - angFixAngles.y) + angMove.y);
	rflSideMove = sinf(flYaw);
	rflForwardMove = cosf(flYaw);
	rflSideMove *= flSpeed;
	rflForwardMove *= flSpeed;

	if (rangNewAngles.x >= 90.f || rangNewAngles.x <= -90.f) rflForwardMove *= -1.f;

	rangNewAngles = backupNew;
}

extern bool g_bSendPacket;

extern double g_dblCurCone;

static void AtTargets(Source::CBasePlayer* pLocal, Source::CUserCmd* cmd)
{
	float flLowest = FLT_MAX;
	Vector vEyePos = pLocal->GetAbsOrigin() + pLocal->GetViewOffset();

	for (INT idx = 1; idx <= Source::Interfaces::Globals->maxClients(); idx++)
	{
		if (idx == Source::Interfaces::Engine->GetLocalPlayer())
		{
			continue;
		}

		Source::CBasePlayer* pEnt = (Source::CBasePlayer*)Source::Interfaces::EntityList->GetClientEntity(idx);
		if (pEnt == nullptr)
		{
			continue;
		}

		if (pEnt->GetNetworkable()->IsDormant())
		{
			continue;
		}

		if (!pEnt->IsAlive())
		{
			continue;
		}

		if (pLocal->GetTeamNum() == pEnt->GetTeamNum())
		{
			continue;
		}

		Vector vOrigin = pEnt->GetAbsOrigin();

		if ((vEyePos - vOrigin).LengthSqr() < flLowest)
		{
			flLowest = (vEyePos - vOrigin).LengthSqr();
			CalcAngle(vEyePos, vOrigin, cmd->viewangles());;
		}
	}
}

bool __fastcall Hooks::ClientMode::CreateMove(Source::IClientMode* pThis, void*, float sampletime, Source::CUserCmd* cmd)
{
	pVmt->Unhook();
	pThis->CreateMove(sampletime, cmd);
	pVmt->Rehook();

	static Vector s_vecLastViewangles;
	static bool s_bWasDrawingLastFrame;

	if (Features::Misc::Freecam::GetIsActivated())
	{
		Features::Misc::Freecam::Update(cmd);
	}

	if (Features::Misc::Freecam::GetIsActivated() || Features::Visuals::ShouldDrawMenu() || Features::Visuals::ShouldDrawConsole() || s_bWasDrawingLastFrame)
	{
		static auto cl_mouseenable = Source::Interfaces::Cvar->FindVar("cl_mouseenable");

		cmd->buttons() = 0;
		cmd->forwardmove() = 0.f;
		cmd->sidemove() = 0.f;
		cmd->upmove() = 0.f;
		cmd->impulse() = 0;
		cmd->mousedx() = 0;
		cmd->mousedy() = 0;
		cmd->viewangles() = s_vecLastViewangles;

		if (s_bWasDrawingLastFrame && !Features::Visuals::ShouldDrawConsole() && !Features::Visuals::ShouldDrawMenu() && !Features::Misc::Freecam::GetIsActivated())
		{
			cl_mouseenable->SetInt(1);
		}
		else
		{
			if (!Features::Misc::Freecam::GetIsActivated())
			{
				cl_mouseenable->SetInt(0);
			}
		}

		s_bWasDrawingLastFrame = Features::Visuals::ShouldDrawMenu() || Features::Visuals::ShouldDrawConsole() || Features::Misc::Freecam::GetIsActivated();
	}
	else
	{
		s_vecLastViewangles = cmd->viewangles();
	}

	if (cmd->command_number() == 0)
	{
		return true;
	}

	/*static int s_LastTickCount;
	if (s_LastTickCount == Source::Interfaces::Globals->tickcount())
	{
		s_bSpeeding = true;
	}
	else*/
	{
		//s_LastTickCount = Source::Interfaces::Globals->tickcount();
		//s_bSpeeding = false;
	}

	bool* bSendPacket = Source::GetSendPacket();
	*bSendPacket = 1;
	
	//Vector vOrig = cmd->viewangles();
	#if defined(GAME_GMOD)
	if (cmd->worldclicking())
	{
		VectorAngles(cmd->worldclickingforward(), cmd->viewangles());
		AngleNormalize(cmd->viewangles());
	}
	#endif

	auto pLocal = (Source::CBasePlayer*)Source::Interfaces::EntityList->GetClientEntity(Source::Interfaces::Engine->GetLocalPlayer());
	if (pLocal == nullptr || !pLocal->IsAlive())
	{
		return true;
	}

#if defined(GAME_L4D2)
	if (pLocal->GetAbsOrigin().IsZero())
	{
		return true;
	}
#endif

	Vector vBackup = cmd->viewangles();

	//Debug::PrintToConsole("%d", pLocal->GetTickBase());

	if (cmd->buttons() & 0x2 && !(pLocal->GetFlags() & 1))
	{
		cmd->buttons() &= ~(0x2);

		cmd->forwardmove() = 0.f;
		float flStrafeYaw = clamp(850.f / pLocal->GetVelocity().Length2D(), 0.f, 90.f);
		if (cmd->command_number() % 2)
		{
			cmd->viewangles().y -= flStrafeYaw;
			cmd->sidemove() = -400.f;
		}
		else
		{
			cmd->viewangles().y += flStrafeYaw;
			cmd->sidemove() = 400.f;
		}
	}
	else if (cmd->buttons() & 0x2 && pLocal->GetFlags() & 1)
	{
		cmd->forwardmove() = 400.f;
	}

	AngleNormalize(cmd->viewangles());

	Features::PositionAdjustment::Prediction::Start(pLocal, cmd);;
	Features::HackvsHack::Fakelag::Run(pLocal, cmd, bSendPacket);

	angFixAngles = cmd->viewangles();
	cmd->viewangles() = vBackup;

	/*AtTargets(pLocal, cmd);
	{
		cmd->viewangles().x = 89.f;

		static bool bTwitch, bMode, bTrick;

		if (*bSendPacket)
		{
			bTwitch = !bTwitch;

			if (bTwitch)
			{
				bTrick = !bTrick;

				cmd->viewangles().y += (bTrick) ? -179.990005f : 180.f;
			}
			else
			{
				cmd->viewangles().y += (bTrick) ? 0.990005f : 0.f;
			}
		}
		else
		{
			if (bMode)
			{
				bMode = false;
			}
			else
			{
				bMode = true;
				bTwitch = !bTwitch;

				if (bTwitch)
				{
					bTrick = !bTrick;

					cmd->viewangles().y += (bTrick) ? -179.990005f : 180.f;
				}
				else
				{
					cmd->viewangles().y += (bTrick) ? 0.990005f : 0.f;
				}
			}
		}

		AngleNormalize(cmd->viewangles());
	}*/

	auto pWeapon = (Source::CBaseWeapon*)Source::Interfaces::EntityList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
	if (pWeapon != nullptr)
	{
		if (Util::Input::GetButtonState('G') && Source::Interfaces::Globals->curtime() >= pWeapon->GetNextPrimaryAttack() && Source::Interfaces::Globals->curtime() >= pLocal->GetNextAttack())
		{
			if (Features::Ragebot::Aimbot::Run(pLocal, pWeapon, cmd, bSendPacket))
			//if(Features::Legitbot::Triggerbot::Run(pLocal, cmd))
			//if(Features::Legitbot::BacktrackAimbot::Run(pLocal, cmd))
			{
			}
		}
	}

	Features::Accuracy::NoSpread::Run(pLocal, pWeapon, cmd);

	Features::PositionAdjustment::Prediction::End(pLocal, cmd);

#if defined(GAME_GMOD)
	AngleVectors(cmd->viewangles(), cmd->worldclickingforward());
	cmd->viewangles() = vBackup;
	cmd->worldclicking() = true;
#endif

	FixMovement(cmd->viewangles(), cmd->forwardmove(), cmd->sidemove());

	//*Source::GetSendPacket() = (cmd->command_number() % 12) == 0;

	g_bSendPacket = *bSendPacket;

	{
		/*static bool s_SetNewline = true;

		//if ((Source::Interfaces::Globals->tickcount() % 10) == 0)
		{
			if (s_SetNewline)
			{
				Source::NET_SetConVar name("name", "\n");
				Source::Interfaces::Engine->GetNetChannelInfo()->SendNetMsg(name, false, false);
			}
			else
			{
				char szbuf[32];
				sprintf_s(szbuf, "cool%d", Source::Interfaces::Globals->tickcount());

				Source::NET_SetConVar name("name", szbuf);
				Source::Interfaces::Engine->GetNetChannelInfo()->SendNetMsg(name, false, false);
			}

			s_SetNewline = !s_SetNewline;
		}*/
	}

	return false;
}

static float Lerp(float delta, float from, float to)
{
	if (delta > 1.f)
	{
		return to;
	}

	if (delta < 0.f)
	{
		return from;
	}

	return from + (to - from) * delta;
}

#if defined(GAME_L4D2)
void __fastcall Hooks::Client::CreateMove(Source::IBaseClientDLL* pThis, void*, int sequence_number, float sample_time, bool active)
{
	pVmt->Unhook();
	pThis->CreateMove(sequence_number, sample_time, active);
	pVmt->Rehook();

	/*void* base;
	__asm mov base, ebp;

	if (Util::Input::GetButtonState('F'))
	{
		static int s_Speed;

		if (s_Speed < 6)
		{
			*(DWORD_PTR*)(*(DWORD_PTR*)base + 0x4) -= 0x5;//shift return address by 5 so it calls createmove again
			s_Speed++;
		}
		else
		{
			s_Speed = 0;
		}
	}*/
}
#elif defined(GAME_CSS_V34)
static int iSequenceNumber;

static void inline InvokeCreateMove()
{

}

static constexpr DWORD_PTR dwpCreateMove = 0x24087270;

__declspec(naked) void Hooks::Client::CreateMove()
{
	DWORD dwpReturnAddress;

	__asm
	{
		push eax
		mov eax, [esp + 4]
		mov dwpReturnAddress, eax
		pop eax

		add esp, 4

		push eax
		mov eax, esi
		mov iSequenceNumber, eax
		pop eax

		call dwpCreateMove

		pushad
	}

	InvokeCreateMove();

	__asm
	{
		popad
		jmp dwpReturnAddress
	}
}
#endif