#ifndef __HEADER_ESP__
#define __HEADER_ESP__
#pragma once

#include "SDK/Source.h"

namespace Features
{
	namespace Visuals
	{
		class ESP;
	}
}

class Features::Visuals::ESP
{
public:
	static void Run();
	static void DrawEntityAndInvokeHook(Source::CBaseEntity* pEntity);
	static void DrawEntity(Source::CBaseEntity* pEntity);
};

#endif