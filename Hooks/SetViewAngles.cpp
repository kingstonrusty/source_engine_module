#include "SetViewAngles.h"

namespace Hooks
{
	namespace Engine
	{
		int g_iLastFrameCount;
	}
}

#if defined(GAME_GMOD)

void __fastcall Hooks::Engine::SetViewAngles(Source::IVEngineClient* pThis, void*, Vector& angles)
{
	if (g_iLastFrameCount == Source::Interfaces::Globals->framecount())
	{
		//return;
	}

	pVmt->Unhook();
	pThis->SetViewAngles(angles);
	pVmt->Rehook();
}

#endif