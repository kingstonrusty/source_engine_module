#include "IClientRenderable.h"

using namespace Source;

void IClientRenderable::InvalidateBoneCache()
{
	static int* s_pModelBoneCounter = *Memory::FindPattern<int**>(MISC_MODELBONECOUNTER, "client.dll");

	m_iMostRecentModelBoneCounter() = *s_pModelBoneCounter - 1;
	m_flLastBoneSetupTime() = -FLT_MAX;
}

/*const Vector& IClientRenderable::GetRenderOrigin()
{

}*/

/*const Vector& IClientRenderable::GetRenderAngles()
{

}*/

const model_t* IClientRenderable::GetModel()
{
	return GET_VFUNC(model_t*(__thiscall*)(IClientRenderable*), this, ICLIENTRENDERABLE_GETMODEL)(this);
}

int IClientRenderable::DrawModel()
{
	return GET_VFUNC(int(__thiscall*)(IClientRenderable*, int), this, ICLIENTRENDERABLE_DRAWMODEL)(this, 1);
}

bool IClientRenderable::SetupBones(matrix3x4* matrices, int maxbones, int mask, float time)
{
	return GET_VFUNC(bool(__thiscall*)(IClientRenderable*, matrix3x4*, int, int, float), this, ICLIENTRENDERABLE_SETUPBONES)(this, matrices, maxbones, mask, time);
}

/* 
if ( *(_DWORD *)(v10 + 5072) != g_iModelBoneCounter )
	if ( a4 >= *(float *)(v10 + 5780) )
    {
      *(_DWORD *)(v10 + 5096) = 0; // readable bones
      *(_DWORD *)(v10 + 5100) = 0; // writeable bones
      *(float *)(v10 + 5780) = *(float *)&a8; // lastsetuptime
    }
    *(_DWORD *)(v10 + 5080) = *(_DWORD *)(v10 + 5084); // prevbonemask
    *(_DWORD *)(v10 + 5084) = 0;*/ // accumulatedbonemask

int& IClientRenderable::m_iMostRecentModelBoneCounter()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(ICLIENTRENDERABLE_MOSTRECENTMODELBONECOUNTER, "client.dll");

	return *(int*)((DWORD_PTR)this + s_dwOffset);
}

int& IClientRenderable::m_fReadableBones()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(ICLIENTRENDERABLE_READABLEBONES, "client.dll");

	return *(int*)((DWORD_PTR)this + s_dwOffset);
}

int& IClientRenderable::m_fWriteableBones()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(ICLIENTRENDERABLE_WRITEABLEBONES, "client.dll");

	return *(int*)((DWORD_PTR)this + s_dwOffset);
}

int& IClientRenderable::m_iPrevBoneMask()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(ICLIENTRENDERABLE_PREVBONEMASK, "client.dll");

	return *(int*)((DWORD_PTR)this + s_dwOffset);
}

int& IClientRenderable::m_iAccumulatedBoneMask()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(ICLIENTRENDERABLE_ACCUMULATEDBONEMASK, "client.dll");

	return *(int*)((DWORD_PTR)this + s_dwOffset);
}

float& IClientRenderable::m_flLastBoneSetupTime()
{
	static DWORD s_dwOffset = *Memory::FindPattern<DWORD*>(ICLIENTRENDERABLE_LASTBONESETUPTIME, "client.dll");

	return *(float*)((DWORD_PTR)this + s_dwOffset);
}